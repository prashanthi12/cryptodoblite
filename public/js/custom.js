$(document).ready(function () {       
alert("test");

    $("#paypal_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
              paypal_id: {
                required: true,
                email: true
            }            
        },
        messages: {          
            paypal_id: {
                required: "Paypal Id is required"              
            }
        }
    });

});


     $("#bankwire_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
              bank_name: {
                required: true                
            },
            swift_code: {
                required: true,
                validswiftcode: true,              
            },
            account_no: {
                required: true                
            },
            account_name: {
                required: true                
            },
            account_address: {
                required: true                
            }                   
        },
        messages: {          
            bank_name: {
                required: "Bank Name is required"              
            },
            swift_code: {
                required: "Swift Code is required",
                validswiftcode : "Enter valid swift code."             
            },
            account_no: {
                required: "Account Number is required"              
            },
            account_name: {
                required: "Account Name is required"              
            },
            account_address: {
                required: "Account Address is required"              
            }
        }
    });


      $("#stpay_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
              stpayname: {
                required: true
              }            
        },
        messages: {          
            stpayname: {
                required: "User Name is required"              
            }
        }
    });

    $("#payeer_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
              payeer_id: {
                required: true
              },
               payeer_email: {
                required: true,
                email: true
              }           
        },
        messages: {          
            payeer_id: {
                required: "Payeer Id is required"              
            },
            payeer_email: {
                required: "Payeer Email is required"              
            }
        }
    });


    $("#advcash_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
             acc_email_id: {
                required: true,
                email: true
              }           
        },
        messages: {          
            acc_email_id: {
                required: "Account Email Id is required"              
            }
        }
    });


    $("#bitcoin_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
             coinname: {
                required: true
              },
            coincode: {
              required: true,
              bitcoinaddress: true,
            }              
        },
        messages: {          
            coinname: {
                required: "Name is required"              
            },
            coincode: {
                required: "Bitcoin address is required" ,
                bitcoinaddress: "Invalid bitcoin address",             
            }
        }
    });


       $("#skrill_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
             email_id: {
                required: true,
                email: true
              }           
        },
        messages: {          
            email_id: {
                required: "Email Id is required"              
            }
        }
    });


       $("#okpay_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
             payid: {
                required: true
              },
              paynumber: {
                required: true
              }             
        },
        messages: {          
            payid: {
                required: "Payid is required"              
            },
            paynumber: {
                required: "Pay Number is required"              
            }
        }
    });

        $("#perfectmoney_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
             payeeaccount: {
                required: true
              }
              
        },
        messages: {          
            payeeaccount: {
                required: "Payee Account is required"              
            }
           
        }
    });


        $("#neteller_form").validate({
        errorClass: 'help-block',
        validClass: 'valid',
        highlight: function(element) {
            $(element).closest('div').addClass("has-error");
        },
        unhighlight: function(element) {
            $(element).closest('div').removeClass("has-error");
        },
        errorPlacement: function(error, element) {
          $(element).closest('div').append(error);
        },
        rules: {             
             merchant_id: {
                required: true
              },
              merchant_key: {
                required: true
              }
              
        },
        messages: {          
            merchant_id: {
                required: "Merchant Id is required"              
            },
           merchant_key: {
                required: "Merchant Key is required"              
            },
        }
    });

});
