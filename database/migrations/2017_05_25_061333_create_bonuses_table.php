<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');   
            $table->integer('value'); 
            $table->boolean('type'); 
            $table->boolean('triggertype')->nullable();           
            $table->integer('plan')->unsigned();
            $table->foreign('plan')->references('id')->on('plans')->onDelete('cascade');;
            $table->boolean('bonus_cretided_to')->nullable();   
            $table->boolean('status');               
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bonuses');
    }
}
