<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            
            $table->increments('id');
            $table->unsignedBigInteger('transaction_id')->nullable();
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
            $table->string('param')->nullable();
            $table->string('param1')->nullable();
            $table->enum('status',['new','active','matured','released','archived', 'rejected', 'problem']);
            $table->double('amount',15,8);
            $table->integer('plan_id')->unsigned();
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('paymentgateway_id')->unsigned();
            $table->foreign('paymentgateway_id')->references('id')->on('paymentgateways')->onDelete('cascade');
            $table->datetime('approved_on')->nullable();
            $table->text('comments_on_approval')->nullable();
            $table->datetime('matured_on')->nullable();
            $table->text('comments_on_maturity')->nullable();
            $table->datetime('released_on')->nullable();
            $table->text('comments_on_release')->nullable();
            $table->datetime('archived_on')->nullable();
            $table->text('comments_on_archive')->nullable();
            $table->datetime('rejected_on')->nullable();
            $table->text('comments_on_reject')->nullable();
            $table->datetime('problem_on')->nullable();
            $table->text('comments_on_problem')->nullable();
            $table->text('bitpay_invoice_id')->nullable();
            $table->text('request')->nullable();
            
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
