<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('useraccounts')->onDelete('cascade');
            $table->double('amount');
            $table->enum('type', ['credit','debit']);
            $table->boolean('status');
            $table->integer('accounting_code_id')->unsigned();
            $table->foreign('accounting_code_id')->references('id')->on('accountingcodes')->onDelete('cascade');
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('transactions');
    }
}
