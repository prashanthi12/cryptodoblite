<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('name');
            $table->integer('plantype_id')->unsigned();
            $table->foreign('plantype_id')->references('id')->on('plantypes')->onDelete('cascade');
            $table->double('min_amount', 15,8);
            $table->double('max_amount', 15,8);
            $table->double('interest_rate', 15,8);
            $table->integer('duration');
            $table->enum('duration_key', ['hours','days', 'weeks', 'months', 'years']);
            $table->boolean('publish')->default(0);
            $table->boolean('active')->default(0);
            $table->boolean('principle_return')->default(0);
            $table->boolean('partial_withdraw_status')->default(0);
            $table->integer('max_partial_withdraw_limit')->nullable();
            $table->integer('minimum_locking_period')->nullable();
            $table->integer('paritial_withdraw_fee')->nullable();
            $table->boolean('compounding')->default(0);
            $table->integer('orderby')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
