<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentgatewaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentgateways', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gatewayname')->unique();
            $table->string('displayname');
            $table->boolean('active');
            $table->boolean('deposit');
            $table->boolean('withdraw')->default('0');
            $table->boolean('e_wallet')->default('0');
            $table->enum('status',['online','offline','bonus','reinvest','e-pin','e-wallet']);
            $table->integer('withdraw_commission')->default('0');
            $table->boolean('deposit_fee_status')->default('0');
            $table->enum('deposit_fee_type',['1','2'])->default('1')->comment('1-Flat, 2-%');
            $table->integer('deposit_fee_value')->default('0');
            $table->boolean('exchange')->default('0');
            $table->double('min_amount_for_ewallet')->default('0');
            $table->double('max_amount_for_ewallet')->default('0');
            $table->double('bonus_amount_for_ewallet')->default('0');
            $table->text('params');
            $table->text('instructions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paymentgateways');
    }
}
