<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserprofilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userprofiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('usergroup_id')->unsigned()->nullable();
            $table->foreign('usergroup_id')->references('id')->on('usergroups')->onDelete('cascade');
            $table->integer('sponsor_id')->unsigned()->nullable();
            $table->foreign('sponsor_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('active')->default('1'); 
            $table->string('transaction_password')->nullable();    
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('country')->nullable()->unsigned();
            $table->foreign('country')->references('id')->on('countries')->onDelete('cascade');
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('ssn')->nullable();
            $table->string('profile_avatar')->nullable();
            $table->string('kyc_doc')->nullable();
            $table->boolean('email_verified')->default('0');
            $table->uuid('email_verification_code')->nullable();
            $table->boolean('mobile_verified')->default('0');
            $table->uuid('mobile_verification_code')->nullable();
            $table->boolean('kyc_verified')->default('0');
            $table->integer('referral_group_id')->nullable()->unsigned();    
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('userprofiles');
    }
}
