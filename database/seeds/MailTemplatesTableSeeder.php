<?php

use Illuminate\Database\Seeder;

class MailTemplatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_templates')->insert([
        	'name' => 'active_deposit_successfull',
           	'subject' => 'Active Deposit Successfull',
           	'mail_content' => 'Hi :name, <br> 
                                Your deposit :amount :currency has been successful. <br> 
                                Plan Name :plan_name <br>
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
        	'name' => 'admin_notify_active_deposit',
           	'subject' => 'Admin Notify Active Deposit',
           	'mail_content' => 'Hi :name, <br> 
                                :username deposited on active deposit. <br> 
                                Deposited Amount :deposited_amount :currency<br>
                                Plan Name :plan_name <br>
                                <a href= ":url">Click to View</a> <br>
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
        	'name' => 'admin_notify_kyc_verify',
           	'subject' => 'Admin Notify KYC Verify',
           	'mail_content' => 'Hi Admin, <br> 
                                :name send the KYC document for your verification. <br> 
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'admin_notify_new_deposit',
            'subject' => 'Admin Notify New Deposit',
            'mail_content' => 'Hi Admin, <br> 
                                :username deposited and waiting for your approval <br>
                                Deposited Amount : :deposited_amount :currency<br>
                                Plan Name : :plan_name <br> 
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to View</a> <br>
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'admin_notify_new_ticket',
            'subject' => 'Admin Notify New Ticket',
            'mail_content' => 'Hi :name, <br> 
                                :user raised a new ticket under :category category with :priority priority in :status status and it is assigned to :staff. Below Ticket details are: <br> 
                                Subject : :subject<br>
                                Description : :content<br>
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'admin_notify_new_user',
            'subject' => 'Admin Notify New User',
            'mail_content' => 'Hi :name, <br> 
                                Registered a new user. Please login to see details.<br> 
                                User Name : :registered_user_name<br>
                                IP Address : :ip_address<br>
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click</a> <br>
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'admin_notify_ticket_status',
            'subject' => 'Admin Notify Ticket Status',
            'mail_content' => 'Hi :name, <br> 
                                :user raised ticket has now changed to :status by :staff. Below Ticket details are: <br> 
                                Subject :subject<br>
                                Description :content<br>
                                Category :category<br>
                                Priority :priority<br>                          
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'admin_send_mail',
            'subject' => ':subject',
            'mail_content' => 'Hi :name, <br> 
                                Subject : :subject<br>
                                Message : :message<br> 
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>                         
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'change_password',
            'subject' => 'Change Password',
            'mail_content' => 'Hi :name, <br> 
                                Your Password is changed successfully.<br>                      
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'contactus',
            'subject' => 'Contact Us',
            'mail_content' => 'Hi :admin, <br> 
                                :name send to the below queries,<br>  
                                :queries<br>
                                :contactno<br>
                                :skypeid<br>                    
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        /*DB::table('mail_templates')->insert([
            'name' => 'coupon_code_send_mail',
            'subject' => 'Coupon Code Send Mail',
            'mail_content' => 'Hi :name, <br> 
                                For coupon value of :amount :currency, the coupon code is given below.<br>  
                                Coupon Code :couponcode<br>                   
                                Thanks & Regards <br> 
                                HYIP Team <br>',
            'status' => 'active',
        ]);*/
        DB::table('mail_templates')->insert([
            'name' => 'create_new_user',
            'subject' => 'Create New User',
            'mail_content' => 'Hi :name, <br> 
                                Your account successfully created by admin. Login credentials are given below,<br>  
                                Your login email address is : :email<br> 
                                Your login password is : :password<br>  
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>                  
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'email_verification',
            'subject' => 'Email Verification',
            'mail_content' => 'Hi :name, <br> 
                                Please verify your email, click this link<br>   
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click To Verify</a> <br>                  
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'epin_couponcode_owner',
            'subject' => 'Epin Couponcode Owner',
            'mail_content' => 'Hi :name, <br> 
                                Your E - Pin coupon is used by :user. Below coupon details are:<br>   
                                Amount :amount :currency<br> 
                                Coupon Code :couponcode<br>                 
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'epin_couponcode_used',
            'subject' => 'Epin Couponcode Used',
            'mail_content' => 'Hi :name, <br> 
                                You have deposited successfully with a coupon value of :amount :currency.<br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'fund_transfer_receiver',
            'subject' => 'Fund Transfer Receiver',
            'mail_content' => 'Hi :name, <br> 
                                You have received a fund amount of :amount :currency from :sender.<br> 
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>                                               
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'fund_transfer_sender',
            'subject' => 'Fund Transfer Sender',
            'mail_content' => 'Hi, <br> 
                                You have transferred a fund amount of :amount :currency to :receiver.<br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'invite_friend',
            'subject' => 'Invite Friend',
            'mail_content' => 'Hi, <br>
                                :message<br> 
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Accept My Invitation</a> <br>                                               
                                Thanks & Regards <br> 
                                :name <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'kyc_approve',
            'subject' => 'KYC Approve',
            'mail_content' => 'Hi :name, <br>
                                Your KYC document approved by admin.<br>                                               
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'kyc_reject',
            'subject' => 'KYC Reject',
            'mail_content' => 'Hi :name, <br>
                                Your KYC document rejeted by admin. Please uploaded new KYC document.<br>                                               
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'new_deposit_successfull',
            'subject' => 'New Deposit Successfull',
            'mail_content' => 'Hi :name, <br>
                                Your deposit :amount :currency has been successful. It will be approved after verification.<br>  
                                Plan Name : :plan_name<br>                                             
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'register_bonus_deposit',
            'subject' => 'Register Bonus Deposit',
            'mail_content' => 'Hi :name, <br>
                                Your register bonus :amount :currency has been deposited successful.<br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'register_new_user',
            'subject' => 'Register New User',
            'mail_content' => 'Hi :name, <br>
                                Your account was successfully created.<br>   
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'reset_password',
            'subject' => 'Reset Password',
            'mail_content' => 'Hi :username, <br>
                                Please reset your password to click below link.<br>   
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Reset Password</a> <br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'reset_transaction_password',
            'subject' => 'Reset Transaction Password',
            'mail_content' => 'Hi :name, <br>
                                Your transaction password is successfully reset.<br> 
                                New transaction password is :new_transaction_password<br> 
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to set new transaction password here.</a> <br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'send_mail_to_user',
            'subject' => 'Send Mail To User',
            'mail_content' => 'Hi :name, <br>
                                :message<br>                                              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'staff_notify_ticket',
            'subject' => 'Staff Notify Ticket',
            'mail_content' => 'Hi :name, <br>
                                :user raised a new ticket under :category category with :priority priority in :status status is assigned to you. Below Ticket details are:<br> 
                                Subject : :subject<br>
                                Description : :content<br>
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>               
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'staff_notify_ticket_status',
            'subject' => 'Staff Notify Ticket Status',
            'mail_content' => 'Hi :name, <br>
                                :user raised ticket status has now changed to :status by :admin. Below Ticket details are:<br> 
                                Subject : :subject<br>
                                Description : :content<br>
                                Category : :category<br>
                                Priority : :priority<br>              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'transaction_password',
            'subject' => 'Transaction Password',
            'mail_content' => 'Hi :name, <br>
                                Your Transaction Password is changed successfully.<br>              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'user_contact_us',
            'subject' => 'User Contact Us',
            'mail_content' => 'Hi :name, <br>
                                You have send a contact us mail to us. Below contact details are:<br>
                                Comment : :queries<br>
                                Contact No : :contactno<br>
                                Skype : :skypeid<br>              
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'user_notify_ticket_status',
            'subject' => 'User Notify Ticket Status',
            'mail_content' => 'Hi :name, <br>
                                Your ticket has now changed to :status. Below Ticket details are:<br>
                                Subject :subject<br>
                                Description :content<br>
                                Category :category<br>  
                                Priority :priority<br>
                                <a href= ":url" style="border: none; color: white; padding: 10px 15px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;">Click to Login</a> <br>           
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'user_penalty',
            'subject' => 'User Penalty',
            'mail_content' => 'Hi :name, <br>
                                Admin send penalty for you. Penalty amount is :amount :currency<br>
                                :comment<br>        
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'user_send_bonus',
            'subject' => 'User Send Bonus',
            'mail_content' => 'Hi :name, <br>
                                Admin send bonus for you. Bonus amount is :amount :currency<br>
                                :comment<br>        
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'user_send_ticket',
            'subject' => 'User Send Ticket',
            'mail_content' => 'Hi :name, <br>
                                You raised a new ticket under :category category with :priority priority in :status status and it is assigned to :staff. Below Ticket details are:<br>
                                Subject : :subject<br>
                                Description : :content<br>      
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'withdraw_approve',
            'subject' => 'Withdraw Approve',
            'mail_content' => 'Hi :name, <br>
                                Your withdraw request :amount :currency is approved by admin, and comments are below.<br>
                                :comments<br>    
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'withdraw_otp',
            'subject' => 'Withdraw OTP',
            'mail_content' => 'Hi :name, <br>
                                Your Withdraw OTP Code is - :otpcode<br>   
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'withdraw_reject',
            'subject' => 'Withdraw Reject',
            'mail_content' => 'Hi :name, <br>
                                Your withdraw request :amount :currency is rejected by admin, for the following below reason,<br> 
                                :comments<br>  
                                Thanks & Regards <br> 
                                :standard_signature <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'add_fund',
            'subject' => 'Add Fund To E-Wallet',
            'mail_content' => 'Hi :name, <br>
                                Ewallet Fund Received<br> 
                                From - :fromname<br>  
                                Amount - :amount :currency<br>
                                Received Amount - :btc_amount :donationcurrency<br>
                                Thanks & Regards <br> 
                                :name <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'approve_fund',
            'subject' => 'Approved E-Wallet Fund',
            'mail_content' => 'Hi :name, <br>
                                Ewallet Fund Approved<br>  
                                Amount - :amount :currency<br>
                                Send Amount - :btc_amount :donationcurrency<br>
                                Approved Amount - :approve_amount :currency<br>
                                Thanks & Regards <br> 
                                :name <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'cancel_fund',
            'subject' => 'Cancelled E-Wallet Fund',
            'mail_content' => 'Hi :name, <br>
                                Ewallet Fund Cancelled<br>  
                                Amount - :amount :currency<br>
                                Send Amount - :btc_amount :donationcurrency<br>
                                Hash ID - :txn_id<br>
                                Thanks & Regards <br> 
                                :name <br>',
            'status' => 'active',
        ]);
        DB::table('mail_templates')->insert([
            'name' => 'withdraw_send',
            'subject' => 'Withdraw Send',
            'mail_content' => 'Hi Admin, <br>
                                :name send from the withdraw request.<br>  
                                Withdraw request amount is :amount :currency<br>
                                Thanks & Regards <br> 
                                :name <br>',
            'status' => 'active',
        ]);
    }
}
