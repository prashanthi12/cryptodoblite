<?php

use Illuminate\Database\Seeder;

class AccountingcodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "zero-transaction",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "income-via-referral-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "income-via-level-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "income-as-bonus",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "income-via-interest",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-withdraw-fee",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-fund-transfer-fee",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-exchange-fee",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-tax",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-admin-fee",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-adjustment",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-admin-account-transfer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-self-account-transfer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-peer-account-transfer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-cash-on-hand",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-banktransfer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-paypal",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-perfectmoney",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-bitcoin",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-skrill",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-netteller",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-solid-trust-pay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-advcash",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);        
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "referral-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "level-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
         DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-bankwire",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-paypal",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-stpay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-payeer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-advcash",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-bitcoin",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-skrill",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-okpay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-cancellation",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-reinvest",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "reinvest-from-balance",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
         DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-okpay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-bankwire",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-paypal",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-stpay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-payeer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-advcash",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-blockio",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-coinpayment",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-bitpay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-skrill",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-okpay",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-reinvest",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "matured-deposit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "fund-transfer",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "fund-transfer-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-registerbonus",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-register-deposit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
          DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-bitcoin-direct",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-perfectmoney",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-perfectmoney",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "income-via-deposit-partial-withdraw",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-partial-withdraw-commission",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "fund-added",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-neteller",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
         DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-cheque",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "withdraw-via-neteller",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-neteller",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "debt-as-penalty",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "interest-via-cheque",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-epin",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-addfund",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-debitfund",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]); 
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-bonusfund",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]); 
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-external-transfer-credit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);

        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-external-transfer-debit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);

        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-internal-transfer-credit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "ewallet-internal-transfer-debit",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
        DB::table('accountingcodes')->insert(
        [
            'accounting_code' => "deposit-via-ewallet",
            'active' => "1",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
        ]);
    }
    
}
