<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->insert(
        [
            'image' => "/uploads/bg/3.jpg",
            'slidertext' => "Slider1 Text Here",
            'url' => "https://demo.larahyip.com",
            'urltext' => "Click Here",
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
             'updated_at' => date("Y-m-d H:i:s"),     
        ]);
        DB::table('sliders')->insert(
        [
            'image' => "/uploads/bg/23.jpg",
            'slidertext' => "Slider2 Text Here",
            'url' => "https://www.google.co.in",
            'urltext' => "Click Here",
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);
        DB::table('sliders')->insert(
        [
            'image' => "/uploads/bg/16.jpg",
            'slidertext' => "Slider3 Text Here",
            'url' => "https://demo.larahyip.com",
            'urltext' => "Click Here",
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);
    }
}
