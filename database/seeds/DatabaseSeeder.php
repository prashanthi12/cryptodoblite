<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(UsergroupsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(UserprofilesTableSeeder::class);
        $this->call(PlantypesTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(AccounttypesTableSeeder::class);
        $this->call(AccountingcodesTableSeeder::class);
        $this->call(UseraccountsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(PaymentgatewaysTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(DepositsTableSeeder::class);
        $this->call(PlacementsTableSeeder::class);
        $this->call(TicketStatusesTableSeeder::class);
        $this->call(TicketPrioritiesTableSeeder::class);
        $this->call(TicketCategoriesTableSeeder::class);
        $this->call(TicketCategoriesUsersTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
        $this->call(ExchangesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(QuotesTableSeeder::class);
        $this->call(ReferralgroupsTableSeeder::class);
        $this->call(MailTemplatesTableSeeder::class);
    }
}
