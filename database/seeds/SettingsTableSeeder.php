<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key'           => 'sitetitle',
            'name'          => 'Site Title',
            'description'   => 'Site Title to show in Browser Bar',
            'value'         => 'LaraHYIP - Robust HYIP Software',
            'field'         => '{"name":"value","label":"Value", "title":"Site Title" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);
        DB::table('settings')->insert([
            'key'           => 'sitename',
            'name'          => 'Site Name',
            'description'   => 'This site name is used in emails and copyrights',
            'value'         => 'LaraHYIP',
            'field'         => '{"name":"value","label":"Value", "title":"Site Title" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);
        DB::table('settings')->insert([
            'key'           => 'sitelogo',
            'name'          => 'Site Logo',
            'description'   => 'Logo of the website. Recommended Size : 220px (w) x 45px (h)',
            'value'         => 'uploads/your_logo_here.png',
            'field'         => '{"name":"value","label":"Value" ,"type":"browse"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);
        DB::table('settings')->insert([
            'key'           => 'currency',
            'name'          => 'Site Currency',
            'description'   => 'Currency of the Website',
            'value'         => 'USD',
            'field'         => '{"name":"value","label":"Value", "title":"Currency" ,"type":"select"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);        

        DB::table('settings')->insert([
            'key'           => 'matrix_width',
            'name'          => 'Matrix Width',
            'description'   => 'Enter the width of the Matrix',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value", "title":"Matrix Width" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        
         DB::table('settings')->insert([
            'key'           => 'pagecount',
            'name'          => 'Pagination',
            'description'   => 'Pagination Count',
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Pagination Count" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'adminemail',
            'name'          => 'Admin Email',
            'description'   => 'Admin Email Address',
            'value'         => 'admin@larahyip.com',
            'field'         => '{"name":"value","label":"Value", "title":"Admin Email" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'contact',
            'name'          => 'Contact Us',
            'description'   => 'Contact us Address',
            'value'         => '<p>Company Name</p><p>Addressline 1</p><p>Address line 2</p><p>City</p><p>State</p><p>ZipCode</p><p>Country</p>',
            'field'         => '{"name":"value","label":"Value", "title":"Contact us Address" ,"type":"textarea"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'captchasitekey',
            'name'          => 'Captcha Sitekey',
            'description'   => 'Google Captcha Sitekey',
            'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Google Captcha Sitekey" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'register_captcha_active',
            'name'          => 'Register Catcha Active',
            'description'   => 'Register Catcha Active',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'address1',
            'name'          => 'Address1',
            'description'   => 'Address1 Active',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

         DB::table('settings')->insert([
            'key'           => 'address2',
            'name'          => 'Address2',
            'description'   => 'Address2 Active',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

         DB::table('settings')->insert([
            'key'           => 'state',
            'name'          => 'State',
            'description'   => 'State Active',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

         DB::table('settings')->insert([
            'key'           => 'city',
            'name'          => 'City',
            'description'   => 'City Active',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'slack_webhook',
            'name'          => 'Slack WebHook',
            'description'   => 'Slack WebHook for deposit notifications',
            // 'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Slack WebHook" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'slack_channel',
            'name'          => 'Slack Channel (Optional)',
            'description'   => 'Slack Channel for deposit notifications',
            // 'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Slack Channel" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
        DB::table('settings')->insert([
            'key'           => 'google_analytics_code',
            'name'          => 'Google Analytics Code',
            'description'   => 'Analytics Code for Google',
            // 'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Google Analytics Code" ,"type":"textarea"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'addthis_code',
            'name'          => 'AddThis Code',
            'description'   => 'Add the Javascript code snippet to enable add this button',
            'value'         => ' <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59f2d90d68c46e76"></script>',
            'field'         => '{"name":"value","label":"Value", "title":"AddThis Code" ,"type":"textarea"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'withdraw_min_amount',
            'name'          => 'Withdraw Minimum Amount',
            'description'   => 'Withdraw Minimum Amount',
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Withdraw Minimum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'withdraw_max_amount',
            'name'          => 'Withdraw Maximum Amount',
            'description'   => 'Withdraw Maximum Amount',
            'value'         => '100',
            'field'         => '{"name":"value","label":"Value", "title":"Withdraw Maximum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgabout",
            'name' => "About Us Banner",
            'description' => "About Us Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"About Us Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bginvest",
            'name' => "Invest Banner",
            'description' => "Invest Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Invest Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgearn",
            'name' => "Earn Banner",
            'description' => "Earn Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Earn Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgnews",
            'name' => "News Banner",
            'description' => "News Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"News Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgreviews",
            'name' => "Reviews Banner",
            'description' => "Reviews Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Reviews Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgexchange",
            'name' => "Exchange Banner",
            'description' => "Exchange Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Exchange Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgfaq",
            'name' => "FAQ Banner",
            'description' => "FAQ Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"FAQ Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgfaq",
            'name' => "FAQ Banner",
            'description' => "FAQ Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"FAQ Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgcontact",
            'name' => "Contact Banner",
            'description' => "Contact Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Contact Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgprivacy",
            'name' => "Privacy Policy Banner",
            'description' => "Privacy Policy Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Privacy Policy Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgterms",
            'name' => "Terms Banner",
            'description' => "Terms Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Terms Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgsecurity",
            'name' => "Security Banner",
            'description' => "Security Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Security Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "bgpages",
            'name' => "Dynamic Pages Banner",
            'description' => "Dynamic Pages Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Dynamic Pages Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);
        DB::table('settings')->insert([
            'key'           => 'sitestarted',
            'name'          => 'Site Started',
            'description'   => 'Site Started Show in Welcome Dashbord',
            'value'         =>  date("Y-m-d"),
            'field'         => '{"name":"value","label":"Value", "title":"Site Started" ,"type":"date"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);
    
        DB::table('settings')->insert(
        [
            'key' => "favicon",
            'name' => "Favicon",
            'description' => "Site Favicon",
            'value'         => '/uploads/favicon.png',
            'field'         => '{"name":"value","label":"Value", "title":"Site Favicon" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);
       
        DB::table('settings')->insert(
        [
            'key' => "NEXMO_SMS_FROM_NUMBER",
            'name' => "NEXMO From Number",
            'description' => "NEXMO SMS From Number",
            'value'         => '919876543210',
            'field'         => '{"name":"value","label":"Value", "title":"NEXMO SMS From Number" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "fundtransfer_min_amount",
            'name' => "Fund Transfer Minimum Amount",
            'description' => "Fund Transfer Minimum Amount",
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Fund Transfer Minimum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "fundtransfer_max_amount",
            'name' => "Fund Transfer Maximum Amount",
            'description' => "Fund Transfer Maximum Amount",
            'value'         => '100',
            'field'         => '{"name":"value","label":"Value", "title":"Fund Transfer Maximum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "fundtransfer_commission",
            'name' => "Fund Transfer commission",
            'description' => "Fund Transfer commission",
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Fund Transfer commission" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "homebanner",
            'name' => "Home Banner",
            'description' => "Home Banner",
            'value'         => 'uploads/banner_comes_here.png',
            'field'         => '{"name":"value","label":"Value", "title":"Home Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "paymentbanner",
            'name' => "Payment Banner",
            'description' => "Payment Banner",
            'value'         => 'uploads/payment_gateway.png',
            'field'         => '{"name":"value","label":"Value", "title":"Payment Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert(
        [
            'key' => "securitybanner",
            'name' => "Security Banner",
            'description' => "Upload Image of Security and SSL Logos to display in footer",
            'value'         => 'uploads/securtity_logo.png',
            'field'         => '{"name":"value","label":"Value", "title":"Security Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

          DB::table('settings')->insert(
        [
            'key' => "homevideo",
            'name' => "Home Video",
            'description' => "Home Video Embedded Code",
            'value'         => '<p class="text-center"><img src="uploads/video_comes_here.png" class="img-resposnive" width="100%"></p>',
            'field'         => '{"name":"value","label":"Value", "title":"Home Video" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

          DB::table('settings')->insert(
        [
            'key' => "homegallery",
            'name' => "Home Gallery",
            'description' => "Home Gallery Code",
            'value'         => '<p class="text-center"><img src="uploads/photo_gallery_comes_here.png" class="img-resposnive" width="100%"></p>',
            'field'         => '{"name":"value","label":"Value", "title":"Home Gallery" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert([
            'key'           => 'mailchimp_status',
            'name'          => 'Mailchimp Status',
            'description'   => 'Status for Mailchimp',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'nexmo_status',
            'name'          => 'Nexmo Status',
            'description'   => 'Status for Nexmo',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        // DB::table('settings')->insert([
        //     'key'           => 'forced_matrix_status',
        //     'name'          => 'Forced Matrix Status',
        //     'description'   => 'Status for Forced Matrix',
        //     'value'         => '0',
        //     'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
        //     'active'        => 1,
        //     'created_at' => date("Y-m-d H:i:s"),
        //     'updated_at' => date("Y-m-d H:i:s"), 
        // ]);

        DB::table('settings')->insert([
            'key'           => 'contactus_captcha_status',
            'name'          => 'Contact us Catcha Status',
            'description'   => 'Status for Contact us Catcha',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('settings')->insert([
            'key'           => 'monthly_withdraw_limit',
            'name'          => 'Monthly Withdraw Limit',
            'description'   => 'Withdraw Limit for Month',
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Withdraw Limit" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        DB::table('settings')->insert([
            'key'           => 'calculator_slider_value',
            'name'          => 'Calculator slider value',
            'description'   => 'Value For Calculator Slider',
            'value'         => '100',
            'field'         => '{"name":"value","label":"Value", "title":"Value For Calculator Slider" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_email_verification_for_deposit',
            'name'          => 'Email verification for deposit',
            'description'   => 'Email verification for deposit',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_email_verification_for_fund_transfer',
            'name'          => 'Force email verification for fund transfer',
            'description'   => 'Force email verification for fund transfer',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_email_verification_for_withdraw',
            'name'          => 'Force email verification for withdraw',
            'description'   => 'Force email verification for withdraw',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_kyc_verification_for_deposit',
            'name'          => 'Force KYC verification for deposit',
            'description'   => 'Force KYC verification for deposit',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_kyc_verification_for_fund_transfer',
            'name'          => 'Force KYC verification for fund transfer',
            'description'   => 'Force KYC verification for fund transfer',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_kyc_verification_for_withdraw',
            'name'          => 'Force KYC Verification For Withdraw',
            'description'   => 'Force KYC Verification For Withdraw',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'force_deposit_down',
            'name'          => 'Force Deposit Down',
            'description'   => 'Force Deposit Down',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

         DB::table('settings')->insert([
            'key'           => 'force_withdraw_down',
            'name'          => 'Force Withdraw Down',
            'description'   => 'Force Withdraw Down',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'default_sponser',
            'name'          => 'Default Sponser',
            'description'   => 'Set the Default Sponser',
            'value'         => 'root@larahyip.com',
            'field'         => '{"name":"value","label":"Value(Enter the EMAIL-ID of ACTIVE MEMBER only)", "title":"Default Sponser" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'kyc_required_status',
            'name'          => 'KYC Required Status',
            'description'   => 'Status for KYC in member profile',
            'value'         => 0,
            'field'         => '{"name":"value","KYC Required Status":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'footercss',
            'name'          => 'Footer Css',
            'description'   => 'Added Footer Css',
            'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Added Footer Css" ,"type":"textarea"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'referral_commission_status',
            'name'          => 'Referral commission status',
            'description'   => 'Status for Referral commission',
            'value'         => 1,
            'field'         => '{"name":"value","Status for Referral commission":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'level_commission_status',
            'name'          => 'Level commission status',
            'description'   => 'Status for Level commission',
            'value'         => 1,
            'field'         => '{"name":"value","Status for Referral commission":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'auto_withdrawal_status',
            'name'          => 'Auto withdrawal status',
            'description'   => 'Status for Auto withdrawal',
            'value'         => 0,
            'field'         => '{"name":"value","Status for Auto withdrawal":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'auto_withdrawal_value',
            'name'          => 'Auto withdrawal value',
            'description'   => 'Value for Auto Withdrawal',
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Value for Auto Withdrawal" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

         DB::table('settings')->insert([
            'key'           => 'daily_withdraw_limit',
            'name'          => 'Daily Withdraw Limit',
            'description'   => 'Please check that the limit count should be less than the Monthly Withdraw Limit Count',
            'value'         => '2',
            'field'         => '{"name":"value","label":"Value", "title":"Daily Withdraw Limit" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);


         DB::table('settings')->insert([
            'key'           => 'headerscript',
            'name'          => 'Header Script',
            'description'   => 'Script for Header',
            'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Script for Header" ,"type":"textarea"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert([
            'key'           => 'footerscript',
            'name'          => 'Footer Script',
            'description'   => 'Script for Footer',
            'value'         => '',
            'field'         => '{"name":"value","label":"Value", "title":"Script for Footer" ,"type":"textarea"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);       

        DB::table('settings')->insert([
            'key'           => 'force_register_down',
            'name'          => 'Force Register Down',
            'description'   => 'Force Register Down',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Force Register Down" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

        DB::table('settings')->insert(
        [
            'key' => "investorsbanner",
            'name' => "Investors Banner",
            'description' => "Investors Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Investors Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "lastinvestorsbanner",
            'name' => "Last Investors Banner",
            'description' => "Last Investors Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Last Investors Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "referralsbanner",
            'name' => "Referrals Banner",
            'description' => "Referrals Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Referrals Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "payoutsbanner",
            'name' => "Payouts Banner",
            'description' => "Payouts Banner",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Payouts Banner" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);
        DB::table('settings')->insert(
        [
            'key' => "bgquotes",
            'name' => "Quotes Banner",
            'description' => "Banner for Quotes",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Banner for Quotes" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

        DB::table('settings')->insert(
        [
            'key' => "bgstats",
            'name' => "Statistics Banner",
            'description' => "Banner for Statistics",
            'value'         => '/uploads/bg/3.jpg',
            'field'         => '{"name":"value","label":"Value", "title":"Banner for Statistics" ,"type":"browse", "disk":"uploads"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

         DB::table('settings')->insert([
            'key'           => 'welcomepopupstatus',
            'name'          => 'Welcome Page Popup Status',
            'description'   => 'Status for Welcome Page',
            'value'         => '0',
            'field'         => '{"name":"value","Status for Welcome Page":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'dashboardpopupstatus',
            'name'          => 'Dashboard Popup Status',
            'description'   => 'Status for Dashboard Popup',
            'value'         => '0',
            'field'         => '{"name":"value","Status for Dashboard Popup":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'footer_link_status',
            'name'          => 'Footer Link Status',
            'description'   => 'Status for Footer Link',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'intial_total_deposit_amount',
            'name'          => 'Intial Total Deposit Amount',
            'description'   => 'Intial Total Deposit Amount',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value", "title":"Intial Total Deposit Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

          DB::table('settings')->insert([
            'key'           => 'intial_total_withdraw_amount',
            'name'          => 'Intial Total Withdraw Amount',
            'description'   => 'Intial Total Withdraw Amount',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value", "title":"Intial Total Withdraw Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),  
        ]);

          DB::table('settings')->insert([
            'key'           => 'last_deposit_amount',
            'name'          => 'Last Deposit Amount',
            'description'   => 'Last Deposit Amount',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value", "title":"Last Deposit Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'last_withdraw_amount',
            'name'          => 'Last Withdraw Amount',
            'description'   => 'Last Withdraw Amount',
            'value'         => '0',
            'field'         => '{"name":"value","label":"Value", "title":"Last Withdraw Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'display_quotes_count',
            'name'          => 'Display Quotes Count',
            'description'   => 'Display Quotes Count',
            'value'         => '2',
            'field'         => '{"name":"value","label":"Value", "title":"Display Quotes Count" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

          DB::table('settings')->insert([
            'key'           => 'display_news_count',
            'name'          => 'Display News Count',
            'description'   => 'Display News Count',
            'value'         => '2',
            'field'         => '{"name":"value","label":"Value", "title":"Display News Count" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

          DB::table('settings')->insert([
            'key'           => 'display_testimonials_count',
            'name'          => 'Display Testimonials Count',
            'description'   => 'Display Testimonials Count',
            'value'         => '2',
            'field'         => '{"name":"value","label":"Value", "title":"Display Testimonials Count" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 

        ]);

        //   DB::table('settings')->insert([
        //     'key'           => 'display_plan_count',
        //     'name'          => 'Display Plan Count',
        //     'description'   => 'Display Plan Count',
        //     'value'         => '2',
        //     'field'         => '{"name":"value","label":"Value", "title":"Display Plan Count" ,"type":"text"}',
        //     'active'        => 1,
        //     'created_at' => date("Y-m-d H:i:s"),
        //     'updated_at' => date("Y-m-d H:i:s"), 

        // ]);

        //   DB::table('settings')->insert([
        //     'key'           => 'google_2step_verification_status',
        //     'name'          => 'Google 2step Verification Status',
        //     'description'   => 'Google 2step Verification Status',
        //     'value'         => '0',
        //     'field'         => '{"name":"value","label":"Value", "title":"Google 2step Verification Status" ,"type":"text"}',
        //     'active'        => 1,
        //     'created_at' => date("Y-m-d H:i:s"),
        //     'updated_at' => date("Y-m-d H:i:s"), 

        // ]);

          DB::table('settings')->insert([
            'key'           => 'coupon_code_minimum_amount',
            'name'          => 'Coupon Code Minimum Amount',
            'description'   => 'Coupon Code Minimum Amount',
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Coupon Code Minimum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'ewallet_fund_min_amount',
            'name'          => 'Ewallet Fund Minimum Amount',
            'description'   => 'Ewallet Fund Minimum Amount',
            'value'         => '5',
            'field'         => '{"name":"value","label":"Value", "title":"Ewallet Fund Minimum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'ewallet_fund_max_amount',
            'name'          => 'Ewallet Fund Maximum Amount',
            'description'   => 'Ewallet Fund Maximum Amount',
            'value'         => '100',
            'field'         => '{"name":"value","label":"Value", "title":"Ewallet Fund Maximum Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'ewallet_fund_bonus_amount',
            'name'          => 'Ewallet Fund Bonus Amount',
            'description'   => 'Ewallet Fund Bonus Amount',
            'value'         => '10',
            'field'         => '{"name":"value","label":"Value", "title":"Ewallet Fund Bonus Amount" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'twofactor_auth_status',
            'name'          => 'Two Factor Authentication Status',
            'description'   => 'Status for Two Factor',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'ewallet_status',
            'name'          => 'E - Wallet Status',
            'description'   => 'Status for E - Wallet',
            'value'         => '1',
            'field'         => '{"name":"value","label":"Value" ,"type":"radio", "options":{"1":"Active", "0":"Inactive"}}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);

          DB::table('settings')->insert([
            'key'           => 'ewallet_btc_address',
            'name'          => 'Ewallet BTC Address',
            'description'   => 'Ewallet BTC Address',
            'value'         => 'mopxdeeYKGd2L4k6Yv2FMLentiYuXg3L3T',
            'field'         => '{"name":"value","label":"Value", "title":"Ewallet BTC Address" ,"type":"text"}',
            'active'        => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"), 
        ]);
    }
    
}
