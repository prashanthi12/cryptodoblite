<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;


class MemberLoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testMemberLogin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/login')
                    ->assertSee('Login')
                    ->value('#email', 'demouser1@larahyip.com')
                    ->value('#password', 'demouser1')
                    ->press('Login')
                    ->assertPathIs('/myaccount/home');
        });
    }
}
