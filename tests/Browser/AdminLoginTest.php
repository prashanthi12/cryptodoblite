<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminLoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAdminLogin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/admin/login')
                    ->assertSee('Admin Login')
                    ->value('#email', 'admin@larahyip.com')
                    ->value('#password', 'admin')
                    ->press('Login')
                    ->assertPathIs('/admin/dashboard');
        });
    }
}
