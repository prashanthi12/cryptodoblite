<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;


class SuperAdminLoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSuperAdmin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/superadmin/login')
                    ->assertSee('Super Admin Login')
                    ->value('#email', 'superadmin@larahyip.com')
                    ->value('#password', 'superadmin')
                    ->press('Login')
                    ->pause('5000')
                    ->assertPathIs('/superadmin/dashboard');
        });
    }
}
