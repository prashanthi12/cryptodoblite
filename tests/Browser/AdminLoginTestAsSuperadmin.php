<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminLoginTestAsSuperadmin extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAdminLoginAsSuperadmin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/admin/login')
                    ->assertSee('Admin Login')
                    ->value('#email', 'superadmin@larahyip.com')
                    ->value('#password', 'superadmin')
                    ->press('Login')
                    ->pause('5000')
                    ->assertSee('/superadmin/dashboard');
        });
    }
}
