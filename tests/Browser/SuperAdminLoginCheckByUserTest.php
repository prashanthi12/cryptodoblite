<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;


class SuperAdminLoginTestCheckByUser extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSuperAdminAsUser()
    {
        $this->browse(function ($browser) {
            $browser->visit('/superadmin/login')
                    ->assertSee('Super Admin Login')
                    ->value('#email', 'demouser1@larahyip.com')
                    ->value('#password', 'demouser1')
                    ->press('Login')
                    ->assertPathIs('/');
        });
    }
}
