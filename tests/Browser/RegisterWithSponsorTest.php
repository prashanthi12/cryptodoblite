<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterWithSponsorTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegisterWithSponsor()
    {
        $this->browse(function ($browser) {
            $browser->visit('/ref/demouser1')
                    ->assertPathIs('/')
                    ->assertSee('demouser1')
                    ->visit('/register')
                    ->value('#name', 'testuser23')
                    ->value('#email', 'testuser23@larahyip.com')
                    ->value('#password', 'testuser23')
                    ->value('#password-confirm', 'testuser23')
                    ->press('Register')
                    ->pause('5000')
                    ->assertPathIs('/myaccount/home')
                    ->assertSee('SPONSOR : DEMOUSER1');
        });
    }
}
