<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminLoginTestAsMember extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAdminLoginAsSuperadmin()
    {
        $this->browse(function ($browser) {
            $browser->visit('/admin/login')
                    ->assertSee('Admin Login')
                    ->value('#email', 'demouser1@larahyip.com')
                    ->value('#password', 'demouser1')
                    ->press('Login')
                    ->pause('5000')
                    ->assertPathIs('/myaccount/home');
        });
    }
}
