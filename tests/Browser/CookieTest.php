<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CookieTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRefCookie()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/ref/demouser1')
                    ->assertSee('demouser1');
        });
    }
    public function testNonRefCookie()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/ref/demouser16666')
                    ->assertDontSee('demouser16666');
        });
    }
}
