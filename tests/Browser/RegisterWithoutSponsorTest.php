<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class RegisterWithoutSponsorTest extends DuskTestCase
{
    
    use DatabaseTransactions;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegistrationWithoutSponsor()
    {
        $this->browse(function ($browser) {
            $browser->visit('/register')
                    ->value('#name', 'testuser22')
                    ->value('#email', 'testuser22@larahyip.com')
                    ->value('#password', 'testuser22')
                    ->value('#password-confirm', 'testuser22')
                    ->press('Register')
                    ->pause('5000')
                    ->assertPathIs('/myaccount/home')
                    ->assertSee('SPONSOR : ROOT');
        });
    }
}
