<?php
return array(

/** set your paypal credential **/

// 'client_id' =>'ARAI3esay9E-C6jzo3ycOun8gMEf2-PRz96PaXG-w3XlpcZCaCvTJ2rNOI9T2-8LMwYG6uIf4x5tC95a',

// 'secret' => 'EE6u3yXh4iKUvcGISZ-o0ILMyEafBttvI5hzULTy9qTbY3GLmBIide1z96z1nh0Sgtb0Ss1uqzu2DdJA',

/**

* SDK configuration 

*/

'settings' => array(

/**

* Available option 'sandbox' or 'live'

*/

// 'mode' => 'sandbox',

/**

* Specify the max request time in seconds

*/

'http.ConnectionTimeOut' => 1000,

/**

* Whether want to log to a file

*/

'log.LogEnabled' => true,

/**

* Specify the file that want to write on

*/

'log.FileName' => storage_path() . '/logs/paypal.log',

/**

* Available option 'FINE', 'INFO', 'WARN' or 'ERROR'

*

* Logging is most verbose in the 'FINE' level and decreases as you

* proceed towards ERROR

*/

'log.LogLevel' => 'FINE'

),

);
?>
