<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminNotifyKycDocVerify;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminNotifyKycVerify;
use App\Models\Mailtemplate;

class AdminNotifyKycDocVerifyMail
{
    protected $userprofile, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminNotifyKycDocVerify $event)
    {
        // dd($event);
        $adminkycnotify = Mailtemplate::where([['name','admin_notify_kyc_verify'],['status','active']])->first();
        if(!is_null($adminkycnotify))
        {
            Mail::to($event->admin->email)->queue(new AdminNotifyKycVerify($event));
        }
    }
}
