<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserActiveDeposit;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActiveDepositSuccessfull;
use App\Models\Mailtemplate;

class UserActiveDepositMail
{
    protected $deposit, $user, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserActiveDeposit $event)
    {
        $activedeposit = Mailtemplate::where([['name','active_deposit_successfull'],['status','active']])->first();
        if(!is_null($activedeposit))
        {
            Mail::to($event->user->email)->queue(new ActiveDepositSuccessfull($event));
        }
    }
}
