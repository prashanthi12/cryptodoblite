<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminWithdrawReject;
use Illuminate\Support\Facades\Mail;
use App\Mail\WithdrawReject;
use App\Models\Mailtemplate;

class WithdrawRejectMail
{
    protected $result, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminWithdrawReject $event)
    {
        $withdrawreject = Mailtemplate::where([['name','withdraw_reject'],['status','active']])->first();
        if(!is_null($withdrawreject))
        {
            Mail::to($event->user->email)->queue(new WithdrawReject($event)); 
        }
    }
}
