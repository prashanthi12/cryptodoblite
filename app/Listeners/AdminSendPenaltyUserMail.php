<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminSendPenalty;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserPenalty;
use App\Models\Mailtemplate;

class AdminSendPenaltyUserMail
{
    protected $user, $penalty;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminSendPenalty $event)
    {
        $userpenalty = Mailtemplate::where([['name','user_penalty'],['status','active']])->first();
        if(!is_null($userpenalty))
        {
            Mail::to($event->user->email)->queue(new UserPenalty($event));
        }
    }
}
