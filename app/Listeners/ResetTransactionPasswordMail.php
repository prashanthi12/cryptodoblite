<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ResetTransactionPasswordSaved;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetTransactionPassword;
use App\Models\Mailtemplate;

class ResetTransactionPasswordMail
{
    protected $user, $newpassword;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ResetTransactionPasswordSaved $event)
    {
        $reset_transactionpassword = Mailtemplate::where([['name','reset_transaction_password'],['status','active']])->first();
        if(!is_null($reset_transactionpassword))
        {
            Mail::to($event->user->email)->queue(new ResetTransactionPassword($event));
        }
    }
}
