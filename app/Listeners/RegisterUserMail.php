<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\NewUserRegister;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\RegisterNewUser;
use App\Traits\RegistersUsers;

class RegisterUserMail
{
    use RegistersUsers;
    
    protected $user, $userprofile, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewUserRegister $event)
    {
       // dd($event);
        // Mail::to($event->user->email)->queue(new RegisterNewUser($event)); 
        $this->registerMail($event);
    }
}
