<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserTicketSend;
use Illuminate\Support\Facades\Mail;
use App\Mail\StaffNotifyTicket;

class StaffNotifyTicketMail
{
    protected $ticket, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserTicketSend $event)
    {
        Mail::to($event->ticket->agent->email)->queue(new StaffNotifyTicket($event)); 
    }
}
