<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminKYCApprove;
use Illuminate\Support\Facades\Mail;
use App\Mail\KYCApprove;
use App\Models\Mailtemplate;

class AdminKYCApproveMail
{
    protected $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminKYCApprove $event)
    {
        $kycapprove = Mailtemplate::where([['name','kyc_approve'],['status','active']])->first();
        if(!is_null($kycapprove))
        {
            Mail::to($event->user->email)->queue(new KYCApprove($event));
        }
    }
}
