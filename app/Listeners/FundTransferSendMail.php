<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\FundTransferMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\FundTransferReceiver;
use App\Traits\FundTransferProcess;

class FundTransferSendMail
{
    use FundTransferProcess;
    
    protected $result, $sender, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(FundTransferMail $event)
    {
        $this->fundTransferMail($event);
        // Mail::to($event->user->email)->queue(new FundTransferReceiver($event)); 
      //  Mail::to($event->sender->email)->queue(new FundTransferSender($event));
    }
}
