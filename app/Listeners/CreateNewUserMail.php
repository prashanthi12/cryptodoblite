<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminsCreateNewUser;
use Illuminate\Support\Facades\Mail;
use App\Mail\CreateNewUser;
use App\Models\Mailtemplate;

class CreateNewUserMail
{
    protected $user, $password;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminsCreateNewUser $event)
    {
        $newuser = Mailtemplate::where([['name','create_new_user'],['status','active']])->first();
        if(!is_null($newuser))
        {
            Mail::to($event->user->email)->queue(new CreateNewUser($event));
        }
    }
}
