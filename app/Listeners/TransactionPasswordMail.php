<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\TransactionPasswordSaved;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransactionPassword;
use App\Models\Mailtemplate;

class TransactionPasswordMail
{
   // protected $userprofile;
    protected $user, $newpassword;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TransactionPasswordSaved $event)
    {
        $transactionpassword = Mailtemplate::where([['name','transaction_password'],['status','active']])->first();
        if(!is_null($transactionpassword))
        {
            Mail::to($event->user->email)->queue(new TransactionPassword($event)); 
        }
    }
}
