<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserNewDeposit;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewDepositSuccessfull;
use App\Traits\DepositProcess;

class UserNewDepositMail
{
    use DepositProcess;
    
    protected $deposit, $user, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserNewDeposit $event)
    {
        // Mail::to($event->user->email)->queue(new NewDepositSuccessfull($event));
        $this->newDepositMail($event);
    }
}
