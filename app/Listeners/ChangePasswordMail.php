<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserChangePassword;
use Illuminate\Support\Facades\Mail;
use App\Mail\ChangePassword;
use App\Models\Mailtemplate;

class ChangePasswordMail
{
    protected $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserChangePassword $event)
    {
        $changepassword = Mailtemplate::where([['name','change_password'],['status','active']])->first();
        if(!is_null($changepassword))
        {
            Mail::to($event->user->email)->queue(new ChangePassword($event));
        }
    }

}
