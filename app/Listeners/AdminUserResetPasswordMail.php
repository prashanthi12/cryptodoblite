<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminUserResetPassword;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Models\Mailtemplate;

class AdminUserResetPasswordMail
{
    protected $user, $token;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminUserResetPassword $event)
    {
        $resetpassword = Mailtemplate::where([['name','reset_password'],['status','active']])->first();
        if(!is_null($resetpassword))
        {
            Mail::to($event->user->email)->queue(new ResetPassword($event));
        }
    }
}
