<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserActiveDeposit;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminNotifyActiveDeposit;
use App\Models\Mailtemplate;

class AdminNotifyActiveDepositMail
{
    protected $deposit, $user, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserActiveDeposit $event)
    {
        $activedeposit = Mailtemplate::where([['name','admin_notify_active_deposit'],['status','active']])->first();
        if(!is_null($activedeposit))
        {
            Mail::to($event->admin->email)->queue(new AdminNotifyActiveDeposit($event));
        }
    }
}
