<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminSendBonus;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserSendBonus;
use App\Models\Mailtemplate;

class AdminSendBonusUserMail
{
    protected $user, $bonus;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminSendBonus $event)
    {
        $sendbonus = Mailtemplate::where([['name','user_send_bonus'],['status','active']])->first();
        if(!is_null($sendbonus))
        {
            Mail::to($event->user->email)->queue(new UserSendBonus($event));
        }     
    }
}
