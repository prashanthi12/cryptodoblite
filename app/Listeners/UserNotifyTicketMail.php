<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\StaffUpdateTicketStatus;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserNotifyTicketStatus;
use App\Traits\TicketProcess;

class UserNotifyTicketMail
{
    use TicketProcess;
    
    protected $statusresult, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StaffUpdateTicketStatus $event)
    {
        // dd($event);
        // Mail::to($event->statusresult->user->email)->queue(new UserNotifyTicketStatus($event)); 
        $this->staffUpdateTicketStatus($event);
    }
}
