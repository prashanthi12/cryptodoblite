<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\FundTransferSender;
use App\Events\FundTransferMail;

class FundTransferSenderMail
{
    protected $result, $sender, $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(FundTransferMail $event)
    {
       // dd($event);
       // Mail::to($event->user->email)->queue(new FundTransferReceiver($event)); 
       Mail::to($event->sender->email)->queue(new FundTransferSender($event));
    }
}
