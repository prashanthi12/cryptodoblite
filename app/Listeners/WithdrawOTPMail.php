<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserWithdrawOTP;
use Illuminate\Support\Facades\Mail;
use App\Mail\WithdrawOtp;
use App\Models\Mailtemplate;

class WithdrawOTPMail
{
    protected $result, $userprofile;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserWithdrawOTP $event)
    {
        $withdrawotp = Mailtemplate::where([['name','withdraw_otp'],['status','active']])->first();
        if(!is_null($withdrawotp))
        {
            Mail::to($event->userprofile->user->email)->queue(new WithdrawOtp($event)); 
        }
    }
}
