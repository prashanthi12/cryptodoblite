<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminKYCReject;
use Illuminate\Support\Facades\Mail;
use App\Mail\KYCReject;
use App\Models\Mailtemplate;

class AdminKYCRejectMail
{
    protected $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(AdminKYCReject $event)
    {
        $kycreject = Mailtemplate::where([['name','kyc_reject'],['status','active']])->first();
        if(!is_null($kycreject))
        {
            Mail::to($event->user->email)->queue(new KYCReject($event));
        }
    }
}
