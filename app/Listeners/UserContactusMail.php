<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ContactusAdded;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserContactus;
use App\Models\Mailtemplate;

class UserContactusMail
{
    protected $contact;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ContactusAdded $event)
    {
        $usercontactus = Mailtemplate::where([['name','user_contact_us'],['status','active']])->first();
        if(!is_null($usercontactus))
        {
            Mail::to($event->contact->email)->queue(new UserContactus($event)); 
        }
    }
}
