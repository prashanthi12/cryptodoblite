<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\NewUserRegister;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminNotifyNewUser;

class AdminNotifyNewUserMail
{
    protected $user, $userprofile, $admin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewUserRegister $event)
    {
        Mail::to($event->admin->email)->queue(new AdminNotifyNewUser($event)); 
    }
}
