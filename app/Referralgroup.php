<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Referralgroup extends Model
{
    use CrudTrait;

    protected $fillable = [
        'name', 'referral_commission', 'level_commission', 'min_amount', 'active', 'is_default'
    ];

    protected $cast = [
        'level_commission' => 'array',
    ];

    public function users() {
    	return $this->hasManyThrough(  
    		'App\User',
            'App\Userprofile',
            'referral_group_id', 
            'id', 
            'id', 
            'id' 
        );
    }

}
