<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Useraccount extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'user_id', 'account_no','description','accounttype_id', 'entity_reference_id','entity_type',
    ];

    protected $dates = ['deleted_at'];

    protected static function boot() {
    parent::boot();

            static::deleting(function($user) {
                $user->transaction()->delete();
                            

            });
    }

    public function user() {
     	return $this->belongsTo('App\User');
    }

    public function accounttype() {
    	return $this->belongsTo('App\Accounttype');
     }

     public function userprofile() {
        return $this->belongsTo('App\Userprofile', 'entity_reference_id', 'id');
    }

    public function transaction() {
        return $this->hasMany('App\Transaction');
    }
    
    public function creditPayments() {
        return $this->hasMany('App\Payment', 'credit_transaction_id', 'id');
    }

    public function debitPayments() {
        return $this->hasMany('App\Payment', 'debit_transaction_id', 'id');
    }

    
}