<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PlanRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd(Input::get());
        Validator::extend('checkmaxamount', function ($attribute, $value, $parameters, $validator) {
                   if (Input::get('min_amount') > Input::get('max_amount'))
                   {
                        return FALSE;
                   }
                   return TRUE;        
        });


        if (Input::get('partial_withdraw_status') == 1)
        {
            $rules['max_partial_withdraw_limit'] =  'required|numeric|min:1|max:100';
            $rules['minimum_locking_period'] =  'required|numeric|min:1';
            $rules['paritial_withdraw_fee'] =  'required|numeric|min:1';
        }

        $rules = [
             'name' => 'required',
             'image' => 'sometimes|required',
             'plantype_id' => 'required',
             'min_amount' => 'required|numeric|min:1',
             'max_amount' => 'required|numeric|checkmaxamount|max:9999999',
             'interest_rate' => 'required|numeric|min:0.001|max:100',
             'duration' => 'required|numeric',
             'duration_key' => 'required', 
             'orderby' => 'numeric',
                      
        ];

 

        return $rules;    
               
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'max_amount.checkmaxamount' => 'Minimum amount greater than maximum amount',
        ];
    }
}
