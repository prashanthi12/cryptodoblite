<?php

namespace App\Http\Requests;

use App\Plan;
use App\Userpayaccounts;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Traits\UserInfo;
use App\Withdraw;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class AutowithdrawalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    use UserInfo;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {  

     
        Validator::extend('checkautowithdrawal', function ($attribute, $value, $parameters, $validator) {
                
                  if (Input::get('amount') < Config::get('settings.withdraw_min_amount'))
                   {
                        return FALSE;
                   }
                   return TRUE;            
        });

        Validator::extend('checkbalance', function ($attribute, $value, $parameters, $validator) {
                 $user = User::where('id', Auth::id())->with(['userprofile','useraccounts','deposits'])->first();

                  $userbalance = $this->getUserBalance($user);

                  
                   if ($value > $userbalance)
                   {
                        return FALSE;
                   }
                   return TRUE;            
        });


         $rules = [
            'amount' => 'required|numeric|checkautowithdrawal|checkbalance',
            'status' => 'required',

        ];       

        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        return [
            'amount.required' => trans('forms.amountrequired'),
            'amount.checkautowithdrawal' => trans('forms.checkautowithdrawal', ['amount' => Config::get('settings.withdraw_min_amount')]),
            'amount.checkbalance' => trans('forms.errorbalance'),
           
        ];
    }

}
