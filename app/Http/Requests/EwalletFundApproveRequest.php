<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Lang;

class EwalletFundApproveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       Validator::extend('checkamount', function ($attribute, $value, $parameters, $validator) 
       {
            if (Input::get('received_amount') < 0)
            {
                return FALSE;
            }
            return TRUE;               
        });

        return [
                  'received_amount'  => 'required|checkamount|numeric',
                  'comment' => 'required',
               ];
    }

    public function messages()
    {
        return [  
             'received_amount.checkamount' => trans('forms.fundamount_error'),     
        ];
    }

}