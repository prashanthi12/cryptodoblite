<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Input;
use Validator;

use App\Plan;
use App\Registrationbonus;

class RegistrationbonusRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                
                if (Input::get('plan') != '')
                {
                     Validator::extend('checkplanbonus', function ($attribute, $value, $parameters, $validator) {  
                        $plan = Registrationbonus::where('plan', Input::get('plan'))->exists();

                        if ($plan)
                        {
                            return FALSE;
                        }
                        return TRUE;    
                    });
                }

                if (Input::get('bonusvalue') != '')
                {
                    
                        $plan = Plan::where('id', Input::get('plan'))->first();

                        //dd($plan);
                        $min_max_validation = 'between:'.$plan->min_amount.','.$plan->max_amount;  
                    
                }             
                       
                $rules = [
                     'name' => 'required|min:5|max:255',
                     'bonusvalue' => 'required|numeric|'.$min_max_validation,
                     //'plan' => 'checkplanbonus',
                ];    

                return $rules;
            }
            case 'PUT':
            {
                if (Input::get('plan') != '')
                {
                     Validator::extend('checkplanbonus', function ($attribute, $value, $parameters, $validator) {  
                        $plan = Registrationbonus::where('plan', Input::get('plan'))->exists();

                        if ($plan)
                        {
                            return FALSE;
                        }
                        return TRUE;    
                    });
                }

                if (Input::get('bonusvalue') != '')
                {
                     $plan = Plan::where('id', Input::get('plan'))->first();

                        //dd($plan);
                        $min_max_validation = 'between:'.$plan->min_amount.','.$plan->max_amount;  
                } 

                $rules = [
                     'name' => 'required|min:5|max:255',
                     'bonusvalue' => 'required|numeric|'.$min_max_validation,
                     //'plan' => 'checkplanbonus',
                ];
                //dd($rules);
                return $rules;
            }
            case 'PATCH':        
            default:break;
        }
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
       return [
            'plan.checkplanbonus' => 'Bonus Already Created This Plan.', 
            'bonusvalue.checkbonusvalue' => 'Bonus value should be below 100',
        ];
    }
}
