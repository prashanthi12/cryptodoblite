<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SliderRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        return [
            'image' => 'required',
            'slidertext' => 'required',
            'url' => 'required|regex:' . $regex,
            'urltext' => 'required',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
             'image.required' => ' Image is required',   
            'slidertext.required' => 'Slider text is required',   
             'url.required' => 'Iamge url is required',   
            'urltext.required' => 'Image url text is required',   
        ];
    }
}
