<?php

namespace App\Http\Requests;
 
use Lang;
use Session;
use Illuminate\Support\Facades\Input;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {               
        $rules = [
            'subject'      => 'required',
           'description'      => 'required',    
        ];

        $files = count($this->input('attachments')) - 1;
            foreach(range(0, $files) as $index) {
            $rules['attachments.' . $index] = 'mimes:png,jpeg,jpg,pdf';        
        }
        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {       

        $message = [
           'subject.required'      => trans('forms.subjectreqmsg'),
            'description.required'      => trans('forms.descriptionreqmsg'),
        ];
        
        $files = count($this->input('attachments'));
            foreach(range(0, $files) as $index) {
            $message['attachments.' . $index] = trans('forms.mimecheck');
            Session::flash('mimecheck', $message['attachments.' . $index]);           
         return $message;
        }
        
        return $message;
    }
    

}
