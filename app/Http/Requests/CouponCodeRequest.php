<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Epin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class CouponCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $rules = [
        //     'couponcode' => 'required',
        // ]; 
        Validator::extend('epinexists', function ($attribute, $value, $parameters, $validator) 
        {   
            $checkepinexists = Epin::where('coupon_code', $value)->first();  
           // dd($checkepinexists);           
                if ($checkepinexists == '')
                {
                   // dd($checkepinexists);
                    return FALSE;
                }
               return TRUE;            
        });    

        Validator::extend('checknewepin', function ($attribute, $value, $parameters, $validator) 
        {     
            $checknewepin = Epin::where([['coupon_code', $value],['status', 'used']])->exists(); 
           // dd($checkepinexists);            
                if ($checknewepin)
                {
                    return FALSE;
                }
               return TRUE;            
        });  

        Validator::extend('couponvaluecheck', function ($attribute, $value, $parameters, $validator) 
        {   
            $couponvaluecheck = Epin::where([['coupon_code', $value],['status', 'new'],['coupon_value', Input::get('amount')]])->first();  
           // dd($checkepinexists);           
                if (is_null($couponvaluecheck))
                {
                    return FALSE;
                }
               return TRUE;            
        });  

        $rules['couponcode'] = 'required|epinexists|checknewepin|couponvaluecheck';                 

        return $rules;

    }

    public function messages()
    {
        return [
            'couponcode.required' => 'Coupon code is required', 
            'couponcode.epinexists' => 'E - Pin is not valid',
            'couponcode.checknewepin' => 'E - Pin is already used',
            'couponcode.couponvaluecheck' => 'E - Pin amount does not match with the deposit amount',
        ];
    }

}
