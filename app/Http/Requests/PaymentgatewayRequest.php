<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;

class PaymentgatewayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
        // return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'gatewayname' => 'required',
            'displayname' => 'required',
            'instructions' => 'required',
        ];

        // For registerbonus, e-pin and e-wallet
        if (Input::get('paymentgateway') != 14 && Input::get('paymentgateway') != 18 && Input::get('paymentgateway') != 19)
        {
            $rules['withdrawcommission'] = 'required|numeric'; 
            $rules['params'] = 'required';                
        }
        return $rules;
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
