<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Epin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class EpinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $mincouponamount = \Config::get('settings.coupon_code_minimum_amount');
        $min_validation = FALSE;
        $min_validation = $mincouponamount;  
        $min_qty_validation = '1';
        $rules = [
            'quantity' => 'required|numeric|min:'.$min_qty_validation,
            'amount' => 'required|numeric|min:'.$min_validation,
        ];  

        Validator::extend('checkbalance', function ($attribute, $value, $parameters, $validator) 
        {                  
                if (Input::get('amount') > Input::get('balanceamount'))
                {
                    return FALSE;
                }
               return TRUE;            
        });    

        $rules['amount'] = 'required|numeric|checkbalance|min:'.$min_validation;                 
        $rules['quantity'] = 'required|min:'.$min_qty_validation;

        return $rules;
    }

    public function messages()
    {
        return [
            'quantity.required' => trans('forms.quantity_req'),  
            'amount.required' => trans('forms.amountrequired'), 
            'amount.checkbalance' => trans('forms.errorbalance'),
        ];
    }

}
