<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Bonus;
use Illuminate\Support\Facades\Input;
use Validator;

class BonusRequest extends \Backpack\CRUD\app\Http\Requests\CrudRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                
                if (Input::get('plan') != '')
                {
                     Validator::extend('checkplanbonus', function ($attribute, $value, $parameters, $validator) {  
                        $plan = Bonus::where('plan', Input::get('plan'))->exists();

                        if ($plan)
                        {
                            return FALSE;
                        }
                        return TRUE;    
                    });
                }

                if (Input::get('bonusvalue') != '')
                {
                     Validator::extend('checkbonusvalue', function ($attribute, $value, $parameters, $validator) {  

                        if (Input::get('bonusvalue') > 100)
                        {
                            return FALSE;
                        }
                        return TRUE;    
                    });
                } 

               
                if (Input::get('triggertype') == 3)
                {
                     Validator::extend('checkregisterbonus', function ($attribute, $value, $parameters, $validator) {  
                        $registerbonus = Bonus::where('triggertype', "3")->exists();
                        if ($registerbonus)
                        {
                            return FALSE;
                        }
                        return TRUE;    
                    });

                     $rules['triggertype'] = 'checkregisterbonus'; 
                } 

                        if (Input::get('deposittype') != '')
                        {
                            $rules = [
                            'deposittype' => 'required',
                            ];    
                        }        

                        $rules = [
                             'name' => 'required|min:5|max:255',
                             'bonusvalue' => 'required|numeric|checkbonusvalue',
                             'plan' => 'checkplanbonus',
                             'bonustype' => 'required',
                             'triggertype' => 'required',
                        ];    

                        return $rules;
            }
            case 'PUT':
            {
                if (Input::get('bonusvalue') != '')
                {
                     Validator::extend('checkbonusvalue', function ($attribute, $value, $parameters, $validator) {  

                        if (Input::get('bonusvalue') > 100)
                        {
                            return FALSE;
                        }
                        return TRUE;    
                    });
                } 

                if (Input::get('triggertype') != '')
                {
                     Validator::extend('checkregisterbonus', function ($attribute, $value, $parameters, $validator) {

                    if (Input::get('triggertype') == 3)
                    {  
                        $registerbonus = Bonus::where('triggertype', "3")->exists();
                        if ($registerbonus)
                        {
                            
                            return FALSE;
                        }
                    }
                    //dd('fff');
                        return TRUE;    
                    });
                } 


                $rules = [
                     'name' => 'required|min:5|max:255',
                     'bonusvalue' => 'required|numeric|checkbonusvalue',
                     'bonustype' => 'required',
                     'triggertype' => 'checkregisterbonus'
                ];
                //dd($rules);
                return $rules;
            }
            case 'PATCH':        
            default:break;
        }
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'plan.checkplanbonus' => 'Bonus Already Created This Plan.', 
            'bonusvalue.checkbonusvalue' => 'Bonus value should be below 100',
             'bonustype.required' => 'Bonus Type is Required',
             'triggertype.required' => 'Trigger Type is Required',
             'triggertype.checkregisterbonus' => 'Register bonus already exists.',
             'deposittype.required' => 'Bonus Credited to is Required',
        ];
    }
}
