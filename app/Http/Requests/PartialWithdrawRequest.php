<?php

namespace App\Http\Requests;

use App\Plan;
 
use Lang;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class PartialWithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('checkminamount', function ($attribute, $value, $parameters, $validator) {
                                 
                   if (Input::get('amount') < \Config::get('settings.withdraw_min_amount'))
                   {
                        return FALSE;
                   }
                   return TRUE;            
        });


        Validator::extend('checkmaxamount', function ($attribute, $value, $parameters, $validator) {

                    $maxpartialamount = Input::get('depositamount') * Input::get('maxpartialamount') / 100 ;

                   // dd($maxpartialamount);

                   if (Input::get('amount') > $maxpartialamount)
                   {
                        return FALSE;
                   }
                   return TRUE;            
        });
       
        $rules = [
            'amount' => 'required|numeric|checkminamount|checkmaxamount',
        ];        

        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        $maxpartialamount = Input::get('depositamount') * Input::get('maxpartialamount') / 100 ;

        return [
            'amount.required' => trans('forms.amountrequired'), 
            'amount.checkminamount' => trans('forms.partialwithdrawminamount', ['amount' => \Config::get('settings.withdraw_min_amount')]), 
           'amount.checkmaxamount' => trans('forms.partialwithdrawmaxamount', ['amount' => $maxpartialamount]),
        ];
    }

}
