<?php

namespace App\Http\Middleware;

use Closure;

class MustBeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_ADMIN)
        {
            return $next($request);
        }

        if (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_SUPERADMIN)
        {
            return redirect(HOME_SUPERADMIN);
        }

        return redirect('/');
    }
}
