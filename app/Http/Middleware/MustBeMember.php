<?php

namespace App\Http\Middleware;

use Closure;

class MustBeMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_MEMBER)
        {
            return $next($request);
        }

        if (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_SUPERADMIN)
        {
            return redirect(HOME_SUPERADMIN);
        }
        elseif (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_ADMIN)
        {
            return redirect(HOME_ADMIN);
        }
         elseif (auth()->check() && auth()->user()->userprofile->usergroup_id == USER_ROLE_STAFF)
        {
            return redirect(HOME_STAFF);
        }

        return redirect('/');
    }
}
