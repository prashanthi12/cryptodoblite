<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Socialite;
use App\SocialAccount;
use App\User;
use App\Userprofile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Useraccount;
use App\Transaction;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\AdminNotifyNewUser;
use Session;
use Illuminate\Support\Facades\Redirect;
// use App\Mail\RegisterNewUser;
use App\Events\NewUserRegister;
use Event;

class SocialAuthFacebookController extends Controller
{
  /**
   * Create a redirect method to facebook api.
   *
   * @return void
   */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }
    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback()
    {
        $facebookuser = Socialite::driver('facebook')->user();
        
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($facebookuser->id)
            ->first();
       
        if($facebookuser->email == null)
        {          
        	$facebookuser->email = \Session::get('facebook_email');
        }

        if ($account) 
        {      
            $user = User::whereEmail($facebookuser->email)->first();
        } 
        else 
        {
            $user = User::whereEmail($facebookuser->email)->first();
            if (!$user) 
            {
                $user = User::create([
                    'email' => $facebookuser->email,
                    'name' => $facebookuser->name,
                    'password' => bcrypt(rand(1,10000)),
                ]);

                $account = SocialAccount::create([
                    'user_id' => $user->id,
                    'provider_user_id' => $facebookuser->id,
                    'provider' => 'facebook'
                ]);    

                // $sponsorUser = User::where('email', Config::get('settings.default_sponser'))->first();
                // $sponsor = $sponsorUser->id;           

                $userprofile = new Userprofile;
                $userprofile->user_id = $user->id;
                $userprofile->usergroup_id = '4';
                $userprofile->sponsor_id = "7";
                $userprofile->active = 1;
                $userprofile->referral_group_id = '1';
                $userprofile->email_verified = 1;
                $userprofile->save();

                $account_no = "U-".(100000 + $user->id )."-"."1";
                $account = new Useraccount;
                $account->account_no = $account_no;
                $account->description = "User Operative Account";
                $account->user_id = $user->id;
                $account->accounttype_id = "3";
                $account->entity_reference_id = $userprofile->id;
                $account->entity_type = "Profile";
                $account->save();

                $transaction = new Transaction;
                $transaction->account_id = $account->id;
                $transaction->amount = "0.0";
                $transaction->type = "credit";
                $transaction->status ="1";
                $transaction->accounting_code_id = "1";
                $transaction->request = "{object:undefined}";
                $transaction->response = "{object:undefined}";
                $transaction->save();
      
                $admin = User::find(2);

                //firing an event
                Event::fire(new NewUserRegister($user, $userprofile, $admin));

                // Mail::to($admin->email)->send(new AdminNotifyNewUser($user));
                // Mail::to($user->email)->queue(new RegisterNewUser($user)); 
                $account->user()->associate($user);
                $account->save();   
            }
        }
        auth()->login($user);
        return Redirect::to('/myaccount/home');
    }

}