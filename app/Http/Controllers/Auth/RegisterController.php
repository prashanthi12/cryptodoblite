<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Userprofile;
use App\Useraccount;
use App\Transaction;
use App\Bonus;
use App\Plan;
use App\Registrationbonus;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Traits\RegistersUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Cookie;
use Config;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\EmailVerification; 
use App\Traits\DepositProcess;
use App\Traits\LogActivity;
use App\Language;
use Session;
use Illuminate\Bus\Queueable;
use App\Referralgroup;
//use  App\Notifications\User\UserRegistrationSuccesful;
//use  App\Notifications\Admin\AdminNotifyNewUser;
// use App\Mail\RegisterNewUser;
// use App\Mail\AdminNotifyNewUser;
use App\Events\NewUserRegister;
use Event;
use App\Traits\PayaccountsProcess;
use App\Paymentgateway;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, DepositProcess, LogActivity, PayaccountsProcess;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/myaccount/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $languages = Language::where('active', 1)->get();    
        Session::put('languageslist', $languages);  
        $this->middleware('guest');
       // dd($this->mailchimp);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {   
        $rules = [
            'name' => 'required|min:6|max:12|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ];

        if (Config::get('settings.register_captcha_active') == '1') 
        {
            $rules['g-recaptcha-response'] = 'required';          
        }  
        $message['g-recaptcha-response.required'] = 'Captcha is required'; 

        return Validator::make($data, $rules, $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $sponsorUser = User::where('email', Config::get('settings.default_sponser'))->first();
            //dd($sponsorUser);
        if (is_null($sponsorUser))
        {
            \Session::put('sponsererror', "Problem on Default Sponser in Admin Settings");            
                return Redirect::to( url('/register'));    
        }
        // $ip = '157.50.21.39';      // \Request::ip();   //'157.50.21.39';  India IP   
        // $signupCountry = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
       // dd($signupCountry);
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            // 'ip_address' => $ip,        
            // 'signup_country' => $signupCountry['country'],
        ]); 

        $checksponsor = Cookie::get('sponsor');
        $checkSponsorExists = User::where('name', $checksponsor)->exists();
        if (is_null($checksponsor) || !$checkSponsorExists ) 
        {         
            $sponsor = $sponsorUser->id;
        }else {
            $sponsor = User::where('name', $checksponsor)->first()->id;
        }

        $referralgroup = Referralgroup::where([
                            ['active', '1'],
                            ['is_default', '1'],
                        ])->first();

        $emailcode = str_random(30);      

        $userprofile = new Userprofile;
        $userprofile->user_id = $user->id;
        $userprofile->usergroup_id = "4";
        $userprofile->sponsor_id = $sponsor;
        $userprofile->email_verification_code = $emailcode;
        $userprofile->referral_group_id = $referralgroup->id;
        $userprofile->save();
       //dd($userprofile);        

        $account_no = "U-".(100000 + $user->id )."-"."1";

        $account = new Useraccount;
        $account->account_no = $account_no;
        $account->description = "User Operative Account";
        $account->user_id = $user->id;
        $account->accounttype_id = "3";
        $account->entity_reference_id = $userprofile->id;
        $account->entity_type = "Profile";
        $account->save();

        $transaction = new Transaction;
        $transaction->account_id = $account->id;
        $transaction->amount = "0.0";
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = "1";
        $transaction->request = "{object:undefined}";
        $transaction->response = "{object:undefined}";
        $transaction->save();

        // For btc address creation
       /* $address=$this->createWalletAddress($user);
        $pgId = Paymentgateway::find(6);    // for bitcoin blockio
        $pg = $this->getPgDetailsByGatewayName($pgId->gatewayname);
        $request = [
            'label' => $user->name,
            'btc_address' => $address,
            'paymentid' => $pg->id,            
        ];
        $request = (object) $request;

        $this->bitcoinWallet($request,$user->id,1,1);*/

        //Register Bonus 
        $registerbonus = Registrationbonus::where('status', "1")->first();
        if ($registerbonus)
        {
            $plan = Plan::where('id', '=', $registerbonus->plan)->first();

            $array = [
                    'amount' => $registerbonus->value,
                    'plan' => $plan->id,
                    'paymentgateway' => 14,
                    'transaction_id' => uniqid(),
                    'user_id' => $user->id
                ];
            $request = (object) $array;

            $deposit = $this->makeDeposit($request);
            $depositapprove = $this->approve($deposit->id);
        }         

        $this->doActivityLog(
                $user,
                $user,
                ['ip' => request()->ip()],
                'register',
                'User Registered'
            );

        $admin = User::find(1);

        //firing an event
        Event::fire(new NewUserRegister($user, $userprofile, $admin));

        // Mail::to($user->email)->queue(new EmailVerification($userprofile));         
        // Mail::to($user->email)->queue(new RegisterNewUser($user));
        // Mail::to($admin->email)->queue(new AdminNotifyNewUser($user));    

        return $user;
    }

}
