<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

use DB;
use App\User;
use App\Userprofile;
use Validator;
use App\Language;
use Session;


class EmailverificationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $languages = Language::where('active', 1)->get();    
        Session::put('languageslist', $languages);  
    }

    /**

     * Check for user Activation Code

     *

     * @param  array  $data

     * @return User

     */

    public function emailverification($token)
    {
        $check = DB::table('userprofiles')->where('email_verification_code', $token)->first();
        if(!is_null($check)){
            $userprofile = Userprofile::find($check->id);
//dd($user);
            if($userprofile->email_verified == 1){

                if (!is_null(Auth::id()))
                {
                    return redirect()->to('myaccount/home')->with('error', trans('forms.user_activate'));
                }

                return redirect()->to('login')->with('error', trans('forms.user_activate'));
            }

            $userprofile->email_verified = 1;
           // $userprofile->email_verification_code = '';
            $userprofile->save();

            // DB::table('userprofiles')->where('email_verification_code', $token)->delete();

            if (!is_null(Auth::id()))
                {
                    return redirect()->to('myaccount/home')->with('status', trans('forms.user_email_verification_success'));
                }

            return redirect()->to('login')->with('success', trans('forms.user_email_verification_success'));

        }

         if (!is_null(Auth::id()))
                {
                    return redirect()->to('myaccount/home')->with('error', trans('forms.token_invalid'));
                }
        return redirect()->to('login')->with('error', trans('forms.token_invalid'));

    }    
}
