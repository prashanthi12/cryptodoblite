<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\Placement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;
use App\Traits\Common;
class NetworkController extends Controller
{
    use Common;
    public function network() {

        $network = Placement::where('user_id', Auth::id())->first();
        if (is_null($network)) {
                $noNetwork = true;
                $myNetwork = null;
                $myNetworkArray = null;
         }else {
        $noNetwork = false;
        $networkKey = $network->alphakey;

        $root_id=$network->root_id;

        $network_root_id=$root_id+1;

        /*$network = Placement::whereraw(" `alphakey` REGEXP '$networkKey.'  AND `user_id` IS NOT NULL ")->with('user', 'spillover')->get();*/

        $width=$this->getSettingValue('matrix_width');  

        if(($width==0)&&($networkKey!='A'))
        {

             $network=Placement::withTrashed()->where([['alphakey', 'like', '%' .$networkKey. '%'],['root_id','=',$network_root_id]])->whereNotNull('user_id')->with('user', 'spillover')->get();
        }

        else
        {
             $network=Placement::withTrashed()->where('alphakey', 'like', '%' .$networkKey. '%')->whereNotNull('user_id')->with('user', 'spillover')->get();

        }
 
        $myNetwork = new Collection;
        foreach ($network  as $myn ) {

            $root = $myn->root_id;

            $userName = $myn->user->name;

            $spilloverName='';
            if(count($myn->spillover)>0)
            {
             $spilloverName = $myn->spillover->name;
            }
           

            $myNetwork->push(['root' => $root, 'username' => $userName, 'spillover' => $spilloverName]);
        }

        $myNetworkArray = new Collection;
        foreach ($myNetwork as $item) {
            $username = $item['username'];
            $spillover = $item['spillover'];
            $tooptip = "";
            $myNetworkArray->push([ $username, $spillover, $tooptip ]);
        }
        $myNetworkArray->shift();
        $myNetworkArray = $myNetworkArray->toJson();
        }
        
        //dd($myNetworkArray);
     
        return view ('home.viewnetwork', [
                'noNetwork' => $noNetwork,
                'myNetwork'  => $myNetwork,
                'myNetworkArray' => $myNetworkArray
            ]);
    }
}
