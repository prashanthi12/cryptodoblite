<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Config;
use App\Http\Requests\EwalletFundCheckRequest;
use App\Http\Requests\EwalletFundRequest;
use Illuminate\Support\Facades\Redirect;
use App\Paymentgateway;
use App\Ewallet;
use App\Traits\EwalletProcess;
use App\Transaction;
use App\Presenters\UserPresenter;
use App\Helpers\HyipHelper;
use App\User;
use App\Userpayaccounts;

class EwalletController extends Controller
{
	use EwalletProcess;

    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    public function create()
	{
		$min_amount = config::get('settings.ewallet_fund_min_amount');
	    $max_amount = config::get('settings.ewallet_fund_max_amount');
	    $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['e_wallet', '=', '1'],
                ['id', '!=', '10']
            ])->get();

	    return view('ewallet.create',[		   	
		   	'min_amount' => $min_amount,
		   	'max_amount' => $max_amount,
		   	'pgs' => $pgs,
		]);
	}

	public  function store(EwalletFundCheckRequest $request)
    {
    	$paymentgateway = $request->paymentgateway;
        $amount = $request->amount; 
        $pgInfo = $this->getPgInfo( $request->paymentgateway);
        $params = $pgInfo[0];
        $instructions = $pgInfo[1]; 
        $user = User::where('id', Auth::id())->first();       
 		\Session::put('ewallet_fund_amount', $request->amount);
 		\Session::put('paymentgateway', $request->paymentgateway);

 		if($request->paymentgateway == '9')  // Bitcoin via direct transfer
 		{
 			return Redirect::to( url('/myaccount/ewallet/addfund'));
 		}
 		if($request->paymentgateway == '1')  // Bank Transfer
 		{
 			$tranaction_uuid = uniqid();
            \Session::put('transactionnumber', $tranaction_uuid);
 			return view('ewallet.partials._bankwire_form', [
 					'paymentgateway' => $paymentgateway,
                    'amount' => $amount,
                    'params' => $params,
                    'transaction_id' => $tranaction_uuid,
                    'instructions' => $instructions,
 			]);
 		}
        if( $request->paymentgateway == 2)  // Paypal
        {
            return view('ewallet.partials._paypal_form', [
                'paymentgateway' => $paymentgateway,
                'username' => $user->name,
                'amount' => $amount,
                'params' => $params,
                'instructions' => $instructions,   
            ]);
        }		  		   
    }

    public function addFundCreate()
    {
		$btc_address = config::get('settings.ewallet_btc_address');
		$btc_amount = HyipHelper::convertBtcAmount(1);
  		$total_btc_amount = $btc_amount * \Session::get('ewallet_fund_amount');

		\Session::put('ewallet_total_btc_amount', $total_btc_amount);			
		\Session::put('ewallet_fund_btc_amount', $btc_amount);

 		return view('ewallet.bitcoin_form',[		   	
			   	'btc_address'=>$btc_address,
			   	'btc_amount'=>$total_btc_amount,
	 	]);  
    }

    public  function addFundStore(EwalletFundRequest $request)
    {
    	$id = Auth::id();		
    	$amount = \Session::get('ewallet_fund_amount'); 
		$btc_amount = \Session::get('ewallet_fund_btc_amount'); 
		$total_btc_amount = \Session::get('ewallet_total_btc_amount'); 

	   	$this->addfund($request,$id,$amount,$btc_amount,$total_btc_amount);
	 	\Session::forget('ewallet_fund_amount'); 
	 	\Session::forget('ewallet_total_btc_amount'); 
	 	\Session::forget('ewallet_fund_btc_amount'); 

		return Redirect::to(url('/myaccount/ewallet'));  	   
    }

    public function show($status) 
    { 	
		$id = Auth::id();
      	$total_amount = Ewallet::where([
                            ['from_user_id', '=', $id],
                            ['type', 'add'],
                            ['status', 'approve'],
                            ])->get() ->sum('amount');

       	$fund = Ewallet::where('from_user_id', '=', $id)->orderBy('id','DESC')->get();
       	$account_id = $this->getUserIdByAccountID($id, 'profile');

        $ewallet = Transaction::where([['account_id', '=', $account_id]])->whereIn('accounting_code_id',['68','69','70'])->orderBy('id','DESC')->get();   
		$internal_transfer = Transaction::where([['account_id', '=', $account_id]])->whereIn('accounting_code_id',['73','74'])->orderBy('id','DESC')->get(); 
                                                              		
 		$ewalletbalance = $this->ewalletBalance(Auth::id());

        $btcbalance = 0;
        $user_accounts = Userpayaccounts::where('user_id', Auth::id())->first();
        
        if(count($user_accounts)>0)
        {
            if($user_accounts->btc_address!='')
            {
                $btcbalance = $this->getWalletBalance($user_accounts->btc_address);
            }
        } 
        return view ('ewallet.viewewallet', [                
                'total_amount'=> $total_amount,
                'fund'=> $fund ,
                'ewallet'=> $ewallet ,
                'ewalletbalance'=> $ewalletbalance,
                'internal_transfer'=> $internal_transfer,
                'status' => $status,
                'btcbalance' => $btcbalance,
                'user_accounts' => $user_accounts,
        ]);
    }

    public function transferform()
    {
        $user = User::where('id', Auth::id())->first();     
        $userbalance = $this->getUserBalance($user);
        return view ('ewallet.transfer_show',['userbalance'=>$userbalance]);
    }

    public function getPgInfo($pgID) 
    {
        $pg = Paymentgateway::where('id', $pgID)->first();
        $params = json_decode($pg->params, true);
        $instructions = $pg->instructions;
        $pgInfo = [$params, $instructions];
        return $pgInfo;
    } 

    public function bankwire(Request $request)
    {
    	//dd($request);
        // $admin = User::find(1);
        $id = Auth::id(); 
        $amount = $request->amount;  
        $btc_amount = "";
        $total_btc_amount = "";    
        $ewallet_deposit = $this->addfund($request,$id,$amount,$btc_amount,$total_btc_amount);       

        if($ewallet_deposit)
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/ewallet');
    } 

    public function paymentcancelled(Request $request)
    {
        return view('ewallet.paymentcancelled');
    }   

    public function store_paypal(Request $request)
    {  
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);

        $id = Auth::id(); 
        $amount = $request->amount;  
        $btc_amount = "";
        $total_btc_amount = "";  
        $ewallet_deposit = $this->addfund($request,$id,$amount,$btc_amount,$total_btc_amount);   
        if($ewallet_deposit)
        {
            \session::put('successmessage',trans('forms.deposit_success_msg'));
        }
        else
        {
            \session::put('failmessage', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/ewallet');
    }
    
}
