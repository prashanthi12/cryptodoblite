<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EpinRequest;
use Illuminate\Support\Facades\Auth;
use App\Epin;
use App\Useraccount;
use App\Transaction;
use App\Accountingcode;
use App\Traits\EwalletProcess;
use Config;

class EpinController extends Controller
{
    use EwalletProcess;

    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    public function index()
    {
    	$epinviewdetails = Epin::where('owner', Auth::id())->paginate(Config::get('settings.pagecount'));
    	return view('epin.index', [
    		'epinviewdetails' => $epinviewdetails,
    	]);
    }

    public function create()
    {
        $ewalletbalance = $this->ewalletBalance(Auth::id()); 
    	return view('epin.create', [
            'ewalletbalance' => $ewalletbalance,
        ]);
    }

    public function store(EpinRequest $request)
    {
    	$qtycount = $request->quantity;
        $maxorderby = "";
        if(is_null($maxorderby))
        {
            $maxorderby = 0;
        }
        $maxorderby = Epin::max('order_by');

    	for($i=1;$i<=$qtycount;$i++)
    	{
	    	$epindetails = new Epin;
	    	$epindetails->coupon_value = $request->amount;
	    	$epindetails->coupon_code = str_random(10);
	    	$epindetails->status = "new";
            $epindetails->order_by = $maxorderby + 1;
	    	$epindetails->owner = Auth::id();
	    	$epindetails->save();
	    }

	    $totaldepositamount = $request->amount * $request->quantity;
	    $user_id = Auth::id();
        $account_id = Useraccount::where([
                ['user_id', '=', $user_id],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $accounting_code_id = Accountingcode::where('accounting_code','deposit-via-epin')->get(['id'])->toArray();
        $accounting_code_id = $accounting_code_id[0]['id'];

        if($epindetails->save())
        {
    	    $transaction = new Transaction;
    	    $transaction->account_id = $account_id;
    	    $transaction->amount = $totaldepositamount;
    	    $transaction->type = "debit";
    	    $transaction->status = "1";
    	    $transaction->accounting_code_id = $accounting_code_id;
            $transaction->request = "{object:undefined}";
            $transaction->response = "{object:undefined}";
            $transaction->comment = $epindetails->order_by;
    	    $transaction->save();

            $request->session()->flash('successmessage', trans('forms.epin_success_message'));
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.epin_failure_message'));
        }
    	return redirect(url('/myaccount/myepin'));
    }

}
