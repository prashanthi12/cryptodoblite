<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\Http\Requests\WithdrawRequest;
use Illuminate\Support\Facades\Auth;
use App\Userpayaccounts;
use App\Paymentgateway;
use App\Withdraw;
use App\User;
use Config;
use App\Traits\WithdrawProcess;
use App\Traits\UserInfo;
use Illuminate\Support\Facades\Mail;
// use App\Mail\WithdrawSend;
use Illuminate\Bus\Queueable;
// use App\Helpers\HyipHelper;
use Nexmo\Laravel\Facade\Nexmo;
use App\Userprofile;
use Illuminate\Support\Facades\Redirect;
// use App\Mail\WithdrawOtp;
use App\Http\Requests\WithdrawOtpRequest;
use App\Http\Requests\AutowithdrawalRequest;
use App\Autowithdrawal;
use App\Events\UserWithdrawOTP;
use App\Events\UserWithdrawSend;
use Event;
use Cache;

class WithdrawController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    use WithdrawProcess, UserInfo;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status)
    {
        if (in_array( $status, array( 'pending', 'completed', 'rejected', 'request')) == FALSE)
        {
            abort(404);
        }
        
        $withdrawlists = Withdraw::where('status', $status)->where('user_id',  Auth::id())->with(['user', 'transaction', 'userpayaccounts'])->latest('updated_at')->paginate(Config::get('settings.pagecount'));
        $user = User::where('id', Auth::id())->first();

        // $pendingsum = Withdraw::where('status', 'pending')->where('user_id',  Auth::id())->sum('amount');
        // $completedsum = Withdraw::where('status', 'completed')->where('user_id',  Auth::id())->sum('amount');
        // $lifetimesum = Withdraw::whereNotIn('status', array('rejected', 'request'))->where('user_id',  Auth::id())->sum('amount');

        return view('withdraw.show', [
                        'withdrawlists' => $withdrawlists,
                        'user' => $user,
                        'status' => $status,
                        // 'pendingsum' => $pendingsum,
                        // 'completedsum' => $completedsum,
                        // 'lifetimesum' => $lifetimesum
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['withdraw', '=', '1'],
                ['id', '!=', '10']
            ])->get();

        $user = User::where('id', Auth::id())->with(['userprofile','useraccounts','deposits'])->first();

        $userbalance = $this->getUserBalance($user);

        $withdraw = Withdraw::where('user_id', Auth::id())->where('autowithdraw', '0')->whereIn('status',['pending','completed']);

        $currentMonth = date('m');
        $monthly_withdraw_count = $withdraw->whereRaw('MONTH(created_at) = ?',[$currentMonth])->get()->count();
        $monthly_remaining_withdraw_limit = \Config::get('settings.monthly_withdraw_limit') - $monthly_withdraw_count;

        $daily_withdraw_count = $withdraw
                            ->whereRaw('Date(created_at) = CURDATE()')
                            ->get()->count();
        $daily_remaining_withdraw_limit = \Config::get('settings.daily_withdraw_limit') - $daily_withdraw_count;

        $isKycApproved = $this->isKycApproved($user);
        $isEmailVerified = $this->isEmailVerified($user);
        //dd($isEmailVerified);
        
        return view('withdraw.create',
            [
                'pgs' => $pgs,
                'userbalance' => $userbalance,                
                'user_withdraw_count' => $monthly_withdraw_count,
                'monthly_remaining_withdraw_limit' => $monthly_remaining_withdraw_limit,
                'force_withdraw_down'  => \Config::get('settings.force_withdraw_down'),
                'isKycApproved' => $isKycApproved,
                'isEmailVerified' => $isEmailVerified,
                'force_email_verification_for_withdraw'  => \Config::get('settings.force_email_verification_for_withdraw'),
                'force_kyc_verification_for_withdraw'  => \Config::get('settings.force_kyc_verification_for_withdraw'),
                'kyc_doc' => $user->userprofile->kyc_doc,
                'daily_remaining_withdraw_limit' => $daily_remaining_withdraw_limit,
                'daily_withdraw_taken_count' => $daily_withdraw_count
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WithdrawRequest $request)
    {
        //dd($request);
        $userprofile = Userprofile::where('user_id', Auth::id() )->with('user')->first();
        // get user id and create cache key
        $userId = $userprofile->user_id;
        $key    = $userId . ':' . $request->totp;

        // use cache to store token to blacklist
        Cache::add($key, true, 4);

        $code = rand(11111, 99999);
        
        if (Config::get('settings.nexmo_status') == '1' && !is_null($userprofile->mobile_verification_code))
        {
            try
            {        
                Nexmo::message()->send([
                'to' => $userprofile->mobile,
                'from' => Config::get('settings.NEXMO_SMS_FROM_NUMBER'),
                'text' => 'Use this verification code '.$code
                ]);                                   
            }
            catch(\Exception $e)
            {
                \Session::put('mobilecodeerror', $e->getMessage());  
                return back();
            }
        }
     
        $request->merge(['mobileotp' => $code]);

        $result = $this->withdrawotp($request);            

        \Session::put('amount', $request->amount);
        \Session::put('paymentgateway', $request->paymentgateway);
        \Session::put('userpayaccountid', $request->userpayaccountid);
        \Session::put('withdrawid', $result->id);  
        
        //firing an event
        Event::fire(new UserWithdrawOTP($result, $userprofile));

        // Mail::to($userprofile->user->email)->queue(new WithdrawOtp($result)); 

        return Redirect::to( url('/myaccount/withdraw/otp/check'));       
    }

    public function otpsendagain(Request $request)
    {
        \Session::put('amount', $request->amount);
        \Session::put('paymentgateway', $request->paymentgateway);
        \Session::put('userpayaccountid', $request->userpayaccountid);
        \Session::put('withdrawid', $request->withdrawid);
        // Again send OTP mail 
        $withdraw = Withdraw::where('id', '=', $request->withdrawid)->first();
        $userprofile = Userprofile::where('user_id', Auth::id() )->with('user')->first();

        //firing an event
      //  Event::fire(new UserWithdrawOTP($withdraw, $userprofile));

        //Mail::to($userprofile->user->email)->queue(new WithdrawOtp($withdraw));
        
        return Redirect::to( url('/myaccount/withdraw/otp/check'));  
    }

    public function otpcheck(Request $request)
    {
        return view('withdraw.otp_check', [
                    'amount' => \Session::get('amount'),
                    'paymentgateway' => \Session::get('paymentgateway'),
                    'userpayaccountid' => \Session::get('userpayaccountid'),
                    'withdrawid' => \Session::get('withdrawid'),                    
                ]);

    }

    public function updatewithdrawdetails(WithdrawOtpRequest $request)
    {

        $result = $this->withdrawrequest($request);
        
        $adminemail = User::where('id', 2)->first();
        $adminemailid = $adminemail->email;       
        $user = User::where('id', Auth::id())->first();
       // dd($user);
        if ($result)
        {
            //firing an event
            Event::fire(new UserWithdrawSend($result, $adminemail, $user));

           // Mail::to($adminemailid)->queue(new WithdrawSend($result));

            $request->session()->flash('successmessage', trans('forms.withdraw_request_success_message'));
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.withdraw_request_error_message'));
        }
        return Redirect::to( url('/myaccount/withdraw/pending'));
    }
   
    public function userpayaccount(Request $request)
    {
        //dd($request);
        if($request->paymentid == 7) //Coinpayment
        {
            $request->paymentid = 9; //Bitcoin Direct
        } 
        $payaccount_result  = Userpayaccounts::where([                                        
                                            ['user_id', '=', Auth::id()],                   
                                            ['active', '=', "1"],
                                            ['paymentgateways_id', '=', $request->paymentid]
                                            ])->get();
        $commissionvalue  = Paymentgateway::where('id', $request->paymentid)->get(['withdraw_commission'])->toArray();
        $admincommission = $commissionvalue[0]['withdraw_commission']; 
        //dd($admincommission);
        return view('withdraw.userpayaccount',
            [
                'payaccount_result' => $payaccount_result,
                'admincommission' => $admincommission
            ]);
    }

    public function viewbitcoinwallet($id)
    {
        $withdraw = Withdraw::where('id', $id)->with('userpayaccounts')->first();
        //dd($withdraw->userpayaccounts->param2);
        
        // $curl_json = HyipHelper::getBitcoinWalletDetails($withdraw->param);

        // $curl_json = json_decode($curl_json, true);

        // $received_amount = '';
        // foreach ($curl_json['vout'] as $vout)
        //  {
        //     if ($vout['scriptPubKey']['addresses'][0] == $withdraw->userpayaccounts->param2)
        //     {
        //         $received_amount .= $vout['value'];
        //     }                               
        //  }

        //  $txhashid = $curl_json['txid'];
        //  $total_confirmations = $curl_json['confirmations'];
        //  $bitcoin_transaction_time = date("Y-m-d H:i:s", $curl_json['time']);
        //  $actual_withdraw_amount = HyipHelper::convertBtcAmount($withdraw->amount);
    
       
         return view('partials._withdraw_bitcoin_wallet_details', [
                'txnhashkey' => $withdraw->param,
                // 'confirmations' => $total_confirmations,
                // 'actual_withdraw_amount' => $actual_withdraw_amount,
                // 'received_amount' => $received_amount,                
                // 'bitcoin_transaction_time' => $bitcoin_transaction_time,
            ]);
    }

    public function autowithdrawal()
    {
        $payaccount_result  = Userpayaccounts::where([                       
                                            ['user_id', '=', Auth::id()],                   
                                            ['active', '=', "1"],
                                            ['current', '=', "1"],                            
                                            ])->get();

        $autowithdrawalresult  = Autowithdrawal::where('user_id', '=', Auth::id())->first();

        $amount = '';
        $payaccount_id = '';
        $status = '';
        $activechecked = '';
        $inactivechecked = '';

        if (!is_null($autowithdrawalresult))
        {
            $amount = $autowithdrawalresult->amount;
            $payaccount_id = $autowithdrawalresult->payaccount_id;
            $status = $autowithdrawalresult->status;
            $activechecked = '';
            $inactivechecked = '';

            if ($status == 1)
            {
                $activechecked = 'checked';
            }
            else
            {
                $inactivechecked = 'checked';
            }
        }        

        //dd($inactivechecked);
        return view('withdraw.autowithdrawal',[
                'payaccount_result' => $payaccount_result,
                'amount' => $amount,
                'payaccount_id' => $payaccount_id,
                'status' => $status,
                'activechecked' => $activechecked,
                'inactivechecked' => $inactivechecked,
            ]);
    }

    public function saveautowithdrawal(AutowithdrawalRequest $request)
    {
        $autowithdrawalresult  = Autowithdrawal::where('user_id', '=', Auth::id())->first();
        //dd($autowithdrawalresult);

        if (is_null($autowithdrawalresult))
        {
            $autowithdrawal = new Autowithdrawal;
            $autowithdrawal->amount = $request->amount;
            $autowithdrawal->user_id = Auth::id();
            $autowithdrawal->payaccount_id = $request->payaccount;
            $autowithdrawal->status = $request->status;
            if ($autowithdrawal->save())
            {
                $request->session()->flash('successmessage', trans('forms.auto_withdrawal_create_success_message'));
            }
            else
            {
                $request->session()->flash('errormessage', trans('forms.auto_withdrawal_create_error_message'));
            }
        }
        else
        {
            $autowithdrawalresult->amount = $request->amount;
            $autowithdrawalresult->payaccount_id = $request->payaccount;
            $autowithdrawalresult->status = $request->status;
            if($autowithdrawalresult->save())
            {
                $request->session()->flash('successmessage', trans('forms.auto_withdrawal_update_success_message'));
            }
            else
            {
                $request->session()->flash('errormessage', trans('forms.auto_withdrawal_update_error_message'));
            }
            

        }
        return back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
