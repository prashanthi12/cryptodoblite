<?php

namespace App\Http\Controllers\Myaccount;
use App\Http\Controllers\Controller;
use App\User;
use App\MailMessage;
use Illuminate\Support\Facades\Auth;
use Config;

class MailMessageController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    public function index()
    {
    	$inbox = MailMessage::where('to_user_id', Auth::id())->paginate(Config::get('settings.pagecount'));
    	return view('home.myinbox', [
    		'inbox' => $inbox,
    	]);
    }
}