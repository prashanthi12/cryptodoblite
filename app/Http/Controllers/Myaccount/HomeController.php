<?php

namespace App\Http\Controllers\Myaccount;

use App\User;
use App\Userprofile;
use App\Useraccount;
use App\Transaction;
use App\Deposit;
use App\Testimonial;
use App\Accountingcode;
use App\News;
use App\Traits\ProfileInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use session;
use Illuminate\Support\Facades\Redirect;
use App\Traits\EwalletProcess;
use App\Traits\Common;
use App\Userpayaccounts;

class HomeController extends Controller
{
    use ProfileInfo, EwalletProcess, Common;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    // public function indexold()
    // {
    //     $user = User::where('id', Auth::id())->with(['userprofile','useraccounts','deposits'])->first();

    //     if ($user->userprofile->active == 0)
    //     {
    //         Auth::logout();
    //         Session::flash('error', trans('myaccount.account_suspended_message'));
    //         return Redirect::back();
    //     }

    //     // $user = User::ByUserType(USER_ROLE_SYSTEM)->first();
    //     // $user = User::has('usergroup_id', USER_ROLE_SYSTEM)->first();

    //     // dd($user);

    //     $checktestimonial = Testimonial::where('user_id', Auth::id())->exists();
    //     $sponsor='';
    //     if($user->userprofile->sponsor_id!='')
    //     {
    //       $sponsor = User::withTrashed()->where('id', $user->userprofile->sponsor_id)->first()->name;
    //     }

    //     $hasProfile = $this->hasProfile($user);

    //     $completedProfile = $this->checkProfileForCompletion($user);

    //     $useraccounts = $user->useraccounts();

    //     $mydeposits = Deposit::with(['plan','interest'])->where('user_id',  Auth::id())->get();

    //     $interest_array = [];

    //     foreach($mydeposits as $mydeposit) {
    //         $interest_array = array_add($interest_array, $mydeposit->id, $mydeposit->interest->pluck('amount')->sum());
    //     }
        
    //     list($deposits, $interests) = array_divide($interest_array);

    //     $total_interest = array_sum($interests);

    //     $newdeposits = $mydeposits->where('status', '=', 'new')->sum('amount');
    //     $matureddeposits = $mydeposits->where('status', '=' ,'matured')->sum('amount');
    //     $releaseddeposits = $mydeposits->where('status', '=' ,'released')->sum('amount');
    //     $archiveddeposits = $mydeposits->where('status', '=', 'archived')->sum('amount');
    //     //dd($archiveddeposits);
    //     $closeddeposits = $matureddeposits + $releaseddeposits + $archiveddeposits;
    //     $lifetimedeposits = $mydeposits->whereNotIn('status', ['new', 'rejected', 'problem'])->sum('amount');
    //     $activedeposits = $mydeposits->where('status', '=', 'active')->sum('amount'); 

    //     $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
    //     $operativeAccountId = $myOpertiveAccount->id;
    //     $myOpertiveAccountNo = $myOpertiveAccount->account_no;

    //     $allUserAccount = $user->useraccounts->whereIn('accounttype_id', [3, 5])->pluck('id')->toarray();
      
    //     // $creditTransactions  = Transaction::whereIn(
    //     //                 'account_id' ,$allUserAccount)
    //     //                 ->where('type', '=', 'credit')->sum('amount');
                         
    //     // $debitTransactions  = Transaction::whereIn(
    //     //                 'account_id' ,$allUserAccount)
    //     //                 ->where('type', '=', 'debit')->sum('amount');

    //     // $balance = $creditTransactions - $debitTransactions;

    //     $balance = $this->getBalance($user);

    //     $referrals = User::where('id', Auth::id() )->with('referrals')->get();
    //     $referralsCount = $referrals[0]->referrals->count();

    //     $depositsByRefferals = [];
    //     $myrefferals = $referrals[0]->referrals->pluck('user_id');

    //     foreach ($myrefferals as $refferal) {
    //             $userLifeTimeDeposit = Deposit::where([
    //                                                             ['status', '!=', 'new'],
    //                                                             ['user_id', '=', $refferal]
    //                                                             ])->sum('amount');
    //             $depositsByRefferals = array_add($depositsByRefferals, $refferal, $userLifeTimeDeposit);
    //     }

    //      list($users, $deposits) = array_divide($depositsByRefferals);
    //      $totalInterestByReferrals = array_sum($deposits);

    //      $latestnews = News::orderBy('id', 'desc')->first();
    //     // dd($latestnews);
      
    //     session(['balance' => $balance]);
    //     session(['mydeposits' => $mydeposits]);
    //     session(['activedeposits' => $activedeposits]);
    //     session(['newdeposits' => $newdeposits]);
    //     session(['closeddeposits' => $closeddeposits]);
    //     session(['lifetimedeposits' => $lifetimedeposits]);
    //     session(['account_id' => $operativeAccountId]);
    //     session(['account_no' => $myOpertiveAccountNo]);
    //     session(['sponsor' => $sponsor ]);
    //     session(['hasProfile' => $hasProfile]);
    //     session(['completedProfile' => $completedProfile]);
    //     session(['totalinterest' => $total_interest]);
    //     session(['referralsCount' => $referralsCount]);
    //     session(['totalInterestByReferrals' => $totalInterestByReferrals]);
    //     session(['totalReffealCommission' => $this->totalReferralCommission($user)]);
    //     session(['totalLevelCommissions' => $this->totalLevelCommission($user)]);
    //     session(['totalBonus' => $this->totalBonus($user)]);
    //     session(['totalLifetimeEarnings' => $this->totalLifeTimeEarnings($user)]);

    //     $testimonialval = 0;
    //       if ($checktestimonial)
    //       {
    //         $testimonialval = 1;
    //       }
    //     session(['checktestimonial' => $testimonialval]);
        
    //     session(['usergroupid' => $user->userprofile->usergroup_id]);
    //     if(isset($user->userprofile->profile_avatar))
    //     {
    //         session(['profileimage' => $user->userprofile->profile_avatar]);
    //     }        
    //     return view('home.dashboard', [
    //                 'mydeposits' => $mydeposits,
    //                 'activedeposits' => $activedeposits,
    //                 'newdeposits' => $newdeposits,
    //                 'matureddeposits' => $matureddeposits,
    //                 'lifetimedeposits' => $lifetimedeposits,
    //                 'kycverified' => $user->userprofile->kyc_verified,
    //                 'emailverified' => $user->userprofile->email_verified,
    //                 'mobilestatus' => $user->userprofile->mobile_verified,
    //                 'mobilecode' => $user->userprofile->mobile_verification_code,
    //                 'latestnews' => $latestnews,
    //                 ]);
    // } 

    public function index()
    {
        $user = User::where('id', Auth::id())->first();
        // dd($user->totalInterest);
        if ($user->IsActive == 0)
        {
            Auth::logout();
            Session::flash('error', trans('myaccount.account_suspended_message'));
            return Redirect::back();
        }
        $checktestimonial = Testimonial::where('user_id', Auth::id())->exists();
        $hasProfile = $this->hasProfile($user);
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
      //  $operativeAccountId = $myOpertiveAccount->id;
        $myOpertiveAccountNo = $myOpertiveAccount->account_no;
      
        session(['account_no' => $myOpertiveAccountNo]);
        session(['hasProfile' => $hasProfile]);
       
        $testimonialval = 0;
        if ($checktestimonial)
        {
            $testimonialval = 1;
        }
        session(['checktestimonial' => $testimonialval]);
        
        session(['usergroupid' => $user->userprofile->usergroup_id]);
        if(isset($user->userprofile->profile_avatar))
        {
            session(['profileimage' => $user->userprofile->profile_avatar]);
        }    
        $ewalletbalance = $this->ewalletBalance(Auth::id()); 

        $btcbalance = 0;
        $user_accounts = Userpayaccounts::where('user_id', Auth::id())->first();
        if(count($user_accounts)>0)
        {
            if($user_accounts->btc_address!='')
            {
                $btcbalance = $this->getWalletBalance($user_accounts->btc_address);
            }
        } 
        return view('home.dashboard', [
                'ewalletbalance' => $ewalletbalance,
                'user' => $user,
                'btcbalance' => $btcbalance,
        ]);
    }  
    
}
