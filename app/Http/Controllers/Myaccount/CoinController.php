<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Userpayaccounts;
use App\Http\Requests\BitcoinSendRequest;
use App\Classes\BlockIo;
use Exception;
use App\Traits\EwalletProcess;
use App\Paymentgateway;
use App\User;
use App\Traits\PayaccountsProcess;

class CoinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use EwalletProcess, PayaccountsProcess;

    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function send_create()
    {       
       return view ('coin.btc_send');
    }

    public function receive_create()
    {
        $pgId = Paymentgateway::find(6);    // for bitcoin blockio
        $pg = $this->getPgDetailsByGatewayName($pgId->gatewayname);

        $user_accounts=Userpayaccounts::where([['user_id', Auth::id()],['paymentgateways_id', $pg->id]])->first();
        $btc_address='';
       
       if(count($user_accounts)>0)
        {
          $btc_address=$user_accounts->btc_address;
        }

       return view ('coin._bitcoin_receive', [
               'btc_address'=>$btc_address,
      ]);
    }

    public function send_store(BitcoinSendRequest $request)
    {
        $amount=sprintf("%.8f", $request->amount);
        try
        {            
            $pgId = Paymentgateway::find(6);    // for bitcoin blockio
		    $pg = $this->getPgDetailsByGatewayName($pgId->gatewayname);

		    $user_accounts=Userpayaccounts::where([['user_id', Auth::id()],['paymentgateways_id', $pg->id]])->first();
            $btc_address=$user_accounts->btc_address;
         
            $params = json_decode($pg->params, true);
            $api_key= $params['api_key'];
            $pin= $params['pin'];   
             
            $version = $params['version']; // API version
            $block_io = new BlockIo( $api_key, $pin, $version);

            $response=$block_io->withdraw_from_addresses(array('amounts' => $amount, 'from_addresses' =>$btc_address,'to_addresses' =>$request->btc_address));
            
            \Session::put('successmessage','Coin Send Successfully');
        }
        catch (Exception $e) {         
            // if an exception happened in the try block above 
            \Session::put('failmessage',$e->getMessage());
        }         
        return Redirect::to(url('/myaccount/type/btc/send'));
    }

    public function create_btcwallet()
    {       
        $user = User::where('id', Auth::id())->with('userprofile')->first();
        $pg = Paymentgateway::find(6);

        // $user_accounts=Userpayaccounts::getAccountDetails(Auth::id(),$pg->id)->first();
        // if(count($user_accounts)==0)
        // {
            $address = $this->createWalletAddress($user);
        // }
        // else
        // {
        //     return $user_accounts->btc_address;
        // }
        $request = [
            'label' => $user->name,
            'btc_address' => $address,
            'paymentid' => $pg->id,            
        ];
        $request = (object) $request;

        $this->bitcoinWallet($request, $user->id, 1, 1);
        return $address;
    }

    public function createWalletAddress($user)
    {
        $pgId = Paymentgateway::find(6);    // for bitcoin blockio
        $pg = $this->getPgDetailsByGatewayName($pgId->gatewayname);
        $params = json_decode($pg->params, true);    
        $address='';
        try
        {
            $api_key= $params['api_key'];
            $pin= $params['pin'];
            $version = $params['version']; // API version
            $block_io = new BlockIo( $api_key, $pin, $version);
            $walletaddress = $block_io->get_new_address(array('label' => $user->name));
            $address = $walletaddress->data->address;
            // dd($block_io);
        }
        /*catch(Exception $e)
        {
          dd($e->getMessage());
        }*/
        finally
        {
            return $address;
        }
    }

    public function getPgDetailsByGatewayName($payment_name) 
    {
        $pg = Paymentgateway::where('gatewayname', $payment_name)->first();              
        return $pg;
    }

}
