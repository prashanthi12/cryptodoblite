<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    public function markasread(Request $request)
    {
        
        $user = User::where('id', Auth::id())->first();
        $notification = $user->notifications()->where('id', $request->notificationid)->first();
        if ($notification)
        {
            $notification->update(['read_at' => Carbon::now()]);
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.notification_error_message'));
        }

        if ((strpos($notification->type, 'KycVerify') !== false) || (strpos($notification->type, 'KycRejected') !== false))
        {
            return Redirect::to( url('/myaccount/viewprofile')); 
        }        
        elseif ((strpos($notification->type, 'DepositApprove') !== false)) 
        {
           return Redirect::to( url('/myaccount/viewdeposits/active')); 
        } 
        elseif ((strpos($notification->type, 'DepositReject') !== false)) 
        {
           return Redirect::to( url('/myaccount/viewdeposits/rejected')); 
        }        
        elseif (strpos($notification->type, 'WithdrawComplete') !== false) 
        {
            return Redirect::to( url('/myaccount/withdraw/completed')); 
        }
        elseif (strpos($notification->type, 'WithdrawCancelled') !== false) 
        {
            return Redirect::to( url('/myaccount/withdraw/rejected')); 
        }
        elseif ((strpos($notification->type, 'MatureDeposit') !== false)) 
        {
           return Redirect::to( url('/myaccount/viewdeposits/matured')); 
        } 
        elseif ((strpos($notification->type, 'DepositRelease') !== false)) 
        {
           return Redirect::to( url('/myaccount/viewdeposits/released')); 
        } 
        elseif ((strpos($notification->type, 'FundTransferReceived') !== false)) 
        {
           return Redirect::to( url('/myaccount/fundtransfer/type/received')); 
        } 
        elseif ((strpos($notification->type, 'BitcoinDepositActive') !== false)) 
        {
           return Redirect::to( url('/myaccount/viewdeposits/active')); 
        } 
        elseif (strpos($notification->type, 'AutoWithdrawalSend') !== false) 
        {
            return Redirect::to( url('/myaccount/withdraw/pending')); 
        }
    }
}
