<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Crypt;
use Google2FA;
use Cache;
use \ParagonIE\ConstantTime\Base32;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests\ValidateSecretRequest;
use Session;

class Google2FAController extends Controller
{
	use ValidatesRequests;

    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    public function twofactor()
    {
        return view('fund.twofactorsettings');
    }

    public function enableTwoFactor(Request $request)
    {
        //generate new secret
        $secret = Google2FA::generateSecretKey();
       // dd($secret);
        //get user
        $user = $request->user();
        
        //encrypt and then save secret
        $user->google2fa_secret = Crypt::encrypt($secret);
        $user->save();
      	//dd($secret);
        //generate image for QR barcode
        $imageDataUri = Google2FA::getQRCodeInline(
            $request->getHttpHost(),
            $user->email,
            $secret
        );

        return view('fund.enableTwoFactor', [
            'image' => $imageDataUri,
            'secret' => $secret
        ]);
    }

    public function disableTwoFactor(Request $request)
    {
        $user = $request->user();

        //make secret column blank
        $user->google2fa_secret = null;
        $user->save();

        return view('fund.disableTwoFactor');
    }

    private function generateSecret()
    {
        $randomBytes = random_bytes(10);
        
        return Base32::encodeUpper($randomBytes); 
    }

    // private function authenticated(Request $request, Authenticatable $user)
    // {
    //     if ($user->google2fa_secret) {
    //         Auth::logout();

    //         $request->session()->put('2fa:user:id', $user->id);

    //         return redirect('2fa/validate');
    //     }

    //     return redirect()->intended($this->redirectTo);
    // }

    public function getValidateToken()
    {
    	//dd(session('2fa:user:id'));
        // if (\Session::get('2fa')) 
        // {
        	//dd('if');
            return view('fund.validate');
       // }
      //  dd('dfghuif');
      //  return redirect('login');
    }

    public function postValidateToken(Request $request)     // ValidateSecretRequest $request
    {
    	//dd($request);
        //get user id and create cache key
        $userId = \Session::get('2fa');
        //dd($userId);
        $key    = $userId . ':' . $request->totp;
        // dd($key);
        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        Auth::loginUsingId($userId);
       // dd('rertert');
        return redirect(url('/myaccount/fundtransfer/type/send'));
      //  return redirect()->intended($this->redirectTo);
    }

}
