<?php

namespace App\Http\Controllers\Myaccount;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Fundtransfer;
use App\Useraccount;
use App\Traits\UserInfo;
use App\Http\Requests\FundRequest;
use App\Traits\FundTransferProcess;
use Illuminate\Support\Facades\Redirect;
use App\Notifications\User\FundTransferReceived;
use Config;
use App\Events\FundTransferMail;
// use App\Events\FundTransferSend;
use Event;
use Cache;

class FundTransferController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'member']);
    }

    use UserInfo, FundTransferProcess;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $fundtransfer = Fundtransfer::latest('updated_at')->with('fundtransfer_to_id', 'fundtransfer_from_id');

        $fromaccount_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $fromaccount_id[0]['id'];

        if ($type == 'send')
        {
            $transferlists = $fundtransfer->where('from_account_id', $account_id)->paginate(Config::get('settings.pagecount'));
        }
        elseif ($type == 'received')
        {
            $transferlists = $fundtransfer->where('to_account_id', $account_id)->paginate(Config::get('settings.pagecount'));
        }
        else
        {
            abort(404);
        }

        return view('fund.show', [
                'type' => $type,
                'transferlists' => $transferlists,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::id())->first();

      //  $userbalance = $this->getUserBalance($user);

        // $isKycApproved = $this->isKycApproved($user);
        // $isEmailVerified = $this->isEmailVerified($user);        

        return view('fund.send',[
               // 'userbalance' => $userbalance,
                'user' => $user,
                // 'isKycApproved' => $isKycApproved,
                // 'isEmailVerified' => $isEmailVerified,
                'force_email_verification_for_fund_transfer'  => \Config::get('settings.force_email_verification_for_fund_transfer'),
                'force_kyc_verification_for_fund_transfer'  => \Config::get('settings.force_kyc_verification_for_fund_transfer'),
               // 'kyc_doc' => $user->userprofile->kyc_doc,
                'transaction_password' => $user->userprofile->transaction_password
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FundRequest $request)
    {
        $sender = User::where('id', Auth::id())->first();
        // get user id and create cache key
        $userId = $sender->id;
        $key    = $userId . ':' . $request->totp;

        // use cache to store token to blacklist
        Cache::add($key, true, 4);

        $result = $this->sendFundTransfer($request);
        if ($result)
        {        
            $user = User::where('name', $request->sendto)->first();

            $user->notify(new FundTransferReceived);

            //firing an event
            Event::fire(new FundTransferMail($result, $sender, $user));
            // Event::fire(new FundTransferSend($result, $sender, $user));

            // Mail::to($user->email)->queue(new FundTransferReceiver($result)); 
            // Mail::to($sender->email)->queue(new FundTransferSender($result));

            $request->session()->flash('successmessage', trans('forms.fund_transfer_success_message'));
        }
        else
        {
            $request->session()->flash('failmessage', trans('forms.fund_transfer_error_message'));
        }
        return Redirect::to( url('/myaccount/fundtransfer/type/send'));  

    }

    public function searchuser(Request $request) 
    {
        $query = $request->get('term','');
        
        $users=User::where('name','LIKE','%'.$query.'%')->get();
        
        $data=array();

        foreach ($users as $users) 
        {
                $data[]=array('value'=>$users->name,'id'=>$users->id);
        }
        if(count($data))
        {
            return $data;
        }
        else
        {
            return ['value'=>'No Result Found','id'=>''];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
