<?php

namespace App\Http\Controllers\Myaccount;

use App\Classes\CoinPayments;
use App\Classes\BlockIo;
use App\Classes\advCash\MerchantWebService;
use App\Http\Requests\DepositRequest;
use App\User;
use App\Plan;
use App\Paymentgateway;
use App\Useraccount;
use App\Deposit;
use App\Accountingcode;
use Carbon\Carbon;
use URL;
use Session;
use Illuminate\Support\Facades\Input;
use App\Traits\DepositProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Traits\LogActivity;
//use  App\Notifications\DepositSuccessfull;
//use  App\Notifications\AdminNotifyNewDeposit;
use App\Helpers\HyipHelper;
use App\Withdraw;
use Config;
use Validator;
use App\Traits\UserInfo;
use App\Http\Requests\PartialWithdrawRequest;
use PDF;
use App\Epin;
use App\Http\Requests\CouponCodeRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\EpinCouponCodeUsed;
use App\Mail\EpinCouponCodeOwner;
use App\Models\Mailtemplate;

/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class DepositController extends Controller
{
    use DepositProcess, LogActivity, UserInfo;

    private $_api_context;

    public function __construct()
    {
        $this->middleware(['auth', 'member']);

        $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['deposit', '=', '1'],
                ['id', '=', '2']
            ])->first(); 

        /** setup PayPal api context **/
        /** It's important for old paypal as reference **/
        // if (!is_null($pgs))
        // {      
        //    $paypal_config = json_decode($pgs->params, true);
        //    $paypal_conf = Config::get('paypal');
        //    $mode = array('mode' => $paypal_config['mode']);
        //    $paypal_conf['settings'] = array_merge( $paypal_conf['settings'], $mode);
        //    //dd($paypal_conf['settings']);

        //     $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_config['client_id'], $paypal_config['secret']));

        //     $this->_api_context->setConfig($paypal_conf['settings']);
        // }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status)
    {      
        if (in_array( $status, array( 'new', 'active', 'matured', 'released', 'rejected', 'archived', 'problem')) == FALSE)
        {
            abort(404);
        }

        $user = User::where('id', Auth::id())->first();
        $mydeposits = Deposit::where([
                        ['user_id', Auth::id()],
                        ['status', $status]
                        ])->with(['plan', 'interest', 'paymentgateway', 'transaction'])->latest('id')->paginate(Config::get('settings.pagecount'));
        return view('home.viewdeposits', [
            'mydeposits' => $mydeposits,
            'status' => $status,
            'user' => $user,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $reinvest = Input::get('reinvest');
        // if ($reinvest != 1)
        // {
        //     abort(404);   
        // }
        //dd($reinvest);
        $user = User::where('id', Auth::id())->with(['userprofile','useraccounts','deposits'])->first();

        $sponsor='';
        if($user->userprofile->sponsor_id!='')
        {
            $sponsor = User::withTrashed()->where('id', $user->userprofile->sponsor_id)->first()->name;
        //dd($sponsor);     
        }   
        
        $plans = Plan::where([
                ['active', '=', "1"],
                ['publish', '=', '1']
            ])->orderBy('orderby','ASC')->get();

        $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['deposit', '=', '1'],
                ['id', '!=', '10']
            ])->get();

        $isKycApproved = $this->isKycApproved($user);
        $isEmailVerified = $this->isEmailVerified($user);

        // $todayTotalDeposit = Deposit::whereRaw('Date(created_at) = CURDATE()')->sum('amount');
        // dd($todayTotalDeposit);

        return view ('deposit.create', [
                'sponsor' => $sponsor,
                'plans' => $plans,
                'pgs' => $pgs,
                'reinvest' => $reinvest,
                'force_deposit_down'  => \Config::get('settings.force_deposit_down'),
                'isKycApproved' => $isKycApproved,
                'isEmailVerified' => $isEmailVerified,
                'force_email_verification_for_deposit'  => \Config::get('settings.force_email_verification_for_deposit'),
                'force_kyc_verification_for_deposit'  => \Config::get('settings.force_kyc_verification_for_deposit'),
                'kyc_doc' => $user->userprofile->kyc_doc,
                'user' => $user,
                // 'todayTotalDeposit' => $todayTotalDeposit,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepositRequest $request)
    {
        $paymentgateway = $request->paymentgateway;
        $plan = $request->plan;
        $amount = $request->amount;     
        $pgInfo = $this->getPgInfo( $request->paymentgateway );
        $params = $pgInfo[0];
      // dd($request);
        $instructions = $pgInfo[1];       
        $user = User::where('id', Auth::id())->first();
        $plandetail = Plan::where('id', $request->plan)->first();
        $pgs = Paymentgateway::where('id', $paymentgateway)->first();
        //dd($pgs);           
        \Session::put('plan', $request->plan);
        \Session::put('amount', $request->amount);
        \Session::put('paymentgateway', $request->paymentgateway);
        \Session::put('depositfeestatus', $pgs->deposit_fee_status);
        \Session::put('depositfeetype', $pgs->deposit_fee_type);
        \Session::put('depositfee', $pgs->deposit_fee_value); 
        \Session::put('chequeno', $request->cheque_no);
        \Session::put('payeername', $request->payeer_name);

        if (Session::get('depositfeestatus') == 1)
        {
            if (Session::get('depositfeetype') == 1) // Flat
            {
                $amount = Session::get('depositfee') + Session::get('amount');
            }
            elseif (Session::get('depositfeetype') == 2) //Percentage
            {                   

                $feeamount = Session::get('amount') * Session::get('depositfee') / 100;
                
                $amount = Session::get('amount') + $feeamount;
            }
        }       

        if( $request->paymentgateway == 1 )
        {               
                $tranaction_uuid = uniqid();
                \Session::put('transactionnumber', $tranaction_uuid);
                \Session::put('tobedeposited', $amount);
                return view('deposit._bankwire_form', [
                        'plan' => $plan,
                        'paymentgateway' => $paymentgateway,
                        'amount' => $amount,
                        'params' => $params,
                        'request'  => $request,
                        'transaction_id' => $tranaction_uuid,
                        'instructions' => $instructions,
                        'depositfeestatus' => Session::get('depositfeestatus'),
                        'depositfeetype' => Session::get('depositfeetype'),
                        'depositfee' => Session::get('depositfee')                        
                    ]);
        }

        if( $request->paymentgateway == 2 ) 
        {
            return view('deposit._paypal_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')    
                ]);
        }

        if( $request->paymentgateway == 3 )
        {
            return view('deposit._stp_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'planname' => $plandetail->name,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 4 )
        {
            return view('deposit._payeer_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 5 )
        {
            $transaction_uuid = uniqid();
            $acsign[] = $params['ac_account_email'];
            $acsign[] = $params['ac_sci_name'];
            $acsign[] = $amount;
            $acsign[] = \ Config::get('settings.currency');
            $acsign[] = $params['ac_secret'];
            $acsign[] = $transaction_uuid;

            \Session::put('transaction_id', $transaction_uuid);

            // Forming a signature
            $ac_sign = hash('sha256', implode(':', $acsign));
            $input['ac_sign'] = $ac_sign;
            return view('deposit._advcash_form', [
                'input' => $input,
                'transaction_id' => $transaction_uuid,
                'instructions' => $instructions,
                'params' => $params,
                'paymentgateway' => $paymentgateway,
                'plan' => $plan,
                'amount' => $amount,
                'ac_sign' => $ac_sign,
                'depositfeestatus' => Session::get('depositfeestatus'),
                'depositfeetype' => Session::get('depositfeetype'),
                'depositfee' => Session::get('depositfee')
            ]);
        }

        if( $request->paymentgateway == 6 )
        {
            return view('deposit._bitcoin_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 7 )
        {
            return view('deposit._bitcoin_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 8 )
        {
            return view('deposit._bitcoin_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 9 )
        {           
           return Redirect::to( url('/myaccount/deposit/bitcoindirect'));         
        }
        
        if( $request->paymentgateway == 11 )
        {
            return view('deposit._skrill_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'planname' => $plandetail->name,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 12 )
        {
            return view('deposit._okpay_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 13 )
        {
            $tranaction_uuid = uniqid();
            return view('deposit._reinvest_deposit', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'transaction_id' => $tranaction_uuid,
                    'instructions' => $instructions,
                    'pgs' => $pgs
                ]);
        }

        if( $request->paymentgateway == 15 )
        {            
            $payment_id = uniqid();           
            return view('deposit._perfectmoney_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'payment_id' => $payment_id,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')
                ]);
        }

        if( $request->paymentgateway == 16 )
        {
            $transaction_number = uniqid();
            return view('deposit._neteller_form', [
                    'paymentgateway' => $paymentgateway,
                    'plan' => $plan,
                    'amount' => $amount,
                    'params' => $params,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee'),
                    'transaction_number' => $transaction_number,
                    'plandetail' => $plandetail,
                    'username' => $user->name   
            ]);
        }

        if( $request->paymentgateway == 17 )    // For cheque
        {               
            $tranaction_uuid = uniqid();
            \Session::put('transactionnumber', $tranaction_uuid);
            \Session::put('tobedeposited', $amount);
            $chequeno = $request->cheque_no;
            $payeername = $request->payeer_name;
            return view('deposit._cheque_form', [
                    'plan' => $plan,
                    'paymentgateway' => $paymentgateway,
                    'amount' => $amount,
                    'params' => $params,
                    'request'  => $request,
                    'transaction_id' => $tranaction_uuid,
                    'instructions' => $instructions,
                    'chequeno' => $chequeno,
                    'payeername' => $payeername,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => Session::get('depositfee')                        
            ]);
        }

        if($request->paymentgateway == 18)  // For E - pin
        {   
            return redirect(url('/myaccount/epin/couponcode/check'));
        }

        if( $request->paymentgateway == 19 )    // For E-Wallet
        {            
            $tranaction_uuid = uniqid();          
            return view('deposit._ewallet_deposit_form', [
                    'paymentgateway' => $paymentgateway,
                    'username' => $user->name,
                    'plan' => $plan,
                    'amount' => $amount,
                    'transaction_id' => $tranaction_uuid,
                    'instructions' => $instructions,               
            ]);
        }

    }

    public function processpaypal(Request $request)
    {     
        $plan = Plan::where('id', $request->plan)->first();
        //dd($plan);
        $payer = new Payer();

        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName($plan->name) /** item name **/

            ->setCurrency(Config::get('settings.currency'))

            ->setQuantity(1)

            ->setPrice($request->get('amount')); /** unit price **/

        $item_list = new ItemList();

        $item_list->setItems(array($item_1));

        $amount = new Amount();

        $amount->setCurrency(Config::get('settings.currency'))

            ->setTotal($request->get('amount'));

        $transaction = new Transaction();

        $transaction->setAmount($amount)

            ->setItemList($item_list)

            ->setDescription('Deposited for '.$plan->name.' plan');

        $redirect_urls = new RedirectUrls();

        $redirect_urls->setReturnUrl(url('myaccount/deposit/paypalstatus')) /** Specify return URL **/

            ->setCancelUrl(url('myaccount/deposit'));

        $payment = new Payment();

        $payment->setIntent('Sale')

            ->setPayer($payer)

            ->setRedirectUrls($redirect_urls)

            ->setTransactions(array($transaction));

            /** dd($payment->create($this->_api_context));exit; **/

        try {

            \Session::put('plan', $plan->id);
            \Session::put('amount', $request->get('amount'));
            \Session::put('paymentgateway', 2);

            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {

                \Session::put('error','Connection timeout');

                return redirect(url('myaccount/deposit'));

                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/

                /** $err_data = json_decode($ex->getData(), true); **/

                /** exit; **/

            } else {

                \Session::put('error','Some error occur, sorry for inconvenient');

                return redirect(url('myaccount/deposit'));

                /** die('Some error occur, sorry for inconvenient'); **/

            }

        }
//dd($payment->getId());
        foreach($payment->getLinks() as $link) {

            if($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();

                break;

            }

        }

        /** add payment ID to session **/

        Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)) {

            /** redirect to paypal **/

            return Redirect::away($redirect_url);

        }

        \Session::put('error','Unknown error occurred');

        return redirect(url('myaccount/deposit'));
    }

    public function getPaypalStatus(Request $request)
    {
        /** Get the payment ID before session clear **/

        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/

        Session::forget('paypal_payment_id');
        //dd(Input::get());

        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error','Payment failed');

           return redirect(url('myaccount/home'));

        }

        $payment = Payment::get($payment_id, $this->_api_context);

        /** PaymentExecution object includes information necessary **/

        /** to execute a PayPal account payment. **/

        /** The payer_id is added to the request query parameters **/

        /** when the user is redirected from paypal back to your site **/

        $execution = new PaymentExecution();

        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/

        $result = $payment->execute($execution, $this->_api_context);

        /** dd($result);exit; /** DEBUG RESULT, remove it later **/

        if ($result->getState() == 'approved') {

            $array = array();

            $array = [
                'amount' => session('amount'),
                'plan' => session('plan'),
                'paymentgateway' => session('paymentgateway')
            ];
            $request = (object) $array;
            $deposit = $this->makeDeposit($request);
            $depositapprove = $this->approve($deposit->id);

            /** it's all right **/

            /** Here Write your database logic like that insert record or value in database if you want **/

            \Session::put('paypalsuccess','Payment success');

            return redirect(url('myaccount/home'));

        }

        \Session::put('error','Payment failed');

        return redirect(url('myaccount/home'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }    


    public function payment(Request $request)
    {
         $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['deposit', '=', '1'],
                ['id', '=', $request->paymentid]
            ])->get(); 


         return view ('deposit.paymentdetails', [
                'payments' => $pgs,
                'plan' => $request->plan,
                'amount' => $request->amount
            ]);      
    }
  
    public function view_plan_amount(Request $request)
    {
        $plan = Plan::where('id', '=', $request->planid)->first();  
        return 'Min : '.$plan->min_amount.' '.config::get('settings.currency').' Max : '.$plan->max_amount.' '.config::get('settings.currency');         
    }

    public function bitcoin(Request $request)
    {
        $user = Auth::user();
        //dd($request); exit;

        if ( $request->payment == 6 )     //Block.io
        {
            
            \Session::put('plan', Session::get('plan'));
            \Session::put('amount', Session::get('amount'));
            $pgInfo = $this->getPgInfo('6');
            $params = $pgInfo[0];
            $instructions = $pgInfo[1];       
            $user = User::where('id', Auth::id())->first();
            $plandetail = Plan::where('id', $request->plan)->first();     

            $version = 2; // API version
            $block_io = new BlockIo( $request->api_key, $request->pin, $version);
            $newAddressInfo = $block_io->get_new_address();
            $btcamount = '';
            $tranaction_uuid = '';
                       
            if (is_array($newAddressInfo))
            {
                
               $blockioapistatus = $newAddressInfo->status;
               $newAddressInfo = $newAddressInfo->data->address;           
               $tranaction_uuid = uniqid();
               $depositfee = HyipHelper::convertBtcAmount(Session::get('depositfee'));    
               $btcamount = HyipHelper::convertBtcAmount(Session::get('amount'));
                if (Session::get('depositfeestatus') == 1)
                {
                    if (Session::get('depositfeetype') == 1) // Flat
                    {
                        $totalamount = Session::get('depositfee') + Session::get('amount');
                        //dd($totalamount);
                        $btcamount = HyipHelper::convertBtcAmount($totalamount);
                    }
                    elseif (Session::get('depositfeetype') == 2) //Percentage
                    {
                        $feeamount = Session::get('amount') * Session::get('depositfee') / 100;                        
                        $totalamount = Session::get('amount') + $feeamount;
                        //dd($totalamount);
                        $btcamount = HyipHelper::convertBtcAmount($totalamount);
                    }
                } 
            }
            else
            {
                $blockioapistatus = $newAddressInfo;
            }
            
            //dd($newAddressInfo);
            //https://chain.so/api/v2/get_address_balance/BTCTEST/2N722hZsvLSjxnCwPhEi9A1fHxPnGpv2dCc
            return view ('deposit.bitcoin_block_io',
            [
                'block_io' => $newAddressInfo,
                'instructions' =>  $instructions,
                'btcamount' => $btcamount,
                'transaction_id' =>  $tranaction_uuid,
                'blockioapistatus' => $blockioapistatus,
                'depositfeestatus' => Session::get('depositfeestatus'),
                'depositfeetype' => Session::get('depositfeetype'),
                'depositfee' => Session::get('depositfee')
            ]);

        }
        
        if ( $request->payment == 7 )     //Coinpayment
        {
            $user = User::where('id', Auth::id())->first();

            $plan = Plan::where('id', Session::get('plan'))->first();

            $tranaction_uuid = uniqid(); 
            \Session::put('uuid', $tranaction_uuid);     
            $ipn_url = url('/deposit/coinpaymentsuccess?uuid='.$tranaction_uuid);
            $cps = new CoinPayments();
            $cps->Setup( $request->coin_private_key, $request->coin_public_key);

            $result = $cps->CreateTransaction($request->price, \Config::get('settings.currency'), $request->currency_bitcoin, $address='', $user->email,$user->name, $plan->name, $plan->id, $invoice='', $custom='', $ipn_url);
            // dd($result);
            if ($result['error'] == "ok" ) {
            $array = [
                'amount' => session('amount'),
                'plan' => session('plan'),
                'paymentgateway' => session('paymentgateway'),
                'coinpaymentrequest' => $result,
                'uuid' => $tranaction_uuid,
                'txn_id' => $result['result']['txn_id'],
                'address' => $result['result']['address'],
                'coinamount' => $result['result']['amount'],
                'user_id' => Auth::user()->id,
            ];
            $getrequest = (object) $array;

            $deposit = $this->makeDeposit($getrequest);

            \Session::put('depositid', $deposit->id);


            //$coininvoiceurl = $result['result']['status_url'];

            //'bitcoin' => $coininvoiceurl;

            } 

            return view ('deposit.bitcoin_coin_payment', [
                'bitcoin' => $result,
                ]);

        }
        if ( $request->payment == 8 )     // Bitpay
        {
            require_once app_path().'/Http/library/bitcoin/bp_lib.php';       
            $randomNo = sprintf("%06d", mt_rand(100000, 999999));       
            $price = $request->price;
            // Added options, create session added requests and make deposit
            $options['redirectURL'] = url('myaccount/deposit/bitpaysuccess');
            $options['transactionSpeed'] = 'low';
            $bitcoin = bpCreateInvoice($randomNo, $price, 'Member Upgrade');
            \Session::put('deposit_bitcoin_invoiceid',$bitcoin['id']);

            $request->merge(['amount' => Session::get('amount')]);
            $request->merge(['plan' => Session::get('plan')]);
            $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
            $request->merge(['transaction_id' => $request->id]);
            $request->merge(['guid' => $request->guid]);
            $request->merge(['token' => $request->token]);

            $deposit = $this->makeDeposit($request);

            return view ('deposit.bitcoin_bitpay', ['bitcoin' => $bitcoin]);
        }       
    }

    public function saveblockio(Request $request)
    {
        //dd($request);
        $result = $this->makeDeposit($request);

        if($result)
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    }
  
    public function advcash_success(Request $request)
    {
        $acc_email_id = $request->acc_email_id;
        $api_name = $request->api_name;
        $api_pwd = $request->api_password;
        $amount = $request->amount;

        $advcash_accemail = $request->admin;   //todo

        require_once app_path().'\Classes\AdvcashCheckout.php';   
        $sendMoneyResponse = new sendMoneyResponse();
        $result=$sendMoneyResponse->return; 
        
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['plan' => Session::get('plan')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
        
        $trans_id=array('id'=>$result);
        $request->merge(['transaction_id' => $trans_id]);

        if( $request->amount != '' && $trans_id != '')
        {
            $deposit = $this->makeDeposit($request);
            $depositapprove = $this->approve($deposit->id);
            if ($deposit && $depositapprove)
            {
                $request->session()->flash('status', trans('forms.deposit_success_msg'));
            }
            else
            {
                $request->session()->flash('error', trans('forms.deposit_error_msg'));
            }          
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    }

    public function processPayment( $request )
    {      
        $deposit_result = $this->makeDeposit($request);
        return $deposit_result;      
    }

    public function bankwire(Request $request )
    {
        $admin = User::find(1);
        $user = Auth::user();        
        $deposit_result = $this->makeDeposit($request);       
       // $user->notify(new DepositSuccessfull($deposit_result));
       // $admin->notify(new AdminNotifyNewDeposit($deposit_result));

        if($deposit_result)
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    } 

    public function bitcoindirect(Request $request)
    {  
           \Session::put('plan', Session::get('plan'));
            \Session::put('amount', Session::get('amount'));
            $pgInfo = $this->getPgInfo('9');
            $params = $pgInfo[0];
            $instructions = $pgInfo[1];       
            $user = User::where('id', Auth::id())->first();
            $plandetail = Plan::where('id', $request->plan)->first();

            $actualamount = HyipHelper::convertBtcAmount(Session::get('amount'));     

            $btcamount = HyipHelper::convertBtcAmount(Session::get('amount'));

            $depositfee = '';
            $feeamount = '';

            if (Session::get('depositfeestatus') == 1)
            {
                if (Session::get('depositfeetype') == 1) // Flat
                {
                    $totalamount = Session::get('depositfee') + Session::get('amount');

                    $feeamount = Session::get('depositfee');

                    $depositfee = HyipHelper::convertBtcAmount(Session::get('depositfee'));
                    //dd($totalamount);
                    $btcamount = HyipHelper::convertBtcAmount($totalamount);
                }
                elseif (Session::get('depositfeetype') == 2) //Percentage
                {                   
                    $feeamount = Session::get('amount') * Session::get('depositfee') / 100;

                    $depositfee = HyipHelper::convertBtcAmount($feeamount);

                    // dd($feeamount);
                    
                    $totalamount = Session::get('amount') + $feeamount;
                    // dd($totalamount);
                    $btcamount = HyipHelper::convertBtcAmount($totalamount);

                    // dd($btcamount);
                }
            }

            $tranaction_uuid = uniqid();          
            return view('deposit._bitcoin_directwallet_form', [
                    'paymentgateway' => '9',
                    'username' => $user->name,
                    'plan' => Session::get('plan'),
                    'amount' => Session::get('amount'),
                    'btcamount' => $btcamount,
                    'params' => $params,
                    'transaction_id' => $tranaction_uuid,
                    'instructions' => $instructions,
                    'depositfeestatus' => Session::get('depositfeestatus'),
                    'depositfeetype' => Session::get('depositfeetype'),
                    'depositfee' => $depositfee,
                    'actualamount' => $actualamount,
                    'feeamount' => $feeamount,
            ]);
    } 

    public function savebitcoindirect(Request $request)
    { 
        Validator::extend('checkbitcoinhashkey', function ($attribute, $value, $parameters, $validator) {
                $checktxnhashkey = Deposit::where('param', Input::get('txnhashkey'))->exists();  
                // dd($checktxnhashkey);

                   if ($checktxnhashkey)
                   {
                        return FALSE;
                   }
                   return TRUE;            
        }, trans('forms.hashkey_error'));

        Validator::extend('checkvalidhashkey', function ($attribute, $value, $parameters, $validator) {
                 
            $response_json = HyipHelper::getBitcoinWalletDetails(Input::get('txnhashkey'));
            $response_json = json_decode($response_json, true);
             //dd($response_json);
            if (is_null($response_json))
            {
                return FALSE;
            }
               return TRUE;            
        }, trans('forms.invalid_hashkey'));  
    
        $validator = Validator::make($request->all(), [
                'txnhashkey'      => 'required|checkbitcoinhashkey|checkvalidhashkey',
            ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        //dd($request);
        $admin = User::find(1);
        $user = Auth::user();   
        
        $response_json = HyipHelper::getBitcoinWalletDetails($request->txnhashkey);
        $response_json = json_decode($response_json, true);

        if (!is_null($response_json))
        {
            $deposit_result = $this->makeDeposit($request); 
        } 
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
            return  Redirect::to('myaccount/home');
        }     
       
        if($deposit_result)
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    }
 
    public function reinvest(Request $request )
    {
        $admin = User::find(1);
        $user = Auth::user(); 
        $balance = $user->balance;
        if (\Session::get('amount') <= $balance)
        {       
            $deposit = $this->makeDeposit($request); 
            $depositapprove = $this->approve($deposit->id);      
            if($deposit)
            {
                $request->session()->flash('status', trans('forms.deposit_success_msg'));
            }
            else
            {
                $request->session()->flash('error', trans('forms.deposit_error_msg'));
            }
        }    
        else
        {
            \session::put('failmessage', "Insufficient Balance");
        }
        return  Redirect::to('myaccount/home');      
    }    

    public function skrillprocess(Request $request )
    {
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['plan' => Session::get('plan')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
        //dd($request);
       
        if($request->transaction_id != '' && $request->msid !='')
        {
            $deposit = $this->makeDeposit($request);
            $depositapprove = $this->approve($deposit->id);
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    } 

    public function bitpayprocess(Request $request )
    {      
        if(\Session::get('deposit_bitcoin_invoiceid') != '')
        {
            require_once app_path().'/Http/library/bitcoin/bp_lib.php';
            $bitcoin_invoice = bpGetInvoice(\Session::get('bitcoin_invoiceid'));       
            if(isset($bitcoin_invoice['status']))
            {              
                if(($bitcoin_invoice['status']=='paid')||($bitcoin_invoice['status']=='confirmed')||($bitcoin_invoice['status']=='complete'))
                {
                    $depositapprove = $this->approve($deposit->id);
                    $request->session()->flash('status', trans('forms.deposit_success_msg'));
                }
            }
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/deposit/bitpaysuccess');
    } 

    public function stpprocess(Request $request )
    {
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['plan' => Session::get('plan')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
        $request->merge(['transaction_id' => $request->tr_id]);
        //dd($request);
        $deposit = $this->makeDeposit($request);
        $depositapprove = $this->approve($deposit->id);
        if($request->transaction_id && $request->status == 'COMPLETE')
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    } 

    public function okpayprocess(Request $request)
    {
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['plan' => Session::get('plan')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
        $request->merge(['transaction_id' => $request->ok_txn_id]);
        //dd($request);
        
        if($request->ok_txn_id)
        {
            $deposit = $this->makeDeposit($request);
            $depositapprove = $this->approve($deposit->id);
            if ($deposit && $depositapprove)
            {
                $request->session()->flash('status', trans('forms.deposit_success_msg'));
            }
            else
            {
                $request->session()->flash('error', trans('forms.deposit_error_msg'));
            }          
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    }

    public function perfectmoneyprocess(Request $request)
    {
        $pgInfo = $this->getPgInfo('15');
        $params = $pgInfo[0];
        //dd($params['alternate_passhprase']);
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['plan' => Session::get('plan')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
        $request->merge(['transaction_id' => $request->PAYMENT_ID]);

        $string =
              $request->PAYMENT_ID.':'.$request->PAYEE_ACCOUNT.':'.
              $request->PAYMENT_AMOUNT.':'.$request->PAYMENT_UNITS.':'.
              $request->PAYMENT_BATCH_NUM.':'.
              $request->PAYER_ACCOUNT.':'.strtoupper(md5($params['alternate_passhprase'])).':'.
              $request->TIMESTAMPGMT;

        $hash=strtoupper(md5($string));

        if($hash==$request->V2_HASH){ // processing payment if only hash is valid

            $deposit = $this->makeDeposit($request);
            $depositapprove = $this->approve($deposit->id);
            $request->session()->flash('status', trans('forms.deposit_success_msg'));  
        }
        else
        { 
            $request->session()->flash('error', 'Hash mismatch: '.$request->V2_HASH.' vs. '.$hash);           
        }
        return  Redirect::to('myaccount/home');
    }

    public function processneteller(Request $request)
    {
        $pgInfo = $this->getPgInfo('16');
        $params = $pgInfo[0];
        // dd($params['Paymentmode']);

        if ($params['paymentmode'] == 'test')
        {
            $paymenturl = 'https://test.api.neteller.com/netdirect';
        }
        else
        {
            $paymenturl = 'https://api.neteller.com/netdirect';
        }

        // open curl connection
        $ch = curl_init($paymenturl);

        // get the vars from POST request and add them as POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query([
            'version' => $request->version,
            'amount' => urlencode($request->amount),
            'currency' => $request->currency,
            'net_account' => urlencode($request->net_account),
            'secure_id' => urlencode($request->secure_id),
            'merchant_id' => urlencode($request->merchant_id),
            'merch_key' => urlencode($request->merch_key),
            'merch_transid' => urlencode($request->merch_transid),
            'language_code' => $request->language_code,
            'merch_name' => urlencode($request->merch_name),
            'merch_account' => urlencode($request->merch_account),
            'custom_1' => urlencode($request->custom_1),
            'custom_2' => urlencode($request->custom_1),
            'custom_3' => urlencode($request->custom_1), 
            'button' => 'Make Transfer'
        ]));

        // set other curl options
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        // execute post
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        $error = '';
        $response = simplexml_load_string($output);
         
         // dd($output->error_message);

        // check if curl request processed or not
        if(($response == false) or ($response == '')) {
            $curlerror = curl_error($ch); 
            $error = 'Server Error. ';
            $request->session()->flash('error', $error); 
            return back();          

        } else {
            
             // dd($response);

            if ($response->error_message != '')
            {
                $json = json_encode($response);
                $error = json_decode($json, TRUE);
                $request->session()->flash('error', $error['error_message']);
                 // close curl connection
                curl_close($ch); 
                   
                return back();
            }
            else
            {                
                if (isset($response->approval))
                {
                    $request->merge(['amount' => Session::get('amount')]);
                    $request->merge(['plan' => Session::get('plan')]);
                    $request->merge(['paymentgateway' => Session::get('paymentgateway')]);
                    $request->merge(['transaction_id' => $request->merch_transid]);

                    if (isset($response->approval))
                    {
                        $request->merge(['responseapproval' => $response->approval]);  
                    }
                    else
                    {
                        $request->merge(['responseapproval' => $response->approval]);
                    }    

                    $deposit = $this->makeDeposit($request);
                    $depositapprove = $this->approve($deposit->id);
                    $request->session()->flash('status', trans('forms.deposit_success_msg'));  
                }
                else
                {
                    $request->session()->flash('error', trans('forms.deposit_error_msg'));  
                }
                 // close curl connection
                curl_close($ch);       
                return  Redirect::to('myaccount/home');
            }            
        }            
    }

    public function partialwithdraw($depositid)
    {
        //dd($depositid);
        $deposit  = Deposit::where('id', $depositid)->with(['plan', 'user'])->first();

        //dd($deposit);
        return view ('deposit.partialwithdraw', [
                        'deposit' => $deposit,
                        ]);
    }

    public function printinvoice()
    {     
        $data = Paymentgateway::where('id', '=', '1')->first(); 
        $params = json_decode($data->params, true);

        $cheque = Paymentgateway::where('id', '=', '17')->first();
        $chequeparams = json_decode($cheque->params, true);
        if($cheque->id == '17')
        {
            $chequeno = \Session::get('chequeno');
            $payeername = \Session::get('payeername');
            $pdf = PDF::loadView('deposit.cheque_print_invoice', [
                        'chequeparams' => $chequeparams,
                        'chequeno' => $chequeno,
                        'payeername' => $payeername,
                ]);
        }
        else
        {
            $pdf = PDF::loadView('deposit.print_invoice', [
                        'params' => $params,
                ]);
        }
        
        return $pdf->download('invoice.pdf');
        //return $pdf->stream();
    }

    public function viewdepositsprintinvoice($depositid)
    {
        $deposit = Deposit::where('id', '=', $depositid)->with('paymentgateway')->first(); 

        if (is_null($deposit))
        {
            abort(404);
        }

        $transactionnumber = $this->getinvoicetransaction($deposit->transaction_id);

        $paymentgateway = Paymentgateway::where('id', '=', $deposit->paymentgateway->id)->first();
        $params = json_decode($paymentgateway->params, true);
        if($paymentgateway->id == '17')
        {
            $transactionrequest = $this->getinvoicetransactionrequest($deposit->transaction_id);
            $pdf = PDF::loadView('home.chequeinvoiceprint', [
                        'params' => $params,
                        'transactionnumber' => $transactionnumber,
                        'amount' => $deposit->amount,
                        'transactionrequest' => $transactionrequest,
                ]);
        }
        else
        {
            $pdf = PDF::loadView('home.invoiceprint', [
                        'params' => $params,
                        'transactionnumber' => $transactionnumber,
                        'amount' => $deposit->amount,
                ]);
        }
        return $pdf->download('invoice.pdf');
        //return $pdf->stream();
    }
    
    public function processpartialwithdraw(PartialWithdrawRequest $request)
    {
        $result = $this->partialwithdrawprocess($request);
        if ($result)
        {
            $request->session()->flash('successmessage', trans('forms.partial_withdraw_success_message'));
        }
        else
        {
            $request->session()->flash('errormessage', trans('forms.partial_withdraw_error_message'));
        }
        return Redirect::to('myaccount/home');
    }

    public function paymentcancelled(Request $request)
    {
        return view('deposit.paymentcancelled');
    } 

    public function bitpaysuccess()
    {
        return view('deposit.bitpaysuccess');
    } 

    public function getPgInfo($pgID) 
    {
        $pg = Paymentgateway::where('id', $pgID)->first();
        $params = json_decode($pg->params, true);
        //dd($params);
        $instructions = $pg->instructions;
        $pgInfo = [$params, $instructions ];
        return $pgInfo;
    }   

    public function cheque(Request $request)
    {       
        $deposit_result = $this->makeDeposit($request);       

        if($deposit_result)
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    } 

    public function couponcodeform()
    {
       // dd(\Session::get('paymentgateway'));
        $paymentgateway = \Session::get('paymentgateway');
        $plan = \Session::get('plan');
        $amount = \Session::get('amount');
        return view('deposit._epin_form', [
                        'paymentgateway' => $paymentgateway,
                        'amount' => $amount,  
                        'plan' => $plan,                   
        ]);
    }

    public function couponcodecheck(CouponCodeRequest $request)
    {
        $deposit_result = $this->updatecouponcodestatus($request);
       // dd($deposit_result);
        if(count($deposit_result) > 0)
        {
            $deposit = $this->makeDeposit($request); 
            $depositapprove = $this->approve($deposit->id);

            $ownerId = $deposit_result->owner;
            $owner = User::where('id', $ownerId)->first();
            $user = User::where('id', Auth::id())->first();

            $couponcodeused = Mailtemplate::where([['name','epin_couponcode_used'],['status','active']])->first();
            if(!is_null($couponcodeused))
            {
                Mail::to($user->email)->queue(new EpinCouponCodeUsed($deposit_result));
            }
            $couponcodeowner = Mailtemplate::where([['name','epin_couponcode_owner'],['status','active']])->first();
            if(!is_null($couponcodeowner))
            {
                Mail::to($owner->email)->queue(new EpinCouponCodeOwner($deposit_result));
            }

            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');
    }

    public function ewallet(Request $request)
    {      
        $admin = User::find(1);
        $user = Auth::user();        
        $deposit = $this->makeDeposit($request); 
        $depositapprove = $this->approve($deposit->id);      
             
        if($deposit)
        {
            $request->session()->flash('status', trans('forms.deposit_success_msg'));
        }
        else
        {
            $request->session()->flash('error', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/home');     
    } 

    public function store_paypal(Request $request )
    {  
        $request->merge(['amount' => Session::get('amount')]);
        $request->merge(['plan' => Session::get('plan')]);
        $request->merge(['paymentgateway' => Session::get('paymentgateway')]);

        $deposit = $this->makeDeposit($request); 
        $depositapprove = $this->approve($deposit->id);  
        if($deposit)
        {
            \session::put('successmessage',trans('forms.deposit_success_msg'));
        }
        else
        {
            \session::put('failmessage', trans('forms.deposit_error_msg'));
        }
        return  Redirect::to('myaccount/deposit');
    }

    public function store_payeer(Request $request )
    {
        $payment_gateway = \Session::get('paymentgateway');
        $pgInfo = $this->getPgInfo($payment_gateway);
        $params = $pgInfo[0];
        $m_key = $params['m_key'];

        $arHash[] = $request['m_params'];
        $arHash[] = $m_key;
        // Forming a signature
        $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));

        if(($request['m_sign'] == $sign_hash) &&($request->m_status == 'success'))
        {  
            $deposit = $this->makeDeposit($request); 
            $depositapprove = $this->approve($deposit->id); 
            if($deposit)
            {
                \session::put('successmessage',trans('forms.deposit_success_msg'));
            }
            else
            {
                \session::put('failmessage', trans('forms.deposit_error_msg'));
            }  
        } 
       else
        {
            \session::put('failmessage', trans('forms.deposit_error_msg'));
        } 
        return  Redirect::to('myaccount/matrixdeposit');
    }

}
