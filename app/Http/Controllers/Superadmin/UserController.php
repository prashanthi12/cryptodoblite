<?php

namespace App\Http\Controllers\Superadmin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use App\Placement;
use App\ActivityLog;
use App\Deposit;
use App\Testimonial;
use App\Ticket;
use App\Fundtransfer;
use App\Transaction;
use App\Useraccount;
use App\Interest;
use App\Userpayaccounts;
use App\Userprofile;
use App\Withdraw;
use Illuminate\Support\Facades\Validator;
use App\Traits\NewUser;

class UserController extends Controller
{    
    use NewUser;

    public function __construct()
    {
        $this->middleware(['auth','admin1']);
    }

    public function index()
    {         
	    $totalusers = User::ByUserType('4')->count(); 
	    $user = User::ByUserType('4')->with('userprofile');
	     
	    $q = Input::get('q');
	    $verified = Input::get('verifiedbutton');
	    $emailverified = Input::get('emailquery');

        if($q != "")
        {
            $users = $user->where('name', 'LIKE', '%' . $q . '%' )->orWhere('email', 'LIKE', '%' . $q . '%' )->paginate (20)->setPath ( '' );
            $pagination = $users->appends ( array (
                    'q' => Input::get ( 'q' ) 
            ));
  
            if (count ( $users ) > 0)
                return view('admin.users', ['users' => $users,'totalusers' => $totalusers])->withQuery ( $q );
            else
            {
              	$users = $user->paginate('20');
                return view ('superadmin.users', [
                        'users' => $users,
                        'totalusers' => $totalusers,
                ]);
            }
        }
        elseif(isset($verified))
        {
            if ($emailverified) 
            {             
              	$users = $user->whereHas('userprofile', function ($query)  
              	{
	                $query->where('email_verified','=','1');
	            })->paginate(20)->setPath('');

              	if (count($users)>0)
                	return view ('superadmin.users',[
	                  	'users' => $users,
	                  	'totalusers' => $totalusers
	                ])->withQuery($emailverified);
              	else
              	{
                	$users = $user->paginate('20');
	                return view ('superadmin.users', [
	                    'users' => $users,
	                    'totalusers' => $totalusers,
	                ]);
              	}
            }   
        }
        else
        {
          	$users = $user->paginate('20');
            return view ('superadmin.users', [
                    'users' => $users,
                    'totalusers' => $totalusers,
            ]);
        }
    }
    
    public function stafflist()
    {
        $users = User::with(['userprofile', 'agent'])->get();
        $staffs = new Collection;
        foreach ($users as $user) {
            if($user->userprofile->usergroup_id == "3") {
                $staffs->push($user);
            }
        }
       // dd($staffs);
        return view ('superadmin.staffs', [
                'staffs' => $staffs
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       
        $usergroup = array('3' => 'Staff', '4' => 'Member');
     
        return view('superadmin.createuser', [
            'usergroup' => $usergroup,
        ]);
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      	// Validator::extend('checkblockedusername', function ($attribute, $value, $parameters, $validator) {
       //      $checkblockedusername = Blockedusername::where('username', Input::get('name'))->exists();               
       //      if ($checkblockedusername)
       //      {
       //          return FALSE;
       //      }
       //      return TRUE;            
       //  }, trans('forms.checkblockedusername'));

      	$validator = Validator::make($request->all(), [
                'usergroup' => 'required',
                'name' => 'required|regex:/^[a-zA-Z0-9]+$/u|min:6|max:12|unique:users',
                'email' => 'required|email|max:255|unique:users',           
                'password' => 'required|min:6',                
        ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }   

        $result = $this->createnewuser($request);

        // $userprofile = $this->admincreateuserprofile($user, $request); 
        if ($request->usergroup == 3)
        {
            return redirect(url('superadmin/staffs')); 
        }
        else
        {
            return redirect(url('superadmin/users')); 
        }
    }    

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
  	public function show($id)
  	{
    	$user = User::where('id', $id)
	      	->with(['userprofile', 'loguser', 'withdraws'])
	      	->first();
       
	    $walletlists = Usercurrencyaccount::where('user_id' , $id)->with('currency')->get();

	    $loginhistory = ActivityLog::whereIn('log_name', array('login', 'logout'))->where('causer_id', $user->id)->orderBy('id', 'DESC')->get();
    	$lastlogin = $loginhistory->where('log_name', 'login')->last();   

	    $withdraw = $user->withdraws;
	    $pendingwithdraws  = $withdraw->where('status', 'pending');
	    $completedwithdraws  = $withdraw->where('status', 'completed');
	    $rejectedwithdraws  = $withdraw->where('status', 'rejected');          
    
	    // Fund Transfers
	    $currency_id = Currency::get()->pluck('id')->toarray();
	    $fundtransfer = Fundtransfer::with('fundtransfer_to_id', 'fundtransfer_from_id', 'transaction')->get();
	    $account_id = Usercurrencyaccount::where('user_id', '=', $id)->whereIn('currency_id', $currency_id)->get()->pluck('id')->toArray();
	    $sendtransferlists = $fundtransfer->whereIn('from_account_id', $account_id);
	    $receivetransferlists = $fundtransfer->whereIn('to_account_id', $account_id);        

    	$depositfundlists = Transaction::where([
                    ['type', 'credit'],
                    ['action', 'deposit'],
            ])
        	->whereIn('account_id', $account_id)
            ->get();
	    $newdepositlist = $depositfundlists->where('deposit_status', 'new');
	    $activedepositlist = $depositfundlists->where('deposit_status', 'active');

    	$approveflag=0;
      		if(($user->userprofile->passport_verified==0&&is_null($user->userprofile->passport_attachment)&&is_null($user->userprofile->id_card_attachment)&&$user->userprofile->id_card_verified== 0)&&is_null($user->userprofile->driving_license_attachment)&&$user->userprofile->driving_license_verified==0&&is_null($user->userprofile->photo_id_attachment)&&$user->userprofile->photo_id_verified==0&&is_null($user->userprofile->bank_attachment)&&($user->userprofile->kyc_approved==0))
	        {
	          	$approveflag=1;
	        }

      	return view('admin.userdetail', [
	        'user' => $user,
	        'sendtransferlists' => $sendtransferlists,
	        'receivetransferlists' => $receivetransferlists,
	        'loginhistory' => $loginhistory,
	        'lastlogin' => $lastlogin,
	        'rejectedwithdraws' => $rejectedwithdraws,
	        'pendingwithdraws' => $pendingwithdraws,
	        'completedwithdraws' => $completedwithdraws,
	        'walletlists' => $walletlists,
	        'newdepositlist' => $newdepositlist,
	        'activedepositlist' => $activedepositlist,
	        'approveflag'=>$approveflag,  
      	]);            
    }

    public function destroy($id)
    {             
        $checkPlacement = Placement::where('spillover_id', $id)->exists();

        $userdetail = User::where('id', $id)->with('userprofile')->first(); 
         // dd($userdetail->userprofile->usergroup_id);
        if ($checkPlacement || $userdetail->userprofile->usergroup_id != 4)
        {              
            \Alert::error('Dont delete this User.')->flash(); 
            return redirect(url('superadmin/users')); 
        }
     
        if (!is_null($id))
        {            
            $deleteActivityLog = ActivityLog::where('causer_id', $id)->delete();
            
            $deleteDeposit = Deposit::where('user_id', $id)->delete();
            $deletePlacement = Placement::where('user_id', $id)->delete();
            $deleteTestimonial = Testimonial::where('user_id', $id)->delete();
            $deleteTicket = Ticket::where('user_id', $id)->delete();
            $useraccount = Useraccount::where('user_id', $id)->get();
            foreach ($useraccount as $deleteid)
            {     
                $gettransactioninFund = Fundtransfer::where('from_account_id', $deleteid['id'])->get();

                foreach ($gettransactioninFund as $fundtransfer)
                {
                    $fundtransfer = Transaction::where('id', $fundtransfer->credit_transaction_id)->delete();
                }

                $transactionInterest = Transaction::where('account_id', $deleteid['id'])->first();

                if (!is_null($transactionInterest))
                {
                    $deleteTransaction = Interest::where('transaction_id', $transactionInterest->id)->delete();
                }
            
               // $gettransactioninFund = Transaction::where([
               //              ['account_id', $deleteid['id']],
               //              ['account_id', 'credit'],
               //              ['accounting_code_id', '51'],
               //              ])->first();

                $deleteTransaction = Transaction::where('account_id', $deleteid['id'])->delete();
                $deleteUseraccount = Useraccount::where('id', $deleteid['id'])->delete();
            }
            $deleteUserpayaccounts = Userpayaccounts::where('user_id', $id)->delete();
            $deleteUserprofile = Userprofile::where('user_id', $id)->delete();
            $deleteWithdraw = Withdraw::where('user_id', $id)->delete();

            $user = User::where('id', $id)->delete();
            
            \Session::put('successmessage', trans('forms.user_delete')); 
            return back();
        }
    }
}