<?php

namespace App\Http\Controllers\Superadmin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use League\Csv\Writer;

class ExportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin1']);
    }

    public function exportUsers()
    {   
        $users = User::with('userprofile')->ByUserType('4')->get();     
        $csv = Writer::createFromFileObject(new \SplTempFileObject());

        if(count($users) > 0)
        {
            $csv->insertOne(['Username','Email','First Name','Last name','Country']);
            
            foreach($users as $user) 
            {
                $country = '';
                // echo '<pre>';
                // print_r($user->userprofile->usercountry);
                if(count($user->userprofile->usercountry) > 0)
                {
                	// dd($user->userprofile->country);
                  $country = $user->userprofile->usercountry->name;
                }
                $csv->insertOne([$user->name, $user->email, $user->userprofile->firstname, $user->userprofile->lastname, $country]);
            }
        }
        else
        {
            $csv->insertOne(['No Records Found']);
            $csv->output('Users_'.date('_d/m/Y_H:i').'.csv');
        }  
        $csv->output('Users_'.date('_d/m/Y_H:i').'.csv');       
    }  

}
