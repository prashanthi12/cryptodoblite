<?php

namespace App\Http\Controllers\Superadmin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Plan;
use App\Bonus;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BonusRequest as StoreRequest;
use App\Http\Requests\BonusRequest as UpdateRequest;

class BonusCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Bonus');
        $this->crud->setRoute("superadmin/bonus");
        $this->crud->setEntityNameStrings('bonus', 'bonuses');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //$this->crud->setFromDb();

        $name = [
                'name' => 'name',
                'label' => "Name",
                'type' => 'text',
        ];
        $this->crud->addField($name, 'update');
        $this->crud->addColumn($name);

         $value = [
                'name' => 'value',
                'label' => "Bonus Value",
                'type' => 'text',
        ];
        $this->crud->addField($value, 'update');
        $this->crud->addColumn($value);

        $type = [
                'name' => 'type',
                'label' => "Bonus Type",
                'type' => 'radio',
                'options' => ['1' => 'Flat', '2' => 'Percentage (%)'],
        ];
        $this->crud->addField($type, 'update');
        $this->crud->addColumn('type');

         $triggertype = [
                'name' => 'triggertype',
                'label' => "Trigger Type",
                 'type' => 'radio',
                'options' => ['1' => 'Deposit', '2' => 'Released Deposit'],
        ];
        $this->crud->addField($triggertype, 'update');
        $this->crud->addColumn('triggertype');

        $bonus_cretided_to = [
                'name' => 'bonus_cretided_to',
                'label' => "Bonus Amount Credited to",
                 'type' => 'radio',
                'options' => ['1' => 'Account Balance', '2' => 'Deposit'],
        ];
        $this->crud->addField($bonus_cretided_to, 'update');
        $this->crud->addColumn('bonus_cretided_to');

        $status = [
                'name' => 'status',
                'label' => "Status",
                 'type' => 'radio',
                'options' => ['0' => 'Inactive', '1' => 'Active'],
        ];
        $this->crud->addField($status, 'update');
        $this->crud->addColumn('status');

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function create()
    {
        $triggertype = array('1' => 'Register', '2' => 'Deposit', '3' => 'Release');
        $deposittype = array('1' => 'Account Balance', '2' => 'Add Deposit');        

        $plans = Plan::where([
                ['active', '=', "1"],
                ['publish', '=', '1']
            ])->get();

        return view('superadmin.createbonus', [
                'triggertype' => $triggertype,
                'deposittype' => $deposittype,
                'plans' => $plans
            ]);
    }


    public function store(StoreRequest $request)
    {
       // dd($request);
        // your additional operations before save here
        //$redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        //return $redirect_location;

        $bonus = new Bonus;
        $bonus->name = $request->name;
        $bonus->value = $request->bonusvalue;
        $bonus->type = $request->bonustype;
        $bonus->triggertype = $request->triggertype;
        $bonus->plan = $request->plan;
        $bonus->bonus_cretided_to = $request->deposittype;
        $bonus->status = $request->status;

        if ($bonus->save())
        {
             \Alert::success('Bonus Added Successfully')->flash();
        }
        return redirect(url('superadmin/bonus')); 
    }

    public function edit($id)
    {
        $bonus = Bonus::where('id', $id)->first();

         $plans = Plan::where([
                ['active', '=', "1"],
                ['publish', '=', '1']
            ])->get();
        //dd($bonus);
         return view('superadmin.editbonus', [
                'bonus' => $bonus,
                'plans' => $plans,
                'id' => $id
            ]);
    }

    public function update(UpdateRequest $request)
    {
        //dd($request);
        // your additional operations before save here
       // $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
       // return $redirect_location;


        $bonus = Bonus::where('id', $request->id)->first();
        $bonus->name = $request->name;
        $bonus->value = $request->bonusvalue;
        $bonus->type = $request->bonustype;
        $bonus->triggertype = $request->triggertype;
        $bonus->plan = $request->plan;
        $bonus->bonus_cretided_to = $request->deposittype;
        $bonus->status = $request->status;

        if ($bonus->save())
        {
             \Alert::success('Bonus Updated Successfully')->flash();
        }
        return redirect(url('superadmin/bonus')); 
    }
}
