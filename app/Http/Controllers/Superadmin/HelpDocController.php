<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HelpDocController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin1']);
    }

    public function index()
    {
    	return view('superadmin.helpdocument');
    }

}
