<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Analytics;
//use Spatie\Analytics\Analytics;
use Spatie\Analytics\Period;
use Illuminate\Support\Collection;


class AnalyticsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    public function index()
    {
        $analyticsDataOne = Analytics::fetchVisitorsAndPageViews(Period::days(7));
        $mostVisitedPages = Analytics::fetchMostVisitedPages(Period::days(7));
        $topReferrers = Analytics::fetchTopReferrers( (Period::days(7)), 10);
        $topBrowsers = Analytics::fetchTopBrowsers( (Period::days(7)), 10);

        $analyticsData = Analytics::fetchTotalVisitorsAndPageViews(Period::days(14));
        $dates = $analyticsData->pluck('date');
        $visitors = $analyticsData->pluck('visitors');
        $pageViews = $analyticsData->pluck('pageViews');


        //dd($mostVisitedPages);
        return view('admin.analytics.showanalytics', [
        	'mostVisitedPages' => $mostVisitedPages,
	'topReferrers' => $topReferrers,
        	'topBrowsers' => $topBrowsers,
        	'dates' => $dates,
        	'visitors' => $visitors,
        	'pageViews' => $pageViews,
        	]);
    }
}
