<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Penalty;
use App\User;
use App\Userprofile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Http\Requests\PenaltyRequest;
use App\Traits\PenaltyProcess;


class PenaltyController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    use PenaltyProcess;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penalties = Penalty::with('user')->get();
        // dd($penalties);
       
        return view('admin.penalty.show', [
                        'penalties' => $penalties,
            ]);
    }  
   
    public function redirectpenalty($userid)
    {
         // dd($userid);
        \Session::put('penaltyuserid', $userid);
        return Redirect::to('admin/penalty');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userlist = Userprofile::where([
                ['active', 1],
                ['usergroup_id', 4]
            ])->with('user')->get();

        return view('admin.penalty.create',[
                'userlists' => $userlist
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenaltyRequest $request)
    {   
        $this->penaltyprocess($request);  
        $request->session()->flash('successmessage', trans('forms.penalty_success_message'));      
        return Redirect::to('admin/penalty/list');
    }    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.withdraw.update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       dd($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   
}
