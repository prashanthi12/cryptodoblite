<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Config;

class NewsletterController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        return view('admin/newsletter');
    }  

}