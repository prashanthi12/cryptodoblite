<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Sendbonus;
use App\User;
use App\Userprofile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Http\Requests\SendBonusRequest;
use App\Traits\SendBonusProcess;


class BonusController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    use SendBonusProcess;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bonuses = Sendbonus::with('user')->get();
         // dd($bonuses);
       
        return view('admin.bonus.show', [
                        'bonuses' => $bonuses,
            ]);
    }  
   
    public function redirectbonus($userid)
    {
        // dd($userid);
        \Session::put('bonususerid', $userid);
        return Redirect::to('admin/bonus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(\Session::get('bonususerid'));
        $userlist = Userprofile::where([
                ['active', 1],
                ['usergroup_id', 4]
            ])->with('user')->get();

        return view('admin.bonus.create',[
                'userlists' => $userlist
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SendBonusRequest $request)
    { 

        // dd($request);               
        $this->sendbonusprocess($request);

        \Session::forget('bonususerid');   
        $request->session()->flash('successmessage', trans('forms.bonus_success_message'));
        
        return Redirect::to('admin/bonus/list');
    }    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.withdraw.update');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       dd($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   
}
