<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Plan;
use App\Paymentgateway;
use App\Useraccount;
use App\Transaction;
use App\Deposit;
use Carbon\Carbon;
use App\Settings;
use App\Payment;
use App\Placement;
use App\Bonus;
use App\Accountingcode;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use  App\Notifications\DepositSuccessfull;
use  App\Notifications\AdminNotifyNewDeposit;
//use App\Traits\PlacementProcessor;
//use App\Traits\CommissionProcessor;
use App\Traits\DepositProcess;
use App\Notifications\User\DepositApprove;
use App\Notifications\User\DepositReject;
use App\Notifications\User\DepositReleased;
use App\Helpers\HyipHelper;

class DepositController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }
    use DepositProcess;
    //use PlacementProcessor;
    //use CommissionProcessor;
    
    public function index($status)
    {     
       $depositlists = Deposit::with(['plan', 'user', 'paymentgateway'])->where('status', $status)->paginate(20);
       
        return view('admin.deposit.show', [
                        'depositlists' => $depositlists,
                        'status' => $status
            ]);
    } 

    public function confirm($id) {

        $deposit = Deposit::where('id', $id)->first();

        if($deposit->status === "new") {        
            return view('admin.deposit.confirm', [
                    'depositid' => $id,
                    'deposit' => $deposit
                ]);
        } else {
            return abort(422);
        }
    }
    
    public function approve(Request $request, $id) {

        $deposit = Deposit::find($id);
        $user = User::where('id', $deposit->user_id)->first();
        $hasActiveDeposit = $this->checkForActiveDeposit($deposit);

        if ($deposit->paymentgateway_id != 14)
        {
            if( !$hasActiveDeposit ) {
                $placement = self::processPlacement($deposit);
            }            

            if (\Config::get('settings.referral_commission_status') == 1)
            {
                $referralCommission = $this->remitReferralCommission($deposit);  
            } 

            if (\Config::get('settings.level_commission_min_amount') <= $deposit->amount)
            {
                $levelCommission = $this->processLevelCommission($deposit);
                $this->createBonusTransaction($deposit);
            } 
        }

        $comments = $request->comment;  

        if (is_null($request->comment))
        {
            $comments = 'approved by admin';
        }     

        $deposit->status = "active";
        $deposit->comments_on_approval = $comments;
        $deposit->approved_on = Carbon::now();
        if ($deposit->plan->duration != '-1')
        {
            $maturityAfterDays = $this->calculateMaturityDays($deposit);
            if ($deposit->plan->duration_key == 'hours')
            {
                $deposit->matured_on = Carbon::now()->addHours($maturityAfterDays);
            }
            else
            {
                $deposit->matured_on = Carbon::now()->addDays($maturityAfterDays);
            }
        }
        if ( $deposit->save())
        {
            $user->notify(new DepositApprove);
            $request->session()->flash('successmessage', trans('forms.deposit_approve_success_message'));
        }
        else
        {             
            $request->session()->flash('errormessage', trans('forms.deposit_approve_failure_message'));
        }  

        return Redirect::to('admin/deposit/active');
    }

    // public function fundadded($id) 
    // {
    //     return view('admin.deposit.fundadded', [
    //             'depositid' => $id
    //         ]);

    // }

    public function fundaddeduser($id, Request $request) 
    {
         // dd($request);
        $deposit = Deposit::find($id);

        if ($deposit->status != 'new')
        {
            session()->flash('successmessage', trans('forms.deposit_fundadd_message'));
            return redirect('admin/deposit/rejected');
        }
        else
        {
             $result = $this->fundaddedtouser($deposit, $request);

            if ($result)
            {
                $user = User::where('id', $deposit->user_id)->first();
                $user->notify(new DepositReject);
                 session()->flash('successmessage', trans('forms.deposit_amount_addto_balance_message'));
            }
            else
            {             
                session()->flash('errormessage', trans('forms.deposit_amount_addto_balance_failmessage'));
            }  

            return redirect('admin/deposit/problem');
        }       
    }

    public function rejectform(Request $request, $id) {
        
        $deposit = Deposit::find($id);
        
        if($deposit->status === "new") {
            return view('admin.deposit.reject', [
                'depositid' => $id
                ]);
        } else {
            return abort(422);
        }
    }

    public function reject(Request $request, $id) {
   
        $validator = Validator::make($request->all(), [
                'comment'         => 'required'               
            ]);

        if ($validator->fails()) {
            return back()
                ->withInput()
                ->withErrors($validator);
        }

        $deposit = Deposit::find($id);
        $deposit->status = "rejected";
        $deposit->rejected_on = Carbon::now();
        $deposit->comments_on_reject = $request->comment;

        $user = $deposit->user;

        if ( $deposit->save())
        {
            $user->notify(new DepositReject);
            $request->session()->flash('successmessage', trans('forms.deposit_reject_message'));
        }
        else
        {             
            $request->session()->flash('errormessage', trans('forms.deposit_reject_failmessage'));
        }  
        return Redirect::to('admin/deposit/rejected');
    }

    public function releasedeposit(Request $request)
    {
        //dd($request);

        $deposit = Deposit::find($request->depositid);

        $bonus = Bonus::where([
                    ['plan', $deposit->plan_id],
                    ['triggertype', 2], // Released
                    ['status', '1']
                    ])->first();

        $result = $this->addedBonustoTransaction($deposit, $bonus);

        $deposit = Deposit::find($request->depositid);
        $deposit->status = "released";
        $deposit->released_on = Carbon::now();
        $deposit->comments_on_release = "Released by Admin on ".Carbon::now();
        if ($deposit->save())
        {
            $this->maturedDepositTransaction($deposit);
            $user = User::where('id', $deposit->user_id)->first();
            $user->notify(new DepositReleased);
            $request->session()->flash('successmessage', trans('forms.deposit_release_message'));
        }
        else
        {             
            $request->session()->flash('errormessage', trans('forms.deposit_release_failmessage'));
        }  
        // todo
        return Redirect::to('admin/deposit/released');
    }

    public function addedBonustoTransaction($deposit, $bonus) {

        if (!is_null($bonus))
        {
            if ($bonus->triggertype == 2) // Released
            {
                // 1 - Flat and 2 - %
                $bonusamount = $bonus->type == 1 ? $bonus->value : ($deposit->amount * $bonus->value)/100;
                //dd($bonusamount);
           
                    $plan = Plan::where('id', '=', $bonus->plan)->first();

                    $request_json = array('bonusid' => $bonus->id, 'type' => 'bonus');
                    $account_id = Useraccount::where([
                            ['user_id', '=', $deposit->user_id],
                            ['entity_type', '=', 'profile']
                        ])->get(['id'])->toArray();
                    $account_id = $account_id[0]['id'];

                    $accountcodeResult  = Accountingcode::where([
                        ['active', '=', "1"],
                        ['accounting_code', '=', "income-as-bonus"],
                        ])->get(['id'])->toArray();
                    $accounting_code = $accountcodeResult[0]['id'];

                    $transaction = new Transaction;
                    $transaction->account_id = $account_id;
                    $transaction->amount = $bonusamount;
                    $transaction->type = "credit";
                    $transaction->status ="1";
                    $transaction->accounting_code_id = $accounting_code;
                    $transaction->request = json_encode($request_json);
                    $transaction->comment = 'Bonus credited for the '. $plan->name . ' Plan Released.';
                    $transaction->save();    
                    return $transaction;               
            }
        }
        else
        {
            return;
        }      
    } 

     public function viewbitcoinwallet($transactionid) {
        $transaction = Transaction::where('id', $transactionid)->first();
        //dd($transaction);
        $request_json = json_decode($transaction->request, true);
        //dd($request_json['btcamount']);

        $response_json = json_decode($transaction->response, true);
        //$url = 'https://testnet.blockexplorer.com/api/tx/'.$response_json['hashid'];

        if (!isset($response_json['hashid']))
        {
             $txhashid = '';
             $total_confirmations = '';
             $actual_deposited_amount = '';
             $bitcoin_transaction_time = '';
             $received_amount = '';
        }
        else
        {
       
            $curl_json = HyipHelper::getBitcoinWalletDetails($response_json['hashid']);

            $curl_json = json_decode($curl_json, true);

            $received_amount = '';
            foreach ($curl_json['vout'] as $vout)
             {
                if ($vout['scriptPubKey']['addresses'][0] == $request_json['admin_address'])
                {
                    $received_amount .= $vout['value'];
                }                               
             }

             $txhashid = $curl_json['txid'];
             $total_confirmations = $curl_json['confirmations'];
             $actual_deposited_amount = $request_json['btcamount'];
             $bitcoin_transaction_time = date("Y-m-d H:i:s", $curl_json['time']);
        }
       
         return view('adminpartials._bitcoin_direct_wallet_details', [
                'txnhashkey' => $txhashid,
                'confirmations' => $total_confirmations,
                'actual_deposited_amount' => $actual_deposited_amount,
                'received_amount' => $received_amount,                
                'bitcoin_transaction_time' => $bitcoin_transaction_time,
            ]);
    }

    public function onlineapprove($id)
    {
        //dd('onlineapprove');
        $deposit = Deposit::find($id);
        $user = User::where('id', $deposit->user_id)->first();
        $hasActiveDeposit = $this->checkForActiveDeposit($deposit);

        if ($deposit->paymentgateway_id != 14)
        {
            if( !$hasActiveDeposit ) {
                $placement = self::processPlacement($deposit);
            }            

            if (\Config::get('settings.referral_commission_status') == 1)
            {
                $referralCommission = $this->remitReferralCommission($deposit);  
            } 

            if (\Config::get('settings.level_commission_min_amount') <= $deposit->amount)
            {
                $levelCommission = $this->processLevelCommission($deposit);
            } 
        }
        $deposit->status = "active";
        $comments = 'approved by admin';
        $deposit->comments_on_approval = $comments;
        $deposit->approved_on = Carbon::now();

        if ($deposit->plan->duration != '-1')
        {
            $maturityAfterDays = $this->calculateMaturityDays($deposit);
            if ($deposit->plan->duration_key == 'hours')
            {
                $deposit->matured_on = Carbon::now()->addHours($maturityAfterDays);
            }
            else
            {
                $deposit->matured_on = Carbon::now()->addDays($maturityAfterDays);
            }
        }
        if ( $deposit->save())
        {
            $user->notify(new DepositApprove);
            \Session()->flash('successmessage', trans('forms.deposit_approve_message'));
        }
        else
        {             
            \Session()->flash('errormessage', trans('forms.deposit_approve_failmessage'));
        }  

        return Redirect::to('admin/deposit/active');
    }

    public function releasedepositwithid($id)
    {
        $deposit = Deposit::find($id);
        //dd($deposit);
        $bonus = Bonus::where([
                    ['plan', $deposit->plan_id],
                    ['triggertype', 2], // Released
                    ['status', '1']
            ])->first();

        $result = $this->addedBonustoTransaction($deposit, $bonus);

        $deposit->status = "released";
        $deposit->released_on = Carbon::now();
        $deposit->comments_on_release = "Released by Admin on ".Carbon::now();
        if ($deposit->save())
        {
            $this->maturedDepositTransaction($deposit);
            $user = User::where('id', $deposit->user_id)->first();
            $user->notify(new DepositReleased);
            \Session()->flash('successmessage', trans('forms.deposit_release_message'));
        }
        else
        {             
            \Session()->flash('errormessage', trans('forms.deposit_release_failmessage'));
        }  
        // todo
        return Redirect::to('admin/deposit/released');
    }
}
