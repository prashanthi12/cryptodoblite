<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ActivityLog;
use App\Session;

class LogController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    public function index()
    {
        $loglists = ActivityLog::with('loguser')->orderBy('id', 'DESC')->paginate('20');

        return view('admin.showlog', [
                'loglists' => $loglists
        ]);
    }

    public function LoggedInUsers()
    {
        $loggedinusers = Session::with('user')->orderBy('id', 'DESC')->get();
        
        return view('admin.loggedinusers', [
                'loggedinusers' => $loggedinusers,
        ]);
    }

}
