<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Validator;
use Carbon\Carbon;
use App\Userprofile;
use App\MailMessage;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\SendMailRequest;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Traits\Common;
use App\Events\MassMail;
use Event;

class SendMailController extends Controller
{
	use Common;

    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    public function index(Request $request)
    {   
        /*$from_date = date('Y-m-d H:i:s',strtotime($request->from_date));
        $to_date = date('Y-m-d H:i:s',strtotime($request->to_date));*/

        $list = MailMessage::with('fromuser', 'touser')->orderby('id','DESC')->groupBy('batch');

        /*if ($from_date != "" && $to_date != "" && isset($request->from_date)) 
        {
            Validator::extend('checkdate', function ($attribute, $value, $parameters, $validator) 
            {   
                $from_date = date('Y-m-d H:i:s',strtotime(Input::get('from_date')));
                $to_date = date('Y-m-d H:i:s',strtotime(Input::get('to_date')));
                if($from_date <= $to_date)
                {
                    return TRUE;
                }
                return FALSE;
            }, trans('forms.invalid_date'));

            $validator = Validator::make($request->all(),[
                'from_date' => 'required|checkdate',
                'to_date' => 'required',
            ]);

            if($validator->fails())
            {
                return redirect(url('/admin/sendmail/'))->withInput()->withErrors($validator);
            }
            
            $list = $list->whereBetween('created_at',[$from_date,$to_date])->paginate(20);
        }  
        else
        {*/
            $list = $list->paginate(20);
        // }

        return view ('admin.sendmail.show', [
                'list' => $list,                              
        ]);
    }

    public function viewmessage($id)
    {
        $sendmail = MailMessage::find($id);
        return view ('admin.sendmail.viewmessage',[
            'sendmail'=>$sendmail,
        ]);
    }

    public function create_massmail()
    {
        $users = array('active' => 'Active Users', 'suspend' => 'Suspend Users', 'all' => 'All Users');
        return view('massmail.create',[
            'users' => $users,
        ]);
    }

    public function store_massmail(SendMailRequest $request)
    { 
    	//firing an event
    	$request=$request->toArray();
    	Event::fire(new \App\Events\MassMail($request));
    	\Session::put('successmessage', 'Send Mail Successfully'); 
       
        return back();  
    }

    public function viewusers($batchid)
    {
        $mailUsers = MailMessage::where('batch', $batchid)->with('touser')->get();
         
        return view('admin.sendmail.viewusers', [
            'mailUsers' => $mailUsers,
        ]);
    }
    
}

?>