<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Testimonial;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials  = Testimonial::orderBy('id', 'DESC')->get();
        return view('admin.testimonials', [
                'testimonials' => $testimonials
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $testimonial = Testimonial::where('id', '=', $id)->first();
        $testimonial->active = $request->changedstatus;
        if( $testimonial->save() )
        {
            $request->session()->flash('successmessage', trans('forms.testimonial_status_success_message')); 
        }
        else
        {
            $request->session()->flash('failmessage', trans('forms.testimonial_status_fail_message')); 
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function moreinfo($id)
    {
        $testimonials  = Testimonial::where('id', '=', $id)->first();
        return view('admin.testimonialsmoreinfo', [
                'testimonials' => $testimonials
            ]);
    }
}
