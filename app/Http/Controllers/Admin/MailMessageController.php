<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\User;
use App\MailMessage;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\AdminSendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Events\AdminMailMessage;
use Event;

class MailMessageController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    public function SendMail(Request $request, $id)
    {
        $user = User::where('id', $id)->first();

        $sendmail = new MailMessage;
        $sendmail->from_user_id = Auth::id();
        $sendmail->subject = $request->subject;
        $sendmail->message = $request->message;
        $sendmail->to_user_id = $id;
        $sendmail->save();

        //firing an event
        Event::fire(new AdminMailMessage($sendmail, $user));

       // Mail::to($user->email)->queue(new AdminSendMail($user, $sendmail));
        $request->session()->flash('successmessage', trans('forms.email_success_message')); 
        return back();
    }
}