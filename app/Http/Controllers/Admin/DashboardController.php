<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Userprofile;
use App\Deposit;
use App\Withdraw;
use App\Transaction;
use App\Useraccount;
use App\Accountingcode;
use App\Conversation;
use App\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Traits\GetData;
use DB;
use App\Traits\TicketProcess;
use App\Fundtransfer;
use App\Traits\UserInfo;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin2']);
    }

    use GetData, TicketProcess, UserInfo;

    public function index() 
    {
        $members = DB::table('users')
            ->join('userprofiles', 'users.id', '=', 'userprofiles.user_id')
            ->where([
                    ['userprofiles.usergroup_id', '4'],
                    ['users.deleted_at', null]
                    ])
            ->get();

        $totalmembers = $members->count();
        $activemembers = $members->where('active', '1')->count();
        $unverifiedmembers = $members->where('kyc_verified', '0')->count();
    	$newdeposits = Deposit::with(['plan', 'user', 'paymentgateway'])->where('status','new')->latest()->take(5)->get();
        $newdepositsCount = Deposit::with(['plan', 'user', 'paymentgateway'])->where('status','new')->count();

        $deposits = Deposit::get();
        $activedeposit = $deposits->where('status', 'active')->count();
        $unapprovedeposit = $newdeposits->count();
        $maturedeposit = $deposits->where('status', 'matured')->count();

        $withdraw = Withdraw::get();
        $pendingwithdraw = $withdraw->where('status', 'pending')->count();
        $completedwithdraw = $withdraw->where('status', 'completed')->count();
        $rejectedwithdraw = $withdraw->where('status', 'rejected')->count();

        $transaction = Transaction::get();
        $sumofinterest = $this->interestcalculation();
        $sumoflevelcommission = $this->levelcommissioncalculation($transaction);
        $sumofreferralcommission = $this->referralcommissioncalculation($transaction);

        $userlist = User::ByUserType(4)->with('userprofile')->get();
        //dd($userlist);
        $conversations = Conversation::with(['userone', 'usertwo','message'])->latest()->first();

        $activelogusers = Session::where('user_id', '!=', 'NULL')->with('user')->orderBy('id', 'DESC')->take(5)->get();
        $activelogusersCount = Session::with('user')->count();

        $pendingkyclists = Userprofile::whereNotIn('usergroup_id', array('1', '2', '3'))->where('kyc_verified', '=', 0)->where('kyc_doc', '!=', '')->with('user')->latest()->take(5)->get();
        $pendingkycCount = Userprofile::whereNotIn('usergroup_id', array('1', '2', '3'))->where('kyc_verified', '=', 0)->where('kyc_doc', '!=', '')->with('user')->count();

        $withdrawlists = Withdraw::where('status', 'pending')->with(['user', 'transaction', 'userpayaccounts'])->orderBy('id', 'DESC')->latest()->take(5)->get();
        $withdrawlistsCount = Withdraw::where('status', 'pending')->with(['user', 'transaction', 'userpayaccounts'])->count();

        $matureddepositlists = Deposit::where('status', 'matured')->with(['plan', 'user', 'paymentgateway'])->latest()->take(5)->get();
        $maturedepositCount = Deposit::where('status', 'matured')->with(['plan', 'user', 'paymentgateway'])->count();

        $signedupuserslists = User::with('loguser')->orderby('created_at', 'desc')->orderby('id', 'desc')->take(5)->get();
       // $sponsor = $this->getSponsor($signedupuserslists);
       // dd($signedupuserslists);
        $signedupusersCount = Userprofile::with('user')->count();

        $transferfunds = Fundtransfer::with('fundtransfer_to_id', 'fundtransfer_from_id', 'transaction')->orderBy('id', 'DESC')->latest()->take(5)->get();
        //dd($transferfunds);
        $transferfundsCount = Fundtransfer::with('fundtransfer_to_id', 'fundtransfer_from_id', 'transaction')->count();

        $userprofile = Userprofile::where('user_id', Auth::id())->with('user')->first(); 
        $ticketlist = $this->pendingticketlist($userprofile); 
       // dd($ticketlist);
        //dd($signedupuserslists);
    	return view('admin.dashboard', [
                'totalmembers' => $totalmembers,
                'activemembers' => $activemembers,
                'unverifiedmembers' => $unverifiedmembers,
                'activedeposit' => $activedeposit,
                'unapprovedeposit' => $unapprovedeposit,
                'maturedeposit' => $maturedeposit,
                'pendingwithdraw' => $pendingwithdraw,
                'completedwithdraw' => $completedwithdraw,
                'rejectedwithdraw' => $rejectedwithdraw,
                'sumofinterest' => $sumofinterest,
                'sumoflevelcommission' => $sumoflevelcommission,
                'sumofreferralcommission' => $sumofreferralcommission,
                'depositlists' => $newdeposits,
                'userlist' => $userlist,
                'conversations' => $conversations,
                'newDepositCount' => $newdepositsCount,
                'activelogusers' => $activelogusers,
                'activelogusersCount' => $activelogusersCount,
                'pendingkyclists' => $pendingkyclists,
                'pendingkycCount' => $pendingkycCount,
                'withdrawlists' => $withdrawlists,
                'withdrawlistsCount' => $withdrawlistsCount,
                'matureddepositlists' => $matureddepositlists,
                'maturedepositCount' => $maturedepositCount,
                'transferfunds' => $transferfunds,
                'transferfundsCount' => $transferfundsCount,
                'signedupuserslists' => $signedupuserslists,
                'signedupusersCount' => $signedupusersCount,
                'ticketlist' => $ticketlist,
        ]);
    }     
}
