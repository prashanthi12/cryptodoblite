<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Socialite;
use Session;
use App\Services\SocialTwitterAccountService;
use App\Http\Requests\SocialRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;

class SocialAuthTwitterController extends Controller
{
  /**
   * Create a redirect method to twitter api.
   *
   * @return void
   */
    public function redirect()
    {
        return Socialite::driver('twitter')->redirect();
    }
    /**
     * Return a callback method from twitter api.
     *
     * @return callback URL from twitter
     */
    public function callback(SocialTwitterAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('twitter')->user());
        auth()->login($user);
        return Redirect::to('/myaccount/home');
    }

    public function socialEmail($id)
    {     
        return view('social.addemail', [
            'social_id' => $id
        ]);
    }

    public function setEmail(Request $request)
    {             
        if($request->social_id == "1")
        {
            \Session::put('twitter_email', $request->email);
            return Redirect::to( url('/redirect')); 
        }
        else
        {
            \Session::put('facebook_email', $request->email);
            return Redirect::to( url('/fbredirect')); 
        }           
    }
    
}