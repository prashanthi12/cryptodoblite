<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketAttachment extends Model
{
	protected $fillable = [
        'ticket_id', 'attachment_file'
    ];

    public function attachments()
    {
        return $this->belongsTo('App\Ticket');
    }
}
