<?php

namespace App\Traits;

use App\Userpayaccounts;
use App\Fundtransfer;
use App\Accountingcode;
use App\Transaction;
use App\User;
use App\Usercurrencyaccount;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Useraccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Config;
use App\Traits\LogActivity;
use App\Models\Mailtemplate;
use Illuminate\Support\Facades\Mail;
use App\Mail\FundTransferReceiver;
use App\Mail\FundTransferSender;

trait FundTransferProcess {

    use LogActivity;

    public function sendFundTransfer($request)
    {
        $fundtransfer = $this->addFundToUser($request);        
        $transaction = $this->createFundTransferCreditTransaction( $request, $fundtransfer);
        $updatewithdraw = $this->updateCreditTransaction( $transaction, $fundtransfer);

        $transaction = $this->createFundTransferDebitTransaction( $request, $fundtransfer);
        $updatewithdraw = $this->updateDebitTransaction( $transaction, $fundtransfer);
        return $fundtransfer;
	}  

    public function addFundToUser($request)
    {
        //dd($request);
        $fromaccount_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $from_account_id = $fromaccount_id[0]['id'];

        $touser = User::where('name', $request->sendto)->first();
        //dd($touser);
        $toaccount_id = Useraccount::where([
                ['user_id', '=', $touser->id],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $to_account_id = $toaccount_id[0]['id'];

        //dd($fundtransferamount);
        $fundtranfer = new Fundtransfer;
        $fundtranfer->amount = $request->amount;
        $fundtranfer->from_account_id = $from_account_id;
        $fundtranfer->to_account_id = $to_account_id;
        $fundtranfer->comments = 'Fund transfer to '.$request->sendto;
        $fundtranfer->save();
        return $fundtranfer;
    }   

    public function createFundTransferCreditTransaction( $request, $fundtransfer)
    {
        $fundtransferamount = $this->addAdminCommission($request);
        $request_json = array('amount' => $request->amount);
        $response_json = array('receiveamount' => $fundtransfer->amount);

        $accountcodeResult  = Accountingcode::where('active', "1"); 
        $accounting_code  = $accountcodeResult->where('accounting_code', 'fund-transfer')->get(['id'])->toArray();     
        $accounting_code = $accounting_code[0]['id'];

        $touser = User::where('name', $request->sendto)->first();

        $account_id = Useraccount::where([
                ['user_id', '=', $touser->id],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
            $account_id = $account_id[0]['id'];
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $fundtransferamount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->response = json_encode($response_json);
        $transaction->save();       
        return $transaction;
    }

     public function addAdminCommission($request)
    {
        //dd('test');
        $request_json = array('userid' => Auth::id(), 'amount' => $request->amount);       
        $admin_com_amount = ($request->amount * Config::get('settings.fundtransfer_commission')) / 100 ; 
        $fund_transfer_remaining_amount = $request->amount - $admin_com_amount;
        //dd($withdraw_amount);
        $account_id = Useraccount::where([
                ['user_id', '=', 2],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $accountcodeResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "fund-transfer-commission"],
            ])->get(['id'])->toArray();
        $accounting_code = $accountcodeResult[0]['id'];

        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $admin_com_amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->save();       
        return $fund_transfer_remaining_amount;
    }

    public function updateCreditTransaction($transaction, $fundtransfer)
    {
        $updatewithdraw = Fundtransfer::where('id', '=', $fundtransfer->id)->first();
        $updatewithdraw->credit_transaction_id = $transaction->id;      
        $updatewithdraw->save();
        return $updatewithdraw;
    }  

    public function createFundTransferDebitTransaction( $request, $fundtransfer)
    {
        $request_json = array('amount' => $request->amount);
        $response_json = array('receiveamount' => $fundtransfer->amount);

        $accountcodeResult  = Accountingcode::where('active', "1"); 
        $accounting_code  = $accountcodeResult->where('accounting_code', 'fund-transfer')->get(['id'])->toArray();     
        $accounting_code = $accounting_code[0]['id'];

        $touser = User::where('name', $request->sendto)->first();

        $account_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
            $account_id = $account_id[0]['id'];
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $request->amount;
        $transaction->type = "debit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->response = json_encode($response_json);
        $transaction->save();       
        return $transaction;
    }

    public function updateDebitTransaction($transaction, $fundtransfer)
    {
        $updatewithdraw = Fundtransfer::where('id', '=', $fundtransfer->id)->first();
        $updatewithdraw->credit_transaction_id = $transaction->id;      
        $updatewithdraw->save();
        return $updatewithdraw;
    }   

    public function fundTransferMail($data)
    {    
        // dd($data);        
        $fundtransferreceiver = Mailtemplate::where([['name','fund_transfer_receiver'],['status','active']])->first();
        if(!is_null($fundtransferreceiver))
        {
            Mail::to($data->user->email)->queue(new FundTransferReceiver($data)); 
        }
        $fundtransfersender = Mailtemplate::where([['name','fund_transfer_sender'],['status','active']])->first();
        if(!is_null($fundtransfersender))
        {
            // dd($data->sender->id);
            $user = User::where('id', $data->sender->id)->first();
            Mail::to($user->email)->queue(new FundTransferSender($data));
        }
    } 

 }