<?php

namespace App\Traits;

use App\Userpayaccounts;
use App\Sendbonus;
use App\Accountingcode;
use App\Transaction;
use App\User;
use App\Useraccount;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\UserSendBonus;
use Illuminate\Bus\Queueable;
use App\Events\AdminSendBonus;
use Event;

trait SendBonusProcess 
{
    public function sendbonusprocess($request)
    {
        $this->sendBonustoUser($request); 
    }

    public function sendBonustoUser($request)
    {       
        // dd($users);
        $accountcodeResult  = Accountingcode::where('active', "1")->where('accounting_code', 'debt-as-penalty')->get(['id'])->toArray();        
        $accounting_code = $accountcodeResult[0]['id'];
        // dd($accounting_code);     

        if (\Session::get('bonususerid') == '')
        {
            $users = $request->users;
            foreach ($users as $user)
            {               
                $response_json = array('transaction_number' => uniqid() );
                $bonus = new Sendbonus;
                $bonus->amount = $request->amount;
                $bonus->user_id = $user;
                $bonus->comments = $request->comment;

                if ($bonus->save())
                {
                    $account_id = Useraccount::where([
                    ['user_id', '=', $user],
                    ['entity_type', '=', 'profile']
                    ])->get(['id'])->toArray();
                    $account_id = $account_id[0]['id'];

                    $transaction = new Transaction;
                    $transaction->account_id = $account_id;
                    $transaction->amount = $request->amount;
                    $transaction->type = "credit";
                    $transaction->status ="1";
                    $transaction->accounting_code_id = $accounting_code;
                    $transaction->response = json_encode($response_json);

                    if ($transaction->save())
                    {
                        $updatebonus = Sendbonus::where('id', '=', $bonus->id)->first();
                        $updatebonus->transaction_id = $transaction->id;      
                        $updatebonus->save();

                        $user = User::where('id', $user)->first();

                        //firing an event
                        Event::fire(new AdminSendBonus($user, $bonus));

                       // Mail::to($user->email)->queue(new UserSendBonus($bonus));
                    }
                }         
            }
        }
        else
        {
                $response_json = array('transaction_number' => uniqid() );
                $bonus = new Sendbonus;
                $bonus->amount = $request->amount;
                $bonus->user_id = \Session::get('bonususerid');
                $bonus->comments = $request->comment;

                if ($bonus->save())
                {
                    $account_id = Useraccount::where([
                    ['user_id', '=', \Session::get('bonususerid')],
                    ['entity_type', '=', 'profile']
                    ])->get(['id'])->toArray();
                    $account_id = $account_id[0]['id'];

                    $transaction = new Transaction;
                    $transaction->account_id = $account_id;
                    $transaction->amount = $request->amount;
                    $transaction->type = "credit";
                    $transaction->status ="1";
                    $transaction->accounting_code_id = $accounting_code;
                    $transaction->response = json_encode($response_json);

                    if ($transaction->save())
                    {
                        $updatebonus = Sendbonus::where('id', '=', $bonus->id)->first();
                        $updatebonus->transaction_id = $transaction->id;      
                        $updatebonus->save();

                        $user = User::where('id', \Session::get('bonususerid'))->first();

                        //firing an event
                        Event::fire(new AdminSendBonus($user, $bonus));

                       // Mail::to($user->email)->queue(new UserSendBonus($bonus));
                    }
                }         
        }   
    }    

 }