<?php

namespace App\Traits;

use App\Userpayaccounts;
use App\Penalty;
use App\Accountingcode;
use App\Transaction;
use App\User;
use App\Useraccount;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\UserPenalty;
use Illuminate\Bus\Queueable;
use App\Events\AdminSendPenalty;
use Event;

trait PenaltyProcess 
{
    public function penaltyprocess($request)
    {
        $this->sendPenatytoUser($request); 
    }

    public function sendPenatytoUser($request)
    {       
        $accountcodeResult  = Accountingcode::where('active', "1")->where('accounting_code', 'debt-as-penalty')->get(['id'])->toArray();        
        $accounting_code = $accountcodeResult[0]['id'];
        // dd($accounting_code);

        if (\Session::get('penaltyuserid') == '')
        {
            $users = $request->users;
            // dd($users);

            foreach ($users as $user)
            {
                $response_json = array('transaction_number' => uniqid() );

                $penalty = new Penalty;
                $penalty->amount = $request->amount;
                $penalty->user_id = $user;
                $penalty->comments = $request->comment;

                if ($penalty->save())
                {
                    $account_id = Useraccount::where([
                    ['user_id', '=', $user],
                    ['entity_type', '=', 'profile']
                    ])->get(['id'])->toArray();
                    $account_id = $account_id[0]['id'];

                    $transaction = new Transaction;
                    $transaction->account_id = $account_id;
                    $transaction->amount = $request->amount;
                    $transaction->type = "debit";
                    $transaction->status ="1";
                    $transaction->accounting_code_id = $accounting_code;
                    $transaction->response = json_encode($response_json);

                    if ($transaction->save())
                    {
                        $updatepenalty = Penalty::where('id', '=', $penalty->id)->first();
                        $updatepenalty->transaction_id = $transaction->id;      
                        $updatepenalty->save();

                        $user = User::where('id', $user)->first();

                        //firing an event
                        Event::fire(new AdminSendPenalty($user, $penalty));

                       // Mail::to($user->email)->queue(new UserPenalty($penalty));
                    }
                }         
            }
        }
        else
        {
            $response_json = array('transaction_number' => uniqid() );

                $penalty = new Penalty;
                $penalty->amount = $request->amount;
                $penalty->user_id = \Session::get('penaltyuserid');
                $penalty->comments = $request->comment;

                if ($penalty->save())
                {
                    $account_id = Useraccount::where([
                    ['user_id', '=', \Session::get('penaltyuserid')],
                    ['entity_type', '=', 'profile']
                    ])->get(['id'])->toArray();
                    $account_id = $account_id[0]['id'];

                    $transaction = new Transaction;
                    $transaction->account_id = $account_id;
                    $transaction->amount = $request->amount;
                    $transaction->type = "debit";
                    $transaction->status ="1";
                    $transaction->accounting_code_id = $accounting_code;
                    $transaction->response = json_encode($response_json);

                    if ($transaction->save())
                    {
                        $updatepenalty = Penalty::where('id', '=', $penalty->id)->first();
                        $updatepenalty->transaction_id = $transaction->id;      
                        $updatepenalty->save();

                        $user = User::where('id', \Session::get('penaltyuserid'))->first();

                        //firing an event
                        Event::fire(new AdminSendPenalty($user, $penalty));
                        
                      //  Mail::to($user->email)->queue(new UserPenalty($penalty));
                    }
                }         
        }
    }    

 }