<?php

namespace App\Traits;
use Config;
use App\Ewallet;
use App\User;
use Carbon\Carbon;
use App\Transaction;
use App\Useraccount;
use Illuminate\Support\Facades\Mail;
use App\Mail\Ewallet\AddFund;
use App\Mail\Ewallet\ApproveFund;
use App\Accountingcode;
use Illuminate\Support\Facades\Auth;
use App\Paymentgateway;
use App\Classes\BlockIo;
use App\Models\Mailtemplate;

trait EwalletProcess
{  
    public function addfund($request,$id,$amount,$btc_amount,$total_btc_amount)
    {
        $paymentId = \Session::get('paymentgateway');
        $btc_address = config::get('settings.ewallet_btc_address');
        $bonus_amount = config::get('settings.ewallet_fund_bonus_amount'); 
        $transaction_id = uniqid();
        $request_json = array('bank_name' => $request->bank_name, 'bank_address' => $request->bank_address, 'swift_code' => $request->swift_code, 'account_name' => $request->account_name, 'account_no' => $request->account_no);
        $response_json = array('transaction_number' => $request->transaction_id);

        if($paymentId == 9)   // Bitcoin via direct transfer
        {
            $create = [
                    'from_user_id' => $id,
                    'to_user_id' => 2,
                    'amount' => $amount,
                    'paymentgateway_id' => $paymentId,
                    'status' => 'pending',
                    'transaction_id' => $transaction_id,
                    'bitcoin_hash_id' => $request->hash_id,
                    'bitcoin_address' => $btc_address,
                    'type' => 'add',
                    'bonus_amount' => $bonus_amount,
                    'btc_amount' => $btc_amount,
                    'total_btc_amount' => $total_btc_amount,                                       
                ];
        }
        if($paymentId == 1)  // Bank Transfer
        {
            $create = [
                    'from_user_id' => $id,
                    'to_user_id' => 2,
                    'amount' => $amount,
                    'paymentgateway_id' => $paymentId,
                    'status' => 'pending',
                    'transaction_id' => $transaction_id,
                    'request' => json_encode($request_json),
                    'response' => json_encode($response_json),
                ];
        }
        if($paymentId == 2)  // Paypal
        {
            $create = [
                    'from_user_id' => $id,
                    'to_user_id' => 2,
                    'amount' => $amount,
                    'paymentgateway_id' => $paymentId,
                    'status' => 'approve',
                ];
        }

        $ewallet = Ewallet::create($create);
        if($paymentId == 2)    // Paypal auto approve
        {
            $id = $ewallet->id;
            $via = 'admin';
            $received_amount = $ewallet->amount;
            $approve_deposit = $this->approveFund($id,$via,$received_amount);
        }

        \Session::put('success', 'Add Fund Successfully.Waiting for admin approval');
        $user = User::find(2);
        $email = $user->email;
        $addfund = Mailtemplate::where([['name','add_fund'],['status','active']])->first();
        if(!is_null($addfund))
        {
            Mail::to($email)->send(new AddFund($ewallet));                  
        }
    }

    public function ewalletBalance($id)
    {
        $account_id = Useraccount::where([
                    ['user_id', '=', $id],
                    ['entity_type', '=', 'profile']
                    ])->first();
        $account_id = $account_id->id;
        $credit = 0;
        $debit = 0;
        $credit = Transaction::where([['account_id', '=', $account_id],
                                        ['type', 'credit'],
                                        ['status', '1']])->whereIn('accounting_code_id', ['68','70','71','73'])->get()->sum('amount');

        $debit = Transaction::where([['account_id', '=', $account_id],
                                        ['type', 'debit'],
                                        ['status', '1']])->whereIn('accounting_code_id', ['69','72'])->get()->sum('amount');

        $balance = bcsub($credit,$debit,10);
        return $balance;
    }

    public function approveFund($id,$via,$received_amount)
    {
        $ewallet = Ewallet::find($id);
        $ewalletlist = Ewallet::with(['FundUser'])->where('id', $id)->get();
        $username = $ewalletlist['0']['FundUser']['name'];

        $amount = $ewallet['amount'];
        $bonus_amount = $ewallet['bonus_amount'];
        $user_id = $ewallet['from_user_id'];
        $update = [
                    'status' => 'approve',
                    'approve_at' => Carbon::now(),
                    'process_via' => $via,
                    'received_amount' => $received_amount              
                ];
    
        if (Ewallet::where('id',$id)->update($update))
        {
            // For Bonus    
            $bonus = ($bonus_amount/100) * $received_amount;
            $account_id = $this->getUserIdByAccountID($user_id,'profile');
            $comment = "Fund  Amount : ".$amount." Received Amount:".$received_amount ;
            if($received_amount > 0)
            {
                $create = [
                            'account_id' => $account_id,
                            'amount' => $received_amount,
                            'type' => 'credit',
                            'status' => 1,
                            'comment' => $comment,
                            'accounting_code_id' => 68,             
                        ];

                Transaction::create($create);
            }
            if($bonus > 0)
            {
                $comment = "Fund Bonus Amount ".$bonus." for Received Amount ".$received_amount ;
                $create = [
                            'account_id' => $account_id,
                            'amount' => $bonus,
                            'type' => 'credit',
                            'status' => 1,
                            'comment' => $comment,
                            'accounting_code_id' => 70,                                  
                        ];
                Transaction::create($create);
            }
            \Session::put('successmessage', 'Fund Approved Successfully');
            $ewallet_details = Ewallet::find($id);
            $user = User::find($user_id);
            $email = $user->email;
            $approvefund = Mailtemplate::where([['name','approve_fund'],['status','active']])->first();
            if(!is_null($approvefund))
            {
                Mail::to($email)->send(new ApproveFund($ewallet_details)); 
            }
        }
        else
        {         
            \Session::put('errormessage', 'Fund Approved Failed!!!. Please Try Again.');       
        }  
    }

    public static function getBitcoinDetails($hashkey)
    {      
        $mode = getenv('BTC_MODE');
        if($mode == 'live')
        {
            $url = 'https://blockexplorer.com/api/tx/'.$hashkey;
        }
        else
        {
            $url = 'https://testnet.blockexplorer.com/api/tx/'.$hashkey;
        }
         
        $ch  = curl_init();
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_json = curl_exec($ch);
        curl_close($ch);
        $curl_json = json_decode($curl_json, true);
        return $curl_json;
    }

    public function getUserIdByAccountID($user_id,$entity_type)
    {
        $account_id = Useraccount::where([
                ['user_id', '=', $user_id],
                ['entity_type', '=', $entity_type]
                ])->get(['id'])->toArray();
        return $account_id= $account_id[0]['id'];
    }

    public function transferToEWallet($request)
    {
        $this->transferCreditTransaction($request);
        $this->transferDebitTransaction($request);
        \Session::put('successmessage', 'Balance added to E-wallet Successfully');
    }

    public function transferCreditTransaction( $request)
    {    
        $request_json = array('amount' => $request->amount);
        $accountcodeResult = Accountingcode::where('active', "1"); 
        $accounting_code = $accountcodeResult->where('accounting_code', 'ewallet-internal-transfer-credit')->get(['id'])->toArray();
        $accounting_code = $accounting_code[0]['id'];     

        $account_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];

        $comment = "Add to E-wallet";
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $request->amount;
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
        $transaction->comment = $comment;
        $transaction->save();       
        return $transaction;
    }

    public function transferDebitTransaction( $request)
    {
        $request_json = array('amount' => $request->amount);
        $accountcodeResult = Accountingcode::where('active', "1"); 
        $accounting_code = $accountcodeResult->where('accounting_code', 'ewallet-internal-transfer-debit')->get(['id'])->toArray();
        $accounting_code = $accounting_code[0]['id'];
  
        $comment = "Transfer to E-wallet";
        $account_id = Useraccount::where([
                ['user_id', '=', Auth::id()],
                ['entity_type', '=', 'profile']
            ])->get(['id'])->toArray();
        $account_id = $account_id[0]['id'];
 
        $transaction = new Transaction;
        $transaction->account_id = $account_id;
        $transaction->amount = $request->amount;
        $transaction->type = "debit";
        $transaction->status ="1";
        $transaction->accounting_code_id = $accounting_code;
        $transaction->request = json_encode($request_json);
         $transaction->comment = $comment;
        $transaction->save();       
        return $transaction;
    }

    public function getWalletBalance($btc_address)
    {
        $pgId = Paymentgateway::find(6);    // for bitcoin blockio
        $pg = $this->getPgDetailsByGatewayName($pgId->gatewayname);
        $params = json_decode($pg->params, true);
        $api_key= $params['api_key'];
        $pin= $params['pin'];

        $version = $params['version']; // API version
        $block_io = new BlockIo( $api_key, $pin, $version);
        $available_balance=0;
        try
        {
            $balance = $block_io->get_address_balance(array('addresses' => $btc_address));
            $available_balance = $balance->data->available_balance;
        }
        finally
        {
            return $available_balance;
        }
    }

    public function getPgDetailsByGatewayName($payment_name) 
    {
        $pg = Paymentgateway::where('gatewayname', $payment_name)->first();              
        return $pg;
    } 

}