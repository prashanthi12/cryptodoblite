<?php 

namespace App\Traits;

use App\User;
use App\Userprofile;
use App\Useraccount;
use App\Transaction;
use App\Deposit;
use App\Accountingcode;
use Illuminate\Database\Eloquent\Collection;
use App\Interest;
trait UserInfo {

    public function getSponsor(user $user) 
    {
        $user_id = $user->id;
        $sponsor_id = Userprofile::where([['user_id', '=', $user_id]])->get();
        $sponsor_id = $sponsor_id->pluck('sponsor_id');
        $sponsor='';
        if($sponsor_id[0]!='')
        {
            $sponsor = User::withTrashed()->where('id', $sponsor_id)->first();
            $sponsor = $sponsor->name;
        }

        return $sponsor;
    }

    public function getReferralCount(user $user)
    {   
        return $user->referrals->count();
    }

    public function getReferralDeposit(user $user)
    {
        $user_id = $user->id;
        $myreferrals = User::where('id', '=', $user_id)->with('referrals')->get();
        $myreferrals =  $myreferrals[0]->referrals->pluck('user_id');
        $referrals = new Collection;
        foreach ($myreferrals as $myreferral) {
            $deposits = Deposit::where([ 
                                ['status', '!=', 'new'],
                                ['user_id', '=', $myreferral]
                                ])->get();
            $userLifeTimeDeposit = $deposits ->sum('amount');
            $referrals->push($userLifeTimeDeposit);
        }
        $TotalReferralDeposit = $referrals->pipe(function ($collection) 
        {
            return $collection->sum();
        });
        return $TotalReferralDeposit;
    }

    public function getUserBalance(user $user)
    {   
        // $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        // $operativeAccountId = $myOpertiveAccount->id;

        $allUserAccount = $user->useraccounts->whereIn('accounttype_id', [3, 5])->pluck('id')->toarray();
        //dd($allUserAccount);
        $creditTransactions  = Transaction::whereIn(
                    'account_id' ,$allUserAccount)
                    ->where([['type', '=', 'credit']])->whereNotIn('accounting_code_id', ['68','70','71','73'])->sum('amount');  
        
        $debitTransactions  = Transaction::whereIn(
                    'account_id' ,$allUserAccount)
                    ->where([['type', '=', 'debit']])->whereNotIn('accounting_code_id', ['69','72'])->sum('amount');
        
        $balance = $creditTransactions - $debitTransactions;
        return $balance;
    }

    public function getActiveDeposit(user $user) 
    {
        $user_id = $user->id;
        $userDeposits = Deposit::with(['plan','interest'])->where('user_id',  $user_id)->get();
        $activeDeposits = $userDeposits->where('status', '=', 'active')->sum('amount');
        return $activeDeposits;
    }

    public function getLifeTimeDeposit(user $user) 
    {
        $user_id = $user->id;
        $userDeposits = Deposit::where('user_id',  $user_id)->get();
        $lifeTimeDeposit = $userDeposits->whereNotIn('status', ['new', 'rejected', 'problem'])->sum('amount');
        return $lifeTimeDeposit;
    }

    public function getLifeTimeEarnings(user $user) 
    {
        $lifeTimeEarings = $this->getUserTotalInterest($user) 
                    + $this->getUserTotalReferralCommission($user);
        return $lifeTimeEarings;
    }

    public function getUserTotalInterest(user $user)
    {
        $user_id = $user->id;
        $userDeposits = Deposit::with(['plan','interest'])->where('user_id',  $user_id)->get();

        $interest_array = [];
            foreach($userDeposits as $userDeposit) {
            $interest_array = array_add($interest_array, $userDeposit->id, $userDeposit->interest->pluck('amount')->sum());
            }
            list($deposits, $interests) = array_divide($interest_array);
        $total_interest = array_sum($interests);
        return $total_interest;
    }

    public function getUserTotalReferralCommission(user $user)
    {
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;
        $referralResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-via-referral-commission"],
        ])->get(['id'])->toArray();
        $referral_code = $referralResult[0]['id'];
        $totalReferralCommission  = Transaction::where([
            ['account_id', '=' ,$operativeAccountId],
            ['type', '=', 'credit'],
            ['accounting_code_id', '=', $referral_code],
            ])->sum('amount');

        return $totalReferralCommission;
    }

    // public function isActiveold(user $user) {
    //     $user_id = $user->id;
    //     $userprofile = Userprofile::where('user_id', $user->id)->first();
    //     $active = $userprofile->active;
    //     return $active;
    // }

    public function isActive(user $user) {
        return $user->userprofile->active;
    }

    // public function isUserProfileCompleted (user $user) {
    //     $user_id = $user->id;
    //         $userprofile = Userprofile::where('user_id', $user->id)->first();
    //         if ( is_null($userprofile->firstname) || is_null($userprofile->lastname) || is_null($userprofile->mobile) 
    //             || is_null($userprofile->country)  || is_null($userprofile->ssn) || is_null($userprofile->kyc_doc) )
    //             {
    //                 return false;
    //             } else {
    //                  return true;
    //             }
    // }
    public function isUserProfileCompleted (User $user) {
        $user_id = $user->id;
        $userprofile = $user->userprofile;
        if (is_null($userprofile->firstname) || is_null($userprofile->lastname) || is_null($userprofile->mobile) || is_null($userprofile->country) || is_null($userprofile->ssn) || is_null($userprofile->kyc_doc))
        {
            return false;
        } else {
            return true;
        }
    }

    public function isKycApproved (user $user) {
        $userprofile = $user->userprofile;
        $kycApproved = $userprofile->kyc_verified;
        return $kycApproved;
    }

    public function isEmailVerified(user $user) {
        $userprofile = $user->userprofile;
        $emailVerified = $userprofile->email_verified;
        return $emailVerified;
    }
        // public function isKycApprovedold (user $user) {
        //     $user_id = $user->id;
        //         $userprofile = Userprofile::where('user_id', $user->id)->first();
        //         $kycApproved = $userprofile->kyc_verified;
        //         return $kycApproved;
        // }

        // public function isEmailVerifiedold(user $user) {
        //     $user_id = $user->id;
        //         $userprofile = Userprofile::where('user_id', $user->id)->first();
        //         $emailVerified = $userprofile->email_verified;
        //         return $emailVerified;
        // }

    public function getEarnings(user $user) 
    {
        $useraccounts = $user->useraccounts();
        $myEarningsAccount = $user->useraccounts->whereIN('accounttype_id', array(3, 5) )->pluck('id')->toarray();

        //Interest account code  
        $earingsResult  = Accountingcode::where('active', "1")
        ->where('accounting_code', 'like', '%interest-via%')
        ->get(['id'])->toArray();

        // Referral Commmission account code
        $referralResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-via-referral-commission"],
            ])->get(['id'])->toArray();
        $referral_code = $referralResult[0]['id'];

        // Level Commmission account code
        $levelcommResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-via-level-commission"],
            ])->get(['id'])->toArray();
        $level_comm_code = $levelcommResult[0]['id'];

        //Bonus account code
        $bonusResult  = Accountingcode::where([
            ['active', '=', "1"],
            ['accounting_code', '=', "income-as-bonus"],
            ])->get(['id'])->toArray();
        $bonus_code = $bonusResult[0]['id'];

       array_push($earingsResult, $referral_code, $level_comm_code, $bonus_code);

        $earningslists = Transaction::with('accountingcode')
                                ->where('type','credit')
                                ->whereIn('accounting_code_id', $earingsResult)
                                ->whereIn('account_id', $myEarningsAccount)
                                ->get();
        return $earningslists;
    }

    public function totalInterest(user $user) 
    {
        $mydeposits = Deposit::with(['interest'])->where('user_id', $user->id)->get();

        $interest_array = [];

        foreach($mydeposits as $mydeposit) {
            $interest_array = array_add($interest_array, $mydeposit->id, $mydeposit->interest->pluck('amount')->sum());
        }     
        list($deposits, $interests) = array_divide($interest_array);
        $total_interest = array_sum($interests);

        return $total_interest;
    }
    
    // public function totalInterest(user $user) 
    // {
    //     $mydeposits = Deposit::where('user_id', $user->id)->pluck('id')->toArray();
    //     $total_interest = Interest::whereIn('deposit_id', $mydeposits)->sum('amount');
 


    //     return $total_interest;
    // }

    public function totalReferralCommission(user $user) 
    {
        //$useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        $referralResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-via-referral-commission"],
        ])->get(['id'])->toArray();
        $referral_code = $referralResult[0]['id'];

        $totalReferralCommission = Transaction::where([
                                                ['account_id', '=', $operativeAccountId],
                                                ['type','=','credit'],
                                                ['accounting_code_id', '=', $referral_code]
                                            ])->sum('amount');
        return $totalReferralCommission;
    }

    public function totalLevelCommission(user $user) 
    {
       // $useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        $levelcommResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-via-level-commission"],
        ])->get(['id'])->toArray();
        $level_comm_code = $levelcommResult[0]['id'];

        $totalLevelCommissions = Transaction::where([
                                                ['account_id', '=', $operativeAccountId],
                                                ['type','=','credit'],
                                                ['accounting_code_id', '=', $level_comm_code]
                                            ])->sum('amount');
        return $totalLevelCommissions;
    }

    public function totalBonus(user $user) 
    {
       // $useraccounts = $user->useraccounts();
        $myOpertiveAccount = $user->useraccounts->where('accounttype_id', 3 )->first();
        $operativeAccountId = $myOpertiveAccount->id;

        $bonusResult  = Accountingcode::where([
        ['active', '=', "1"],
        ['accounting_code', '=', "income-as-bonus"],
        ])->get(['id'])->toArray();
        $bonus_code = $bonusResult[0]['id'];

        $totalbonus = Transaction::where([
                                        ['account_id', '=', $operativeAccountId],
                                        ['type','=','credit'],
                                        ['accounting_code_id', '=', $bonus_code]
                                    ])->sum('amount');
        return $totalbonus;
    }

    public function totalLifeTimeEarnings(user $user) 
    {
        $totalLifeTimeEarnings = $this->totalInterest($user) + $this->totalReferralCommission($user) + $this->totalLevelCommission($user) + $this->totalBonus($user);

        return $totalLifeTimeEarnings;
    }

 }