<?php 

namespace App\Traits;

use App\Ticket;
use App\TicketComments;
use App\TicketCategoriesUsers;
use App\TicketAttachment;
use App\User;
use Config;
use Response;
use File;
use App\Helpers\HyipHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminNotifyNewTicket;
use App\Mail\StaffNotifyTicket;
use App\Mail\UserSendTicket;
use App\Models\Mailtemplate;
use App\Mail\UserNotifyTicketStatus;
use App\Mail\AdminNotifyTicketStatus;
use App\Mail\StaffNotifyTicketStatus;

trait TicketProcess {

    public function myticketlist($userprofile) 
    {
        $ticketresult = Ticket::with('category', 'status', 'agent');

        if($userprofile->usergroup_id == 3)
        {
            $ticketresult  = $ticketresult->where('agent_id', Auth::id())->get();
        }
        elseif ($userprofile->usergroup_id == 4) 
        {
            $ticketresult  = $ticketresult->where('user_id', Auth::id())->paginate(Config::get('settings.pagecount'));
        }
        else
        {
            $ticketresult = $ticketresult->get();
        } 
        return $ticketresult;      
    } 

    public function getTicketdetails($id) 
    {
        $ticketdetails = Ticket::where('id', '=', $id)->with('user', 'category', 'priority', 'status', 'agent', 'attachments')->first();
        //dd($ticketdetails);

        return $ticketdetails;
    } 

    public function downloadattachments($id) 
    {
        $attachment = TicketAttachment::where('id', '=', $id)->first();
        $path = $attachment->attachment_file;
        //dd($path);
        $headers = array('Content-Type' => File::mimeType($path));        
        return response()->download($path, $attachment->attachment_file, $headers);  
    }   

    public function getCommentlists($id) 
    {
        $commentlists = TicketComments::where('ticket_id', '=', $id)->with('user')->paginate(Config::get('settings.pagecount'));
        
        return $commentlists;
    }  

    public function storeTicketComment($request) 
    {     
        $ticketcomment = new TicketComments;
        $ticketcomment->content = $request->comment; 
        $ticketcomment->user_id = Auth::id();  
        $ticketcomment->ticket_id = $request->ticket_id;     
        //$ticketcomment->save();

        if ($ticketcomment->save())
        {
            return $ticketcomment;
        }
        return FALSE;
    } 

    public function makeTicket($request)
    {
        $agent = TicketCategoriesUsers::where('category_id', '=', $request->category)->first(); 
        $ticket = new Ticket;
        $ticket->subject = $request->subject;
        $ticket->content = $request->description;
        $ticket->status_id = 1;
        $ticket->priority_id = $request->priority;
        $ticket->user_id = Auth::id();
        $ticket->agent_id = $agent->user_id;
        $ticket->category_id = $request->category;
        $ticket->save();
        return $ticket;
    }

    public function makeTicketattachments($request, $ticket)
    {     
        $files = $request->attachments;
        //dd($files);
        // start count how many uploaded
        $uploadcount = 0;
        if( count($files) > 0)
        {
            foreach($files as $file) {
                $user = User::findOrFail(Auth::user()->id);
                $fileNewName = 'tickets/'.$user->name.'_'.time();
                //dd($fileNewName);
             
                $imgObject = Storage::put($fileNewName, $file, 'public');
                //dd($imgObject);
                $tickets = Storage::url($imgObject);
                $destinationPath = $imgObject;
                //dd($tickets);
                $TicketAttachment = new TicketAttachment;
                $TicketAttachment->attachment_file = $tickets;
                $TicketAttachment->ticket_id = $ticket->id;
                $TicketAttachment->save();
                $uploadcount ++;
            }
            if($uploadcount != count($files))
            {
                $request->session()->flash('failmessage', trans('forms.ticket_failure_message'));   
                return FALSE;
            }
        }        
    }

    public function updateStatus($statusid, $id) 
    {        
        $ticketstatus = Ticket::where('id', '=', $id)->with('user','agent','priority','category','status')->first(); 
        $ticketstatus->status_id = $statusid; 

        if ($ticketstatus->save())
        {
            return $ticketstatus;
        }

        return FALSE;
    }  

    public function pendingticketlist($userprofile) 
    {
        $ticketresult = Ticket::where('status_id', 1)->with('category', 'status', 'agent');
        if($userprofile->usergroup_id == 3)
        {
            $ticketresult  = $ticketresult->where('agent_id', Auth::id())->get();
        }
        elseif ($userprofile->usergroup_id == 4) 
        {
            $ticketresult  = $ticketresult->where('user_id', Auth::id())->get();
        }
        else
        {
            $ticketresult = $ticketresult->orderby('created_at', 'desc')->take(5)->get();
        } 
        return $ticketresult;      
    }   

    public function userSendTicket($data)
    {
        $admincnotifynewticket = Mailtemplate::where([['name','admin_notify_new_ticket'],['status','active']])->first();
        if(!is_null($admincnotifynewticket))
        {
            // dd($data);
            Mail::to($data->admin->email)->queue(new AdminNotifyNewTicket($data->ticket, $data->admin)); 
        }
        $staffnotifyticket = Mailtemplate::where([['name','staff_notify_ticket'],['status','active']])->first();
        if(!is_null($staffnotifyticket))
        {
            // dd($data);
            Mail::to($data->ticket->agent->email)->queue(new StaffNotifyTicket($data->ticket));
        }
        $usersendticket = Mailtemplate::where([['name','user_send_ticket'],['status','active']])->first();
        if(!is_null($usersendticket))
        {
            Mail::to($data->ticket->user->email)->queue(new UserSendTicket($data->ticket));
        }
    } 

    public function staffUpdateTicketStatus($data)
    {
        // dd($data->admin);
        $usernotifyticketstatus = Mailtemplate::where([['name','user_notify_ticket_status'],['status','active']])->first();
        if(!is_null($usernotifyticketstatus))
        {
            // dd($data);
            Mail::to($data->statusresult->user->email)->queue(new UserNotifyTicketStatus($data)); 
        }
        $adminnotifyticket = Mailtemplate::where([['name','admin_notify_ticket_status'],['status','active']])->first();
        if(!is_null($adminnotifyticket))
        {
            $admin = User::find(2);
            Mail::to($admin->email)->queue(new AdminNotifyTicketStatus($data));
        }
    }     

    public function adminUpdateTicketStatus($data)
    {
        // dd($data->admin);
        $usernotifyticketstatus = Mailtemplate::where([['name','user_notify_ticket_status'],['status','active']])->first();
        if(!is_null($usernotifyticketstatus))
        {
            // dd($data);
            Mail::to($data->statusresult->user->email)->queue(new UserNotifyTicketStatus($data)); 
        }
        $staffnotifyticketstatus = Mailtemplate::where([['name','staff_notify_ticket_status'],['status','active']])->first();
        if(!is_null($staffnotifyticketstatus))
        {
            $staff = User::where('id', $data->staff->id)->first();
            // dd($data);
            Mail::to($staff->email)->queue(new StaffNotifyTicketStatus($data));
        }
    }                   

}