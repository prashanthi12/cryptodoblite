<?php

namespace App\Traits;
use App\Transaction;
use App\Useraccount;
use App\Accountingcode;

trait GetData {

   public function interestcalculation() 
     {
           $InterestAccount = Useraccount::where('accounttype_id', 5 )->pluck('id')->toarray();
           if (is_null($InterestAccount))
            {
                return '0';
            }
             $interestResult  = Accountingcode::where('active', "1")
            ->where('accounting_code', 'like', '%interest-via%')
            ->get(['id'])->toArray();
            $interest_code = $interestResult[0]['id'];

            //dd($InterestAccount);
            $sumofinterest = Transaction::where([
                                        ['type','=','credit'],
                                        ['accounting_code_id', '=',  $interest_code]
                                    ])->
                                whereIn('account_id', $InterestAccount)->sum('amount');

           return $sumofinterest;
     }

     public function levelcommissioncalculation($transaction) 
     {
           $sumoflevelcommission = $transaction->where(
                    'type',  'credit')->where('accounting_code_id', 3)
           ->sum('amount');  
           return $sumoflevelcommission;
     }

     public function referralcommissioncalculation($transaction) 
     {
           $sumofreferralcommission = $transaction->where(
                    'type',  'credit')->where('accounting_code_id', 2)
           ->sum('amount');  
           return $sumofreferralcommission;
     }

 }