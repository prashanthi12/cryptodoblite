<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Userpayaccounts extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'paymentgateways_id','active','current','param1','param2','param3','param4','param5','param6','param7','param8'
    ];

    public function payment() {
        return $this->belongsTo('App\Paymentgateway', 'paymentgateways_id', 'id');
    }

    protected $dates = ['deleted_at'];
}
