<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class News extends Model
{
	use CrudTrait;
	
	protected $appends = array('story');

    protected $fillable = [
        'title', 'story', 'language', 'active'
    ]; 

    public function getStoryAttribute($story)
    {
    	return \Purify::clean($story);
    }
    
}
