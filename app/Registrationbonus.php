<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Registrationbonus extends Model
{
    protected $fillable = [
        'name', 'value', 'plan', 'status'
    ];

    public function plan() {
        return $this->belongsTo('App\Plan');
    }
     
}
