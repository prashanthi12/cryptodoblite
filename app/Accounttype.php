<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounttype extends Model
{   
   protected $fillable = [
        'account_type', 'active',
    ];

    public function useraccount() {
    	return $this->hasMany('App\Useraccount');
    }
    
}
