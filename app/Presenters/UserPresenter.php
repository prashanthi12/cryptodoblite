<?php
namespace App\Presenters;
use Laracasts\Presenter\Presenter;
use App\User;
use App\Transaction;
use App\Deposit;
use App\Message;
use Illuminate\Support\Facades\Auth;

class UserPresenter extends Presenter
{
    public function getUsername($id)
    {
        $user = User::withTrashed()->where('id', $id)->first();
        return $user->name;
    }

    public function getPlanname($id)
    {
        $transaction = Transaction::where('id', $id)->first();
        $param = json_decode($transaction->request, true);
        //dd($param);
        $depositid = $param['depositid'];
        $deposit = Deposit::where('id', $depositid)->with('plan')->first();
        return $deposit->plan->name;
    }

    public function getMessageUnreadCount($conversationid)
    {
        $messagecount = Message::where([
            ['conversation_id', $conversationid],
            ['user_id', '!=', Auth::id()],
            ['is_seen', 0]
            ])->count();      

        return $messagecount;
    }

    public function getSignupCountry($ip)
    {
        $country = '';
        $signupCountry = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip)); 
        if(isset($signupCountry['country']))
        {
            $country = $signupCountry['country'];
        }
        return $country;
    }

    public function getBitcoinUrl($hashkey)
    {
        $mode = getenv('BTC_MODE');
        if($mode == 'live')
        {
            $url = 'https://blockexplorer.com/tx/'.$hashkey;
        }
        else
        {
            $url = 'https://testnet.blockexplorer.com/tx/'.$hashkey;
        }
        return $url;
    }
    
}