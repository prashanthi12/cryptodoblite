<?php
namespace App\Presenters;
use Laracasts\Presenter\Presenter;
use App\Transaction;
use App\Helpers\HyipHelper;

class TransactionPresenter extends Presenter
{
    public function getTransactionNumber($id)
    {
        $transaction = Transaction::where('id', $id)->first();
        $param = json_decode($transaction->response, true);
      // dd($param);
        if (is_null($param) || $param=="{object:undefined}")
        {
          //  dd('iuhdfsiudfhs');
            return '';
        }
        return $param['transaction_number'];
    }

    public function getBitcoinActualAmount($transactionid)
    {
        $transaction = Transaction::where('id', $transactionid)->first();
        $request_json = json_decode($transaction->request, true);
        $response_json = json_decode($transaction->response, true);       
        $actual_deposited_amount = $request_json['btcamount'];
        return $actual_deposited_amount;
    }


    public function getBitcoinReceivedAmount($transactionid)
    {
        $transaction = Transaction::where('id', $transactionid)->first();
        $request_json = json_decode($transaction->request, true);
        $response_json = json_decode($transaction->response, true);

        // $url = 'https://testnet.blockexplorer.com/api/tx/'.$response_json['hashid'];
        
         $curl_json = HyipHelper::getBitcoinWalletDetails($response_json['hashid']);

        $curl_json = json_decode($curl_json, true);

        $received_amount = '';
        foreach ($curl_json['vout'] as $vout)
         {
            if ($vout['scriptPubKey']['addresses'][0] == $request_json['admin_address'])
            {
                $received_amount .= $vout['value'];
            }                               
         }
        return $received_amount;
    }
}