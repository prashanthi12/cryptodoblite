<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = "App\Presenters\UserPresenter";

    protected $table = "transactions";

    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'account_id', 'amount','type','status','accounting_code_id', 'request_obj','response_obj', 'comment'];

    public function fromaccount() {
        return $this->belongsTo('App\Useraccount');
    }

    public function toaccount() {
        return $this->belongsTo('App\Useraccount');
    }

    public function accountingcode() {
        return $this->belongsTo('App\Accountingcode', 'accounting_code_id', 'id');
    }

}