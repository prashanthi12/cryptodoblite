<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Quote extends Model
{
	use CrudTrait;

    protected $fillable = [
        'authorimage', 'text', 'authorname', 'language', 'active'
    ];

    public function getTextAttribute($text)
    {
    	return \Purify::clean($text);
    }
}
