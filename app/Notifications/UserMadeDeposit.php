<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class UserMadeDeposit extends Notification
{
    use Queueable;

    private $_deposit, $_user_name, $_plan_name, $_pg_name;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($deposit, $user_name, $plan_name, $pg_name)
    {
        //
        $this->_deposit = $deposit;
        $this->_user_name = $user_name;
        $this->_plan_name = $plan_name;
        $this->_pg_name = $pg_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['mail', 'slack'];
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('A new deposit has been made')
                    ->action('View Details', url('/'))
                    ->line('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        // dd($notifiable);
        // dd($this->_deposit);

        /* Just checking whether a slack webhook exists.. */
        if (empty(config('settings.slack_webhook')) == TRUE)
        {
            /* if not available.. nothing to do */
            return;
        }

        $slack_msg = new SlackMessage;
        $slack_msg->from(config('settings.sitename'))
                    ->content('A new deposit has been made!')
                    ->attachment(function ($attachment) {
                        $attachment->title('Deposit Details', url(URL_ADMIN_VIEW_DEPOSIT . $this->_deposit->id))
                            ->fields([
                                'Amount' => $this->_deposit->amount,
                                'Plan' => $this->_plan_name,
                                'User' => $this->_user_name,
                                'Payment Gateway' => $this->_pg_name
                            ]);
                    });

        $slack_channel = config('settings.slack_channel');

        /* If a slack channel is provided in settings... */
        if (empty($slack_channel) == FALSE)
        {
            /* .. and not start with # .. */
            if ( $slack_channel[0] != '#')
            {
                /* append the # before using it */
                $slack_channel = '#'. $slack_channel;
            }

            $slack_msg->to($slack_channel);
        }

        /*return (new SlackMessage)
                    // ->to('#larahyip')
                    // ->to($slack_channel)
                    // ->from('LaraHYIP')
                    ->from(config('settings.sitename'))
                    // ->content(sprintf('A new deposit of %s has been made on plan *%s* by user *%s*', $this->_deposit->amount, $this->_plan_name, $this->_user_name));
                    ->content('A new deposit has been made!')
                    ->attachment(function ($attachment) {
                        $attachment->title('Deposit Details', url(URL_ADMIN_VIEW_DEPOSIT . $this->_deposit->id))
                            ->fields([
                                'Amount' => $this->_deposit->amount,
                                'Plan' => $this->_plan_name,
                                'User' => $this->_user_name
                            ]);
                    });*/

        return $slack_msg;
    }
}
