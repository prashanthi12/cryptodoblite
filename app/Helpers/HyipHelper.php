<?php

namespace App\Helpers;
use App\Userprofile;

class HyipHelper
{
    public static function plan_label($plan_id)
    {
        switch ($plan_id)
        {
            case PLANTYPE_DAILY:
                return 'day';

            case PLANTYPE_DAILY_BUSINESS:
                return 'business day';

            case PLANTYPE_HOURLY:
                return 'hour';

            case PLANTYPE_WEEKLY:
                return 'week';

            case PLANTYPE_MONTHLY:
                return 'month';

            case PLANTYPE_QUATERLY:
                return 'quater';

            case PLANTYPE_YEARLY:
                return 'year';
        }

        return 'unknown';
    }

    public static function is_superadmin($userid)
    {
        if ($userid == '')
        {
            return FALSE;
        }
        $userprofile = Userprofile::where('user_id', $userid)->first();  
        if($userprofile->usergroup_id == 1)
        {
            return TRUE;
        }
        return FALSE;
    }

    public static function is_admin($userid)
    {
        if ($userid == '')
        {
            return FALSE;
        }
        $userprofile = Userprofile::where('user_id', $userid)->first();  
        if($userprofile->usergroup_id == 2)
        {
            return TRUE;
        }
        return FALSE;
    }

    public static function is_staff($userid)
    {
        if ($userid == '')
        {
            return FALSE;
        }
        $userprofile = Userprofile::where('user_id', $userid)->first(); 

        if($userprofile->usergroup_id == 3)
        {
            return TRUE;
        }
        return FALSE;
    }

    public static function is_user($userid)
    {
        $userprofile = Userprofile::where('user_id', $userid)->first(); 

        if($userprofile->usergroup_id == 4)
        {
            return TRUE;
        }
        if ($userid == '')
        {
            return FALSE;
        }
     
        return FALSE;
    }

    public static function getBitcoinWalletDetails($hashkey)
    {     
        $mode = getenv('BTC_MODE');
        if($mode == 'live')
        {
            $url = 'https://blockexplorer.com/api/tx/'.$hashkey;
        }
        else
        {
            $url = 'https://testnet.blockexplorer.com/api/tx/'.$hashkey;
        } 

       // $url = 'https://blockexplorer.com/api/tx/'.$hashkey;
        $ch  = curl_init();
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_json = curl_exec($ch);
        curl_close($ch);
        return $curl_json;
    }

    public static function convertBtcAmount($amount)
    {
        if (\Config::get('settings.currency') != 'USD')
        {             
            $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, 'https://openexchangerates.org/api/latest.json?app_id=35101787495844a0a0046ff25bd6604e&base=USD');
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $convertSiteCurrency = curl_exec($ch);              
          curl_close($ch);

          $convertSiteCurrency = json_decode($convertSiteCurrency, true);

          
          foreach ($convertSiteCurrency['rates'] as $key => $value)
          {
                if ($key == \Config::get('settings.currency'))
                {
                    $currencyvalue = $value;
                }
          }
         // echo $currencyvalue.'----';
           $amount = $amount / $currencyvalue; 
        }

        $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, "https://blockchain.info/tobtc?currency=USD&value=" . $amount);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          $btcamount = curl_exec($ch);              
          curl_close($ch);
        return $btcamount;
    }
}