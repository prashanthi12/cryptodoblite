<?php

namespace App\Services;
use App\SocialAccount;
use App\User;
use App\Userprofile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Useraccount;
use App\Transaction;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\RegisterNewUser;
// use App\Mail\AdminNotifyNewUser;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Events\NewUserRegister;
use Event;

class SocialTwitterAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        //dd($providerUser);
        $account = SocialAccount::whereProvider('twitter')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        $email = \Session::get('twitter_email');

        if ($account)
        {
            return $account->user;
        } 
        else 
        {
            $user = User::whereEmail($email)->first();
            if (!$user) 
            {
                $user = User::create([
                    'email' => $email,
                    'name' => $providerUser->getName(),
                    'password' => bcrypt(rand(1,10000)),
                ]);

                $account = SocialAccount::create([
                    'user_id' => $user->id,
                    'provider_user_id' => $providerUser->getId(),
                    'provider' => 'twitter',
                ]);       

            // $sponsorUser = User::where('email', Config::get('settings.default_sponser'))->first();
            // $sponsor = $sponsorUser->id;           

                $userprofile = new Userprofile;
                $userprofile->user_id = $user->id;
                $userprofile->usergroup_id = '4';
                $userprofile->sponsor_id = '7';
                $userprofile->active = 1;
                $userprofile->referral_group_id = '1';
                $userprofile->email_verified = 1;
                $userprofile->save();

                $account_no = "U-".(100000 + $user->id )."-"."1";
                $account = new Useraccount;
                $account->account_no = $account_no;
                $account->description = "User Operative Account";
                $account->user_id = $user->id;
                $account->accounttype_id = "3";
                $account->entity_reference_id = $userprofile->id;
                $account->entity_type = "Profile";
                $account->save();

                $transaction = new Transaction;
                $transaction->account_id = $account->id;
                $transaction->amount = "0.0";
                $transaction->type = "credit";
                $transaction->status ="1";
                $transaction->accounting_code_id = "1";
                $transaction->request = "{object:undefined}";
                $transaction->response = "{object:undefined}";
                $transaction->save();
      
                $admin = User::find(1);

                //firing an event
                Event::fire(new NewUserRegister($user, $userprofile, $admin));
                
                // Mail::to($user->email)->queue(new RegisterNewUser($user));
                // Mail::to($admin->email)->send(new AdminNotifyNewUser($user));
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
}