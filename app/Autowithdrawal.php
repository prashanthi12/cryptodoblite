<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Autowithdrawal extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'amount', 'user_id', 'payaccount_id', 'status'
    ];
    protected $dates = ['deleted_at'];
}
