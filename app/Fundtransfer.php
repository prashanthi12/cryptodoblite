<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fundtransfer extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = "App\Presenters\UserPresenter";

    protected $table = "fundtransfers";

    protected $fillable = [
        'amount', 'from_account_id', 'debit_transaction_id', 'to_account_id', 'credit_transaction_id', 'admin_commission', 'comments', 'active'
    ]; 

    protected $dates = ['deleted_at'];
    
    public function user() {
        return $this->belongsTo('App\User');
    }

    public function transaction() {
        return $this->belongsTo('App\Transaction');
    }

    public function fundtransfer_from_id() {
        return $this->belongsTo('App\Useraccount', 'from_account_id', 'id')->withTrashed();
    }

    public function fundtransfer_to_id() {
        return $this->belongsTo('App\Useraccount', 'to_account_id', 'id')->withTrashed();
    }

    
}
