<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Settings extends Model
{
	use CrudTrait;
	
    protected $fillable = [
        'key', 'name', 'email', 'password', 'description', 'value', 'field', 'active',
    ];
    
}
