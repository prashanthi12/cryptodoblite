<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	protected $fillable = [
        'name', 'short_name', 'iso_code', 'tel_prefix'
    ];

    public function userprofile() {
        return $this->belongsTo('App\Userprofile', 'country', 'id');
    }
}
