<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TicketStatus extends Model
{
	use CrudTrait;

	protected $fillable = [
        'name', 'color'
    ];
    
    public function status()
    {
        return $this->hasMany('App\Ticket', 'status_id', 'id');
    }

}
