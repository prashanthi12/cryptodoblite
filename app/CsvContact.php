<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CsvContact extends Model
{
    public $fillable = ['first_name', 'last_name', 'email'];
}
