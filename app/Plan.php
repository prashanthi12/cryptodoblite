<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Plan extends Model
{
    use CrudTrait;

    protected $fillable = [
        'image', 'name','plantype_id', 'min_amount', 'max_amount', 'interest_rate','duration','duration_key','publish','active','principle_return','partial_withdraw_status','max_partial_withdraw_limit','minimum_locking_period','partial_withdraw_fee','compounding','orderby'
    ];

    /**
     * Return the type of the plan
     *
     * @return array
     */
    public function plantype()
    {
    	return $this->belongsTo('App\Plantype', 'plantype_id', 'id');
    }

    /**
     * Returns the deposits of the plan.
     *
     * @return type
     */
    public function deposits()
    {
        return $this->hasMany('App\Deposit');
    }

    public function activeDeposits()
    {
        return $this->hasMany('App\Deposit')->where('status', 'active');
    }

    /**
     * Return query filter by active
     *
     * @param Object $query
     * @return Object
     */
    public function scopeActive($query)
    {
        return $query->where('active', VAL_ACTIVE_INT);
    }
}
