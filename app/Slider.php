<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Slider extends Model
{
	use CrudTrait;

    protected $fillable = [
        'image', 'slidertext', 'url', 'urltext', 'active'
    ];
     
}
