<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class Deposit extends Model
{
    use PresentableTrait, SoftDeletes;

    protected $presenter = "App\Presenters\TransactionPresenter";

    protected $fillable = [
        'transaction_id', 'status','amount','plan_id', 'user_id', 'paymentgateway_id', 'approved_on', 'comments_on_approval', 'matured_on', 'comments_on_maturity', 'released_on', 'comments_on_release', 'archived_on', 'comments_on_archive', 'rejected_on', 'comments_on_reject', 'problem_on', 'comments_on_problem'
    ];

    protected $dates = ['approved_on', 'matured_on', 'released_on', 'archived_on', 'rejected_on', 'problem_on', 'deleted_at'];

    protected $append = ['levelcommission', 'paymentGatewayName', 'planName'];

    public function transaction () {
    	return $this->hasOne('App\Transaction', 'id', 'transaction_id');
    }

    public function plan() {
    	return $this->belongsTo('App\Plan');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function paymentgateway() {
            return $this->belongsTo('App\Paymentgateway');
    }

    public function interest() {
        return $this->hasMany('App\Interest');
    }

//query scope on deposits

    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function scopeNew($query)
    {
        return $query->where('status', 'new');
    }

    public function scopeMatured($query)
    {
        return $query->where('status', 'matured');
    }

    public function scopeReleased($query)
    {
        return $query->where('status', 'released');
    }

    public function scopePlanID($query, $arg_plan_id)
    {
        return $query->where('plan_id', $arg_plan_id);
    }

    public function getLevelcommissionAttribute() 
    {    
        $user = User::where('id', $this->user_id)->with('userprofile')->first();

        $uplines = $user->uplines;

        $benificiaryUsers = collect();

        foreach ($uplines as $key =>$upline) {
                $uplineUser = User::find($upline);
                $benificiaryUsers->push($uplineUser);
        }
        
        $levelCommission = collect();
        
        foreach($benificiaryUsers as $key=>$benificiaryUser) {
            $levelCommissionArray = json_decode($benificiaryUser->referralgroup->level_commission);    

           
            $maxCommission = count($levelCommissionArray);

            $commission = new \stdClass;
            $commission->level = $key+1;
            $commission->beneficiary = $benificiaryUser;
          
            if($key < $maxCommission){
                     $commission->percentage = $levelCommissionArray[$key]->commission_value;
                     $commission->commission = $levelCommissionArray[$key]->commission_value * $this->amount * 0.01 ;
                 } else {
                     $commission->percentage = 0;
                     $commission->commission = 0 ;
                 }      
                 $levelCommission->push($commission);
            }
         
            return $levelCommission;
    }

    public function getPaymentGatewayNameAttribute()
    {
        return $this->paymentgateway->displayname;
    }

    public function getPlanNameAttribute()
    {
        return $this->plan->name;
    }

}
