<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Paymentgateway extends Model
{
	use CrudTrait;
	
    protected $fillable = [
        'gatewayname', 'displayname','active','deposit', 'withdraw','e-wallet','withdraw_commission','deposit_fee_status','deposit_fee_type','deposit_fee_value','exchange','params', 'instructions',
    ];

    public function deposits() 
    {
    	return $this->hasMany('App\Deposit');
    }

    public function ewallet()
    {
    	return $this->belongsTo('App\Ewallet', 'paymentgateway_id', 'id');
    }
    
}
