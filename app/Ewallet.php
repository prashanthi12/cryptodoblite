<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class Ewallet extends Model
{
	use PresentableTrait;

 	protected $presenter = "App\Presenters\UserPresenter";
 	
    protected $table = 'ewallet';

    protected $fillable = ['from_user_id','to_user_id','amount','paymentgateway_id','status','transaction_id','request','response','bitcoin_hash_id','bitcoin_address','type','cancel_at','approve_at','comments_approve','comments_cancel','process_via','comments_pending','bonus_amount','received_amount','btc_amount','total_btc_amount'];
    protected $dates = ['cancel_at','approve_at'];
    protected $with = ['paymentgateway'];

    public function FundUser()
    {
    	return $this->belongsTo('App\User','from_user_id');
    }

    public function paymentgateway()
    {
    	return $this->hasOne('App\Paymentgateway', 'id', 'paymentgateway_id');
    }
    
}
