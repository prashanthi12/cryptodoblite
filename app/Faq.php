<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Faq extends Model
{
	use CrudTrait;

    protected $fillable = [
        'title', 'description', 'order', 'language', 'active'
    ]; 

    public function getDescriptionAttribute($description)
    {
    	return \Purify::clean($description);
    }
   
}
