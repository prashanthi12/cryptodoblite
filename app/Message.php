<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;


class Message extends Model
{
    use PresentableTrait;
    use SoftDeletes;

    protected $presenter = "App\Presenters\UserPresenter";

    protected $table = "messages";

    protected $appends = array('message');

    protected $fillable = [
        'message', 'is_seen', 'deleted_from_sender', 'user_id', 'conversation_id'
    ]; 

    protected $dates = ['deleted_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function conversation() {
        return $this->belongsTo('App\Conversation');
    }

    public function getMessageAttribute($message)
    {
        return \Purify::clean($message);
    }

}