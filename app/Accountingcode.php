<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Accountingcode extends Model
{
	use CrudTrait;
	
    protected $fillable = [
        'accounting_code', 'active',
    ];

    public function transaction() {
    	return $this->hasMany('App\Transaction');
    }
    
}
