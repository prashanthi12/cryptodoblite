<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plantype extends Model
{
    protected $fillable = [
        'plantype', 'active',
    ];

    public function plans() {
    	return $this->hasMany('App\Plan', 'plantype_id', 'id');
    }
}
