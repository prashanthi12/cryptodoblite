<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Firewall extends Model
{
    use CrudTrait;

    protected $table = 'firewall';
    protected $fillable = [
    	'ip_address', 'whitelisted'
    ];
    
}
