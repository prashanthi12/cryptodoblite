<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\Crontestmail;
use App\User;
use App\Traits\LogActivity;

class CronTest extends Command
{

    use LogActivity;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:cron_test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Live cron test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // dd($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
     
         // Mail::to('anbu@gegosoft.com')->queue(new Crontestmail());

        $user = User::find('6');
        // dd($user);
        $this->doActivityLog(
                $user,
                $user,
                ['ip' => request()->ip()],
                'Cron',
                'Cron run successfully'
            );
    }

}
