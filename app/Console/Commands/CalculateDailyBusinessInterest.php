<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CalculatesInterests;

class CalculateDailyBusinessInterest extends Command
{
    use CalculatesInterests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:daily_business_interest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Daily Business Interest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setPlanTypeID(PLANTYPE_DAILY_BUSINESS);
        // dd($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $activedeposits = $this->get_deposits();
        // dd($activedeposits);

        $plans = $this->get_plans();
        // dd($plans);

        foreach ($plans as $plan)
        {
            $this->calcuateInterests($plan->activeDeposits, $plan);
        }
    }

}
