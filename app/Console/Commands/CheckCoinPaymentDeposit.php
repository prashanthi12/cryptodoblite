<?php

namespace App\Console\Commands;
use App\Deposit;
use App\User;
use App\Transaction;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Notifications\User\BitcoinDepositActive;
use App\Traits\DepositProcess;
use App\Helpers\HyipHelper;
use App\Classes\CoinPayments;
use App\Paymentgateway;
use Illuminate\Support\Facades\Mail;
use App\Mail\CoinPaymentNotification;
use Illuminate\Bus\Queueable;

class CheckCoinPaymentDeposit extends Command
{
     use DepositProcess;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:check_coin_payment_deposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Bitcoin Coin Payment Deposit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //dd(Carbon::now()->format('d/m/Y H:i:s'));
        $deposits = Deposit::where([
                    'paymentgateway_id'   => '7',
                    'status'   => 'new'
            
        ])->get();

        $pgs = Paymentgateway::where([
                ['active', '=', '1'],
                ['deposit', '=', '1'],
                ['id', '=', '7']
        ])->first(); 

        $paymentparams = json_decode($pgs->params, true);

        $cps = new CoinPayments();
        $cps->Setup( $paymentparams['private_key'], $paymentparams['public_key']);
        //dd($cps);

        if (!$deposits->isEmpty())
        {
            foreach ($deposits as $deposit)
            {
                $transaction = Transaction::where('id', $deposit['transaction_id'])->first();
                $response_json = json_decode($transaction->response, true);           
                //dd($response_json['coinamount']);
               
                $result = $cps->get_tx_info($deposit->param1);

                if($result['result']['status'] == 100)
                {
                    if ($result['result']['receivedf'] == $response_json['coinamount'])
                    {
                        $depositapprove = $this->approve($deposit['id']);
                        if ($depositapprove)
                        {
                            $user = User::where('id', $deposit['user_id'])->first();
                            $user->notify(new CoinpaymentDepositActive);
                        }               

                    }
                    else
                    {     
                            $deposit = Deposit::find($deposit['id']);
                            $deposit->status = "problem";
                            $deposit->comments_on_problem = "amount different than actual amount";
                            $deposit->problem_on = Carbon::now();   
                            $deposit->save();  
                            // $admin = User::find(1);
                            // Mail::to($admin->email)->queue(new CoinPaymentNotification($deposit));                  
                        
                    }
                    
                }            
                                   
            }
        }
    }
}
