<?php

namespace App\Console\Commands;
use App\Deposit;
use App\User;
use App\Transaction;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Notifications\User\BitcoinDepositActive;
use App\Traits\DepositProcess;
use App\Helpers\HyipHelper;

class CheckBitcoinDeposit extends Command
{
     use DepositProcess;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:check_bitcoin_deposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Bitcoin New Deposit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //dd(Carbon::now()->format('d/m/Y H:i:s'));
        $deposits = Deposit::where([
                    'paymentgateway_id'   => '9',
                    'status'   => 'new'
            
            ])->get();
        //dd($deposits);
        if (!$deposits->isEmpty())
        {
            foreach ($deposits as $deposit)
            {
                $transaction = Transaction::where('id', $deposit['transaction_id'])->first();
                $request_json = json_decode($transaction->request, true);
                $response_json = json_decode($transaction->response, true);           
          
                $curl_json = HyipHelper::getBitcoinWalletDetails($response_json['hashid']);

                $curl_json = json_decode($curl_json, true);

                $received_amount = '';
                $received_address = '';
                 foreach ($curl_json['vout'] as $vout)
                 {
                    if ($vout['scriptPubKey']['addresses'][0] == $request_json['admin_address'])
                    {
                        $received_amount .= $vout['value'];
                        $received_address .= $vout['scriptPubKey']['addresses'][0];
                    }                               
                 }
                // dd($received_amount);
          
                $hashid = $curl_json['txid'];
                $confirmations = $curl_json['confirmations'];
                $bitcoin_transaction_time = date("Y-m-d", $curl_json['time']);

                $deposit_created_date = $deposit->created_at->format('Y-m-d');

                //dd($confirmations);

                // echo $received_amount.'------------'.$request_json['btcamount'];

                // echo $received_address.'---------------'.$request_json['admin_address'];
                
                if ($confirmations == 1 && $received_amount == $request_json['btcamount'] 
                    && $received_address == $request_json['admin_address'] )
                {
                    // dd('33');
                  
                     $depositapprove = $this->approve($deposit['id']);
                    
                    if ( $depositapprove->save())
                    {
                            $user = User::where('id', $deposit['user_id'])->first();
                            $user->notify(new BitcoinDepositActive);
                    }

                }
                                   
            }
        }
        
    }
}
