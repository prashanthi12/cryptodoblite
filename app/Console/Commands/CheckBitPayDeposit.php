<?php

namespace App\Console\Commands;
use App\Deposit;
use Illuminate\Console\Command;
use App\Traits\DepositProcess;

class CheckBitPayDeposit extends Command
{
     use DepositProcess;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:check_bitpay_deposit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Bitpay New Deposit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $deposits = Deposit::where([
                    'paymentgateway_id'   => '8',
                    'status'   => 'new'
            
            ])->get();
        //dd($deposits);
        if (!$deposits->isEmpty())
        {
            foreach ($deposits as $deposit)
            {

                require_once app_path().'/Http/library/bitcoin/bp_lib.php';  
                $bitcoin_invoice= bpGetInvoice($deposit->bitpay_invoice_id);
                
                if(isset($bitcoin_invoice['status']))
                {
                        if(($bitcoin_invoice['status']=='paid')||($bitcoin_invoice['status']=='confirmed')||($bitcoin_invoice['status']=='complete'))
                           {
                        
                                $depositapprove = $this->approve($deposit->id);
                       
                            }
                }
                                   
            }
        }
        
    }
}
