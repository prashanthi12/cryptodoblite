<?php

namespace App\Console\Commands;
use App\Deposit;
use App\User;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Notifications\User\MatureDeposit;

class MatureDepositAction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:mature_deposit_action';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Mature Deposits';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deposits = Deposit::where('status', 'active')->with('plan')->get();
        
        //dd(date('Y-m-d H:i:s'));
        if (!$deposits->isEmpty())
        {
            foreach ($deposits as $deposit)
            {
                if ($deposit->plan->duration != '-1')
                {
                    if (!is_null($deposit->matured_on))
                    {
                         if ($deposit->matured_on < Carbon::now())
                        {
                            $maturedeposit = Deposit::where('id', $deposit->id)->with('user')->first();
                            $user = User::where('id', $maturedeposit->user_id)->first();
                            $maturedeposit->status = 'matured';
                            $maturedeposit->comments_on_maturity = 'auto matured on '.Carbon::now();
                            $maturedeposit->save();
                            $user->notify(new MatureDeposit);

                        }
                    }               
                }         

            }
        }
    }
}
