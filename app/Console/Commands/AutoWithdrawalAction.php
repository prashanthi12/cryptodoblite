<?php

namespace App\Console\Commands;
use App\User;
use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Settings;
use App\Autowithdrawal;
use App\Traits\WithdrawProcess;
use App\Userpayaccounts;
use App\Notifications\User\AutoWithdrawalSend;
use App\Traits\UserInfo;

class AutoWithdrawalAction extends Command
{
    use WithdrawProcess, UserInfo;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:auto_withdrawal_action';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic Withdrawal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $autoWithdrawalStatus = Settings::where('key' , 'auto_withdrawal_status')->first();

        if ($autoWithdrawalStatus->value == 1)
        {
                $autowithdrawal = Autowithdrawal::where([
                                ['status', 1],
                                ])->get();

            if (!$autowithdrawal->isEmpty())
            {
            foreach($autowithdrawal as $data)
            {
                $user = User::where('id', $data['user_id'])->with(['userprofile','useraccounts','deposits'])->first();

                $userbalance = $this->getUserBalance($user);

                if ($data['amount'] < $userbalance)
                {
                    $request = [];
                $request = array_merge($request, ['amount' => $data['amount']]);
                $request = array_merge($request, ['userpayaccountid' => $data['payaccount_id']]);
                $request = array_merge($request, ['mobileotp' => '']);
                $request = array_merge($request, ['otpcode' => '']);
                $request = array_merge($request, ['user_id' => $data['user_id']]);
                $request = (object)$request;          
               
               $withdraw = $this->createNewWithdraw($request);

                   if ($withdraw)
                   {
                        $getPaymentId = Userpayaccounts::where('id' , $data['payaccount_id'])->first();
                        

                        $result = [];
                        $result = array_merge($result, ['amount' => $data['amount']]);
                        $result = array_merge($result, ['userpayaccountid' => $data['payaccount_id']]);
                        $result = array_merge($result, ['mobileotp' => '']);
                        $result = array_merge($result, ['otpcode' => '']);
                        $result = array_merge($result, ['user_id' => $data['user_id']]);
                        $result = array_merge($result, ['otpcode' => '']);
                        $result = array_merge($result, ['paymentgateway' => $getPaymentId->paymentgateways_id]);
                        $result = array_merge($result, ['withdrawid' => $withdraw->id]);
                        $result = (object)$result;
                        // dd($result);
                        $withdrawresult = $this->withdrawrequest($result, $withdraw);
                        if ($withdrawresult)
                        {
                            $withdrawresult  = Withdraw::where('id', '=', $withdrawresult->id)->first();
                            $withdrawresult->autowithdraw = 1;
                            $withdrawresult->save();

                            $autowithdrawalresult  = Autowithdrawal::where('id', '=', $data['id'])->first();
                            $autowithdrawalresult->amount = 0;
                            $autowithdrawalresult->save();
                            $user->notify(new AutoWithdrawalSend);
                        }            

                   }      


                }

                
            }
           }

        }

    
        
    }
}
