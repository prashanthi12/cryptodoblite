<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CalculatesInterests;

class CalculateDailyVariableInterest extends Command
{
    use CalculatesInterests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:daily_variable_interest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Daily Variable Interest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setPlanTypeID(PLANTYPE_DAILY_VARIABLE_INTEREST);
        // dd($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $plans = $this->get_plans();
        
        foreach ($plans as $plan)
        {
            $this->calcuateInterests($plan->activeDeposits, $plan);
        }
    }

}
