<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Userprofile;
use App\Useraccount;
use App\Placement;
use App\Referralgroup;
use App\Transaction;
use App\Settings;
use Carbon\Carbon;

class CreateRootUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:rootuser {--default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Root User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $width = Settings::where('key', 'matrix_width')->pluck('value');
        $width = $width[0];
/*
        $user = new User;
        $user->name = "root";
        $user->email = "root@larahyip.com";
        $user->password = bcrypt("root");
        $user->save();*/
        if($this->option('default')) {
            $username = "root";
            $email = "root@larahyip.com";
        }else {
            $username = $this->ask('Enter the username');
            $email = $this->ask('Enter the email'); 
        }
      
        $password=$username;

        $user = new User;
        $user->name = $username;
        $user->email =$email;
        $user->password = bcrypt($password);
        $user->save();

        $this->info("Root user added {$user->name} - User Id : {$user->id}");

        $update=[
                    'value'=>$email,
                 ];
        Settings::where('key', 'default_sponser')->update($update);

        $referralgroup = Referralgroup::where([
                        ['active', '1'],
                        ['is_default', '1'],
                    ])->first();

        $userprofile = new Userprofile;
        $userprofile->user_id = $user->id;
        $userprofile->usergroup_id = "4";       
        $userprofile->referral_group_id = $referralgroup->id;
        $userprofile->save();

        $account_no = "U-".(100000 + $user->id )."-"."1";

        $account = new Useraccount;
        $account->account_no = $account_no;
        $account->description = "User Operative Account";
        $account->user_id = $user->id;
        $account->accounttype_id = "3";
        $account->entity_reference_id = $userprofile->id;
        $account->entity_type = "Profile";
        $account->save();

        $transaction = new Transaction;
        $transaction->account_id = $account->id;
        $transaction->amount = "0.0";
        $transaction->type = "credit";
        $transaction->status ="1";
        $transaction->accounting_code_id = "1";
        $transaction->request = "{object:undefined}";
        $transaction->response = "{object:undefined}";
        $transaction->save();

        
            $root = new Placement;
            $root->user_id = $user->id;
            $root->root_id = "0";
            $root->spillover_id = "0";
            $root->alphakey = "A";
            $root->active = "1";
            $root->save();
        
        
        if ($width==0) {
            $width=$width+1;
        }
    
      
        for ($i =0; $i  <  $width; $i++) {
                    $placement = new Placement;
                    $placement->root_id = 1;
                    $placement->spillover_id = $user->id;
                    $placement->alphakey = $root->alphakey;
                    $placement->active = "0";
                    $placement->save();
        };
    }
}
