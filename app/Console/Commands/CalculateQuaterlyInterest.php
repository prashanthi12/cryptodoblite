<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\CalculatesInterests;

class CalculateQuaterlyInterest extends Command
{
    use CalculatesInterests;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larahyip:quaterly_interest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Quaterly Interest';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->setPlanTypeID(PLANTYPE_QUATERLY);
        // dd($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $activedeposits = $this->get_deposits();
        // dd($activedeposits);

        $plans = $this->get_plans();
        // dd($plans);

        foreach ($plans as $plan)
        {
            $this->calcuateInterests($plan->activeDeposits, $plan);
        }
    }

}
