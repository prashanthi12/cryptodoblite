<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Mailtemplate;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The contact instance.
     *
     * @var Contact
     */
    protected $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->contact);
        return $this->markdown('emails.contact')
                    ->with([
                        'queries' => $this->contact->qu,
                        'message' => $this->invite->message,
                        'sender' => $user->name,
                        'actionText' => 'Join Now',
                        'actionUrl' => url(),
                        'thanks_regards_text' => trans('mail.thanks_regards_text')
                    ]);
                    
        /*$admin = User::find(2);
        $contact = Mailtemplate::where([['name','contact'],['status','active']])->first();
        $subject = $contact->subject;
        $mail_content = $contact->mail_content;

        $mail_content = str_replace(":name", $user->name, $mail_content);
        $mail_content = str_replace(":queries", $this->contact->qu, $mail_content);
        $mail_content = str_replace(":contactno", $this->sendmail->sendmail->message, $mail_content);
        $mail_content = str_replace(":skypeid", $url, $mail_content);

        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);   */        
    }
}
