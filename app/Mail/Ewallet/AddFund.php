<?php

namespace App\Mail\Ewallet;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Models\Mailtemplate;

class AddFund extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $ewallet;

    public function __construct($ewallet)
    {
        $this->ewallet = $ewallet;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = User::where('id', $this->ewallet['to_user_id'])->with('userprofile')->first();
        $name = $user->name;
         
        if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        {
            $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        } 

        $user_from = User::where('id', $this->ewallet['from_user_id'])->with('userprofile')->first();
        $fromname = $user_from->name;
        // dd($this->ewallet);
        $amount=$this->ewallet['amount'];
        $btc_amount=$this->ewallet['total_btc_amount'];
       
       /* return $this->subject('Add Fund To E-Wallet')
                    ->markdown('emails.ewallet.addfund') ->with([
                    'name' => $name,
                    'fromname'=>$fromname,
                    'amount'=>$amount,
                    'btc_amount'=>$btc_amount,                
                ]);*/

        $addfund = Mailtemplate::where([['name','add_fund'],['status','active']])->first();
        $subject = $addfund->subject;
        $mail_content = $addfund->mail_content;

        $mail_content = str_replace(":name", $name, $mail_content); 
        $mail_content = str_replace(":fromname", $fromname, $mail_content);
        $mail_content = str_replace(":amount", $amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":btc_amount", $btc_amount, $mail_content);
        $mail_content = str_replace(":donationcurrency", \config::get('settings.donation_currency'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
