<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Userprofile;
use App\Events\AdminKYCReject;
use App\Models\Mailtemplate;

class KYCReject extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The userdetails instance.
     *
     * @var userdetails
     */
    protected $userdetails;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminKYCReject $userdetails)
    {
        $this->userdetails = $userdetails; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*return $this->markdown('emails.admin.kycreject')
                    ->with([
                        'name' => $this->userdetails->user->userprofile->firstname.' '. $this->userdetails->user->userprofile->lastname,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $kycreject = Mailtemplate::where([['name','kyc_reject'],['status','active']])->first();
        $subject = $kycreject->subject;
        $mail_content = $kycreject->mail_content;

        $mail_content = str_replace(":name", $this->userdetails->user->userprofile->firstname.' '. $this->userdetails->user->userprofile->lastname, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
