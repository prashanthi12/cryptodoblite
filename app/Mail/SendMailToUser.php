<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\MailMessage;
use App\Models\Mailtemplate;

class SendMailToUser extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

	/**
     * The User instance.
     *
     * @var User
     */
    protected $sendmail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MailMessage $sendmail)
    {
        $this->sendmail = $sendmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = User::where('id', $this->sendmail->to_user_id)->first();
      
        /*return $this->subject($this->sendmail->subject)->markdown('emails.admin.sendmail')->with(
        [                                           
            'username' => $user->name,   
            'message' => $this->sendmail->message,                                             
            'signature' => trans('mail.signature'),                       
        ]);*/

        $sendmail = Mailtemplate::where([['name','send_mail_to_user'],['status','active']])->first();
        $subject = $sendmail->subject;
        $mail_content = $sendmail->mail_content;

        $mail_content = str_replace(":name", $user->name, $mail_content); 
        $mail_content = str_replace(":message", $this->sendmail->message, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
    
}