<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Fundtransfer;
// use App\Useraccount;
use App\Events\FundTransferMail;
use App\Models\Mailtemplate;

class FundTransferSender extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $result;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($result)
    {
        $this->result = $result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        // $fromuser = Useraccount::where('id', $this->result->from_account_id)->first();
        // $senderID = $fromuser->user_id;
        // $sender = User::where('id', $senderID)->first();
        // $touser = Useraccount::where('id', $this->result->to_account_id)->first();
        // $receiverID = $touser->user_id;
        // $receiver = User::where('id', $receiverID)->first();

        /*return $this->markdown('emails.fundtransfer.fundtransfersender')
                    ->with([
                        'amount' => $this->result->result->amount,
                        'sender' => $this->result->sender->name,
                        'receiver' => $this->result->user->name,
                        'signature' => trans('mail.signature'),
        ]);*/

        $fundtransfersender = Mailtemplate::where([['name','fund_transfer_sender'],['status','active']])->first();
        $subject = $fundtransfersender->subject;
        $mail_content = $fundtransfersender->mail_content;

        $mail_content = str_replace(":name", $this->result->sender->name, $mail_content);
        $mail_content = str_replace(":receiver", $this->result->user->name, $mail_content);
        $mail_content = str_replace(":amount", $this->result->result->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
