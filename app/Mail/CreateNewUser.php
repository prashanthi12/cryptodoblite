<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
use App\Events\AdminsCreateNewUser;
use App\Models\Mailtemplate;

class CreateNewUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The password instance.
     *
     * @var password
     */
    protected $password;

    /**
     * The user instance.
     *
     * @var user
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminsCreateNewUser $user)
    {
        $this->user = $user;
       // $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->user);
        /*return $this->markdown('emails.admin.createnewuser')
                    ->with([  
                        'message' => trans('mail.create_new_user_content'), 
                        'login_email_text' =>  trans('mail.admin_new_user_login_email_text', [ 'email' => $this->user->user->email]),
                        'login_password_text' => trans('mail.admin_new_user_login_password_text', [ 'password' => $this->user->password]),                   
                        'name' => $this->user->user->name,  
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/login')
        ]);*/

        $url = url('/login');
        $newuser = Mailtemplate::where([['name','create_new_user'],['status','active']])->first();
        $subject = $newuser->subject;
        $mail_content = $newuser->mail_content;

        $mail_content = str_replace(":name", $this->user->user->name, $mail_content);
        $mail_content = str_replace(":email", $this->user->user->email, $mail_content);
        $mail_content = str_replace(":password", $this->user->password, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
