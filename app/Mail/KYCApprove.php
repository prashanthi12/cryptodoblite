<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Userprofile;
use App\Events\AdminKYCApprove;
use App\Models\Mailtemplate;

class KYCApprove extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The userdetails instance.
     *
     * @var userdetails
     */
    protected $userdetails;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminKYCApprove $userdetails)
    {
        $this->userdetails = $userdetails; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*return $this->markdown('emails.admin.kycapprove')
                    ->with([
                        'name' => $this->userdetails->user->userprofile->firstname.' '. $this->userdetails->user->userprofile->lastname,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $kycapprove = Mailtemplate::where([['name','kyc_approve'],['status','active']])->first();
        $subject = $kycapprove->subject;
        $mail_content = $kycapprove->mail_content;

        $mail_content = str_replace(":name", $this->userdetails->user->userprofile->firstname.' '. $this->userdetails->user->userprofile->lastname, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
