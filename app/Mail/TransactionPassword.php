<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\TransactionPasswordSaved;
use App\Models\Mailtemplate;

class TransactionPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    protected $userprofile;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TransactionPasswordSaved $userprofile)
    {
        $this->userprofile = $userprofile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
     
        //dd($user);
        /*return $this->markdown('emails.profile.transactionpassword')
                    ->with([                        
                        'message' => trans('mail.transaction_password_changed'),
                        'name' => $this->userprofile->user->name,  
                        'signature' => trans('mail.signature'),
        ]);*/

        $transactionpassword = Mailtemplate::where([['name','transaction_password'],['status','active']])->first();
        $subject = $transactionpassword->subject;
        $mail_content = $transactionpassword->mail_content;

        $mail_content = str_replace(":name", $this->userprofile->user->name, $mail_content); 
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}