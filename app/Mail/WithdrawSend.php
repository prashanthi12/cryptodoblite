<?php

namespace App\Mail;
use App\Withdraw;
use App\User;
use App\Userprofile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserWithdrawSend;
use App\Models\Mailtemplate;

class WithdrawSend extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The contact instance.
     *
     * @var Withdraw
     */
    protected $withdraw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserWithdrawSend $withdraw)
    {
        $this->withdraw = $withdraw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->withdraw);
        //$user = User::where('id', Auth::id())->with('userprofile')->first();
       // $user = User::where('id', $this->withdraw->user_id)->with('userprofile')->first();

        /*return $this->markdown('emails.withdraw.withdrawrequest')
                    ->with([
                        'amount' => $this->withdraw->result->amount,
                        'name' => $this->withdraw->user->name,
                        'user_signature' => trans('mail.user_signature')
                    ]);*/

        $withdrawsend = Mailtemplate::where([['name','withdraw_send'],['status','active']])->first();
        $subject = $withdrawsend->subject;
        $mail_content = $withdrawsend->mail_content;

        $mail_content = str_replace(":name", $this->withdraw->user->name, $mail_content); 
        $mail_content = str_replace(":amount", $this->withdraw->result->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
