<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
// use App\Plan;
// use App\Transaction;
use App\Events\UserActiveDeposit;
use App\Models\Mailtemplate;

class ActiveDepositSuccessfull extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserActiveDeposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*return $this->markdown('emails.deposit.activedepositsuccessfull')
                ->with([
                    'amount' => $this->deposit->deposit->amount,
                    'name' => $this->deposit->user->name,
                    'plan_name' => $this->deposit->deposit->plan_id,
                   // 'transaction_number' => $response_json['transaction_number'],
                    'signature' => trans('mail.signature')
                ]);*/

        $activedeposit = Mailtemplate::where([['name','active_deposit_successfull'],['status','active']])->first();
        $subject = $activedeposit->subject;
        $mail_content = $activedeposit->mail_content;

        $mail_content = str_replace(":name", $this->deposit->user->name, $mail_content);
        $mail_content = str_replace(":amount", $this->deposit->deposit->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":plan_name", $this->deposit->deposit->plan_id, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);  
    }
}
