<?php

namespace App\Mail;
use App\Withdraw;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminWithdrawReject;
use App\Models\Mailtemplate;

class WithdrawReject extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Withdraw
     */
    protected $withdraw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminWithdrawReject $withdraw)
    {
         $this->withdraw = $withdraw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->withdraw);
      //  $user = User::where('id', $this->withdraw->user_id)->with('userprofile')->first();
        //dd($user);
        /*return $this->markdown('emails.withdraw.withdrawreject')
                    ->with([
                        'comments' => $this->withdraw->result->comments_on_rejected,
                        'amount' => $this->withdraw->result->amount,
                        'name' => $this->withdraw->user->name,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $withdrawreject = Mailtemplate::where([['name','withdraw_reject'],['status','active']])->first();
        $subject = $withdrawreject->subject;
        $mail_content = $withdrawreject->mail_content;

        $mail_content = str_replace(":name", $this->withdraw->user->name, $mail_content); 
        $mail_content = str_replace(":amount", $this->withdraw->result->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":comments", $this->withdraw->result->comments_on_rejected, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
