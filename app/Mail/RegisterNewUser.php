<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Events\NewUserRegister;
use App\Models\Mailtemplate;

class RegisterNewUser extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->user->name);
        /*return $this->markdown('emails.registernewuser')
                    ->with([  
                        'message' => trans('mail.new_user_registered_content'),                                 
                        'name' => $this->user->user->name,  
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/login')
                    ]);*/

        $url = url('/login');
        $regnewuser = Mailtemplate::where([['name','register_new_user'],['status','active']])->first();
        $subject = $regnewuser->subject;
        $mail_content = $regnewuser->mail_content;

        $mail_content = str_replace(":name", $this->user->name, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}