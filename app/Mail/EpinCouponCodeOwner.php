<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Epin;
use App\Models\Mailtemplate;

class EpinCouponCodeOwner extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $deposit_result;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Epin $deposit_result)
    {
       // $this->user = $user;
        $this->deposit_result = $deposit_result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        $ownerId = $this->deposit_result->owner;
        $userId = $this->deposit_result->used_by;
        $owner = User::where('id', $ownerId)->first();
        $user = User::where('id', $userId)->first();
       // dd($user);
        /*return $this->markdown('emails.deposit.epincouponcodeowner')
                    ->with([
                        'name' => $owner->name,
                        'user' => $user->name,
                        'amount' => $this->deposit_result->coupon_value,
                        'couponcode' => $this->deposit_result->coupon_code,
                        'signature' => trans('mail.signature')
        ]);*/

        $couponcodeowner = Mailtemplate::where([['name','epin_couponcode_owner'],['status','active']])->first();
        $subject = $couponcodeowner->subject;
        $mail_content = $couponcodeowner->mail_content;

        $mail_content = str_replace(":name", $owner->name, $mail_content);
        $mail_content = str_replace(":user", $user->name, $mail_content);
        $mail_content = str_replace(":amount", $this->deposit_result->coupon_value, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":couponcode", $this->deposit_result->coupon_code, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
