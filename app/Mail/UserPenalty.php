<?php

namespace App\Mail;
// use App\Penalty;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminSendPenalty;
use App\Models\Mailtemplate;

class UserPenalty extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Penalty
     */
    protected $penalty;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminSendPenalty $penalty)
    {
        $this->penalty = $penalty;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $user = User::where('id', $this->penalty->user_id)->with('userprofile')->first();
        // $name = $user->name;

        //  if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        //  {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        //  }

        /*return $this->markdown('emails.senduserpenalty')
                    ->with([
                        'comment' => $this->penalty->penalty->comments,
                        'amount' => $this->penalty->penalty->amount,
                        'name' => $this->penalty->user->name,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $userpenalty = Mailtemplate::where([['name','user_penalty'],['status','active']])->first();
        $subject = $userpenalty->subject;
        $mail_content = $userpenalty->mail_content;

        $mail_content = str_replace(":name", $this->penalty->user->name, $mail_content); 
        $mail_content = str_replace(":amount", $this->penalty->penalty->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":comment", $this->penalty->penalty->comments, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
