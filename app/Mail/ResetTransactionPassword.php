<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
use App\Events\ResetTransactionPasswordSaved;
use App\Models\Mailtemplate;

class ResetTransactionPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The token instance.
     *
     * @var Token
     */
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ResetTransactionPasswordSaved $password)
    {
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $user = User::where('id', Auth::id())->with('userprofile')->first();
        // //dd($user);

        // $name = $user->name;
        // if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        // {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        // }

        /*return $this->markdown('emails.profile.reset-transactionpassword')
                    ->with([ 
                       'reset_transaction_password_content' => trans('mail.reset_transaction_password_content'),
                       'reset_transaction_password_link' => url('myaccount/transaction_password'),
                       'transaction_password_link_text' => trans('mail.transaction_password_link_text'),
                       'new_transaction_password_content' => trans('mail.new_transaction_password_content'),
                        'new_transaction_password' => $this->password->newpassword,
                        'name' => $this->password->user->name,  
                        'signature' => trans('mail.signature'),
        ]);*/

        $url = url('myaccount/transaction_password');
        $reset_transactionpassword = Mailtemplate::where([['name','reset_transaction_password'],['status','active']])->first();
        $subject = $reset_transactionpassword->subject;
        $mail_content = $reset_transactionpassword->mail_content;

        $mail_content = str_replace(":name", $this->password->user->name, $mail_content); 
        $mail_content = str_replace(":new_transaction_password", $this->password->newpassword, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}