<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Deposit;
use App\Plan;
use App\Models\Mailtemplate;

class AdminNotifyActiveDeposit extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;
   
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        $plan = Plan::where('id', $this->deposit->plan_id)->first(); 

        $user = User::where('id', Auth::id())->with('userprofile')->first();
        $name = $user->name;

        if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        {
            $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        } 
        $admin = User::find(2);
        $url = url('/admin/deposit/active');
        /*return $this->markdown('emails.deposit.adminnotifyactivedeposit')
                    ->with([
                        'content' => trans('mail.admin_notify_active_deposit_content', ['username' => $name]),
                        'plan_name' => $plan->name,
                        'deposited_amount' => $this->deposit->amount,
                        'active_deposit_link' => url('/admin/deposit/active'),
                        'active_deposit_link_text' => trans('mail.deposit_link_text'),
                        'signature' => trans('mail.signature'),
        ]);*/

        $activedeposit = Mailtemplate::where([['name','admin_notify_active_deposit'],['status','active']])->first();
        $subject = $activedeposit->subject;
        $mail_content = $activedeposit->mail_content;

        $mail_content = str_replace(":name", $admin->name, $mail_content);
        $mail_content = str_replace(":username", $name, $mail_content);
        $mail_content = str_replace(":deposited_amount", $this->deposit->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":plan_name", $plan->name, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);  
    }
}
