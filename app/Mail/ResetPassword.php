<?php

namespace App\Mail;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminUserResetPassword;
use App\Models\Mailtemplate;

class ResetPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The token instance.
     *
     * @var Token
     */
    protected $token;

    /**
     * The userdetails instance.
     *
     * @var userdetails
     */
    protected $userdetails;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminUserResetPassword $userdetails)
    {
        $this->userdetails = $userdetails; 
       // $this->token = $token; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->token);
        //$user = User::where('id', Auth::id())->with('userprofile')->first();
        
        /*return $this->markdown('emails.admin.resetpassword')
                    ->with([
                        'resetlink' => url('/password/reset/'.$this->userdetails->token),
                        'resetlinktext' => trans('mail.resetlinktext'),
                        'message' => trans('mail.reset_password_message'),
                        'username' => $this->userdetails->user->name,                        
                        'signature' => trans('mail.signature'),
                    ]);*/

        $url = url('/password/reset/'.$this->userdetails->token);
        $resetpassword = Mailtemplate::where([['name','reset_password'],['status','active']])->first();
        $subject = $resetpassword->subject;
        $mail_content = $resetpassword->mail_content;

        $mail_content = str_replace(":username", $this->userdetails->user->name, $mail_content);        
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
