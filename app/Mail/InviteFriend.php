<?php

namespace App\Mail;

use App\Invite;
use App\User;
use App\Userprofile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\InviteNewFriend;
use App\Models\Mailtemplate;

class InviteFriend extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The invite instance.
     *
     * @var Invite
     */
    protected $invite;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(InviteNewFriend $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = User::where('id', Auth::id())->with('userprofile')->first();
        //dd($user);
        /*return $this->markdown('emails.invite_friend')
                    ->with([
                        'url' => $this->invite->invite->link,
                        'message' => rawurldecode($this->invite->invite->message),
                        'sender' => $user->userprofile->firstname.' '. $user->userprofile->lastname,
                        'actionText' => trans('mail.invite_friend_action_button_text'),
                        'user_signature' => trans('mail.user_signature')
                    ]);*/

        $invite_friend = Mailtemplate::where([['name','invite_friend'],['status','active']])->first();
        $subject = $invite_friend->subject;
        $mail_content = $invite_friend->mail_content;

        $mail_content = str_replace(":name", $user->userprofile->firstname.' '. $user->userprofile->lastname, $mail_content);
        $mail_content = str_replace(":message", rawurldecode($this->invite->invite->message), $mail_content);
        $mail_content = str_replace(":url", $this->invite->invite->link, $mail_content);

        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
