<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\MailMessage;
use App\Events\AdminMailMessage;
use App\Models\Mailtemplate;

class AdminSendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $sendmail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminMailMessage $sendmail)
    {
        // $this->user = $user;
        $this->sendmail = $sendmail; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->user);
        /*return $this->markdown('emails.admin.adminsendmail')
                    ->with([                        
                        'name' => $this->sendmail->user->name,
                        'subject' => $this->sendmail->sendmail->subject,
                        'message' => $this->sendmail->sendmail->message,                                  
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/login')
                    ]);*/

        $url = url('/login');
        $adminsendmail = Mailtemplate::where([['name','admin_send_mail'],['status','active']])->first();
        $subject = $adminsendmail->subject;
        $mail_content = $adminsendmail->mail_content;

        $mail_content = str_replace(":name", $this->sendmail->user->name, $mail_content);
        $mail_content = str_replace(":subject", $this->sendmail->sendmail->subject, $mail_content);
        $mail_content = str_replace(":message", $this->sendmail->sendmail->message, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
