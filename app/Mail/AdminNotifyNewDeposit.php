<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
use App\Plan;
// use App\Events\UserNewDeposit;
use App\Models\Mailtemplate;

class AdminNotifyNewDeposit extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {        
        $plan = Plan::where('id', $this->deposit->plan_id)->first();
        // //dd($plan);

        // $user = User::where('id', Auth::id())->with('userprofile')->first();
        // $name = $user->name;

        // if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        // {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        // } 

        /*return $this->markdown('emails.deposit.adminnotifynewdeposit')
                    ->with([
                        'content' => trans('mail.admin_notify_new_deposit_content', ['username' => $this->deposit->deposit->name] ),
                        'new_deposit_link' => url('/admin/actions/newdeposit'),
                        'plan_name' => $this->deposit->deposit->plan_id,
                        'deposited_amount' => $this->deposit->deposit->amount,
                        'new_deposit_link_text' => trans('mail.deposit_link_text'),
                        'signature' => trans('mail.signature'),
        ]);*/

        $url = url('/admin/actions/newdeposit');
        $adminnotifynewdeposit = Mailtemplate::where([['name','admin_notify_new_deposit'],['status','active']])->first();
        $subject = $adminnotifynewdeposit->subject;
        $mail_content = $adminnotifynewdeposit->mail_content;

        $mail_content = str_replace(":username", $this->deposit->user->name, $mail_content);
        $mail_content = str_replace(":deposited_amount", $this->deposit->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":plan_name", $plan->name, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
