<?php

namespace App\Mail;
// use App\Sendbonus;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminSendBonus;
use App\Models\Mailtemplate;

class UserSendBonus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Bonus
     */
    protected $bonus;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminSendBonus $bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $user = User::where('id', $this->bonus->user_id)->with('userprofile')->first();
        // $name = $user->name;

        //  if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        //  {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        //  }

        /*return $this->markdown('emails.senduserbonus')
                    ->with([
                        'comment' => $this->bonus->bonus->comments,
                        'amount' => $this->bonus->bonus->amount,
                        'name' => $this->bonus->user->name,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $sendbonus = Mailtemplate::where([['name','user_send_bonus'],['status','active']])->first();
        $subject = $sendbonus->subject;
        $mail_content = $sendbonus->mail_content;

        $mail_content = str_replace(":name", $this->bonus->user->name, $mail_content); 
        $mail_content = str_replace(":amount", $this->bonus->bonus->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":comment", $this->bonus->bonus->comments, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
