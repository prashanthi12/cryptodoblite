<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserChangePassword;
use App\Models\Mailtemplate;

class ChangePassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserChangePassword $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*return $this->markdown('emails.profile.changepassword')
                    ->with([                        
                        'message' => trans('mail.password_changed'),
                        'name' => $this->user->user->name,  
                        'signature' => trans('mail.signature'),
                    ]);*/

        $changepassword = Mailtemplate::where([['name','change_password'],['status','active']])->first();
        $subject = $changepassword->subject;
        $mail_content = $changepassword->mail_content;

        $mail_content = str_replace(":name", $this->user->user->name, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
