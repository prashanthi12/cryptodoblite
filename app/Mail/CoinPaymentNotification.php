<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Deposit;
use URL;

class CoinPaymentNotification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Deposit $deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = User::where('id', $this->deposit->user_id)->with('userprofile')->first();
         $name = $user->name;

         if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
         {
            $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
         }
         $deposit_id = $this->deposit->id;
         $url = url('/admin/deposit/new');
        return $this->markdown('emails.deposit.coinpaymentnotification')
                ->with([
                    
                    'coin_payment_content' => trans('mail.coin_payment_content',             ['depositid' => $deposit_id]),
                    'new_deposit_link' => $url,
                    'deposit_link_text' => trans('mail.deposit_link_text'),
                    'signature' => trans('mail.signature'),
                ]);
    }
}
