<?php

namespace App\Mail;
// use App\Userprofile;
// use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Events\NewUserRegister;
use App\Models\Mailtemplate;

class EmailVerification extends Mailable implements ShouldQueue
{
     /**
     * The contact instance.
     *
     * @var Contact
     */
    protected $userprofile;


    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userprofile)
    {
         $this->userprofile = $userprofile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      //  $user = User::find($this->userprofile->user_id);
        /*return $this->markdown('emails.emailverification')
                    ->with([
                        'link' => $this->userprofile->userprofile->email_verification_code,
                        'name' => $this->userprofile->user->name,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $url = url('/emailverification/'.$this->userprofile->email_verification_code);
        $newuser = Mailtemplate::where([['name','email_verification'],['status','active']])->first();
        $subject = $newuser->subject;
        $mail_content = $newuser->mail_content;

        $mail_content = str_replace(":name", $this->userprofile->user->name, $mail_content);
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
