<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Epin;

class CouponCodeSendMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $user, $couponcode;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Epin $couponcode)
    {
        $this->user = $user;
        $this->couponcode = $couponcode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
        /*return $this->markdown('emails.deposit.couponcodesendmail')
                    ->with([
                        'name' => $this->user->name,
                        'amount' => $this->couponcode->coupon_value,
                        'couponcode' => $this->couponcode->coupon_code,
                        'signature' => trans('mail.signature')
        ]);*/

        $contactus = Mailtemplate::where([['name','coupon_code_send_mail'],['status','active']])->first();
        $subject = $contactus->subject;
        $mail_content = $contactus->mail_content;

        $mail_content = str_replace(":name", $this->user->name, $mail_content);
        $mail_content = str_replace(":amount", $this->couponcode->coupon_value, $mail_content);
        $mail_content = str_replace(":currency", config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":couponcode", $this->couponcode->coupon_code, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
