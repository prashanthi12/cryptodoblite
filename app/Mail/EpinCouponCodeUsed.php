<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Epin;
use App\Models\Mailtemplate;

class EpinCouponCodeUsed extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The transaction instance.
     *
     * @var transaction
     */
    protected $deposit_result;
  
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Epin $deposit_result)
    {
       // $this->user = $user;
        $this->deposit_result = $deposit_result;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 
      //  $ownerId = $this->deposit_result->owner;
        $userId = $this->deposit_result->used_by;
       // $owner = User::where('id', $ownerId)->pluck('name');
        $user = User::where('id', $userId)->first();

        /*return $this->markdown('emails.deposit.epincouponcodeused')
                    ->with([
                        'name' => $user->name,
                      //  'user' => $user,
                        'amount' => $this->deposit_result->coupon_value,
                      //  'couponcode' => $this->deposit_result->coupon_code,
                        'signature' => trans('mail.signature')
        ]);*/

        $couponcodeused = Mailtemplate::where([['name','epin_couponcode_used'],['status','active']])->first();
        $subject = $couponcodeused->subject;
        $mail_content = $couponcodeused->mail_content;

        $mail_content = str_replace(":name", $user->name, $mail_content);
        $mail_content = str_replace(":amount", $this->deposit_result->coupon_value, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
