<?php

namespace App\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\User;
// use App\Deposit;
use App\Plan;
// use App\Transaction;
// use App\Paymentgateway;
// use App\Events\UserNewDeposit;
use App\Models\Mailtemplate;

class NewDepositSuccessfull extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The deposit instance.
     *
     * @var deposit
     */
    protected $deposit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($deposit)
    {
        $this->deposit = $deposit;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->deposit);
        $plan = Plan::where('id', $this->deposit->plan_id)->first();
        // //dd($plan);
        // $transaction = Transaction::where('id', $this->deposit->transaction_id)->first();
        // $response_json = json_decode($transaction->response, true);

        // $user = User::where('id', $this->deposit->user_id)->with('userprofile')->first();
        // $name = $user->name;

        // $paymentgatewayId = $this->deposit->paymentgateway_id;
        // if($paymentgatewayId == '17')
        // {
        //     $paymentgateway = Paymentgateway::where('id', $this->deposit->paymentgateway_id)->first();
        //     $response = json_decode($paymentgateway->params, true);
        // }

        // if(!is_null($user->userprofile->firstname) && !is_null($user->userprofile->lastname))
        // {
        //     $name = $user->userprofile->firstname.' '. $user->userprofile->lastname;
        // }

        /*return $this->markdown('emails.deposit.newdepositsuccessfull')
                ->with([
                    'amount' => $this->deposit->deposit->amount,
                    'name' => $this->deposit->user->name,
                    'plan_name' => $this->deposit->deposit->plan_id,
                   // 'transaction_number' => $response_json['transaction_number'],
                    // 'bank_name' => $response['bank_name'],
                    // 'account_no' => $response['account_no'],
                    // 'payee_name' => $response['payee_name'],
                    'signature' => trans('mail.signature'),
        ]);*/

        $newdepositsuccess = Mailtemplate::where([['name','new_deposit_successfull'],['status','active']])->first();
        $subject = $newdepositsuccess->subject;
        $mail_content = $newdepositsuccess->mail_content;

        $mail_content = str_replace(":name", $this->deposit->user->name, $mail_content);
        $mail_content = str_replace(":amount", $this->deposit->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":plan_name", $plan->name, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]); 
    }
}
