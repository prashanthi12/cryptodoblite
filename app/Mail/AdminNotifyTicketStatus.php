<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\StaffUpdateTicketStatus;
use App\Models\Mailtemplate;

class AdminNotifyTicketStatus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $statusresult;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(StaffUpdateTicketStatus $statusresult)
    {
        $this->statusresult = $statusresult;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        /*return $this->markdown('emails.ticket.adminnotifyticketstatus')
                    ->with([  
                        'name' => $this->statusresult->admin->name,                      
                        'user' => $this->statusresult->statusresult->user->name,
                        'staff' => $this->statusresult->statusresult->agent->name,
                        'subject' => $this->statusresult->statusresult->subject,
                        'content' => rawurldecode($this->statusresult->statusresult->content),  
                        'status' => $this->statusresult->statusresult->status->name,
                        'priority' => $this->statusresult->statusresult->priority->name,
                        'category' => $this->statusresult->statusresult->category->name,                                
                        'signature' => trans('mail.signature'),
        ]);*/

        $adminnotifyticket = Mailtemplate::where([['name','admin_notify_ticket_status'],['status','active']])->first();
        $subject = $adminnotifyticket->subject;
        $mail_content = $adminnotifyticket->mail_content;

        $mail_content = str_replace(":name", $this->statusresult->admin->name, $mail_content);
        $mail_content = str_replace(":user", $this->statusresult->statusresult->user->name, $mail_content);
        $mail_content = str_replace(":status", $this->statusresult->statusresult->status->name, $mail_content);
        $mail_content = str_replace(":staff", $this->statusresult->statusresult->agent->name, $mail_content);
        $mail_content = str_replace(":subject", $this->statusresult->statusresult->subject, $mail_content);
        $mail_content = str_replace(":content", rawurldecode($this->statusresult->statusresult->content), $mail_content);
        $mail_content = str_replace(":category", $this->statusresult->statusresult->category->name, $mail_content);
        $mail_content = str_replace(":priority", $this->statusresult->statusresult->priority->name, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
