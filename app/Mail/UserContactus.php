<?php

namespace App\Mail;
// use App\Contact;
use App\Events\ContactusAdded;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Mailtemplate;

class UserContactus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The contact instance.
     *
     * @var Contact
     */
    protected $contact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContactusAdded $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->contact->contact->contactno);
        /*return $this->markdown('emails.usercontactmail')
                    ->with([
                        'contactno' => $this->contact->contact->contactno,
                        'skypeid' => $this->contact->contact->skype_gtalk,
                        'queries' => $this->contact->contact->queries,
                        'fromname' => $this->contact->contact->fullname,
                        'signature' => trans('mail.user_signature', array('name' => $this->contact->contact->fullname))
        ]);*/

        $usercontactus = Mailtemplate::where([['name','user_contact_us'],['status','active']])->first();
        $subject = $usercontactus->subject;
        $mail_content = $usercontactus->mail_content;

        $mail_content = str_replace(":name", $this->contact->contact->fullname, $mail_content); 
        $mail_content = str_replace(":queries", rawurldecode($this->contact->contact->queries), $mail_content);
        $mail_content = str_replace(":contactno", $this->contact->contact->contactno, $mail_content);
        $mail_content = str_replace(":skypeid", $this->contact->contact->skype_gtalk, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
