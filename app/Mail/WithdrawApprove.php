<?php

namespace App\Mail;
use App\Withdraw;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\AdminWithdrawApprove;
use App\Models\Mailtemplate;

class WithdrawApprove extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

       /**
     * The contact instance.
     *
     * @var Withdraw
     */
    protected $withdraw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AdminWithdrawApprove $withdraw)
    {
        $this->withdraw = $withdraw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->withdraw);
       // $user = User::where('id', $this->withdraw->user_id)->with('userprofile')->first();
        //dd($user);
        /*return $this->markdown('emails.withdraw.withdrawapprove')
                    ->with([
                        'comments' => $this->withdraw->result->comments_on_complete,
                        'amount' => $this->withdraw->result->amount,
                        'name' => $this->withdraw->user->name,
                        'signature' => trans('mail.signature'),
                    ]);*/

        $withdrawapprove = Mailtemplate::where([['name','withdraw_approve'],['status','active']])->first();
        $subject = $withdrawapprove->subject;
        $mail_content = $withdrawapprove->mail_content;

        $mail_content = str_replace(":name", $this->withdraw->user->name, $mail_content); 
        $mail_content = str_replace(":amount", $this->withdraw->result->amount, $mail_content);
        $mail_content = str_replace(":currency", \config::get('settings.currency'), $mail_content);
        $mail_content = str_replace(":comments", $this->withdraw->result->comments_on_complete, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
