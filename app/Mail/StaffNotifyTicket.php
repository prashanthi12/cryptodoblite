<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserTicketSend;
use App\Models\Mailtemplate;

class StaffNotifyTicket extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

     /**
     * The User instance.
     *
     * @var User
     */
    protected $ticket;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        /*return $this->markdown('emails.ticket.staffnotifyticket')
                    ->with([                        
                        'user' => $this->ticket->ticket->user->name,
                        'name' => $this->ticket->ticket->staff->name,
                        'subject' => $this->ticket->ticket->subject,
                        'content' => rawurldecode($this->ticket->ticket->content),
                        'status' => $this->ticket->ticket->status->name,
                        'priority' => $this->ticket->ticket->priority->name,
                        'category' => $this->ticket->ticket->category->name,                               
                        'signature' => trans('mail.signature'),
                        'actionText' => trans('mail.click_to_login'),                   
                        'actionUrl' => url('/login')
        ]);*/

        $url = url('/login');
        $staffnotifyticket = Mailtemplate::where([['name','staff_notify_ticket'],['status','active']])->first();
        $subject = $staffnotifyticket->subject;
        $mail_content = $staffnotifyticket->mail_content;

        $mail_content = str_replace(":name", $this->ticket->agent->name, $mail_content); 
        $mail_content = str_replace(":user", $this->ticket->user->name, $mail_content);
        $mail_content = str_replace(":category", $this->ticket->category->name, $mail_content); 
        $mail_content = str_replace(":priority", $this->ticket->priority->name, $mail_content);
        $mail_content = str_replace(":status", $this->ticket->status->name, $mail_content); 
        $mail_content = str_replace(":subject", $this->ticket->subject, $mail_content);
        $mail_content = str_replace(":content", rawurldecode($this->ticket->content), $mail_content); 
        $mail_content = str_replace(":url", $url, $mail_content);
        $mail_content = str_replace(":standard_signature", trans('mail.signature'), $mail_content);
        
        return $this->markdown('emails.mailcontent')
                    ->subject($subject)
                    ->with([
                        'content' => $mail_content,
                    ]);
    }
}
