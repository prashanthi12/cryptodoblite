<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Exchange extends Model
{
	use CrudTrait;

    protected $fillable = [
        'code', 'order', 'active'
    ];
  
}
