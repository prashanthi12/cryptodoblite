<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailMessage extends Model
{
	protected $table = "mailmessages";

	protected $fillable = [
        'from_user_id', 'subject', 'message', 'to_user_id', 'type', 'batch'
    ]; 

    protected $appends = array('message');

    public function fromuser() {
        return $this->hasOne('App\User', 'id', 'from_user_id');
    }

    public function touser() {
        return $this->hasOne('App\User', 'id', 'to_user_id');
    }

    public function getMessageAttribute($message)
    {
        return \Purify::clean($message);
    }
}
