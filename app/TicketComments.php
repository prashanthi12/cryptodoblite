<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketComments extends Model
{
	protected $fillable = [
        'content', 'user_id', 'ticket_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
  
}
