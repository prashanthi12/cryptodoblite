<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class TicketPriorities extends Model
{
	use CrudTrait;

	protected $fillable = [
        'name', 'color'
    ];

    public function priority()
    {
        return $this->hasMany('App\Ticket', 'priority_id', 'id');
    }
}
