<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Page extends Model
{
	use CrudTrait;
	
	protected $fillable = [
        'title', 'description', 'navlabel', 'slug', 'content', 'seotitle', 'seodescription', 'seokeyword', 'active', 'language'
    ]; 

    public function getContentAttribute($content)
    {
    	return \Purify::clean($content);
    }
     
}
