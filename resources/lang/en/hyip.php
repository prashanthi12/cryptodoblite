<?php 

 return [
    "plan_name" => "Plan Name",
    "plan_type" => "Plan Type",
    "duration" => "Duration",
    "min_deposit" => "Min Deposit",
    "max_deposit" => "Max Deposit",
    "days" => "Days",
    "interest_rate" => "Interest Rate",
    "principal_return" => "Principal Return",
    "yes" => "YES",
    'no' => "NO",
    'compounding' => "Compounding",
    'partial_withdraw' => "Partial Withdraw",
    'max_partial_withdraw' => "Maximum Partial Withdraw Allowed",
    'partial_withdraw_locking' => "Minimum Locking Period for partial withdraw",
    'calculate_interest' => 'Calculate Interest',
    'view_all_plans' => 'View all plans',
    'calculator' => 'Calculator',
    
];