<div class="grid">
  @foreach($perfectmoney_result as $perfectmoney)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.payee_account_lbl') }} : </small><br/>
        {{ $perfectmoney->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.autowithdraw') }} : </small><br/>       
          @if($perfectmoney->current == '1')
              <a class="perfectmoneyauto" href="{{ url('myaccount/currentaccounts/'.$perfectmoney->id.'/'.$perfectmoney->paymentgateways_id.'/'.$perfectmoney->current)}}">{{ trans('forms.yes') }}</a>
          @else
              <a class="perfectmoneyauto" href="{{ url('myaccount/currentaccounts/'.$perfectmoney->id.'/'.$perfectmoney->paymentgateways_id.'/'.$perfectmoney->current)}}">{{ trans('forms.no') }}</a>
          @endif
      </p>
    </div>   
    <div class="">
        <p>
          <small>{{ trans('forms.actions') }} :</small><br/>
            <form method="post" class="perfectmoneyremovefrm" action="{{ url('myaccount/removeaccount/'.$perfectmoney->id.'') }}">
              {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </form>
        </p>
    </div>
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".perfectmoneyremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".perfectmoneyauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







