<h4>{{ trans('forms.my_bank_wire') }}</h4>
@if(count($bankwire_result) > 0)
   @include('home._mypayaccounts_bankwire_details')
@endif
<p>
<a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#bank">{{ trans('forms.add_bank_account') }}</a>                       
</p>
<hr>
 <!-- Modal -->
  <div class="modal fade" id="bank" role="dialog" style="margin: 50px;"> 
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('forms.bank_details') }}</h4>
        </div>
        <div class="modal-body">
             <form method="post" id="bankwire_form" action="{{ url('myaccount/payaccounts')}}">
            {{ csrf_field() }}
              <div class="form-group">
                <label class="control-label">{{ trans('forms.bank_name_lbl') }}:</label>
                <input type="text" class="form-control" id="bank_name" name="bank_name" required>
              </div> 
              <div class="form-group">
                <label class="control-label">{{ trans('forms.swift_code_lbl') }}:</label>
                <input type="text" class="form-control" id="swift_code" name="swift_code" required>
              </div> 
              <div class="form-group">
                <label class="control-label">{{ trans('forms.account_no_lbl') }}:</label>
                <input type="text" class="form-control" id="account_no" name="account_no" required>
              </div> 
              <div class="form-group">
                <label class="control-label">{{ trans('forms.account_name_lbl') }}:</label>
                <input type="text" class="form-control" id="account_name" name="account_name" required>
              </div> 
              <div class="form-group">
                <label class="control-label">{{ trans('forms.account_address_lbl') }}:</label>
                <input type="text" class="form-control" id="account_address" name="account_address" required>
              </div> 
              <div class="form-group">
                    <input type="hidden" name="paymentid" value="1">
                   <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">        
              </div>           
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.close') }}</button>
        </div>
      </div>  
  </div>
</div>