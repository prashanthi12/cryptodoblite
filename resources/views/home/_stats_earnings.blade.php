 <div class="boxy mt-20">
    <div class="col-md-12">
        <div class="col-md-3">
            <p>{{ trans('myaccount.earnings_interest') }}</p>
            <h3 class="is-amount">{{ $user->totalInterest }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="col-md-2">
            <p>{{ trans('myaccount.referral_commission') }}</p>
            <h3 class="is-amount">{{ $user->referralCommissionTransactions->sum('amount') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="col-md-2">
            <p>{{ trans('myaccount.level_commission') }}</p>
            <h3 class="is-amount">{{ $user->levelCommissionTransactions->sum('amount') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="col-md-2">
            <p>{{ trans('myaccount.bonus') }}</p>
            <h3 class="is-amount">{{ $user->totalBonus->sum('amount') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="col-md-3">
            <p>{{ trans('myaccount.life_time') }} <br/> {{ trans('myaccount.earnings') }}</p>
            <h3 class="is-amount">{{ $user->totalLifeTimeEarnings }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
    </div>
</div>