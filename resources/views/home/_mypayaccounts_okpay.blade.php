<h4>{{ trans('forms.my_okpay') }}</h4>
@if(count($okpay_result) > 0)
   @include('home._mypayaccounts_okpay_details')
@endif
<p>
<a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#okpay_modal">{{ trans('forms.add_okpay') }}</a>                       
</p>
<hr>
 <!-- Modal -->
  <div class="modal fade" id="okpay_modal" role="dialog" style="margin: 100px;">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ trans('forms.okpay_details') }}</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="okpay_form" action="{{ url('myaccount/payaccounts')}}">
            {{ csrf_field() }}
              <div class="form-group">
                <label for="recipient-name" class="control-label">{{ trans('forms.okpay_payid_lbl') }}:</label>
                <input type="text" class="form-control" id="payid" name="payid" required>
              </div> 
              <div class="form-group">
                <label for="recipient-name" class="control-label">{{ trans('forms.okpay_paynumber_lbl') }}:</label>
                <input type="text" class="form-control" id="paynumber" name="paynumber" required>
              </div> 
              <div class="form-group">
                <input type="hidden" name="paymentid" value="12">
                <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">     
              </div>           
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.close') }}</button>        
        </div>
    </div> 
  </div>
</div>