<div class="grid">
  @foreach($skrill_result as $skrill)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.skrill_email_lbl') }} : </small><br/>
        {{ $skrill->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.autowithdraw') }} : </small><br/>       
          @if($skrill->current == '1')
              <a class="skrillauto" href="{{ url('myaccount/currentaccounts/'.$skrill->id.'/'.$skrill->paymentgateways_id.'/'.$skrill->current)}}">{{ trans('forms.yes') }}</a>
          @else
              <a class="skrillauto" href="{{ url('myaccount/currentaccounts/'.$skrill->id.'/'.$skrill->paymentgateways_id.'/'.$skrill->current)}}">{{ trans('forms.no') }}</a>
          @endif
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.actions') }} :</small><br/>
            <form method="post" class="skrillremovefrm" action="{{ url('myaccount/removeaccount/'.$skrill->id.'') }}">
              {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </form>
        </p>
    </div>
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".skrillremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".skrillauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







