<div class="grid">
  @foreach($paypal_result as $paypal)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.paypal_id_lbl') }} : </small><br/>
        {{ $paypal->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.autowithdraw') }} : </small><br/>       
            @if($paypal->current == '1')
                <a class="paypalauto" href="{{ url('myaccount/currentaccounts/'.$paypal->id.'/'.$paypal->paymentgateways_id.'/'.$paypal->current)}}">{{ trans('forms.yes') }}</a>
            @else
                <a class="paypalauto" href="{{ url('myaccount/currentaccounts/'.$paypal->id.'/'.$paypal->paymentgateways_id.'/'.$paypal->current)}}">{{ trans('forms.no') }}</a>
            @endif
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.actions') }} :</small><br/>
            <form method="post" class="paypalremovefrm" action="{{ url('myaccount/removeaccount/'.$paypal->id.'') }}">
                {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </form>
        </p>
    </div>
    </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".paypalremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".paypalauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







