<div class="grid">
@if (count($mydeposits) > 0)
	@foreach ($mydeposits as $mydeposit)
    <div class="grid grid-6 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      	<p>
	        <small>{{ trans('myaccount.plan_name') }} : </small><br/>
	        <a href="{{ url('invest/plan/'.$mydeposit->plan->id) }}">{{ $mydeposit->planName }}</a>
	    </p>
    </div>
    <div class="">
      	<p>
        	<small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }}) : </small><br/>
        		{{ $mydeposit->amount }}<br>
        	<small>{{ trans('myaccount.payment_method') }} : </small><br>
        		{{ $mydeposit->paymentGatewayName }}
      	</p>
    </div>   
    <div class="">
        <p>
        	@if ($status == 'active' || $status == 'matured' || $status == 'released' || $status == 'problem' || $status == 'rejected')
            <small>{{ trans('myaccount.comments') }} :</small><br/>
	            @if ($status == 'active')
					<span data-html="true" data-toggle="active-comment-popover"  data-content="{{ $mydeposit->comments_on_approval }}">
					{{  substr($mydeposit->comments_on_approval, 0, 15) }}...</span><br/>
					<small>{{ trans('myaccount.start_date') }} : </small><br/>
					{{ isset($mydeposit->approved_on) ? $mydeposit->approved_on->diffForHumans(). '|' : '' }}
					@if ($mydeposit->plan->duration != -1)
						{{ \Carbon\Carbon::parse($mydeposit->matured_on)->diffInDays() }} Days 
					@else
						{{ trans('myaccount.unlimited') }}
					@endif
				@endif
				@if ($status == 'matured')
					<span data-html="true" data-toggle="matured-comment-popover"  data-content="{{ $mydeposit->comments_on_maturity }}">
					{{  substr($mydeposit->comments_on_maturity, 0, 15) }}...</span><br/>
					<small>{{ trans('myaccount.maturity_date') }} : </small><br/>
					{{ isset($mydeposit->matured_on) ?  $mydeposit->matured_on->diffForHumans(). '|' : '' }}
					@if ($mydeposit->plan->duration != -1)
						{{ \Carbon\Carbon::parse($mydeposit->matured_on)->diffInDays() }} Days 
					@else
						{{ trans('myaccount.unlimited') }}
					@endif
				@endif
				@if ($status == 'released')
					<span data-html="true" data-toggle="released-comment-popover"  data-content="{{ $mydeposit->comments_on_release }}">
					{{  substr($mydeposit->comments_on_release, 0, 15) }}...</span><br/>
					<small>{{ trans('myaccount.releaseddate') }} : </small><br/>
					{{ $mydeposit->released_on->diffForHumans() }}
				@endif
				@if ($status == 'rejected')
					<span data-html="true" data-toggle="rejected-comment-popover"  data-content="{{ $mydeposit->comments_on_reject }}">
					{{  substr($mydeposit->comments_on_reject, 0, 15) }}...</span><br/>
					<small>{{ trans('myaccount.rejected_date') }} : </small><br/>
					{{ $mydeposit->rejected_on->diffForHumans() }}
				@endif
				@if ($status == 'problem')
					<span data-html="true" data-toggle="problem-comment-popover"  data-content="{{ $mydeposit->comments_on_problem }}">
					{{  substr($mydeposit->comments_on_problem, 0, 15) }}...</span><br/>
					<small>{{ trans('myaccount.problem_on') }} : </small><br/>
					{{ $mydeposit->problem_on->diffForHumans() }}
				@endif
            @endif
        </p>
    </div>
    @if ($status == 'active' || $status == 'matured' || $status == 'released')
    <div class="">
      	<p>
        	<small>{{ trans('myaccount.interest_generated') }} ({{ config::get('settings.currency') }}) : </small><br/>
        		{{ $mydeposit->interest->pluck('amount')->sum() }}<br/>
        	<small>{{ trans('myaccount.partial_withdraw') }}</small><br/>
    		@if ($mydeposit->plan->partial_withdraw_status == 1)
				@if (\Carbon\Carbon::now()->format('d/m/Y') >= $mydeposit->approved_on->addDays($mydeposit->plan->minimum_locking_period)->format('d/m/Y'))
					<a href="{{ url('myaccount/deposit/partialwithdraw/'.$mydeposit->id) }}">{{	trans('myaccount.partial_withdraw') }}</a>
				@else
					--
				@endif
			@else
				--
			@endif
      	</p>
    </div>
    @endif
    <div class="">
      	<p>
        	<small>{{ trans('myaccount.transaction_number') }} : </small><br/>
        		@php	    
		   		if (isset($mydeposit->transaction->response))
		   		{
		       		$param = json_decode($mydeposit->transaction->response, true);
		       		echo $param['transaction_number'];
		        }
		        else
		        {
		        	echo "-";
		        }
	        	@endphp
	        <br>
        	@if ($status == 'new')
			<small>{{ trans('myaccount.invoice') }}</small><br/>
				@if ($mydeposit->paymentgateway->id == 1 || $mydeposit->paymentgateway->id == 17)
					<td> <a href="{{ url('myaccount/viewdeposits/printinvoice/'.$mydeposit->id) }}" class="btn btn-info btn-xs" target="_blank" id="printinvoice">{{ trans('myaccount.view_invoice') }}</a></td>
				@else
					<td>--</td>
				@endif
			@endif
      	</p>
    </div>
    </div>
  @endforeach 
@else
	@if ($status == 'released')
		<div style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.no_deposit_found') }}</div>
	@elseif ($status == 'matured')
		<div style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.no_deposit_found') }}</div>
	@elseif ($status == 'rejected')
		<div style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.no_deposit_found') }}</div>
	@elseif ($status == 'active')
		<div style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.no_deposit_found') }}</div>
	@else
		<div style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.no_deposit_found') }}</div>
	@endif
@endif
</div>
{{ $mydeposits->links() }}

@push('bottomscripts')
<script>  
    $("#printinvoice").on("click", function(){
        return confirm("{{ trans('myaccount.print_invoive_alert') }}");
    });

    $('[data-toggle="active-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

    $('[data-toggle="matured-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

    $('[data-toggle="released-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

    $('[data-toggle="rejected-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

    $('[data-toggle="problem-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

</script>
@endpush







