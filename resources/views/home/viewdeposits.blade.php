@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.mydeposits') }} - {{ ucfirst($status) }}</div>
    <div class="panel-body">
       <div class="row">
            <div class="col-md-12">
                @include('layouts.message')
                @include('home._stats_deposit')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="tab" class="btn-group" >
              <a href="{{ url('myaccount/viewdeposits/new') }}" class="btn {{ $status == 'new' ? 'active' : '' }}" >{{ trans('myaccount.new') }}</a>
              <a href="{{ url('myaccount/viewdeposits/active') }}" class="btn {{ $status == 'active' ? 'active' : '' }}" >{{ trans('myaccount.active') }}</a>
              <a href="{{ url('myaccount/viewdeposits/matured') }}" class="btn {{ $status == 'matured' ? 'active' : '' }}" >{{ trans('myaccount.matured') }}</a>
              <a href="{{ url('myaccount/viewdeposits/released') }}" class="btn {{ $status == 'released' ? 'active' : '' }}" >{{ trans('myaccount.released') }}</a>
              <a href="{{ url('myaccount/viewdeposits/rejected') }}" class="btn {{ $status == 'rejected' ? 'active' : '' }}" >{{ trans('myaccount.rejected') }}</a>
               <a href="{{ url('myaccount/viewdeposits/problem') }}" class="btn {{ $status == 'problem' ? 'active' : '' }}" >{{ trans('myaccount.problem') }}</a>
              <a href="{{ url('myaccount/viewdeposits/archived') }}" class="btn {{ $status == 'archived' ? 'active' : '' }}" >{{ trans('myaccount.archived') }}</a>
            </div>
                @include('home.deposittable')
            </div>
        </div>
    </div>
</div>
@endsection
