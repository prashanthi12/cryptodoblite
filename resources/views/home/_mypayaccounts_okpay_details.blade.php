<div class="grid">
  @foreach($okpay_result as $okpay)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.okpay_payid_lbl') }} : </small><br/>
        {{ $okpay->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.okpay_paynumber_lbl') }} : </small><br/>       
          {{ $okpay->param2 }}
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.autowithdraw') }} :</small><br/>
            @if($okpay->current == '1')
                <a class="okpayauto" href="{{ url('myaccount/currentaccounts/'.$okpay->id.'/'.$okpay->paymentgateways_id.'/'.$okpay->current)}}">{{ trans('forms.yes') }}</a>
            @else
                <a class="okpayauto" href="{{ url('myaccount/currentaccounts/'.$okpay->id.'/'.$okpay->paymentgateways_id.'/'.$okpay->current)}}">{{ trans('forms.no') }}</a>
            @endif
        </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.actions') }} : </small><br/>       
          <form method="post" class="okpayremovefrm" action="{{ url('myaccount/removeaccount/'.$okpay->id.'') }}">
            {{ csrf_field()}}
              <div class="form-group">
                  <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
              </div>
          </form>
      </p>
    </div> 
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".okpayremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".okpayauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







