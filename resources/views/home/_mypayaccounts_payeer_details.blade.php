<div class="grid">
  @foreach($payeer_result as $payeer)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.payeer_id_lbl') }} : </small><br/>
        {{ $payeer->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.payeer_email_lbl') }} : </small><br/>       
          {{ $payeer->param2 }}
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.autowithdraw') }} :</small><br/>
            @if($payeer->current == '1')
                <a class="payeerauto" href="{{ url('myaccount/currentaccounts/'.$payeer->id.'/'.$payeer->paymentgateways_id.'/'.$payeer->current)}}">{{ trans('forms.yes') }}</a>
            @else
                <a class="payeerauto" href="{{ url('myaccount/currentaccounts/'.$payeer->id.'/'.$payeer->paymentgateways_id.'/'.$payeer->current)}}">{{ trans('forms.no') }}</a>
            @endif
        </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.actions') }} : </small><br/>       
          <form method="post" class="payeerremovefrm" action="{{ url('myaccount/removeaccount/'.$payeer->id.'') }}">
            {{ csrf_field()}}
              <div class="form-group">
                  <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
              </div>
          </form>
      </p>
    </div> 
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".payeerremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".payeerauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







