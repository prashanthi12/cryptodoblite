<div class="grid">
  @foreach($stpay_result as $stpay)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.user_name_lbl') }} : </small><br/>
        {{ $stpay->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.autowithdraw') }} : </small><br/>       
          @if($stpay->current == '1')
              <a class="stpayauto" href="{{ url('myaccount/currentaccounts/'.$stpay->id.'/'.$stpay->paymentgateways_id.'/'.$stpay->current)}}">{{ trans('forms.yes') }}</a>
          @else
              <a class="stpayauto" href="{{ url('myaccount/currentaccounts/'.$stpay->id.'/'.$stpay->paymentgateways_id.'/'.$stpay->current)}}">{{ trans('forms.no') }}</a>
          @endif
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.actions') }} :</small><br/>
            <form method="post" class="stpayremovefrm" action="{{ url('myaccount/removeaccount/'.$stpay->id.'') }}">
              {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </form>
        </p>
    </div>
    </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".stpayremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".stpayauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







