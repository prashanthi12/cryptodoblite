<table class="table dataTable">
    <thead>
        <th>{{ trans('myaccount.level') }}</th>
        <th>{{ trans('myaccount.username') }}</th>
        <th>{{ trans('myaccount.placedunder') }}</th>
    </thead>
    <tbody>
        @if (count($myNetwork) > 0)
            @foreach ($myNetwork as $item)
            <tr>
                <td>{{ $item['root'] }}</td>
                <td>{{ $item['username'] }}</td>
                <td>{{ $item['spillover'] }}</td>
            </tr>
            @endforeach
        @else
            <tr>
                <td colspan="3">{{ trans('myaccount.nousersfound') }}</td>
            </tr>
        @endif
    </tbody>
</table>

