<div class="grid">
@if (count($levelCommissions) > 0)
    @foreach($levelCommissions as $levelCommission)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $levelCommission->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.comment') }} : </small><br/>       
          {!! $levelCommission->comment !!}<br>
      </p>
    </div>   
    <div class="">
        <p style="text-align: center;">
            <small>{{ trans('myaccount.datetime') }} :</small><br/>
                {{ $levelCommission->created_at->diffForHumans() }}
        </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('myaccount.nolevelcommissionfound') }}</div>
@endif
</div>
{{ $levelCommissions->links() }}







