@extends('layouts.myaccount')
@section('content')
            <div class="panel panel-default">
                <div class="panel-heading">
                	My Payment Accounts
                </div>
              
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                               @if (session('status'))
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    	@if($paypal)
                    	   @include('home._mypayaccounts_paypal')
                    	@endif
                    	</div>
                    </div>

                    <div class="row">
                    	<div class="col-md-12">
                    	@if($bank)
                    	   @include('home._mypayaccounts_bankwire')
                    	@endif
                    	</div>
                    </div>

                     <div class="row">
                        <div class="col-md-12">
                        @if($stp)
                           @include('home._mypayaccounts_stpay')
                        @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        @if($payeer)
                           @include('home._mypayaccounts_payeer')
                        @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        @if($advcash)
                           @include('home._mypayaccounts_advcash')
                        @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        @if($bitcoin_direct)
                           @include('home._mypayaccounts_bitcoin_direct')
                        @endif
                        </div>
                    </div>                   

                    <div class="row">
                        <div class="col-md-12">
                        @if($skrill)
                           @include('home._mypayaccounts_skrill')
                        @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        @if($okpay)
                           @include('home._mypayaccounts_okpay')
                        @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        @if($perfectmoney)
                           @include('home._mypayaccounts_perfectmoney')
                        @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        @if($neteller)
                           @include('home._mypayaccounts_neteller')
                        @endif
                        </div>
                    </div>

                    @if($paypal == '0' && $bank == '0' && $stp == '0' && $payeer == '0' && $advcash == '0' && $bitcoin_direct == '0' && $skrill == '0' && $okpay == '0' && $perfectmoney == '0' && $neteller == '0')
                        <div class="row">
                        <div class="col-md-12">
                            {{ trans('forms.no_withdraw_payment_found') }}
                        </div>
                        </div>

                    @endif
            </div>
           </div>
@endsection

@push('bottomscripts')
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/jquery.validate.js') }}"></script>
@endpush