<p>
    <div class="deposit-grid">
        <div class="deposit-item active-deposit">
                <p>{{ trans('myaccount.active_deposits') }}</p>
                <h3 class="is-amount">{{ session('activedeposits') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="deposit-item new-deposit">
                <p>{{ trans('myaccount.new_deposits') }}</p>
                <h3 class="is-amount">{{ session('newdeposits') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="deposit-item closed-deposit">
                <p>{{ trans('myaccount.closed_deposits') }}</p>
                <h3 class="is-amount">{{ session('closeddeposits') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
        <div class="deposit-item lifttime-deposit">
                <p>{{ trans('myaccount.life_time_deposits') }}</p>
                <h3 class="is-amount">{{ session('lifetimedeposits') }} <small>{{ config::get('settings.currency') }}</small></h3>
        </div>
    </div>
</p>
