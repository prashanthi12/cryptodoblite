<div class="boxy mb-20">
    <div class="col-md-3">
        <p>{{ trans('myaccount.pending') }}</p>
        <h3 class="is-amount">{{ $user->pendingWithdraw }}<small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-3">
        <p>{{ trans('myaccount.completed') }}</p>
        <h3 class="is-amount">{{ $user->CompletedWithdraw }}<small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-3">
        <p>{{ trans('myaccount.lifetime') }}</p>
        <h3 class="is-amount">{{ $user->LifeTimeWithdraw }}<small>{{ config::get('settings.currency') }}</small></h3>
    </div>
</div>