@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.my_earnings') }}</div>
    <div class="panel-body">
       <div class="row">
            <div class="col-md-12">
                @include('home._stats_earnings')
            </div>
        </div>
        <div class="row mt-20">
            <div class="col-md-12">
                <div id="tab" class="btn-group" >
                  <a href="{{ url('myaccount/earnings/interest') }}" class="btn {{ $status == 'interest' ? 'active' : '' }}" >{{ trans('myaccount.interest') }}</a>
                  <a href="{{ url('myaccount/earnings/referral') }}" class="btn {{ $status == 'referral' ? 'active' : '' }}" >{{ trans('myaccount.referral_commission') }}</a>
                  <a href="{{ url('myaccount/earnings/level') }}" class="btn {{ $status == 'level' ? 'active' : '' }}" >{{ trans('myaccount.level_commission') }}</a>
                   <a href="{{ url('myaccount/earnings/bonus') }}" class="btn {{ $status == 'bonus' ? 'active' : '' }}" >{{ trans('myaccount.bonus') }}</a>
                    <a href="{{ url('myaccount/earnings/partialwithdraw') }}" class="btn {{ $status == 'partialwithdraw' ? 'active' : '' }}" >{{ trans('myaccount.partialwithdraw') }}</a>
                </div>
                @if ($status == 'interest')
                    @include('home._table_interest') 
                @elseif ($status == 'referral')
                    @include('home._table_referral')
                @elseif ($status == 'level')
                    @include('home._table_level')
                @elseif ($status == 'bonus')
                    @include('home._table_bonus')
                @elseif ($status == 'partialwithdraw')
                    @include('home._table_partialwithdraw')
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
