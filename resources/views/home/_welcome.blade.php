<section class="user profile has--profile">
    <div class="row">
        <div class="col-md-12">
            <p>{{ trans('myaccount.welcometitle', ['username' => Auth::user()->name]) }}<p>
            <p>{{ trans('myaccount.welcometext') }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <a href="{{ url('/myaccount/profile') }}" class="btn btn-primary btn-block">{{ trans('myaccount.ctabutton') }}</a>
        </div>
    </div>
</section>