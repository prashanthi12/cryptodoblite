@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
<div class="panel-heading">{{ trans('myaccount.myinbox') }}</div>
<div class="panel-body">
<div class="grid">
@if (count($inbox) > 0)
  @foreach ($inbox as $myinbox)   
    <div class="grid grid-2-300-auto mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
      <div class="">
        <p>
            <strong>{{ $myinbox['subject'] }}</strong><br>
            <small>{{ $myinbox['created_at']->diffForHumans() }}</small>
        </p>     
      </div>
      
      <div class="" >           
        <p style="text-align: left;">   
            {{ $myinbox['message'] }}
        </p>
      </div>
    </div>
  @endforeach 
@else
		<div class="">{{ trans('myaccount.no_messages_found') }}</div>
@endif
</div>
</div>
</div>
{{ $inbox->links() }}
@endsection

