<div class="grid">
  @foreach($bankwire_result as $bankwire)
    <div class="grid grid-5 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.bank_name_lbl') }} : </small><br/>
        {{ $bankwire->param1 }} <br/>
        <small>{{ trans('forms.swift_code_lbl') }} : </small><br/>
        {{ $bankwire->param2 }} <br/>
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.account_no_lbl') }} : </small><br/>
        {{ $bankwire->param3 }} <br/>
        <small>{{ trans('forms.account_name_lbl') }} : </small><br/>
        {{ $bankwire->param4 }} <br/>
      </p>
    </div>   
    <div class="">
        <p>
          <small>{{ trans('forms.account_address_lbl') }} : </small><br/>
          {{ $bankwire->param5 }} <br/>
        </p>
    </div>
    <div class="">
        <p>
          <small>{{ trans('forms.autowithdraw') }} : </small><br/>
          @if($bankwire->current == '1')
              <a class="bankauto" href="{{ url('myaccount/currentaccounts/'.$bankwire->id.'/'.$bankwire->paymentgateways_id.'/'.$bankwire->current)}}">{{ trans('forms.yes') }}</a>
          @else
              <a class="bankauto" href="{{ url('myaccount/currentaccounts/'.$bankwire->id.'/'.$bankwire->paymentgateways_id.'/'.$bankwire->current)}}">{{ trans('forms.no') }}</a>
          @endif
        </p>
    </div>
    <div class="">
        <p>
          <small>{{ trans('forms.actions') }} : </small><br/>
          <form method="post" class="bankremovefrm" action="{{ url('myaccount/removeaccount/'.$bankwire->id.'') }}">
            {{ csrf_field()}}
              <div class="form-group">
                  <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
              </div>
          </form>
        </p>
    </div>
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".bankremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".bankauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







