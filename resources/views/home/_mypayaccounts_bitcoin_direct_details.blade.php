<div class="grid">
  @foreach($bitcoin_result as $bitcoin)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.bitcoin_coinname_lbl') }} : </small><br/>
        {{ $bitcoin->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.bitcoin_btccode_lbl') }} : </small><br/>       
          {{ $bitcoin->param2 }}
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.autowithdraw') }} :</small><br/>
            @if($bitcoin->current == '1')
                <a class="bitcoinauto" href="{{ url('myaccount/currentaccounts/'.$bitcoin->id.'/'.$bitcoin->paymentgateways_id.'/'.$bitcoin->current)}}">{{ trans('forms.yes') }}</a>
            @else
                <a class="bitcoinauto" href="{{ url('myaccount/currentaccounts/'.$bitcoin->id.'/'.$bitcoin->paymentgateways_id.'/'.$bitcoin->current)}}">{{ trans('forms.no') }}</a>
            @endif
        </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.actions') }} : </small><br/>       
          <form method="post" class="bitremovefrm" action="{{ url('myaccount/removeaccount/'.$bitcoin->id.'') }}">
            {{ csrf_field()}}
              <div class="form-group">
                  <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
              </div>
          </form>
      </p>
    </div> 
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".bitremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".bitcoinauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







