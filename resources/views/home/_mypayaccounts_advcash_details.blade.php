<div class="grid">
  @foreach($advcash_result as $advcash)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.account_emailid_lbl') }} : </small><br/>
        {{ $advcash->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.autowithdraw') }} : </small><br/>       
          @if($advcash->current == '1')
              <a class="advcashauto" href="{{ url('myaccount/currentaccounts/'.$advcash->id.'/'.$advcash->paymentgateways_id.'/'.$advcash->current)}}">{{ trans('forms.yes') }}</a>
          @else
              <a class="advcashauto" href="{{ url('myaccount/currentaccounts/'.$advcash->id.'/'.$advcash->paymentgateways_id.'/'.$advcash->current)}}">{{ trans('forms.no') }}</a>
          @endif
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.actions') }} :</small><br/>
            <form method="post" class="advcashremovefrm" action="{{ url('myaccount/removeaccount/'.$advcash->id.'') }}">
              {{ csrf_field()}}
                <div class="form-group">
                    <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
                </div>
            </form>
        </p>
    </div>
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".advcashremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
     $(".advcashauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







