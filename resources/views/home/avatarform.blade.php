<div class="col-md-10 col-md-offset-1">

<form method="post" action="{{ url('myaccount/saveavatar')}}" class="form-horizontal" id="changeavatar" enctype="multipart/form-data">
{{ csrf_field() }}

    <div class="form-group{{ $errors->has('profileimage') ? ' has-error' : '' }}">
        <label>{{ trans('forms.profile_image_lbl') }}</label>
        <input type="file" name="profileimage">
        <small class="text-danger">{{ $errors->first('profileimage') }}</small>
    </div>

    <div class="form-group">
        {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();">
        <a href="" class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>