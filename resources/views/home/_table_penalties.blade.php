<div class="grid">
@if (count($penalties) > 0)
    @foreach($penalties as $penalty)
    <div class="grid grid-2 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>       
          <strong>{{ $penalty->amount }}</strong>
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.comment') }} : </small><br/>       
          {{ $penalty->comments }}<br>
          {{ $penalty->created_at->diffForHumans() }}
      </p>     
    </div>
    </div>
  @endforeach 
@else
        <div class="">{{ trans('myaccount.nopenaltyfound') }}</div>
@endif
</div>
{{ $penalties->links() }}


