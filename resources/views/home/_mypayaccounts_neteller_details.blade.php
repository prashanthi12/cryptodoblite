<div class="grid">
  @foreach($neteller_result as $neteller)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.merchant_id_lbl') }} : </small><br/>
        {{ $neteller->param1 }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.merchant_key_lbl') }} : </small><br/>       
          {{ $neteller->param2 }}
      </p>
    </div>   
    <div class="">
        <p>
            <small>{{ trans('forms.autowithdraw') }} :</small><br/>
            @if($neteller->current == '1')
                <a class="netellerauto" href="{{ url('myaccount/currentaccounts/'.$neteller->id.'/'.$neteller->paymentgateways_id.'/'.$neteller->current)}}">{{ trans('forms.yes') }}</a>
            @else
                <a class="netellerauto" href="{{ url('myaccount/currentaccounts/'.$neteller->id.'/'.$neteller->paymentgateways_id.'/'.$neteller->current)}}">{{ trans('forms.no') }}</a>
            @endif
        </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.actions') }} : </small><br/>       
          <form method="post" class="netellerremovefrm" action="{{ url('myaccount/removeaccount/'.$neteller->id.'') }}">
            {{ csrf_field()}}
              <div class="form-group">
                  <button type="submit" value="Remove" class="btn btn-danger btn-xs" onclick="this.disabled=true;this.form.submit();"><span class="glyphicon glyphicon-remove"></span></button>
              </div>
          </form>
      </p>
    </div> 
  </div>
  @endforeach 
</div>

@push('bottomscripts')
<script>
    $(".netellerremovefrm").on("submit", function(){
        return confirm("{{ trans('forms.pay_account_remove_alert') }}");
    });
    $(".netellerauto").on("click", function(){
        return confirm("{{ trans('forms.auto_withdraw_alert') }}");
    });
</script>
@endpush







