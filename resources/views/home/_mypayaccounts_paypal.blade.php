<h4>{{ trans('forms.my_paypal_accounts') }}</h4> 
@if(count($paypal_result) > 0)
     @include('home._mypayaccounts_paypal_details') 
@endif
<p>
    <a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#paypal">{{ trans('forms.add_paypal_account') }}</a>
</p>
<hr>
<!-- Modal -->
<div class="modal fade" id="paypal" role="dialog" style="margin: 120px;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ trans('forms.paypal_details') }}</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="paypal_form" action="{{ url('myaccount/payaccounts')}}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('paypal_id') ? ' has-error' : '' }}">
                        <label for="recipient-name" class="control-label">{{ trans('forms.paypal_id_lbl') }}:</label>
                        <input type="email" class="form-control" id="paypal_id" name="paypal_id" required>
                    </div>
                    <!-- <div class="form-group{{ $errors->has('paypal_id') ? ' has-error' : '' }}">
                <label for="recipient-name" class="control-label">{{ trans('forms.active') }}:</label>
                <input type="checkbox" id="paypal_active" name="paypal_active">         
              </div> 
              <div class="form-group{{ $errors->has('paypal_id') ? ' has-error' : '' }}">
                <label for="recipient-name" class="control-label">{{ trans('forms.default') }}:</label>
                <input type="checkbox" id="paypal_default" name="paypal_default">      
              </div>  -->
                    <div class="form-group">
                        <input type="hidden" name="paymentid" value="2">
                        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="payment" type="submit" onclick="this.disabled=true;this.form.submit();">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('forms.close') }}</button>
            </div>
        </div>
    </div>
</div>
