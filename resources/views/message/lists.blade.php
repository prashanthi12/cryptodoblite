<div class="grid">
@if (count($messages) > 0)
    @foreach($messages as $data)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.from') }} : </small><br/>       
          {{ $data->userone->name }}<br>
        <small>{{ trans('forms.to') }} : </small><br/>       
          {{ $data->usertwo->name }}
      </p>
    </div>
    <div class="">
      <p style="text-align: left;">
        <small>{{ trans('forms.message') }} :</small><br/>
          <a href="{{ url('myaccount/message/conversation/'.$data['id']) }}">
              <p> {!! $data->message->first()->message !!} 
                <span class="label label-info"> {{ $data->message->count() }}</span>
                  @if( $data->message->last()->is_seen == 0 ) 
                <span class="label label-success"> New </span>
                  @endif
              </p>
          </a> 
          <small>{{ trans('forms.createdon') }} :</small><br/>
          {{ $data->created_at->diffForHumans() }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('forms.lastreplyby') }} :</small><br/>
          {{ $data->message->last()->user->name }}<br>
        <small>{{ trans('forms.lastreplyon') }} :</small><br/>
          {{ $data->message->last()->created_at->diffForHumans() }}
      </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="">{{ trans('forms.nomessagefound') }}</div>
@endif
</div>
{{ $messages->links() }}


