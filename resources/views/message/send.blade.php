@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.messageform') }}
    <a href="{{ url('myaccount/message/list') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
                 @if (session('mobilecodeerror'))
                    <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('mobilecodeerror') }}
                    </div>
                    @endif
                @include('layouts.message')
            </div>
        </div>

        <div class="row">

            <div class="col-md-12">
                @include('message.sendform')
            </div>
        </div>
    </div>
</div>
@endsection

