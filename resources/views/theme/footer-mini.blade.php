<div class="dark-footer">
<div class="container">

</div>
</div>
<div class="dark-copy-footer stick-to-bottom">
<nav class="navbar navbar-default navbar-bottom mb-0">
    <div class="container">
       
    </div>
    <div class="container">
        <div class="mt-10">
                <p class="copy-text">&copy;Copyright {{ Carbon\Carbon::now()->year }}. {{Config::get('settings.sitename') }} {{ trans('welcome.footer_text') }}</p>
                        <p class="copy-text"><small>Server Time : {{ Carbon\Carbon::now()->format('d/m/Y H:i:s') }}</small></p>
        </div>
    </div>
</nav>
</div>
</div>
