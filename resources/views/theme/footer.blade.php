<div class="dark-footer">
<div class="container">
    <div class="row mt-50 mb-50">
    <div class="col-md-4">
        <p class="clearfix">
            <a class="navbar-brand-footer" href="{{ url('/') }}" title="{{ Config::get('settings.sitename') }}">
            <img src="{{ url(Config::get('settings.sitelogo')) }}" alt="{{ Config::get('settings.sitename') }}"  class="clear-fix">       
            </a>
        </p>
        <p class="footer-text clearfix">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. </p>
    </div>
    <div class="col-md-4 visible-xs visible-sd">
         <ul class="footer-menu-mobile">
            <li><a href="{{ url('/news') }}" class="footer-menu-item">{{ trans('mainnavigation.menunews') }}</a></li>
            <li><a href="{{ url('/reviews') }}" class="footer-menu-item">{{ trans('mainnavigation.menureviews') }}</a></li>
            <li><a href="{{ url('/exchange') }}" class="footer-menu-item">{{ trans('mainnavigation.menuexchange') }}</a></li>
            <li><a href="{{ url('/faq') }}" class="footer-menu-item">{{ trans('mainnavigation.menufaq') }}</a></li>
            <li><a href="{{ url('/earn') }}" class="footer-menu-item">{{ trans('mainnavigation.menuearn') }}</a></li>           
            <li><a href="{{ url('/privacy') }}" class="footer-menu-item">{{ trans('mainnavigation.menuprivacypolicy') }}</a></li>
            <li><a href="{{ url('/terms') }}" class="footer-menu-item">{{ trans('mainnavigation.menutermsservice') }}</a></li>
            <li><a href="{{ url('/security') }}" class="footer-menu-item">{{ trans('mainnavigation.menusecuritytips') }}</a></li>
        </ul>
    </div>
    <div class="col-md-2 visible-md visible-lg">
         <ul class="footer-menu-vertical">
            <li><a href="{{ url('/news') }}" class="footer-menu-item">{{ trans('mainnavigation.menunews') }}</a></li>
            <li><a href="{{ url('/reviews') }}" class="footer-menu-item">{{ trans('mainnavigation.menureviews') }}</a></li>
            <li><a href="{{ url('/exchange') }}" class="footer-menu-item">{{ trans('mainnavigation.menuexchange') }}</a></li>
            <li><a href="{{ url('/faq') }}" class="footer-menu-item">{{ trans('mainnavigation.menufaq') }}</a></li>
        </ul>
    </div>
    <div class="col-md-2">
     <ul class="footer-menu-vertical visible-md visible-lg">
            <li><a href="{{ url('/earn') }}" class="footer-menu-item">{{ trans('mainnavigation.menuearn') }}</a></li>           
            <li><a href="{{ url('/privacy') }}" class="footer-menu-item">{{ trans('mainnavigation.menuprivacypolicy') }}</a></li>
            <li><a href="{{ url('/terms') }}" class="footer-menu-item">{{ trans('mainnavigation.menutermsservice') }}</a></li>
            <li><a href="{{ url('/security') }}" class="footer-menu-item">{{ trans('mainnavigation.menusecuritytips') }}</a></li>
        </ul>
    </div>
    <div class="col-md-4">
    </div>
    </div>
</div>
</div>
<div class="dark-copy-footer">
<nav class="navbar navbar-default navbar-bottom mb-0">
    <div class="container">
       
    </div>
    <div class="container">
        <div class="mt-10">
                <p class="copy-text">&copy;Copyright {{ Carbon\Carbon::now()->year }}. {{Config::get('settings.sitename') }} {{ trans('welcome.footer_text') }}</p>
                        <p class="copy-text"><small>Server Time : {{ Carbon\Carbon::now()->format('d/m/Y H:i:s') }}</small></p>
        </div>
    </div>
</nav>
</div>
</div>
