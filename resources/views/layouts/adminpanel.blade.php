<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Config::get('settings.sitename') }} :: Admin Panel</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/sweetalert2.min.css">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app" class="admin-panel admin-panel-wrapper">
        <div class="admin-content-wrapper">
        <div id="admin-panel" >
        <nav class="navbar navbar-default navbar-fixed-top navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#admin-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <!-- Branding Image -->
                    <a   href="{{ url('/') }}" title="{{ Config::get('settings.sitename') }}">
                        <img src="{{ url(Config::get('settings.sitelogo')) }}" alt="{{ Config::get('settings.sitename') }}" class="admin-brand-image" >       
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="admin-navbar-collapse">
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">{{ trans('mainnavigation.menulogin') }}</a></li>
                            <li><a href="{{ route('register') }}">{{ trans('mainnavigation.menuregister') }}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('admin/changepassword') }}">
                                            {{ trans('mainnavigation.menuchangepassword') }}
                                        </a>
                                      
                                    </li>
                                     @if(Auth::user()->isImpersonating())
                                         <li>
                                            <a href="{{ url('users/stop') }}" >{{ trans('mainnavigation.stop_impersonate') }}
                                            </a>                            
                                        </li>
                                    @endif
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ trans('mainnavigation.menulogout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-admin  navbar-fixed-top navbar-fixed-top-2">
        <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle admin-navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                         <i class="glyphicon glyphicon-option-vertical"></i>
                    </button>
            <div class="container">
                  <div class="menu-bar">

                  
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                       @include('layouts.partials.adminmenu')
                     
                    </div>
                  </div>
            </div>
        </nav>
        <div class="mt-105">
              @yield('content')
        </div>
        </div>
        </div>
        <div class="footer-wrapper">
        <footer>
        @include('adminpartials.footer')
        </footer>
        </div>
    </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


    <script src="/js/sweetalert2.min.js"></script>  
    <!-- DataTables -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    @stack('scripts')
    @stack('styles')
</body>
</html>
