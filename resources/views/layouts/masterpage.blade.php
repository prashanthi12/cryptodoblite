<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
        @yield('seometa')
 <title>{{ Config::get('settings.sitetitle') }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ Config::get('settings.favicon') }}" type="image/x-icon">

    <!-- Add css for new design -->
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}">
    
    <!-- Scripts -->
    <script>
        window.Laravel = {!!json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
    </script>
</head>
<body>
    <div id="app">
        @include('layouts.sitelinks')
            <div class="push-me-top">
            <div class="row-fluid">
                @yield('banner')
            </div>
            </div>
            <div class="row mb-100">
                @yield('content')
            </div>
        @include('layouts.footer')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('bottomscripts')
    {!! Config::get('settings.footerscript') !!}
    @include('layouts.footercss')  
</body>
</html>
