@php
    $currentUser = Auth::user(); 
    if (isset($currentUser->unreadNotifications))
    {
        $usernotifications = $currentUser->unreadNotifications;
        session(['usernotifications'=> $usernotifications]);             
    } 
    if(!is_null($currentUser))
    {
        $auto_withdrawal_status = \Config::get('settings.auto_withdrawal_status');
    }     
@endphp

@if (Auth::guest() && Cookie::get('sponsor') )
    <div class="row-fluid top-sponsor-bar">
        <div class="container">
            <div style="margin-top: 8px;"><p>{{ trans('welcome.invite')}} : {{ Cookie::get('sponsor') }}</p></div>
        </div>
    </div>
@endif

<!-- <nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <nav class="navbar navbar-static-top">
            <div class="nav-top">
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">{{ trans('mainnavigation.menulogin') }}</a></li>
                        <li><a href="{{ url('/register') }}">{{ trans('mainnavigation.menuregister') }}</a></li>
                        <li>
                            <select name="language_switch" id="language_switch" class="form-control" style="margin: 6px 0">
                                <option value="English" >English</option>
                                <option value="Romanian" >Romanian</option>
                            </select>
                        </li>   
                    @else
                        <li><a href="{{ url('myaccount/home') }}">{{ trans('mainnavigation.menumyaccount') }}</a></li>
                        @if (session('usergroupid') == 4)
                            <li class="dropdown">
                                @include('partials.notifications')
                            </li>
                        @endif
                        <li class="dropdown">                   
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @if (session('usergroupid') == 4)
                                    <li>
                                        <a href="{{ url('myaccount/profile/validate2fa') }}" >{{ trans('mainnavigation.menueditprofile') }}
                                        </a>                            
                                    </li>
                                     <li>
                                        <a href="{{ url('myaccount/change_password') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                        </a>                            
                                    </li>
                                    <li>
                                        <a href="{{ url('myaccount/transaction_password') }}" >{{ trans('mainnavigation.menutransactionpassword') }}
                                        </a>
                                       
                                    </li>
                                    <li>
                                        <a href="{{ url('myaccount/changeavatar') }}" >{{ trans('mainnavigation.menuechangeavatar') }}
                                        </a>
                                       
                                    </li>
                                    <li>
                                        <a href="{{ url('myaccount/twofactor') }}">{{ trans('mainnavigation.two_factor') }}</a>
                                    </li>
                                    @if($auto_withdrawal_status == 1)
                                    <li>
                                        <a href="{{ url('myaccount/autowithdrawal') }}" >{{ trans('mainnavigation.menuautowithdrawal') }}
                                        </a>
                                       
                                    </li>
                                    @endif
                                    @if(Auth::user()->isImpersonating())
                                     <li>
                                        <a href="{{ url('users/stop') }}" >{{ trans('mainnavigation.stop_impersonate') }}
                                        </a>                            
                                    </li>
                                    @endif
                                @else
                                    @if (session('usergroupid') == 3)
                                         <li>
                                            <a href="{{ url('staff/changepassword') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                            </a>                            
                                        </li>
                                    @endif
                                @endif
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ trans('mainnavigation.menulogout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif             
                </ul>
            </div>
        </nav>
    </div>
</nav> -->

<nav class="navbar">
<div class="">
    <div class="nav-bottom">
        <div class="navbar-header">            
            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}" title="{{ Config::get('settings.sitename') }}">
                <img src="{{ Config::get('settings.sitelogo') }}" alt="Crypto Invest">       
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                
            </button>
        </div> 
        <!-- Collapsed Hamburger -->
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <!-- <ul class="nav navbar-nav">
            </ul> -->
            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                <li><a href="{{ url('/') }}"><span>{{ trans('mainnavigation.menuhome') }}</span></a></li>
                <li><a href="{{ url('/about') }}"><span>{{ trans('mainnavigation.menuabout') }}</span></a></li>
                <li><a href="{{ url('/invest') }}"><span>{{ trans('mainnavigation.menuinvest') }}</span></a></li>           
                <li><a href="{{ url('/contact') }}"><span>{{ trans('mainnavigation.menucontact') }}</span></a></li>
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">{{ trans('mainnavigation.menulogin') }}</a></li>
                    <li><a href="{{ url('/register') }}">{{ trans('mainnavigation.menuregister') }}</a></li>
                    @if(Session::get('languageslist')->count() > 1)
                    <li>
                        <select name="language_switch" id="language_switch" class="form-control" style="margin: 6px 0">
                        @foreach (Session::get('languageslist') as $lang)
                            <option value="{{ url('/setlocale/'.$lang->abbr) }}" {{ Session::get('locale') == $lang->abbr ? 'selected' : '' }}>{{ $lang->name }}</option>
                        @endforeach
                        </select>
                    </li> 
                    @endif  
                @else
                    <li><a href="{{ url('myaccount/home') }}">{{ trans('mainnavigation.menumyaccount') }}</a></li>
                    @if (session('usergroupid') == 4)
                        <li class="dropdown">
                            @include('partials.notifications')
                        </li>
                    @endif
                    <li class="dropdown">                   
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            @if (session('usergroupid') == 4)
                                <li>
                                    <a href="{{ url('myaccount/profile/validate2fa') }}" >{{ trans('mainnavigation.menueditprofile') }}
                                    </a>                            
                                </li>
                                 <li>
                                    <a href="{{ url('myaccount/change_password') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                    </a>                            
                                </li>
                                <li>
                                    <a href="{{ url('myaccount/transaction_password') }}" >{{ trans('mainnavigation.menutransactionpassword') }}
                                    </a>
                                   
                                </li>
                                <li>
                                    <a href="{{ url('myaccount/changeavatar') }}" >{{ trans('mainnavigation.menuechangeavatar') }}
                                    </a>
                                   
                                </li>
                                <li>
                                    <a href="{{ url('myaccount/twofactor') }}">{{ trans('mainnavigation.two_factor') }}</a>
                                </li>
                                @if($auto_withdrawal_status == 1)
                                <li>
                                    <a href="{{ url('myaccount/autowithdrawal') }}" >{{ trans('mainnavigation.menuautowithdrawal') }}
                                    </a>
                                   
                                </li>
                                @endif
                                @if(Auth::user()->isImpersonating())
                                 <li>
                                    <a href="{{ url('users/stop') }}" >{{ trans('mainnavigation.stop_impersonate') }}
                                    </a>                            
                                </li>
                                @endif
                            @else
                                @if (session('usergroupid') == 3)
                                     <li>
                                        <a href="{{ url('staff/changepassword') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                        </a>                            
                                    </li>
                                @endif
                            @endif
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ trans('mainnavigation.menulogout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif 
            </ul>
        </div>
    </div>
</div>
</nav>


@push('bottomscripts')
<style type="text/css">
    .navbar
    {
        margin-bottom: 1px;
        border: none;
    }
    .navbar-default
    {
        background-color: none;
    }
    .navbar-default .navbar-nav > li a
    {
        color: #fba561;
        font-size: 1.6rem;
    }
    .dropdown-menu > li a
    {
        color: #333333 !important;
    }
</style>

<script>
        jQuery(document).ready(function($) {
            $("#language_switch").change(function() {
                window.location.href = $(this).val();
            })
        });
</script>
@endpush
