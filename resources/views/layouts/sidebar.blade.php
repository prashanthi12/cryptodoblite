<div class="">
    <div class="profile-sidebar" style="margin-bottom: 30px;">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
            @if (session('profileimage'))
            <img src="{{ url(session('profileimage')) }}" class="img-responsive" alt="">
            @else
            <img src="http://placehold.it/120x120?Image+Here" class="img-responsive" alt="">
            @endif   
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name">
                <p class="text-uppercase">{{ Auth::user()->name }}</p>
                <p class="text-uppercase"><small>{{ trans('myaccountssidebar.sponsor') }} : {{ session('sponsor') }}</small></p>
            </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="sidebar-button-group">
            <a type="button" class="btn btn-success btn-block" href=" {{ url('myaccount/deposit') }}">{{ trans('myaccount.make_deposit') }}</a>
            <a type="button" class="btn btn-danger btn-block" href=" {{ url('myaccount/invite_friend') }}">{{ trans('myaccount.refer_friend') }}</a>
              <button id="nav-2-button" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <i class="glyphicon glyphicon glyphicon-option-vertical">MENU</i>
            </button>
        </div>
        <!-- END SIDEBAR BUTTONS -->
      
        <!-- SIDEBAR MENU -->
        <div  class="profile-usermenu collapse navbar-collapse" id="sidebar-navbar-collapse">
            <ul class="nav">
                <li>
                    <a href="{{ url('/myaccount/home') }}">
                        <i class="glyphicon glyphicon-th"></i> {{ trans('sidebar.dashboard') }} </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/viewprofile') }}">
                        <i class="glyphicon glyphicon-user"></i> {{ trans('sidebar.profile') }}</a>
                </li>
                @if(\Config::get('settings.ewallet_status') == '1')
                <li>
                    <a href="{{ url('/myaccount/ewallet/history') }}">
                        <i class="glyphicon glyphicon-briefcase"></i> {{ trans('sidebar.ewallet') }}
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{ url('/myaccount/myepin') }}">
                        <i class="glyphicon glyphicon-pushpin"></i> {{ trans('sidebar.myepin') }} </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/viewpayaccounts') }}">
                        <i class="glyphicon glyphicon-credit-card"></i> {{ trans('sidebar.mypayaccounts') }}</a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/viewdeposits/new') }}" target="_self">
                        <i class="glyphicon glyphicon-usd"></i> {{ trans('sidebar.deposits') }} </a>
                </li>          
                <li>
                    <a href="{{ url('myaccount/withdraw/pending') }}" target="_self"><i class="glyphicon glyphicon-ok"></i>{{ trans('sidebar.withdraws') }} </a>                          
                </li>
                <li>
                    <a href="{{ url('myaccount/fundtransfer/type/send') }}" target="_self"><i class="glyphicon glyphicon-random"></i>{{ trans('sidebar.fund_transfer') }}</a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/earnings/interest') }}">
                        <i class="glyphicon glyphicon-usd"></i> {{ trans('sidebar.earnings') }} </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/bonus') }}">
                        <i class="glyphicon glyphicon-plus"></i> {{ trans('sidebar.bonus') }} </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/penalties') }}">
                        <i class="glyphicon glyphicon-minus"></i> {{ trans('sidebar.penalties') }}
                        </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/referrals') }}">
                        <i class="glyphicon glyphicon-link"></i>  {{ trans('sidebar.referrals') }}</a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/network') }}">
                        <i class="glyphicon glyphicon-cloud"></i> {{ trans('sidebar.networks') }} </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/ticket') }}">
                        <i class="glyphicon glyphicon-tasks"></i> {{ trans('sidebar.supporttickets') }} </a>
                </li>
                <li>
                    <a href="{{ url('/myaccount/message/list') }}">
                        <i class="glyphicon glyphicon-envelope"></i> {{ trans('sidebar.message') }} </a>
                </li>
                @if (session('checktestimonial') == 0)
                <li>
                    <a href="{{ url('/myaccount/testimonial') }}">
                        <i class="glyphicon glyphicon-flag"></i> {{ trans('sidebar.addtestimonial') }} </a>
                </li>
                @endif
                <li>
                    <a href="{{ url('/myaccount/myinbox') }}">
                        <i class="glyphicon glyphicon-comment"></i> {{ trans('sidebar.myinbox') }} </a>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
</div>
