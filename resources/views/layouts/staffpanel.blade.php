<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Config::get('settings.sitename') }} :: Staff Panel</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                     <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}" title="{{ Config::get('settings.sitename') }}">
                    <img src="{{ Config::get('settings.sitelogo') }}" alt="{{ Config::get('settings.sitename') }}">        
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else                         

                            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                        <ul class="dropdown-menu" role="menu">
                             <li>
                                <a href="{{ url('staff/changepassword') }}" >{{ trans('mainnavigation.menuchangepassword') }}
                                </a>                            
                            </li>
                             @if(Auth::user()->isImpersonating())
                             <li>
                                <a href="{{ url('users/stop') }}" >{{ trans('mainnavigation.stop_impersonate') }}
                                </a>                            
                            </li>
                            @endif
                            
                          
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ trans('mainnavigation.menulogout') }}
                                        </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-admin  navbar-fixed-top navbar-fixed-top-2 push-me-top">
            <div class="container">
                    <ul class="nav navbar-nav">

                        <li class="admin-menu"><li class="admin-menu"><a href="{{ url('staff/dashboard') }}"><i class="glyphicon glyphicon-th"></i>
                                 <br/>{{ trans('sidebar.dashboard') }}</a></li>
                       
                         <li class="admin-menu"><li class="admin-menu"><a href="{{ url('staff/ticket') }}"><i class="glyphicon glyphicon-edit"></i>
                                 <br/>{{ trans('sidebar.supporttickets') }}</a></li>
                    </ul>
            </div>
        </nav>
        <div class="mt-120">
        @yield('content')
        </div>
        <footer>
        @include('adminpartials.footer')
        </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    @stack('scripts')
</body>
</html>
