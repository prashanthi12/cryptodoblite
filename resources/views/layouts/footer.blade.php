<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center">
                <img src="{{ url('/uploads/img/footer_logo.png') }}">
                <ul class="list-unstyled list-inline nav-justified">
                    <li><a href="{{ url('/contact') }}">{{ trans('mainnavigation.menucontact') }}</a></li>
                    <li><a href="{{ url('/faq') }}">Support</a></li>
                    <li><a href="{{ url('/terms') }}">{{ trans('mainnavigation.menutermsservice') }}</a></li>
                    <li><a href="{{ url('/privacy') }}">{{ trans('mainnavigation.menuprivacypolicy') }}</a></li>
                </ul>
                <p class="text-justify"><strong>{{ trans('mainnavigation.disclaimer') }}</strong>{{ trans('mainnavigation.disclaimercontent') }}</p>
            </div>
            <div class="col-md-6" style="margin: 50px 0;">
                <div class="row">
                    <div class="col-xs-3"><a href="#"><img src="{{ url('/uploads/img/facebook.png') }}"></a></div>
                    <div class="col-xs-3"><a href="#"><img src="{{ url('/uploads/img/twitter.png') }}"></a></div>
                    <div class="col-xs-3"><a href="#"><img src="{{ url('/uploads/img/telegram.png') }}"></a></div>
                    <div class="col-xs-3"><a href="#"><img src="{{ url('/uploads/img/youtube.png') }}"></a></div>
                </div>
            </div>
        </div>
    </div>
</footer>