<table class="table table-bordered table-striped dataTable" id="transferdatatable">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('myaccount.ewallet_amount') }} ({{ config::get('settings.currency') }})</th>
			<th>{{ trans('myaccount.ewallet_datetime') }}</th>
			<th>{{ trans('myaccount.type') }}</th>			
			<th>{{ trans('myaccount.ewallet_comment') }}</th>	
		</tr>
	</thead>
	<tbody>
	@if (count($internal_transfer ) > 0)
		@foreach($internal_transfer  as $internal_transfer)
			<tr>
				<td>{{ $loop->iteration }}</td>		
				<td>{{ $internal_transfer->amount }} </td>
				<td>{{ $internal_transfer->created_at->format('d/m/Y H:i:s') }}</td>	
				@if($internal_transfer->type=='credit')
				<td><span class="label label-success">{{ ucfirst($internal_transfer->type) }}</span></td>
				@elseif($internal_transfer->type=='debit')
				<td><span class="label label-warning">{{ ucfirst($internal_transfer->type) }}</span></td>		
				@endif
				<td>{{ $internal_transfer->comment }}</td>
			</tr>
		@endforeach
	@endif
	</tbody>
</table>

@push('scripts')
<script>
$(document).ready(function(){
    $('#transferdatatable').DataTable();           
});
</script>
@endpush