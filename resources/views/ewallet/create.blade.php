@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.ewallet_addfund') }}
    <a href="{{ url('myaccount/ewallet/history') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
                @include('layouts.message')
            </div>
        </div>
        <div class="row">
            @include('ewallet.addfundform')
        </div>
    </div>
</div>
@endsection
