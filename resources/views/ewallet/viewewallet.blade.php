@extends('layouts.myaccount')
@section('content')
<div class="panel panel-default">
    <div class="row">
        <div class="col-md-12">
            @include('layouts.message')
        </div>
    </div>
    <div class="panel-heading">{{ trans('myaccount.ewallet_title') }}
        <a href="{{ url('myaccount/ewallet') }}" class="pull-right">{{ trans('myaccount.ewallet_addfund') }}</a>
    </div>
    <div class="panel-body">
        <div class="row">
            <!-- <div class="col-md-6">          
                {{ trans('myaccount.ewallet_fund_total') }} 
                <h3 class="is-amount">{{$total_amount}} ({{ config::get('settings.currency') }})</h3>
            </div> -->
            <div class="col-md-6">
                {{ trans('myaccount.ewallet_total') }} 
                <h3 class="is-amount">{{ $ewalletbalance }} ({{ config::get('settings.currency') }})</h3>
            </div>
            <div class="col-md-6">
                {{ trans('myaccount.btc_total') }} 
                @if(count($user_accounts)>0)
                    <p>{{$user_accounts->btc_address}}</p>
                    <h3 class="is-amount">{{ $btcbalance }} (BTC)</h3>
                    <p class="btc"><a href="{{url('myaccount/type/btc/send')}}" class="btn btn-primary">{{trans('myaccount.send')}}</a> 
                    <a href="{{url('myaccount/type/btc/receive')}}" class="btn btn-primary">{{trans('myaccount.receive')}}</a></p>
                @else
                    <p><a href="#" class="card-link btn btn-primary btn-sm btcaddress">Create BTC Wallet Address</a></p>
                    <p id="btcaddress"></p> 
                @endif
                <p style="display:none" class="btc"><a href="{{url('myaccount/type/btc/send')}}" class="card-link btn btn-primary">{{trans('myaccount.send')}}</a> <a href="{{url('myaccount/type/btc/receive')}}" class="card-link btn btn-primary">{{trans('myaccount.receive')}}</a> </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="tab" class="btn-group" >
                    <a href="{{ url('myaccount/ewallet/history') }}" class="btn {{ $status == 'history' ? 'active' : '' }}" >{{ trans('myaccount.e-wallet_history') }}</a>
                    <a href="{{ url('myaccount/ewallet/fundhistory') }}" class="btn {{ $status == 'fundhistory' ? 'active' : '' }}" >{{ trans('myaccount.fund_history') }}</a>
                </div>
                @if ($status == 'history')
                    @include('ewallet._table_ewallet') 
                @elseif ($status == 'fundhistory')
                    @include('ewallet._table_ewallet_fund')
                @endif
            </div>
        </div>
        <!-- <div class="row mt-20">
            <div class="col-md-12">          
                <h3 class="withdraw_title">{{ trans('myaccount.internal_transfer_history') }}</h3>
                <div class="row responsive">
                    @include('ewallet._table_internal_transfer')
                </div>
            </div>
        </div> -->
    </div>
</div>
@endsection

@push('bottomscripts')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    $(document).ready(function(){
        $(".btcaddress").click(function(e){
            e.preventDefault();
            $.ajax({type: "POST",
                url: "{{url('myaccount/createwallet/btc')}}",            
                success:function(result){
                    if(result!='')
                    {
                        $("#btcaddress").html(result);
                        $(".btcaddress").css('display','none');
                        $(".btc").css('display','block');
                    }
                    else
                    {
                        $("#btcaddress").html("Try After Sometime");
                    }
                }
            });
        });
    });
</script>
@endpush