
        @foreach($commentlists as $data)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    {{ $data->user->name }}
                    <span class="pull-right"> {{ $data->created_at->diffForHumans() }} </span>
                </h3>
            </div>
            <div class="panel-body">
                <div class="content">
                    <p>{{ $data['content'] }}</p>
                </div>
            </div>
        </div>
        @endforeach

       {{ $commentlists->links() }}