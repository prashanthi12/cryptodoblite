<div class="grid">
@if (count($result))
    @foreach($result as $data)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('forms.subject') }} : </small><br/>       
            <a href="{{ url('myaccount/ticket/'.$data['id']) }}">              
                {{ $data['subject'] }}
            </a>
      </p>
    </div>
    <div class="">
      <p style="text-align: left;">
        <small>{{ trans('forms.status') }} : </small><br/>       
          {{ $data->status->name }}<br>
        <small>{{ trans('forms.agent') }} :</small><br/>
            {{ $data->agent->name }}
      </p>
    </div>
    
    <div class="">
      <p>
        <small>{{ trans('forms.createdon') }} :</small><br/>
          {{ $data->created_at->diffForHumans() }}<br>
        <small>{{ trans('forms.lastupdated') }} :</small><br/>
          {{ $data->updated_at->diffForHumans() }}
      </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="">{{ trans('forms.noticketsfound') }}</div>
@endif
</div>
{{ $result->links() }}


