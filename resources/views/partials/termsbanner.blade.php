<!-- <div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgterms')) }})"> -->
<div class="bg-pages">
    <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-orange">{{ trans('terms.title') }}</h1> 
            <h3 class="title-orange">{{ trans('terms.description') }}</h3>       
        </hgroup>
    </div>
</div> 
