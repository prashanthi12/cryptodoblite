<!-- <div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgnews')) }})"> -->
<div class="bg-pages">
    <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-orange">{{ trans('pages.newstitle') }}</h1>        
            <h3 class="title-orange">{{ trans('pages.newssubtitle') }}</h3>
        </hgroup>
    </div>
</div> 
