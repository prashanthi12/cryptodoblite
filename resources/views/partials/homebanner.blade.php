<header>
  <div class="container">
    <div class="row">
      <div class="col-md-6 text-center">
        <img src="{{ url('/uploads/img/header_img.png') }}">
      </div>
      <div class="col-md-6">
        <h3>DISCOVER</h3>
        <h1>SUCCESS FORMULA</h1>
        <h3>APPROCH TO INVESTING IN EVER EVOLVING CRYPTO CURRENCY MARKET</h3>
        <a href="{{ url('/invest') }}" type="button" class="btn btn-custom btn-lg">START AN INVESTMENT</a>
      </div>
    </div>
  </div>
</header>

@push('bottomscripts')
<style type="text/css">
    .btn:hover
    {
        color: white !important;
    }
</style>
@endpush