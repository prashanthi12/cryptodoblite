<!-- <div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgabout')) }})"> -->
<div class="bg-pages">
    <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-orange">{{ trans('aboutus.title') }}</h1>        
            <h3 class="title-orange">{{ trans('aboutus.description') }}</h3>
        </hgroup>
    </div>
</div> 
