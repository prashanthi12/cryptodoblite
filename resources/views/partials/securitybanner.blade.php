<!-- <div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgsecurity')) }})"> -->
<div class="bg-pages">
    <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-orange">{{ trans('securitytips.title') }}</h1>  
            <h3 class="title-orange">{{ trans('securitytips.description') }}</h3>           
        </hgroup>
    </div>
</div> 
