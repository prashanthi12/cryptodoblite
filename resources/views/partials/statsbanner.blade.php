<!-- <div class="bg-pages" style="background-image: url({{ url(Config::get('settings.bgstats')) }})"> -->
<div class="bg-pages">
    <div class="container v-center page-title">
        <hgroup>
            <h1 class="title-orange">{{ trans('pages.statstitle') }}</h1>        
            <h3 class="title-orange">{{ trans('pages.statsdesc') }}</h3>
        </hgroup>
    </div>
</div> 
