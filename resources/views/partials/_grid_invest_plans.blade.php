@unless( is_null($plans) )
			<div class="col-md-12">
			<div class="">
			@foreach ($plans as $plan)
			<div class="mt-10 mb-10">
				<div class="row">
					<div class="col-md-12">
						<h3>{{ ucfirst($plan->name) }}</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p>
						@if (is_null($plan->image))
							<img src="http://placehold.it/250x200">
						@else
						<img src="{{ url($plan->image) }}" class="plan-image plan-image-grid" width="250px;" height="200px;">
						@endif
						</p>
						<p>          
							<button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal">
                         		{{ trans('hyip.calculate_interest') }}		
                			</button>
    					</p>
					</div>
					<div class="col-md-6">
						<table class="table table-bordered table-striped">
							<tr>
								<td width="65%">{{ trans('hyip.plan_type') }} </td>
								<td width="35%">{{ ucfirst($plan->plantype->plantype) }}</td>
							</tr>
							<tr>
								<td>{{ trans('hyip.interest_rate') }}</td>
								<td>{{ $plan->interest_rate }} %</td>
							</tr>
							<tr>
								<td>{{ trans('hyip.duration') }} </td>
								<td>{{ $plan->duration }} {{ $plan->duration_key }}</td>
							</tr>
							<tr>
								<td>{{ trans('hyip.min_deposit') }}</td>
								<td> {{ sprintf("%.2f", $plan->min_amount) }} {{ Config::get('settings.currency') }}</td>
							</tr>
							<tr>
								<td> {{ trans('hyip.max_deposit') }}</td>
								<td> {{ sprintf("%.2f", $plan->max_amount) }} {{ Config::get('settings.currency') }}</td>
							</tr>
							<tr>
								<td> {{ trans('hyip.principal_return') }}</td>
								<td>
									@if($plan->principle_return)
										{{ trans('hyip.yes') }}
									@else 
										{{ trans('hyip.no') }}
									@endif
								</td>
							</tr>
							@if($plan->compounding)
							<tr>
								<td> {{ trans('hyip.compounding') }}</td>
								<td>
									{{ trans('hyip.yes') }}
								</td>
							</tr>
							@endif
							@if($plan->partial_withdraw_status)
							<tr>
								<td> {{ trans('hyip.partial_withdraw') }}</td>
								<td>
										{{ trans('hyip.yes') }}
								</td>
							</tr>
							@endif
							@if($plan->partial_withdraw_status)
							<tr>
								<td>{{ trans('hyip.max_partial_withdraw') }}</td>
								<td>{{ $plan->max_partial_withdraw_limit }} % </td>
							</tr>
							<tr>
								<td>{{ trans('hyip.partial_withdraw_locking') }}</td>
								<td>{{ $plan->minimum_locking_period }} {{ $plan->duration_key }}</td>
							</tr>
							@endif
						</table>
					</div>
					
				</div>
			</div>
			<hr>
			@endforeach
			</div>
			</div>
@endunless