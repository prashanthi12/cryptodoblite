@unless( is_null($plans) )
	<table class="table table-bordered" style="background-color: #fff">
		<thead>
			<tr>
				<th>#</th>
				<th>{{ trans('pages.plan_name') }}</th>
				<th>{{ trans('pages.plan_type') }}</th>
				<th>{{ trans('pages.interest_rate') }}</th>
				<th>{{ trans('pages.duration') }}</th>
				<th>{{ trans('pages.min_deposit') }} ({{ config::get('settings.currency') }})</th>
				<th>{{ trans('pages.max_deposit') }} ({{ config::get('settings.currency') }})</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($plans as $plan)
			<tr>
				<td>{{ $loop->iteration }}</td>
				<td><a href="{{ url('invest/plan/'.$plan->id) }}">{{ ucfirst($plan->name) }}</a></td>
				<td>{{ ucfirst($plan->plantype->plantype) }}</td>
				<td>{{ $plan->interest_rate }} %</td>
				<td>{{ $plan->duration }}</td>
				<td class="text-right">{{ $plan->min_amount }} </td>
				<td class="text-right">{{ $plan->max_amount }} </td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endunless