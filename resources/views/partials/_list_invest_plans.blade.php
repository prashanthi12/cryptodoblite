@unless( is_null($plans) )
			<ul class="flex plan-grid">	
				@foreach ($plans as $plan)
				<li class="plan-list-item">
				<div class="flex flex-c">
						<div class="plan-image">
							<a href="{{ url('invest/plan/'.$plan->id) }}">
								@if (is_null($plan->image))
								<img src="http://placehold.it/250x200">
								@else
								<img src="{{ url($plan->image) }}" class="plan-image plan-image-welcome" width="250px;" height="200px;">
								@endif
							</a>
						</div>
						<div class="plan-info-1">
							<dl class="plan-info-list plan-name dl-horizontal">
								<dt>{{ trans('hyip.plan_name') }}</dt>
								<dd>{{ ucfirst($plan->name) }}</dd>
								<dt>{{ trans('hyip.plan_type') }} </dt>
								<dd>{{ ucfirst($plan->plantype->plantype) }}</dd>
								<dt>{{ trans('hyip.duration') }}</dt>
								<dd>{{ $plan->duration }} {{ $plan->duration_key }}</dd>
								<dt>{{ trans('hyip.min_deposit') }}</dt>
								<dd>{{ $plan->min_amount }} {{ Config::get('settings.currency') }}</dd>
								<dt>{{ trans('hyip.max_deposit') }}</dt>
								<dd>{{ $plan->max_amount }} {{ Config::get('settings.currency') }}</dd>
								<dt>{{ trans('hyip.interest_rate') }}</dt>
								<dd>{{ $plan->interest_rate }} %</dd>
							</dl>
						</div>
					</li>
				@endforeach
				
			</ul>
@endunless
<p class="text-center mt-20 mb-40">

<a href="{{ url('/invest') }}" class="btn btn-lg btn-primary">{{ trans('hyip.view_all_plans') }}</a>
          <button type="button" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModal">
                          {{ trans('hyip.calculate_interest') }}
                </button>
                    </p>
