<div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->
  <!-- <div class="overlay"></div> -->

  <!-- Indicators -->
  <ol class="carousel-indicators">

  @foreach ($slider as $data)
  <li data-target="#bs-carousel" data-slide-to="{{ ($loop->iteration)-1 }}" class ="{{ $loop->iteration == 1 ? 'active' : '' }}"></li>
  @endforeach
  </ol>
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    @foreach ($slider as $data)
    @php
    $path = str_replace('\\', '/', $data['image'])
    @endphp
    <div class="item slides {{ $loop->iteration == 1 ? 'active' : '' }}">
      <div class="slide-1" style="background-image: url({{ $path }})">
      </div>
      <div class="hero">
        <hgroup>
            <h3>{{ $data['slidertext'] }}</h3>        
        </hgroup>
        <a href="{{ url($data['url']) }}" class="btn btn-hero btn-lg" target="_blank">{{ $data['urltext'] }}</a>
      </div>
    </div>
    @endforeach
  </div> 
</div>