@extends('layouts.myaccount') 

@section('content')
<h1 class="page-title mb-20 dark-text">{{ trans('myaccount.btc_receive') }}</h1>
<div class="d-card w-600">
    <div style="background-color: #fff; padding: 20px; width: 250px">
    <div id="qrcode_btc" class="mb-20"></div>
    </div>
    <p class="btc-receive big-text"><b>{{ $btc_address }}</b></p>
    <input type="button" id="copy" value="Copy" class="btn btn-primary" onclick="copyToClipboard('.btc-receive')">
</div>
@endsection

@push('bottomscripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>
<script type="text/javascript">

$(function() {
    $('#qrcode_btc').qrcode({    
        text: "{{ $btc_address }}",
        ecLevel: 'L',
        width: 200,
        height: 200,
    });
});

// Copy to clipboard example
  //document.querySelector("#visible-button").onclick = function() {
  // Select the content
  //document.querySelector("#visible-input").select();
 
  // Copy to the clipboard
 // document.execCommand('copy');
//};
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  $('#copy').val("Copied");
  $('#copy').addClass("copytext");
}
</script>
@endpush   