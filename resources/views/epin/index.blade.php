@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
      {{ trans('myaccount.myepins') }}
        <a href="{{ url('myaccount/myepin/create') }}" class="pull-right">{{ trans('myaccount.createepin') }}</a>
  </div>
<div class="panel-body">
<div class="grid">
@if(count($epinviewdetails) > 0)
    @foreach($epinviewdetails as $details)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.couponvalue') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $details['coupon_value'] }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.couponcode') }} : </small><br/>       
          {{ $details['coupon_code'] }}<br>
        <small>{{ trans('myaccount.createdon') }} :</small><br/>
          {{ $details['created_at']->diffForHumans() }}
      </p>
    </div>   
    <div class="">
      <p>
        <small>{{ trans('myaccount.status') }} :</small><br/>
              {{ $details['status'] }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.usedby') }}</small><br/>
            @if($details['used_by'] != "")
                  {{ $details['used_by'] }}
            @else
                    - 
            @endif 
          <br>
        <small>{{ trans('myaccount.usedon') }} : </small><br/>       
          @if($details['used_by'] != "")
              {{ $details['updated_at']->diffForHumans() }}
          @else
              - 
          @endif
      </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.no_fundtransfer_found') }}</div>
@endif
</div>
{{ $epinviewdetails->links() }}
</div>
</div>
@endsection





