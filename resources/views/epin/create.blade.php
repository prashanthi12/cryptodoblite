@extends('layouts.myaccount')
 @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.create-epin') }}</div>
    <div class="panel-body">
      <div class="row">
        <div class="row"> 
          <div class="col-md-10 col-md-offset-1">
            <div class="col-md-10 col-md-offset-1">
              <div id="depositfrm">
                <form action="{{ url('/myaccount/myepin/create') }}" method="POST" id="myepin">
                  {{ csrf_field() }}
                    <input type="hidden" name="balanceamount" id="balanceamount" class='form-control' value="{{ $ewalletbalance }}">
                    
                  <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                    <label>{{ trans('myaccount.epin_value_amount_lbl') }}</label>
                    <input type="text" name="amount" id="amount" class='form-control' value="{{ old('amount') }}">
                    <small class="text-danger">{{ $errors->first('amount') }}</small>
                  </div>

                  <div class="form-group{{ $errors->has('quantity') ? ' has-error' : '' }}">
                    <label>{{ trans('myaccount.quantity_lbl') }}</label>
                    <input type="text" name="quantity" id="quantity" class='form-control' value="{{ old('quantity') }}">
                    <small class="text-danger">{{ $errors->first('quantity') }}</small>
                  </div>

                  <div class="form-group">    
                    <input value="{{ trans('myaccount.create_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
                    <a href="" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
                  </div>
                </form>
              </div>
            </div>  
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
