<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
        @if (count($quotes))
            @foreach($quotes as $data)
                @php
                    $lang = \Session::get('locale');
                @endphp
                <div class="col-md-12">
                    <div id="postlist">
                        <div class="panel">
                            <div class="panel-heading">
                                <div class="text-center">
                                    <div class="row">
                                        <div class="col-sm-9">
                                            <h3 class="pull-left"><img src="{{ $data->authorimage }}" style="width: 120px; height: 120px;"></h3>
                                        </div>
                                        <div class="col-sm-3">
                                            <h4 class="pull-right" style="margin-top: 32px;">
                                            <small><em>{{ $data->authorname }}</em></small>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>       
                            <div class="panel-body">
                                {{ $data->text }}
                            </div>        
                        </div>
                    </div>       
                </div>
            @endforeach
        @else
            <div class="col-md-12">
                <center>{{ trans('pages.noquotesfound') }}</center>
            </div>
        @endif
        </div>
    </div>
</div>
