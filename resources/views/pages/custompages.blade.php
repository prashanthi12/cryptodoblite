@extends('layouts.main') 
@section('pagesbanner')
    @include('partials.pagesbanner')
@endsection
@section('content')
<div class="page-content">
<div class="row bg-color-1">
    <div class="container">
        <div class="col-md-3">
            <nav class="nav-sidebar">
                <ul class="nav">
                    @foreach ($pages as $data)
                    <?php $activeclass = '' ?>
                    @if ($slug == $data['slug'])
                        <?php $activeclass = 'active'  ?>
                    @endif
                    @php
                        $lang = \Session::get('locale');
                    @endphp
                    <li class="{{ $activeclass }}"><a href="{{ url('/page/'.$lang.'/'.$data['slug']) }}">{{ $data['navlabel'] }}</a></li>  
                    @endforeach             
                </ul>
            </nav>
        </div>
        <div class="col-md-9">
            {!! $pagedetails->content !!}
        </div>
    </div>
</div>
</div>
@endsection