<div class="container">
<div class="row">
<div class="container mt-20 mb-20">
@if (count($exchange))
@foreach($exchange as $data)
    <div class="col-md-12">
        <div id="postlist">
            <div class="panel">      
                
            <div class="panel-body">
                {!! $data->code !!}
            </div>        
        </div>
    </div>       
</div>
@endforeach
@else
    <div class="col-md-12">
        {{ trans('pages.noexchangefound') }}
    </div>
@endif
</div>
</div>
</div>
{{ $exchange->links() }}