<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{{ trans('pages.username') }}</th>
                        <th>{{ trans('pages.datetime') }}</th>                    
                        <th>{{ trans('pages.amount') }} ({{  \Config::get('settings.currency') }})</th>
                        <th>{{ trans('pages.paymentmode') }}</th>
                    </tr>
                </thead>
                <tbody> 
                    @if (count($payouts) > 0)
                    @foreach($payouts as $data)
                        <tr>
                            <td>{{ ucfirst($data->user->name) }}</td>
                            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
                            <td>{{ $data->amount }} </td>
                            <td>{{ $data->userpayaccounts->payment->displayname }}
                        </tr>
                    @endforeach
                    @else
                        <tr><td colspan="5">{{ trans('pages.no_records_found') }}</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{{ $payouts->links() }}