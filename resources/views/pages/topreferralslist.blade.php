<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
            <table class="table table-bordered" id="topreferraldatatable">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans('pages.username') }}</th>
                        <th>{{ trans('pages.referrals') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($usersData) > 0)
                    @foreach($usersData as $name => $refCount)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ ucfirst($name) }}</td>
                            <td>{{ $refCount }}</td>
                        </tr>
                    @endforeach
                    @else
                    <tr><td colspan="4">{{ trans('pages.no_records_found') }}</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@push('bottomscripts')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {    

     $('#topreferraldatatable').DataTable();

});    

</script>
@endpush

