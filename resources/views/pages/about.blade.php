@extends('layouts.masterpage') 
@section('banner')
	@include('partials.aboutbanner')
@endsection

@section('content')
<div class="page-content">
<div class="row bg-color-1">
    <div class="container">
        @if(count($pages) == 0)
            <div class="col-md-12">
                {!! trans('aboutus.content') !!}
            </div>
        @else
        <div class="mt-50 mb-100">
            <div class="col-md-3">
                <div class="boxy boxy-white">
                    <nav class="nav-sidebar">
                        <ul class="nav">
                            @foreach ($pages as $data)
                                <li><a href="{{ url('/page/'.$lang.'/'.$data['slug']) }}">{{ $data['navlabel'] }}</a></li>  
                            @endforeach               
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-9">
                {!! trans('aboutus.content') !!}
            </div>
        </div>
        @endif
    </div>
</div>
</div>
@endsection
