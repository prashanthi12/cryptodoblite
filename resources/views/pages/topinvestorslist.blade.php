<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
            <table class="table table-bordered" id="topinvestordatatable">
                <thead>
                    <tr>
                        <th>#</th>  
                        <th>{{ trans('myaccount.username') }}</th>                         
                        <th>{{ trans('myaccount.amount') }} ({{ (\Config::get('settings.currency')) }})</th>
                         
                    </tr>
                </thead>
                <tbody>
                    @if (count($topinvestors) > 0)
                    @foreach($topinvestors as $name => $amount)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ ucfirst($name) }}</td>
                            <td>
                            {{ $amount }} 
                            </td>
                            
                        </tr>
                    @endforeach
                    @else
                        <tr><td colspan="4">{{ trans('pages.no_records_found') }}</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@push('bottomscripts')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {    

     $('#topinvestordatatable').DataTable();

});    

</script>
@endpush