@extends('layouts.main') 
@section('banner')
	@include('partials.investbanner')
@endsection
@section('content')
<div class="page-content">
	<div class="row">
		<div class="container">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<h3>{{ trans('pages.invest') }}</h3>
						<div >
							@include('partials._grid_invest_plans')
						</div>
						
				</div>
			</div>
		</div>

	</div>
	</div>
	</div>
@include('partials/calculator_modal')
@endsection
