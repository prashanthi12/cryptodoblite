<div class="container">
    <div class="row">
        <div class="container mt-20 mb-20">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td> {{ trans('welcome.site_started') }} </td>
                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime(config::get('settings.sitestarted')))->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <td> {{ trans('welcome.total_accounts') }} </td>
                        <td>{{ $totalAccount }}</td>
                    </tr>
                    <tr>
                        <td> {{ trans('welcome.new_account') }} </td>
                        @if(isset($newAccount->name))
                            <td>{{ $newAccount->name }}</td>
                        @else
                            <td>-</td>
                        @endif
                    </tr>
                    <tr>
                        <td> {{ trans('welcome.total_deposit') }} </td>
                        <td>{{ config::get('settings.intial_total_deposit_amount') + $totalDeposits }} {{ config::get('settings.currency') }}</td>
                    </tr>
                    <tr>
                        <td> {{ trans('welcome.total_withdraw') }} </td>
                        <td>{{ config::get('settings.intial_total_withdraw_amount') + $totalWithdraws }} {{ config::get('settings.currency') }}</td>
                    </tr>              
                    <tr>
                        <td> {{ trans('welcome.last_deposit') }} </td>
                        @if ($last_deposit)
                            <td>{{ config::get('settings.last_deposit_amount') + $last_deposit->amount }} {{ config::get('settings.currency') }}</td>
                        @else
                            <td>{{ config::get('settings.last_deposit_amount') }} {{ config::get('settings.currency') }}</td>
                        @endif
                    </tr>
                    <tr>
                        <td> {{ trans('welcome.last_withdraw') }} </td>
                        @if ($last_withdraw)
                            <td>{{ config::get('settings.last_withdraw_amount') + $last_withdraw->amount }} {{ config::get('settings.currency') }}</td>
                        @else
                            <td>{{ config::get('settings.last_withdraw_amount') }} {{ config::get('settings.currency') }}</td>
                        @endif
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
