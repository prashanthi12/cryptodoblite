<div class="grid">
@if (count($transferlists) > 0)
    @foreach($transferlists as $data)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        @if ($type == 'send')
            <small>{{ trans('myaccount.receiver') }} : </small><br/>       
            {{ $data->present()->getUsername($data->fundtransfer_to_id->user_id) }} 
        @elseif ($type == 'received') 
            <small>{{ trans('myaccount.sender') }} : </small><br/>
            {{ $data->present()->getUsername($data->fundtransfer_from_id->user_id) }}
        @endif        
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }}) : </small><br/>       
          {{ $data->amount }}<br>
      </p>
    </div>
    
    <div class="">
      <p>
        <small>{{ trans('myaccount.send_date') }} :</small><br/>
          {{ $data->created_at->diffForHumans() }}<br>
      </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.no_fundtransfer_found') }}</div>
@endif
</div>
{{ $transferlists->links() }}


