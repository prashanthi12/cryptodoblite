@extends('layouts.myaccount')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('forms.2fa_secret_key') }}</div>
        <div class="panel-body">
            {{ trans('forms.2fa_removed') }}
            <br /><br />
            <a href="{{ url('/myaccount/twofactor') }}">{{ trans('forms.go_back_btn') }}</a>
        </div>
    </div>
@endsection