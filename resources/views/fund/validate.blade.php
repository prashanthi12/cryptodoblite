@extends('layouts.myaccount')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('forms.2fa') }}</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/myaccount/2fa/validate') }}">
                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('totp') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">{{ trans('forms.one_time_password') }}</label>
                    <div class="col-md-6">
                        <input type="number" class="form-control" name="totp">
                        @if ($errors->has('totp'))
                        <span class="help-block">
                            <strong>{{ $errors->first('totp') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();">
                            <i class="fa fa-btn fa-mobile"></i>{{ trans('forms.validate') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection