@extends('layouts.main')
@section('content')
<div class="container mt-40">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('forms.login') }}</div>
                <div class="panel-body">
                     @if (session('success'))
                    <div class="alert alert-success">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('success') }}
                    </div>
                    @endif
                    @if (session('error'))
                    <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('error') }}
                    </div>
                    @endif
                    @if (session('message'))
                    <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('message') }}
                    </div>
                    @endif
                    @if (session('codemessage'))
                    <div class="alert alert-danger">
                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ session('codemessage') }}
                    </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ trans('forms.emailaddress') }}</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{ trans('forms.password') }}</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ trans('forms.rememberme') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" onclick="this.disabled=true;this.form.submit();">
                                    {{ trans('forms.login') }}
                                </button>
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    {{ trans('forms.forgotpassword') }}
                                </a><br />
                        <div class="form-group">
                        @if(getenv('API_FACEBOOK') == 'true' || getenv('API_TWITTER') == 'true' || getenv('API_GOOGLE') == 'true')
                            <p style="margin-left:25px">OR</p>
                        @endif
                        @if(getenv('API_FACEBOOK') == 'true')
                            <div class="col-md-4">
                                <a href="{{url('/socialemail/2')}}" class="btn btn-primary">Facebook Login</a>
                            </div>
                        @endif
                        @if(getenv('API_TWITTER') == 'true')
                            <div class="col-md-3">
                                <a href="{{url('/socialemail/1')}}" class="btn btn-primary">Twitter Login</a>
                            </div>
                        @endif
                        @if(getenv('API_GOOGLE') == 'true')
                            <div class="col-md-3">
                                <a href="{{url('/goredirect')}}" class="btn btn-primary">GooglePlus Login</a>
                            </div>  
                        @endif                    
                        </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
