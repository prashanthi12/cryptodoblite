<div class="col-md-12 ">
<form method="post" action="{{ url('superadmin/bonus/'.$id)}}" class="form-horizontal" id="contact">
{{ csrf_field() }}    
    <input name="_method" value="PUT" type="hidden">
    <input type="hidden" name="id" value="{{ $id }}">
   
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label>Name</label>
        <input name="name" class="form-control" value="{{ $bonus->name }}" type="text">
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div> 

    <div class="form-group{{ $errors->has('bonusvalue') ? ' has-error' : '' }}">
    <label>Bonus Value</label>
        <input name="bonusvalue" class="form-control" value="{{ $bonus->value }}" type="text" >
        <small class="text-danger">{{ $errors->first('bonusvalue') }}</small>
    </div> 

    <div class="form-group">
    <label>Bonus Type</label>
        <label for="publish_1">
            <input type="radio" id="publish_1" name="bonustype" value="1" {{ $bonus->type == 1 ? 'checked' : '' }}> Flat
        </label>
        <label for="publish_1">
            <input type="radio" id="publish_1" name="bonustype" value="2" {{ $bonus->type == 2 ? 'checked' : '' }}> Percentage (%)
        </label>
    </div> 

     <div class="form-group" id="plans" >
        <label>Deposit Plans</label>
        <select class="form-control" id="plan" name="plan" >
            @foreach ($plans as $plan)
                <option value="{{ $plan->id }}" {{ $bonus->plan == $plan->id ? "selected" : "" }}>{{ $plan->name }}</option>
            @endforeach
        </select>
        <small class="text-danger">{{ $errors->first('plan') }}</small>
    </div>

     <div class="form-group">
    <label>Trigger Type</label><br>

         <input type="radio" name="triggertype" id="triggertype" value="1" {{ $bonus->triggertype == 1 ? 'checked' : '' }}> Deposit<br>
         <input type="radio" name="triggertype" id="triggertype" value="2" {{ $bonus->triggertype == 2 ? 'checked' : '' }}> Release<br>        
    </div>

    @php
    $bonus_cretided_div_style = 'none';
    if ($bonus->triggertype == 1)
    {
        $bonus_cretided_div_style = 'block';
    }
    @endphp

    <div class="form-group" id="bonus_cretided" style="display:{{ $bonus_cretided_div_style }}">
    <label>Bonus Amount Credit To</label><br>
        <input type="radio" name="deposittype" value="1" {{ $bonus->bonus_cretided_to == 1 ? 'checked' : '' }}> Account Balance<br>
         <input type="radio" name="deposittype" value="2" {{ $bonus->bonus_cretided_to == 2 ? 'checked' : '' }}> Deposit<br>
        <small class="text-danger">{{ $errors->first('deposittype') }}</small>
    </div>

   

     <div class="form-group">
    <label>Status</label>
        <label for="publish_1">
            <input type="radio" id="publish_1" name="status" value="0" {{ $bonus->status == 0 ? 'checked' : '' }}> Inactive
        </label>
        <label for="publish_1">
            <input type="radio" id="publish_1" name="status" value="1" {{ $bonus->status == 1 ? 'checked' : '' }}> Active
        </label>
    </div> 

    
    
    <div class="form-group">
        {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
        <a href=" " class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>

@push('bottomscripts')
<script>
    $(document).ready(function() {
    $('input[type=radio][name=triggertype]').change(function() {
        if (this.value == '1') {
            $('#bonus_cretided').show();
        }
        else if (this.value == '2') {
            $('#bonus_cretided').hide();
        }
    });
});

</script>
@endpush

