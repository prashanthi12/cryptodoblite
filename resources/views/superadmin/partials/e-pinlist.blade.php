<table class="table table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>Gateway Name</th>
			<th>Deposit</th>
			<th>Active</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@if (count($paymentgatewaylist) > 0)
		@foreach ($paymentgatewaylist as $paymentgateway)
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td>{{ $paymentgateway->displayname }}</td>
			@if($paymentgateway->deposit == '1')
				<td>True</td>
			@else
				<td>False</td>
			@endif
			@if($paymentgateway->active == '1')
				<td>True</td>
			@else
				<td>False</td>
			@endif
			<td><a href="{{url('/superadmin/paymentgateway/e-pin/edit/'.$paymentgateway->id)}}" class="btn btn-xs btn-default">Edit</a></td>
		@endforeach
		@endif
	</tbody>
</table>


