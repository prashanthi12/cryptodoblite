<table class="table table-bordered">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ trans('myaccount.plan_name') }}</th>
			<th>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</th>
			<th>{{ trans('myaccount.payment_method') }}</th>
			<th>{{ trans('myaccount.start_date') }}</th>
			<th>{{ trans('myaccount.maturity_date') }}</th>
			<th>{{ trans('myaccount.interest_generated') }} ({{ config::get('settings.currency') }})</th>
			<th>{{ trans('myaccount.partial_withdraw') }}</th>
			<th>{{ trans('myaccount.transaction_number') }}</th>
			<th>{{ trans('myaccount.invoice') }}</th>
		</tr>
	</thead>
	<tbody>
		@if (count($mydeposits) > 0)
		@foreach ($mydeposits as $mydeposit)
		<tr>
			<td>{{ $loop->iteration }}</td>
			<td><a href="">fgfgjhjgj</a></td>
			<td colspan="12">{{ trans('myaccount.no_deposit_found') }}</td>
			<td colspan="11">{{ trans('myaccount.no_deposit_found') }}</td>
			<td colspan="9">{{ trans('myaccount.no_deposit_found') }}</td>
			<td colspan="10">{{ trans('myaccount.no_deposit_found') }}</td>
			<td colspan="8">{{ trans('myaccount.no_deposit_found') }}</td>
		@endif
	</tbody>
</table>
{{ $mydeposits->links() }}

@push('bottomscripts')
<script>
   
     $("#printinvoice").on("click", function(){
        return confirm("{{ trans('myaccount.print_invoive_alert') }}");
    });

    $('[data-toggle="active-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

     $('[data-toggle="matured-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

    $('[data-toggle="released-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

     $('[data-toggle="rejected-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

     $('[data-toggle="problem-comment-popover"]').popover({
        placement : 'left',
        trigger : 'hover'
    });

</script>
@endpush