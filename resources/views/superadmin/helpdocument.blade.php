@extends('backpack::layout')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">       
            <h3>Help Document</h3>
            <div class="panel panel-default">
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12 ">           
                                <div class="form-group">
                                    <p>Please download the help document below for your reference.</p>
                                    <iframe height="500" src="{{ url('/uploads/helpdocument/Laravelhyip_Installation.pdf') }}" width="755"></iframe>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection