<table class="table dataTable" id="staffsdatatable">
    <thead>
        <tr>        
            <th>Name</th>
            <th>Date of Join</th>
            <th>Tickets</th>  
            <th>Actions</th>    
        </tr>
    </thead>
    <tbody>
    @foreach($staffs as $data)
        <tr>
            <td>{{ $data->name }}</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ count($data->agent) }}</td>
            <td>
                <a href="{{ url('/users/'.$data->id.'/impersonate') }}" class="btn btn-primary btn-xs"> Login as User</a>&nbsp;
                <a id="reset" href="{{ url('/admin/users/resetpassword/'.$data->id) }}" class="btn btn-danger btn-xs">Reset Password</a>
            </td>      
        </tr>
    @endforeach
    </tbody>
</table>

@push('scripts')
<script>
$(document).ready(function(){
    $('#staffsdatatable').DataTable();

        $("#reset").on("click", function(){
        return confirm("Do you want to password reset for this user.?");
    });
});
</script>
@endpush