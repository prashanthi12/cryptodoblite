@extends('backpack::layout')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">      
            <h3>Payment Gateways List</h3>
            <div class="panel panel-default">  
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="tab" class="btn-group" >
                                <a href="{{ url('superadmin/paymentgateway/online') }}" class="btn {{ $status == 'online' ? 'active' : '' }}">Online</a>
                                <a href="{{ url('superadmin/paymentgateway/offline') }}" class="btn {{ $status == 'offline' ? 'active' : '' }}">Offline</a>
                                <a href="{{ url('superadmin/paymentgateway/reinvest') }}" class="btn {{ $status == 'reinvest' ? 'active' : '' }}">Reinvest</a>
                                <a href="{{ url('superadmin/paymentgateway/e-pin') }}" class="btn {{ $status == 'e-pin' ? 'active' : '' }}">E - Pin</a>
                                <a href="{{ url('superadmin/paymentgateway/e-wallet') }}" class="btn {{ $status == 'e-wallet' ? 'active' : '' }}">E - Wallet</a>
                            </div>
                            @if($status == 'online')
                                @include('superadmin.partials.onlinepaymentlist')
                            @endif
                            @if($status == 'offline')
                                @include('superadmin.partials.offlinepaymentlist')
                            @endif
                            @if($status == 'reinvest')
                                @include('superadmin.partials.reinvestlist')
                            @endif
                            @if($status == 'e-pin')
                                @include('superadmin.partials.e-pinlist')
                            @endif
                            @if($status == 'e-wallet')
                                @include('superadmin.partials.e-walletlist')
                            @endif                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
