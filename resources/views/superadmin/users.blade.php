@extends('backpack::layout')
@section('header')
  <section class="content-header">
    <h1>Users</h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">User Lists</li>
      </ol>
  </section>
@endsection

@section('content')
  <div class="row">
      <div class="col-md-12">
          <div class="box box-default">
              <div class="box-header with-border">
                  <div class="box-title">User Lists</div>
              </div>
              <a href="{{ url('superadmin/exportusers') }}" class="btn btn-success pull-right">Export</a>
              <br><br>
              <div class="row">
                <div class="col-sm-12">
                  <form  method="POST" role="search">
                    {{ csrf_field() }}
                      <div class="input-group">
                          <input type="text" class="form-control" name="q"
                              placeholder="Search users by Username or Email"> <span class="input-group-btn">
                              <button type="submit" class="btn btn-default">
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                          </span>
                      </div>
                  </form>
                      @include('layouts.message')
                      @include('superadmin._user_list')
                </div>
              </div>
          </div>
      </div>
  </div>
@endsection

       @include('superadmin.datatable') 

@push('scripts')
<script>
    $(document).ready(function(){
        $('#userdatatable').DataTable();
    });
</script>
@endpush


