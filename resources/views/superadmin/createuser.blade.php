@extends('backpack::layout')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">   
            <div class="col-md-6 col-md-offset-3">   
            <h3>Add User Form</h3>
            <p class="pull-right"><a href="{{ url('superadmin/users') }}">Back to User List</a></p>
            <div class="panel panel-default"> 
                <div class="panel-body">     
                    <div class="row">
                        <div class="col-md-12">                 
                            @include('superadmin._userform')                    
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection
