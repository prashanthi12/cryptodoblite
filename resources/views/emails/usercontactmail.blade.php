@component('mail::message')

{{ trans('mail.hi_text', ['name' => $fromname]) }},<br>

{{ trans('mail.user_contactus_content') }}<br>
<p>{{ trans('mail.comment') }} : {{ rawurldecode($queries) }}</p>
<p>{{ trans('mail.contactno') }} : {{ $contactno }}</p>
<p>{{ trans('mail.skype') }} : {{ $skypeid }}</p>

{!! $signature !!}
@endcomponent