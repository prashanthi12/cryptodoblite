@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.subject') }} : {{ $subject }}</p>
<p>{{ trans('mail.message') }} : {{ $message }}</p>
<p>
    @component('mail::button', ['url' => $actionUrl])
    {{ $actionText }}
    @endcomponent
</p>
{!! $signature !!}
  
@endcomponent



