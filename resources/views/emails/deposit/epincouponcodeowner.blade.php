@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.owner_coupon_code_content', array(
    'user' => $user)) }} </p>

<p>{{ trans('mail.amount') }} : {{ $amount }} {{ config::get('settings.currency') }}</p>
<p>{{ trans('mail.coupon_code') }} : {{ $couponcode }} </p>

{!! $signature !!}
@endcomponent
