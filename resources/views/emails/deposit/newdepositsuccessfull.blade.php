@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<P>
{{ trans('mail.new_deposit_content', array(
    'amount' => $amount, 'currency' => config::get('settings.currency') ) ) }}
</P> 

<p>{{ trans('mail.plan_name') }} : {{ $plan_name }} </p>

{!! $signature !!}
@endcomponent
