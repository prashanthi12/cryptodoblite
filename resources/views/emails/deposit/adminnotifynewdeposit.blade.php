@component('mail::message')

{{ trans('mail.hi_text', ['name' => 'Admin']) }},<br>

<p>{{$content }}</p>
<p>{{ trans('mail.deposited_amount') }} : {{ $deposited_amount }} {{ \Config::get('settings.currency') }}</p>
<p>{{ trans('mail.plan_name') }} : {{ $plan_name }} </p>
<p>
    @component('mail::button', ['url' => $new_deposit_link])
    {{ $new_deposit_link_text }}
    @endcomponent
</p>

{!! $signature !!}  
@endcomponent
