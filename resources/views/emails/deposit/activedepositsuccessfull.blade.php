@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.active_deposit_content', array(
    'amount' => $amount, 'currency' => config::get('settings.currency') ) ) }} </p>

<p>{{ trans('mail.plan_name') }} : {{ $plan_name }} </p>



{!! $signature !!}
@endcomponent
