@component('mail::message')
 
{{ trans('mail.hi_text', ['name' => 'admin']) }},<br>

<p>{{ trans('mail.coin_payment_content') }} </p>

 <p>  
     @component('mail::button', ['url' => $new_deposit_link])
    {{ $deposit_link_text }}
    @endcomponent
 </p>

 {!! $signature !!}
 @endcomponent
