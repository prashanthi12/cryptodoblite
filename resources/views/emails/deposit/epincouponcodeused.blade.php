@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.user_coupon_code_content', array(
    'amount' => $amount, 'currency' => config::get('settings.currency') ) ) }} </p>

{!! $signature !!}
@endcomponent
