@component('mail::message')

{{ trans('mail.hi_text', ['name' => $name]) }},<br>

<p>{{ trans('mail.penalty_content', array(
    'amount' => $amount, 'currency' => config::get('settings.currency') ) ) }} </p>

<p>{!! $comment !!} </p>

{!! $signature !!}
@endcomponent
