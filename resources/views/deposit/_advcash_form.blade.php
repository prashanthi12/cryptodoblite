@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.advcash') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">            
          <p> {{ $instructions }}</p>
          @if ($depositfeestatus == 1)
            <p>
                {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 
                 @if (Session::get('depositfeetype') == 1) 
                  Flat
                @elseif (Session::get('depositfeetype') == 2)
                  %
                @endif
            </p>
            <p>
                {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
            </p>
          @endif

          <form id="makeDonation" action="https://wallet.advcash.com/sci/" method="post" >
            {{ csrf_field() }}
              <input type="hidden" name="ac_account_email" value="{{ $params['ac_account_email'] }}" />
              <input type="hidden" name="ac_sci_name" value="{{ $params['ac_sci_name'] }}" />
              <input type="hidden" name="amount" value="{{ $amount }}">   
              <input type="hidden" name="plan" value="{{ $plan }}"> 
              <input type="hidden" name="payment" value="{{ $paymentgateway }}">    
              <input type="hidden" name="ac_currency" value="{{ Config::get('settings.currency') }}">
              <input type="hidden" name="ac_order_id" value="{{ $transaction_id }}" />
              <input type="hidden" name="ac_sign" value="{{ $ac_sign }}" />
               <!-- Optional Fields -->
              <input type="hidden" name="ac_success_url" value="{{ url('myaccount/deposit/advcash')}}" />
              <input type="hidden" name="ac_success_url_method" value="GET" />
              <input type="hidden" name="ac_fail_url" value="{{ url('myaccount/deposit/paymentcancelled')}}" />
              <input type="hidden" name="ac_fail_url_method" value="GET" />
              <input type="hidden" name="ac_status_url" value="{{ url('myaccount/deposit/paymentcancelled')}}" />
              <input type="hidden" name="ac_status_url_method" value="GET" />
              <input type="text" name="ac_comments" value="Comment" />
              <center><input type=image style="width:100px;height:50px;" src="{{ asset('images/advcash.jpg') }}" ></center>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection



