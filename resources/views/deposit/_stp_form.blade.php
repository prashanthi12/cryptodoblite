@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.stpay') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
               @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif

    <form action="https://solidtrustpay.com/handle.php" method="post">
    <!-- <input type=hidden name="testmode" value="on" /> -->
    <input type=hidden name="merchantAccount" value="{{ $params['account_login'] }}" />
    <input type=hidden name="amount" value="{{ $amount }}" />
    <input type=hidden name="currency" value="{{ Config::get('settings.currency') }}" />
    <input type=hidden name="item_id" value="{{ $planname.' plan' }}" />
    <input type=hidden name="notify_url" value="{{ url('myaccount/deposit/stpprocess') }}" />
    <input type='hidden' name="confirm_url" value="{{ url('myaccount/deposit/stpprocess') }}" />
    <input type=hidden name="return_url" value="{{ url('myaccount/home') }}" />
    <input type=hidden name="cancel_url" value="{{ url('myaccount/deposit/paymentcancelled') }}" />
    <center><input name="cartImage" src="{{ asset('images/sp.gif') }}" style="width:100px;height:50px;" type="image" border="0"></center>
  </form>
            </div>
        </div>
    </div>
</div>
@endsection