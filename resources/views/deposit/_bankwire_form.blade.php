@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.make_deposit') }}</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
            <p> {{ $instructions }}</p>

             @if ($depositfeestatus == 1)
                  <p>
                      {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                       @if (Session::get('depositfeetype') == 1) 
                        Flat
                      @elseif (Session::get('depositfeetype') == 2)
                        %
                      @endif
                  </p>

                    <p>
                        {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif
            <form method="POST" action="{{ url('myaccount/deposit/bankwire') }}">  
                 {{ csrf_field() }}
                  <div class="form-group">
                      <label class="col-md-4 control-label">{{ trans('forms.amount_to_be_deposited') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $amount }} {{ \Config::get('settings.currency') }}</label>
                          <input name="amount" class="form-control" value="{{ $amount }}" type="hidden">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-md-4 control-label">{{ trans('forms.transaction_ref_id') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $transaction_id }}</label>
                          <input name="transaction_id" class="form-control" value="{{ $transaction_id }}" type="hidden">
                          </div>
                        </div>
                        <div class="form-group">
                          <input name="plan" class="form-control" value="{{ $plan }}" type="hidden">
                          </div>
                          <div class="form-group">
                          <input name="paymentgateway" class="form-control" value="{{ $paymentgateway }}" type="hidden">
                          </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">{{ trans('forms.bank_name_lbl') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $params['bank_name'] }}</label>
                          <input name="bank_name" class="form-control" value="{{ $params['bank_name'] }}" type="hidden">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">{{ trans('forms.address') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $params['bank_address'] }}</label>
                          <input name="bank_address" class="form-control" value="{{ $params['bank_address'] }}" type="hidden">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">{{ trans('forms.swift_code_lbl') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $params['swift_code'] }}</label>
                          <input name="swift_code" class="form-control" value="{{ $params['swift_code'] }}" type="hidden">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">{{ trans('forms.account_name_lbl') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $params['account_name'] }}</label>
                          <input name="account_name" class="form-control" value="{{ $params['account_name'] }}" type="hidden">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4 control-label">{{ trans('forms.account_no_lbl') }}</label>
                          <div class="col-md-8">
                             <label for="input01">{{ $params['account_no'] }}</label>
                          <input name="account_no" class="form-control" value="{{ $params['account_no'] }}" type="hidden">
                          </div>
                        </div>
                        <div class="form-group mt-20">    
                        <a href="{{ url('myaccount/deposit/printinvoice') }}" class="btn btn-info" target="_blank">{{ trans('forms.printinvoice') }}</a>
                        </div>
                        <div class="form-group mt-20">    
                            <input value="{{ trans('forms.submit_complete_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
                            <a href="{{ url('myaccount/deposit') }}" class="btn btn-default">{{ trans('forms.back') }}</a>
                        </div>
                </form>
        </div>
      </div>
    </div>
</div>
@endsection


            