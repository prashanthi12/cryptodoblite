@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.bitcoin') }} - {{ trans('forms.coinpayment') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            @if( $bitcoin['error'] == "ok" )
		<div class="col-md-8 col-md-offset-2">
		    <a href="{{ $bitcoin['result']['status_url'] }}"  class="btn btn-primary btn-block" id="ext" target="_blank">{{ trans('forms.coin_payment_btn') }}</a>
		</div>
	@else 
		<div class="col-md-10 col-md-offset-1">
		   	<h4> {{ trans('forms.error_api_settings') }} </h4>
		   	<p> {{ $bitcoin['error'] }}</p>
		</div>
	@endif
        </div>
    </div>
</div>
@endsection



