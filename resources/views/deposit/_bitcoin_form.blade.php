 @extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.bitcoin') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
              @if( $paymentgateway == 6)
                @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif

              @elseif( $paymentgateway == 7)

                @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                            @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif

              @elseif( $paymentgateway == 8)

                @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                            @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif

              @endif

 <form id="makeDonation" action="{{ url('myaccount/deposit/bitcoin')}}" method="post" >
        {{ csrf_field() }}
        <input type="hidden" name="payment" value="{{ $paymentgateway }}">
        <input type="hidden" name="generic" value="1">
        <input name="action" type="hidden" value="deposit">
        <input type="hidden" name="user_id" value=1>
        <input type="hidden" name="price" value="{{ $amount }}">  
        @if( $paymentgateway == 6) 
        <input type="hidden" name="api_key" value="{{ $params['api_key'] }}">
        <input type="hidden" name="pin" value="{{ $params['pin'] }}">
        @endif
        @if( $paymentgateway == 7)   
        <div class="form-group" id="coin_type">
          <label>{{ trans('forms.select_bitcoin_coin_lbl') }}</label>    
          @include('partials.coinpayments')     
        </div>
        <input type="hidden" name="coin_public_key" value="{{ $params['public_key'] }}">
        <input type="hidden" name="coin_private_key" value="{{ $params['private_key'] }}">
        @endif 
        <input type="hidden" name="currency" value="{{ Config::get('settings.currency') }}">
        <center><input type=image style="width:100px;height:50px;" src="{{ asset('images/bitcoin.png') }}" ></center>
</form>

            </div>
        </div>
    </div>
</div>
@endsection