@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.bitcoin') }} - {{ trans('forms.bitpay') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
			<div class="col-md-10 col-md-offset-1">

			@if(isset( $bitcoin['url'] ))
		    <iframe src="{{ $bitcoin['url'] }}" style="width:650px;height:500px"></iframe>

		    @else
		    <p  class="text-danger">{{ucfirst($bitcoin['error']['message'])}}</p>
		    @endif

			</div>
        </div>
    </div>
</div>
@endsection



