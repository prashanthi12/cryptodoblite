@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.partial_withdraw') }}</div>
    <div class="panel-body">          
        <div class="row">       
            @include('deposit.partialwithdrawform')                  
        </div>
    </div>
</div>
</div>
@endsection
