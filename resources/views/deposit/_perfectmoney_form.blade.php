@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.perfectmoney') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
              @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 
                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif
           
               @if ($params['payee_account'] != '' && $params['payee_name'] != '' && $params['alternate_passhprase'] != '')
                <form action="https://perfectmoney.is/api/step1.asp" method="POST">
                <p>
                    <input type="hidden" name="PAYMENT_ID" value="{{  $payment_id  }}" />
                    <input type="hidden" name="PAYEE_ACCOUNT" value="{{ $params['payee_account'] }}">
                    <input type="hidden" name="PAYEE_NAME" value="{{ $params['payee_name'] }}">
                    <input type="hidden" name="PAYMENT_AMOUNT" value="{{  $amount  }}">
                    <input type="hidden" name="PAYMENT_UNITS" value="{{ Config::get('settings.currency') }}">
                    <input type="hidden" name="STATUS_URL" 
                        value="{{ url('myaccount/deposit/perfectmoneyprocess') }}">
                    <input type="hidden" name="PAYMENT_URL" 
                        value="{{ url('myaccount/deposit/perfectmoneyprocess') }}">
                    <input type="hidden" name="PAYMENT_URL_METHOD" value="LINK" />
                    <input type="hidden" name="NOPAYMENT_URL" 
                        value="{{ url('myaccount/deposit/paymentcancelled') }}"> 
                    <input type="hidden" name="NOPAYMENT_URL_METHOD" value="LINK" />                  
                    <center>
                       <input type=image src="{{ asset('images/perfectmoney.png') }}" style="width: 120px;">
                    </center>
                </p>
                </form>
                 @else

                    <h4> {{ trans('forms.error_payment_settings') }} </h4>
                    <p><a href="{{ url('myaccount/deposit') }}" >{{ trans('forms.back') }}</a></p>

          @endif

            </div>
        </div>
    </div>
</div>
@endsection
