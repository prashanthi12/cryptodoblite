@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.make_deposit') }}</div>
    <div class="panel-body">
            <div class="row">
                <div class="col-md-12"> 
                   @if (session('error'))
                        <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
        <div class="row">       


            @if ($force_deposit_down == 0)       

            <div class="col-md-10 col-md-offset-1">

                @if ($todayTotalDeposit >= \Config::get('settings.todaydepositlimit')) 

                <div class="col-md-10 col-md-offset-1">
                    <p>{{ trans('forms.day_deposit_down_info_message') }}</p>
                </div>

                @else


                    @if ($force_email_verification_for_deposit == 0 && $force_kyc_verification_for_deposit == 0) 

                        @include('deposit.depositform')                  

                        
                    @else
                        
                             @if ($force_email_verification_for_deposit == 1 || $force_kyc_verification_for_deposit == 1) 


                                    @if ($isEmailVerified == 0 && $force_email_verification_for_deposit == 1)
                                         <p>{{ trans('forms.email_verification_incomplete_deposit_alert') }}</p>

                                    @else
                                        @if (($isKycApproved  == 0 || $isKycApproved ==2) && $force_kyc_verification_for_deposit == 1)
                                             <p>{{ trans('forms.kyc_incomplete_deposit_alert') }}</p>
                                                    <a href="{{ url('/myaccount/profile') }}" class="btn btn-primary btn-lg">{{ trans('myaccount.ctabutton') }}</a>

                                        @else
                                            @include('deposit.depositform')
                                        @endif
                                        
                                    @endif

                            @endif
                    @endif
                @endif
           

            @else
                <div class="col-md-10 col-md-offset-1">
                    <p>{{ trans('forms.force_deposit_down_info_message') }}</p>
                </div>

                  </div>
            @endif
        </div>
    </div>
</div>
</div>
@endsection
