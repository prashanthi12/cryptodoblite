@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.epinrequest') }}
    <a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                @include('layouts.message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <form method="post" action="{{ url('myaccount/epin/couponcode/check')}}" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="amount" value="{{ $amount }}">
                    <input type="hidden" name="paymentgateway" value="{{ $paymentgateway }}">
                    <input type="hidden" name="plan" value="{{ $plan }}">
                    
                    <div class="form-group{{ $errors->has('couponcode') ? ' has-error' : '' }}">
                        <label>{{ trans('forms.epin_coupon_code_lbl') }}</label>
                        <input type="text" name="couponcode" id="couponcode" class='form-control' value="{{ old('couponcode') }}">
                        <small class="text-danger">{{ $errors->first('couponcode') }}</small>
                    </div>   

                    <div class="form-group">    
                        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
                        <a href="" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
                    </div>  
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
