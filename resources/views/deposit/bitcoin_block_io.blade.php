@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.bitcoin') }} - {{ trans('forms.blockio') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>

    <div class="panel-body">
        <div class="row">	
            		
            @if ($blockioapistatus == 'success')
             <div class="well">
                    <p> {{ $instructions }}</p>
                    <p class="text-center">
                        <strong class="bitcoin-address"
                            data-bc-amount="{{ Session::get('amount') }}"
                            data-bc-label="bitcoinaddress.js project"
                            data-bc-message="{{ $btcamount }} BTC For Deposit"
                            data-bc-address="{{ $block_io }}">{!! $block_io }}</strong>
                    </p>
            </div>

             <div id="bitcoin-address-template" class="bitcoin-address-container" hidden>

                <div>
                    <span class="bitcoin-address"></span>
                </div>

                    <a href="#" class="bitcoin-address-action bitcoin-address-action-qr">
                    <i class="fa fa-qrcode"></i>
                    {{ trans('forms.qr_code') }}
                    </a>


                     <div class="bitcoin-action-hint bitcoin-action-hint-qr">
                    <p>
                        {{ trans('forms.bitcoin_blockio_notes') }}
                    </p>

                    @if ($depositfeestatus == 1)
                      <p>
                            {{ trans('forms.deposit_charge') }} : {{ $depositfee }} BTC ({{ Session::get('depositfee') }} {{ \Config::get('settings.currency') }})
                      </p>

                      
                      <p>
                           {{ trans('forms.total_send_amount') }} : {{ $btcamount  }} BTC
                      </p>
                      </p>
                    @else
                      <p>
                      {{ trans('forms.total_send_amount') }} : {{ $btcamount }} BTC
                      </p>
                    @endif

                <div class="bitcoin-address-qr-container">
                    <!-- Filled in by JS on action click -->
                </div>
                 <br/>
               <form method="POST" action="{{ url('myaccount/deposit/saveblockio') }}">  
                 {{ csrf_field() }}
                 <input name="amount" class="form-control" value="{{ Session::get('amount') }}" type="hidden">
                 <input name="transaction_id" class="form-control" value="{{ $transaction_id }}" type="hidden">
                 <input name="plan" class="form-control" value="{{ Session::get('plan') }}" type="hidden">
                 <input name="btcamount" class="form-control" value="{{ $btcamount }}" type="hidden">
                 <input name="transactionaddress" class="form-control" value="{{ $block_io }}" type="hidden">
                 <input name="paymentgateway" class="form-control" value="6" type="hidden">                  
                    <div class="form-group mt-20">    
                      <input value="{{ trans('forms.submit_complete_btn') }}" class="btn btn-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
                       <a href="{{ url('myaccount/deposit') }}" class="btn btn-default">{{ trans('forms.cancel_button_label') }}</a>
                    </div>
                </form>
             </div>
             </div>
             @else
                     <div class="col-md-10 col-md-offset-1">
                    <h4> {{ trans('forms.error_api_settings') }} </h4>
                    <p> {{ $blockioapistatus }}</p>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@push('bottomscripts')
<script src="{{ asset('js/bitcoinaddress.js') }}"></script>
@endpush



