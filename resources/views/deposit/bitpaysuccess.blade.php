
@if(session('status'))
<span>{{ trans('forms.bitpaysuccess') }}</span>
@else
{{session('error')}}
@endif
<p>Please return to <a href="{{ url('myaccount/home') }}" target="_parent">our homepage.</a></p>