@extends('layouts.myaccount') 
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.payeer') }}<a href="{{ url('myaccount/deposit') }}" class="pull-right">{{ trans('forms.deposit') }}</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">            
              <p> {{ $instructions }}</p>
               @if ($depositfeestatus == 1)
                  <p>
                            {{ trans('forms.deposit_charge') }} : {{ Session::get('depositfee') }} 

                             @if (Session::get('depositfeetype') == 1) 
                              Flat
                            @elseif (Session::get('depositfeetype') == 2)
                              %
                            @endif
                    </p>

                    <p>
                            {{ trans('forms.total_deposit_amount') }} : {{ $amount  }} {{ \Config::get('settings.currency') }}
                    </p>

              @endif

      <?php
          $m_shop = $params['payeer_id'];
          $m_orderid = '1';
          $m_curr = Config::get('settings.currency');
          $m_desc = base64_encode("testing");
          $m_key = $params['m_key'];

          $arHash = array(
            $m_shop,
            $m_orderid,
            $amount,
            $m_curr,
            $m_desc,
            $m_key
          );
          $sign = strtoupper(hash('sha256', implode(':', $arHash)));
       ?>

        <!-- <form method="GET" action="https://payeer.com/merchant/"> -->
        <form method="POST" action="https://payeer.com/merchant/">
            <input name="m_shop" value="{{ $m_shop }}" type="hidden">
            <input name="m_orderid" value="{{ $m_orderid }}" type="hidden">
            <input name="m_amount" value="{{ $amount }}" type="hidden">
            <input name="m_curr" value="{{ $m_curr }}" type="hidden">
            <input name="m_desc" value="{{ $m_desc }}" type="hidden">
            <input name="m_sign" value="{{ $sign }}" type="hidden">         
          <center>  <input name="m_process" src="{{ asset('images/payeer.png') }}" style="width:100px;height:50px;" id="epin.jpg" type="image"></center>
        </form>
            </div>
        </div>
    </div>
</div>
@endsection