@extends('layouts.main') 
@section('banner')
    @include('partials.homebanner')
@endsection
@section('content')
{{-- start of first row --}}
<div class="row bg-color-1">
        <div class="container">
            <div class="mt-50 mb-50">
            <center>
            <h1>{{ trans('welcome.welcome') }} {{ Config::get('settings.sitename') }}</h1>
            <p>{{ trans('welcome.welcometext') }}</p>
            @if (Auth::guest())
                <a href="{{ url('/register') }}" class="btn btn-primary btn-lg hidden-xs">{{ trans('welcome.ctabutton1') }}</a>
                <a href="{{ url('/register') }}" class="btn btn-primary btn-block visible-xs">{{ trans('welcome.ctabutton1') }}</a>
            @else
                <a href="{{ url('/myaccount/home') }}" class="btn btn-primary btn-lg hidden-xs ">{{ trans('welcome.ctabutton2') }}</a>
                <a href="{{ url('/myaccount/home') }}" class="btn btn-primary btn-block visible-xs ">{{ trans('welcome.ctabutton2') }}</a>
            @endif
            </center>
        </div>
</div>
{{-- end of first tow--}}
{{-- start of second row --}}
 <div class="row bg-color-2">
<div class="container">
    <div class="row">
        <center>
        <div class="col-md-10 col-md-offset-1">
            <h1>{{ trans('welcome.featuretitle') }}</h1>
            <p> {{ trans('welcome.featureleadtext' )}}</p>
        </div>
        </center>
    </div>
    <div class="row pb-30 p-20">
        <div class="col-md-3">
            <center>
                <h1><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></h1>
                <h2>{{ trans('welcome.featureheading1') }}</h2>
                <p>{{ trans('welcome.featuretext1') }}</p>
            </center>
        </div>
        <div class="col-md-3">
            <center>
                <h1><span class="glyphicon glyphicon-king" aria-hidden="true"></span></h1>
                <h2>{{ trans('welcome.featureheading2') }}</h2>
                <p>{{ trans('welcome.featuretext2') }}</p>
            </center>
        </div>
        <div class="col-md-3">
            <center>
                <h1><span class="glyphicon glyphicon-flash" aria-hidden="true"></span></h1>
                <h2>{{ trans('welcome.featureheading3') }}</h2>
                <p>{{ trans('welcome.featuretext3') }}</p>
            </center>
        </div>
        <div class="col-md-3">
            <center>
                <h1><span class="glyphicon glyphicon-gift" aria-hidden="true"></span></h1>
                <h2>{{ trans('welcome.featureheading4') }}</h2>
                <p>{{ trans('welcome.featuretext4') }}</p>
            </center>
        </div>
    </div>
    </div>
</div>
{{-- end of second row --}}
{{-- start of third row --}}
<div class="row bg-color-1">
<div class="container">
    <div class="mt-50 mb-50">
        <div class="row">
            <div class="v-center">
                    <div class="col-md-6">
                       {!! Config::get('settings.homevideo') !!}
                    </div>
                    <div class="col-md-6">
                        <h3>{!! trans('welcome.highlights1') !!}</h3>
                        <p class="text-justified">{!! trans('welcome.highlightstext1') !!}</p>
                    </div>
            </div>
        </div>
        </div>
</div>
</div>
{{-- End of third row --}}
{{-- start of fourth row --}}
<div class="row bg-color-2">
    <div class="container">
        <div class="mt-50 mb-50">
            <div class="row">
                <div class="v-center">
                    <div class="col-md-6">
                        <h3>{!! trans('welcome.highlights2') !!}</h3>
                        <p class="text-justified">{!! trans('welcome.highlightstext2') !!}</p>
                    </div>
                    <div class="col-md-6">
                        {!! Config::get('settings.homegallery') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- End of fourth row --}}
{{-- Start of fifth row --}}
<div class="row bg-color-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <center>
                    <h2>{{ trans('welcome.deposit_plans') }}</h2>
                    <p>{{ trans('welcome.depositleadtext') }}</p>
                </center>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('partials._list_invest_plans')
            </div>
        </div>
    </div>
</div>
{{-- End of fifth row --}}
{{-- Start of sixth row --}}
<div class="row bg-color-1">
<div class="container">
    <div class=" col-md-4">
    <h3>{{ trans('welcome.site_statistics') }}</h3>
        @include('partials._welcome_statistics')
    </div>
    <div class="col-md-4">
    <h3>{{ trans('welcome.latest_investments') }} </h3>
        @include('partials._welcome_deposit')
    </div>
    <div class="col-md-4">
    <h3>{{ trans('welcome.latest_withdrawals') }} </h3>
        @include('partials._welcome_withdraw')
    </div>
</div>
</div>
{{-- End of sixth row --}}
{{-- Start of seventh row --}}
<div class="row bg-color-2">
    <div class="container mt-50 p-20">
        <div class="col-md-4 ">
            <h2>{{ trans('welcome.latest_news') }}</h2>
            @if (count($news) > 0)
            <div class="news-slider">
                <ul>                
                    @foreach ($news as $news)
                        <li>
                            <h4><strong>{{ $news['title'] }}</strong> <small><em>{{ $news->created_at->diffForHumans() }}</em></small></h4>
                            <p>                             
                                {!! $news->story !!} ... <a href="{{ url('/news') }}">{{ trans('welcome.read_more') }}</a>
                            </p>               
                        </li>
                    @endforeach                            
                </ul>
            </div>
            @else
                <p>{{ trans('pages.nonewsfound') }}</p>
            @endif        
        </div>
        <div class="col-md-4">
            <h2>{{ trans('welcome.testimonials') }}</h2>
            @if (count($testimonials) > 0)              
                <div class="testimonial-slider">
                    <ul>    
                    @foreach ($testimonials as $testimonial)                
                        <li>
                            <p>
                            @php
                                list($output)=explode("\n",wordwrap(strip_tags(rawurldecode($testimonial->description)),300),300);   
                            @endphp
                                {!! $testimonial->description !!} ... <a href="{{ url('/reviews') }}">{{ trans('welcome.read_more') }}</a>
                            </p>
                            <p><strong><small><em>By&nbsp;{{ ucfirst($testimonial->testimonialuser->name) }}&nbsp;on&nbsp;{{ $testimonial->created_at->diffForHumans() }}</em></small></strong>
                                <strong class="pull-right">
                                    @for ($i=1; $i <= 5 ; $i++)
                                      <span class="glyphicon glyphicon-star{{ ($i <= $testimonial->rating) ? '' : '-empty'}}"></span>
                                    @endfor                                 
                                </strong>
                            </p>
                        </li>
                    @endforeach                 
                    </ul>
                </div>
            @else
                <p>{{ trans('welcome.no_testimonial_found') }}</p>
            @endif          
        </div>
        <div class="col-md-4">
            <h2>{{ trans('referralprogram.title') }}</h2>
            <p><div class='wordwrap'>
                {!! trans('referralprogram.content') !!}
                <a href="{{ url('/earn') }}">More Info</a>
            </div></p>          
        </div>
    </div>
</div>
{{-- End of seventh row --}}
{{-- Start of eighth row  --}}
<div class="row">
    <div class="container mt-50 mb-50">
        <div class="col-md-12">
            <center>
                <div class="col-md-12">
                    @if (count($quotes) > 0)              
                        <div class="quote-slider">
                            <ul>    
                                @foreach ($quotes as $quote)                
                                    <li>
                                        <img src="{{ $quote->authorimage }}" class="img-resposnive" style="width: 120px;height: 120px;">
                                        <p>{!! $quote->text !!}</p>
                                        <p><strong>{{ ucfirst($quote->authorname) }}</strong>
                                        </p>
                                    </li>
                                @endforeach                 
                            </ul>
                        </div>
                    @else
                        <p>{{ trans('welcome.noquotesfound') }}</p>                         
                    @endif          
                </div>
            </center>
        </div>
    </div>
</div>
{{-- End of eighth row  --}}
{{-- Start of 9th row --}}
<div class="row bg-color-2">
    <div class="container mb-50 mt-50">
        <div class="row">
                <div class="col-md-12">
                    <center>
                        <h3>{{ trans('welcome.we_accept') }}</h3>
                        <img class="img-responsive" src="{{ url(Config::get('settings.paymentbanner') ) }}"> 
                    </center>
                </div>
        </div>
</div>
</div>
{{-- End of 9th row --}}
<!-- modal start -->
@include('partials.calculator_modal')
<!-- Popup Modal when first in website -->
<div class="modal fade" id="visitor_modal" role="dialog" style="margin: 50px;">
    <div class="modal-dialog">
<!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ trans('popup.welcomeMessageTitle') }}</h4>                 
            </div>
            <div class="modal-body">                       
                <div class="panel-body">
                    <p>{!! trans('popup.welcomeMessageContent') !!}</p>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- Popup Modal ends -->

</div><!-- /.modal -->
@endsection
@php
if (\Config::get('settings.welcomepopupstatus') == 1)
{
    if (\Session::get('welcomepopupmessage') == '')
    {
    @endphp
    @push('bottomscripts')
    <script>
         $(function() {
           $('#visitor_modal').modal('show');
        });
    </script>
    @endpush
    @php
    }
}
\Session::put('welcomepopupmessage', '1')
@endphp


@push('customscripts')
    <script>
        jQuery(document).ready(function($) {
            $('.testimonial-slider').unslider({
                autoplay: true,
                infinite: true,
                nav: false,
                arrows: {
                    prev: '<a class="unslider-arrow prev"><i class="glyphicon glyphicon-chevron-left"></i></a>',
                    next: '<a class="unslider-arrow next"><i class="glyphicon glyphicon-chevron-right"></a>',
                }

                });
            $('.quote-slider').unslider({
                autoplay: true,
                infinite: true,
                nav: false,
                arrows:  {
                    next: '<li class="glyphicon glyphicon-chevron-right" style="color: #23527c; padding-left: 20px;"><a class="pager"></a></li>',
                    prev: '<li class="glyphicon glyphicon-chevron-left" style="color: #23527c;"><a class="pager"></a></li>',
                }
            });
            $('.news-slider').unslider({
                animation: 'horizontal',
                autoplay: false,
                nav:false,
                arrows: {
                    prev: '<a class="unslider-arrow prev"><i class="glyphicon glyphicon-chevron-left"></i></a>',
                    next: '<a class="unslider-arrow next"><i class="glyphicon glyphicon-chevron-right"></a>',
                }

                });
            $('.plan-slider').unslider({
                animation: 'horizontal',
                autoplay: false,
                nav:true,
                items:4,
                arrows: false,
                });
             $('.plan-slider-mobile').unslider({
                animation: 'horizontal',
                autoplay: false,
                nav:true,
                arrows: false,
                });
        });
    </script>
@endpush
