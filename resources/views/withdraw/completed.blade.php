<div class="grid">
@if (count($withdrawlists) > 0)
    @foreach($withdrawlists as $data)
    <div class="grid grid-4 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
        <p>
            <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
            {{ $data->amount }}
        </p>
    </div>
    <div class="">
        <p>
            <small>{{ trans('myaccount.payment') }} : </small><br/>       
                @include('withdraw.popoveruserpayaccounts')<br>
            <small>{{ trans('myaccount.request_date') }} : </small><br/>       
                {{ $data->created_at->diffForHumans() }}
        </p>
    </div>  
    <div class="">
        <p>
            <small>{{ trans('myaccount.bitcoin_details') }} :</small><br/>
                @if ($data->userpayaccounts->paymentgateways_id == 9)
                    <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $data->id }}'>{{ trans('myaccount.view_details') }}</a>
                @else
                    -
                @endif 
                <br>
            <small>{{ trans('myaccount.transaction_number') }} :</small><br/>
            @php        
                if (isset($data->transaction->response))
                {
                    $param = json_decode($data->transaction->response, true);
                    echo $param['transaction_number'];
                }
                else
                {
                    echo "-";
                }
            @endphp
        </p>
    </div> 
    <div class="">
        <p>
            <small>{{ trans('myaccount.comments') }} ({{ trans('myaccount.hover_content') }}) :</small><br/>
                <span data-html="true" data-toggle="comment-popover" data-content="{{ $data->comments_on_complete }}">{{ substr($data->comments_on_complete, 0, 15) }}...</span><br>
            <small>{{ trans('myaccount.completed_date') }} :</small><br/>
                {{ $data->completed_on->diffForHumans() }}
        </p>
    </div>
    </div>
    @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.nowithdraws') }}</div>
@endif
</div>
{{ $withdrawlists->links() }}

@push('bottomscripts')
<script>
$('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
});

$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('[data-toggle="comment-popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
@endpush







