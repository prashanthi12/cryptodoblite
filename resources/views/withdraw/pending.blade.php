<div class="grid">
@if (count($withdrawlists) > 0)
    @foreach($withdrawlists as $data)
    <div class="grid grid-3 mt-10 mb-10" style="border:1px solid #ddd; padding: 10px;">
    <div class="">
      <p>
        <small>{{ trans('myaccount.amount') }} ({{ config::get('settings.currency') }})</small><br/>
        {{ $data->amount }}
      </p>
    </div>
    <div class="">
      <p>
        <small>{{ trans('myaccount.payment') }} : </small><br/>       
          @include('withdraw.popoveruserpayaccounts')<br>
      </p>
    </div>   
    <div class="">
        <p style="text-align: center;">
            <small>{{ trans('myaccount.request_date') }} :</small><br/>
                {{ $data->created_at->diffForHumans() }}
        </p>
    </div>
    </div>
  @endforeach 
@else
    <div class="" style="border:1px solid #ddd; padding: 10px;">{{ trans('forms.nowithdraws') }}</div>
@endif
</div>
{{ $withdrawlists->links() }}

@push('bottomscripts')
<script>
  $(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
@endpush







