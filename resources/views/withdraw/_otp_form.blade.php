<div class="col-md-10 col-md-offset-1">
 <form method="post" action="{{ url('myaccount/withdraw/otp/check')}}" class="form-horizontal">

 {{ csrf_field() }}

    <input type="hidden" name="amount" value="{{ $amount }}">
    <input type="hidden" name="paymentgateway" value="{{ $paymentgateway }}">
    <input type="hidden" name="userpayaccountid" value="{{ $userpayaccountid }}">
    <input type="hidden" name="withdrawid" value="{{ $withdrawid }}">

    <div class="form-group{{ $errors->has('otpcode') ? ' has-error' : '' }}">
        <label>{{ trans('forms.withdraw_otp_code_lbl') }}</label>
        <input type="text" name="otpcode" id="otpcode" class='form-control' value="{{ old('otpcode') }}">
        <small class="text-danger">{{ $errors->first('otpcode') }}</small>
        <small class="otpcode"></small>
    </div>   

    <div class="form-group">    
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
        <a href="{{ url('myaccount/withdraw') }}" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
    </div>  

</form>
</div>


