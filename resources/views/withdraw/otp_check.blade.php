@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.withdrawrequest') }}
    <a href="{{ url('myaccount/withdraw/pending') }}" class="pull-right">{{ trans('myaccount.back_to_list') }}</a>
    </div>
    <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
                @include('layouts.message')
            </div>
        </div>
        <div class="row">
            @include('withdraw._otp_form')
        </div>
    </div>
</div>
@endsection
