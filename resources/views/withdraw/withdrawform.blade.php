<div class="col-md-10 col-md-offset-1">
 <form method="post" action="{{ url('myaccount/withdraw')}}" class="form-horizontal">

 {{ csrf_field() }}

	<div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
	    <label>{{ trans('forms.deposit_amount_lbl') }}</label>
	    <input type="text" name="amount" id="amount" class='form-control' value="{{ old('amount') }}">
	    <small class="text-danger">{{ $errors->first('amount') }}</small>
	    <small class="plan_amount"></small>
	</div>

	<div class="form-group{{ $errors->has('paymentgateway') ? ' has-error' : '' }}">
	    <label>{{ trans('forms.deposit_payment_lbl') }}</label>
	    <select class="form-control" id="pgs" name="paymentgateway" onchange="userpayaccount(this.value);">	    	
	    	<option value="" >{{ trans('forms.deposit_payment_lbl') }}</option>
	    	@foreach ($pgs as $pg)
	    		<option value="{{ $pg->id }}" >{{ $pg->displayname }}</option>
	    	@endforeach
	    </select>
	    <small class="text-danger">{{ $errors->first('paymentgateway') }}</small>
	</div>

	@if(Config::get('settings.twofactor_auth_status') == '1')
	<div class="form-group{{ $errors->has('totp') ? ' has-error' : '' }}">
        <label>{{ trans('forms.one_time_password') }}</label>
        <input type="number" class="form-control" name="totp">
        <small class="text-danger">{{ $errors->first('totp') }}</small>
    </div>	
	@endif
	
	<div id="userpayaccount"></div>

    <div class="form-group">    
    	<input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();"> 	
    	<a href="{{ url('myaccount/withdraw') }}" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
    </div>  

</form>
</div>

@push('bottomscripts')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
function userpayaccount(paymentid)
{ 
     $.post( "withdraw/userpayaccount", { paymentid: paymentid })
      .done(function( data ) {
        $('#userpayaccount').html(data);
   });
}

</script>
@endpush

