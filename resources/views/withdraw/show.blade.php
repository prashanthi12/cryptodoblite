@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('myaccount.mywithdraws') }}
    <a href="{{ url('myaccount/withdraw') }}" class="pull-right">{{ trans('sidebar.newrequest') }}</a>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-md-12">
                 @include('layouts.message')
                @include('home._stats_withdraw')
            </div>
        </div>
         <div class="row">
            <div class="col-md-12">
             <div id="tab" class="btn-group" >
              <a href="{{ url('myaccount/withdraw/request') }}" class="btn {{ $status == 'request' ? 'active' : '' }}" >{{ trans('myaccount.request') }}</a>
              <a href="{{ url('myaccount/withdraw/pending') }}" class="btn {{ $status == 'pending' ? 'active' : '' }}" >{{ trans('myaccount.pending') }}</a>
              <a href="{{ url('myaccount/withdraw/completed') }}" class="btn {{ $status == 'completed' ? 'active' : '' }}" >{{ trans('myaccount.completed') }}</a>
              <a href="{{ url('myaccount/withdraw/rejected') }}" class="btn {{ $status == 'rejected' ? 'active' : '' }}" >{{ trans('myaccount.rejected') }}</a>
            </div>
                @if ($status == 'request')
                    @include('withdraw.request') 
                @elseif ($status == 'pending')
                    @include('withdraw.pending') 
                @elseif ($status == 'completed')
                    @include('withdraw.completed')
                @elseif ($status == 'rejected')
                    @include('withdraw.rejected')
                @endif
            </div>
        </div>
       
    </div>
</div>
@endsection
