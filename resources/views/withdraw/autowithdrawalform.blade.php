<div class="col-md-10 col-md-offset-1">
 <form method="post" action="{{ url('myaccount/autowithdrawal')}}" class="form-horizontal">

 {{ csrf_field() }}

<p>{{ trans('forms.autowithdrawamount') }} {{ Config::get('settings.auto_withdrawal_value') }} {{ config::get('settings.currency') }}</p>

    <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
        <label>{{ trans('forms.deposit_amount_lbl') }}</label>
        <input type="text" name="amount" id="amount" class='form-control' value="{{ $amount }}">
        <small class="text-danger">{{ $errors->first('amount') }}</small>
        <small class="plan_amount"></small>
    </div>

    <div class="form-group{{ $errors->has('payaccount') ? ' has-error' : '' }}">
        <label>{{ trans('forms.payaccounts_lbl') }}</label>
        <select class="form-control" id="payaccount" name="payaccount">
            @foreach ($payaccount_result as $payaccount_result)
                <option value="{{ $payaccount_result->id }}" {{ ($payaccount_id == $payaccount_result->id ? "selected":"") }}>{{ $payaccount_result->payment->displayname }}</option>
            @endforeach
        </select>
        <small class="text-danger">{{ $errors->first('payaccount') }}</small>
    </div>  

     <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
        <label>{{ trans('forms.status') }}</label>
        <label class="radio-inline">
          <input type="radio" name="status" value="1" {{ $activechecked }}>Active
        </label>

         <label class="radio-inline">
          <input type="radio" name="status" value="0" {{ $inactivechecked }}>Inactive
        </label>
        
        
        <small class="text-danger">{{ $errors->first('status') }}</small>
        <small class="plan_amount"></small>
    </div>
   
    <div class="form-group">    
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-success btn-deposit-success" type="submit" onclick="this.disabled=true;this.form.submit();">   
        <a href="" class="btn btn-default btn-deposit-res">{{ trans('forms.reset') }}</a>
    </div>  

</form>
</div>

