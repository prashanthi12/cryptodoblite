@extends('layouts.myaccount') @section('content')
<div class="panel panel-default">
    <div class="panel-heading">{{ trans('forms.set_withdraw') }}
    
    </div>
    <div class="panel-body">
         <div class="row">
            <div class="col-md-12">
                  @include('layouts.message')
            </div>
        </div>
        <div class="row">

            @if ($payaccount_result)
                 @include('withdraw.autowithdrawalform')
            @else
                <div class="col-md-10 col-md-offset-1">
        
                    <center>
                                        <p>{{ trans('forms.set_auto_withdrawal_alert') }}</p>
                                            <a href="{{ url('/myaccount/viewpayaccounts') }}" class="btn btn-primary btn-lg">{{ trans('forms.payaccounts_btn') }}</a>
                                </center>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
