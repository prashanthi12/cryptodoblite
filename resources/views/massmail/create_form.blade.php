<div class="col-md-6">
    <form method="post" class="form-horizontal">
        {{ csrf_field() }}
            <div class="form-group{{ $errors->has('to') ? 'has-error' : '' }}">
                <label>To</label>
                    <select class="form-control" id="to" name="to">
                        <option value="">Select</option>
                            @foreach($users as $key=>$to)
                                <option value="{{ $key }}" {{ (Form::old("to") == $key ? "selected":"") }}>{{ $to }}</option>
                            @endforeach
                    </select>
                <small class="text-danger"> {{ $errors->first('to') }}</small> 
            </div>
    
            <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                <label>Subject</label>
                <input type="text" name="subject" value="{{ old('subject') }}" class="form-control"  placeholder="Enter the  subject">
                <small class="text-danger">{{ $errors->first('subject') }}</small>
            </div> 

            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                <label>Message</label>
                <textarea  rows="5" class="form-control"  placeholder="Message " name="message" id="message" >{{ old('message') }}</textarea>
                <small class="text-danger">{{ $errors->first('message') }}</small>
            </div>  
    
            <div class="form-group">
                <input value="Send" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
                <a href="" class="btn btn-primary">{{ trans('forms.reset') }}</a>
            </div>
    </form>
</div>

@push('scripts')
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
$(document).ready(function() {
  CKEDITOR.replace( 'message' );
 
});
</script>
@endpush