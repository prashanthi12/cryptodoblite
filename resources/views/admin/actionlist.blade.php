@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>{{ ucfirst($actionname) }} List</h3>   
            </div>   
            <div class="boxy boxy-white">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                    </div>
                </div>      
                    @if ($actionname == 'newdeposit')
                        @include('adminpartials._newdepositlist')
                    @elseif ($actionname == 'maturedeposit')
                        @include('admin.deposit.matureddeposit_list')
                    @elseif ($actionname == 'withdraw')
                        @include('admin.withdraw.pendinglists')
                    @elseif ($actionname == 'kyc')
                        @include('adminpartials._kycuserlist')
                    @endif
            </div>
        </div>
    </div>
</section>
@endsection
