@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="container">
        <h3>{{ trans('admin.dashboard') }}</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('layouts.message')
        </div>
    </div>  
    <div class="row">
        <div class="container">
            <div class="col-md-3">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            {{ trans('admin.users') }}
                        </p>
                        <a class="card-header-icon">
                            <span class="icon">
                            <i class="fa fa-angle-down"></i>
                          </span>
                        </a>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <p>{{ trans('admin.totalusers') }} : {{ $totalmembers }}</p>
                            <p>{{ trans('admin.activeusers') }} : {{ $activemembers }}</p>
                            <p>{{ trans('admin.unverifiedusers') }} : {{ $unverifiedmembers }}</p>
                        </div>
                    </div>                    
                </div>
            </div>
            {{-- column2 starts here --}}
            <div class="col-md-3">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            {{ trans('admin.deposits') }}
                        </p>
                        <a class="card-header-icon">
                            <span class="icon">
                            <i class="fa fa-angle-down"></i>
                          </span>
                        </a>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <p>{{ trans('admin.activedeposit') }} : <a href="{{ url('admin/deposit/active') }}">{{ $activedeposit }}</a></p>
                            <p>{{ trans('admin.unapproveddeposit') }} : <a href="{{ url('admin/deposit/new') }}">{{ $unapprovedeposit }}</a></p>
                            <p>{{ trans('admin.matureddeposit') }} : <a href="{{ url('admin/deposit/matured') }}">{{ $maturedeposit }}</a></p>
                        </div>
                    </div>                   
                </div>
            </div>
            {{-- column3 starts here --}}
            <div class="col-md-3">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            {{ trans('admin.withdraw') }}
                        </p>
                        <a class="card-header-icon">
                            <span class="icon">
                            <i class="fa fa-angle-down"></i>
                          </span>
                        </a>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <p>{{ trans('admin.pendingpayouts') }} : <a href="{{ url('admin/withdraw/pending') }}">{{ $pendingwithdraw }}</a></p>
                            <p>{{ trans('admin.completedpayouts') }} : <a href="{{ url('admin/withdraw/completed') }}">{{ $completedwithdraw }}</a></p>
                            <p>{{ trans('admin.rejectedpayouts') }} : <a href="{{ url('admin/withdraw/rejected') }}">{{ $rejectedwithdraw }}</a></p>
                        </div>
                    </div>                  
                </div>
            </div>
            {{-- column 4 starts here --}}
            <div class="col-md-3">
                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            {{ trans('admin.paid') }}
                        </p>
                        <a class="card-header-icon">
                            <span class="icon">
                            <i class="fa fa-angle-down"></i>
                          </span>
                        </a>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            <p>{{ trans('admin.asinterest') }} : {{ $sumofinterest }} {{ config::get('settings.currency') }}</p>
                            <p>{{ trans('admin.asrefcommission') }} : {{ $sumofreferralcommission }} {{ config::get('settings.currency') }}</p>
                            <p>{{ trans('admin.aslevelcommission') }} : {{ $sumoflevelcommission }} {{ config::get('settings.currency') }}</p>
                            <!-- <p>As Bonus Commission :</p> -->
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="container">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>{{ trans('admin.depositapproval') }}</h4>
                    </div>                
                       @include('adminpartials.widget_pending_deposit')
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>{{ trans('admin.quickmessage') }}</h4>
                        </div>                
                           @include('adminpartials._quickmessage')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="panel panel-default col-md-6">
                <div class="panel-heading">
                    <h4>{{ trans('admin.loggedinusers') }}</h4>
                </div>                
                    @include('adminpartials._activelogusers')
            </div>
            <div class="panel panel-default col-md-6">
                <div class="panel-heading">
                    <h4>{{ trans('admin.kycapproval') }}</h4>
                </div>                
                    @include('adminpartials.widget_pending_kyc')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="widget-card col-md-6">
                <div class="widget-heading">
                    <h4>{{ trans('admin.pendingwithdrawapproval') }}</h4>
                </div> 
                <div class="widget-body">               
                    @include('adminpartials.widget_pending_withdraw')
                </div>
                <div class="widget-footer">               
                    <div>
                        @if($withdrawlistsCount > 5)
                           <p class="text-center"> <a href="{{ url('admin/actions/withdraw') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_pendingwithdraw') }}</a></p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="panel panel-default col-md-6">
                <div class="panel-heading">
                    <h4>{{ trans('admin.pendingmatureddepositapproval') }}</h4>
                </div>                
                    @include('adminpartials.widget_pending_matured_deposit')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="widget-card col-md-6">
                <div class="widget-heading">
                    <h4>{{ trans('admin.latest_tickets') }}</h4>
                </div>
                <div class="widget-body">                
                    @include('adminpartials.widget_latest_tickets')
                </div>
                <div class="widget-footer">
                    <div>
                        @if(count($ticketlist) > 5)
                           <p class="text-center"> <a href="{{ url('admin/ticket') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_tickets') }}</a></p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="panel panel-default col-md-6">
                <div class="panel-heading">
                    <h4>{{ trans('admin.recent_fundtransfers') }}</h4>
                </div>                
                    @include('adminpartials.widget_recent_fundtransfers')
            </div>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="widget-card col-md-6">
                <div class="widget-heading">
                    <h4>{{ trans('admin.recent_signups') }}</h4>
                </div>
                <div class="widget-body">               
                    @include('adminpartials.widget_recent_signups')
                </div>
                <div class="widget-footer">
                    <div>
                        @if($signedupusersCount > 5)
                           <p class="text-center"> <a href="{{ url('admin/users') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_recentsignups') }}</a></p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="panel panel-default col-md-6">
            </div>
        </div>
    </div>
</section>
@endsection
