@extends('layouts.adminpanel')
@section('content')
<section class="section">
    <div class="container">
        <div class="content">
            <h3>Details <small>( {{ $user->name }} )</small>
            <a href="{{ url('/admin/users') }}" class="btn btn-link pull-right ">{{ trans('admin.backtouserslist') }}</a>
            </h3>
        </div>
        <div class="col-md-12">
            @include('layouts.message')
        </div>
    </div>

    @include('adminpartials.userdetailsummary')
    @include('adminpartials.userdetailtabs')
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#datatable').DataTable();
    });

      $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
</script>
@endpush

