@extends('layouts.adminpanel')
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-6 col-md-offset-3">
            <h3>{{ trans('admin.changereferralgroup') }}
            	<a href="{{ url('/admin/users/'.$user->user->name) }}" class="btn btn-link pull-right ">{{ trans('admin.backtouser') }}</a>
            </h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
							<form method="post" action="{{ url('admin/users/updatereferralgroup/'.$user->id.'') }}" class="form-horizontal" id="contact">
								{{ csrf_field() }} 
								<div>
							        <p>{{ trans('admin.username') }} : {{ $user->user->name }} </p>					
							    </div>    								    
							    <div class="form-group{{ $errors->has('referralgroup') ? ' has-error' : '' }}">
							        <select class="form-control" id="referralgroup" name="referralgroup" required>
							        <option value="">Select Referral Group</option>
							            @foreach ($referralgroups as $referralgroup)
								            <option value="{{ $referralgroup->id }}">{{ $referralgroup->name }}</option>
								        @endforeach
							        </select>
							        <small class="text-danger">{{ $errors->first('referralgroup') }}</small>
							    </div>
							    <div class="form-group">
							    	<input value="{{ trans('forms.submit_btn') }}" class="btn btn-success" id="referralgroup" type="submit" onclick="this.disabled=true;this.form.submit();">
							    	<a href="" class='btn btn-default'>{{ trans('forms.reset') }}</a>
							    </div>
							</form>
                    	</div>
                	</div>
            	</div>
        	</div>
        </div>
    </div>
</section>
@endsection
