<div class="col-md-12 ">
<form method="post" action="{{ url('admin/withdraw/reject/'.$withdrawid)}}" class="form-horizontal" id="contact">
{{ csrf_field() }}

    <div class="form-group">
      User Name : {{ $withdrawdetail->user->name }}
    </div> 
    <div class="form-group">
      Amount : {{ $withdrawdetail->amount }} {{ config::get('settings.currency') }}
    </div>     
    @include('admin.withdraw.userpayaccount')
    <input type="hidden" name="transactionid" value="{{ $withdrawdetail->transaction_id }}">
    <input type="hidden" name="userid" value="{{ $withdrawdetail->user_id }}">
    <input type="hidden" name="amount" value="{{ $withdrawdetail->amount }}">
 
    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <textarea  rows="5" class="form-control"  placeholder="Comments" name="comment">{{ old('comment') }}</textarea>
        <small class="text-danger">{{ $errors->first('comment') }}</small>
    </div>  
    
    <div class="form-group">
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
        <a href=" " class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>
