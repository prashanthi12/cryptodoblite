@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ ucfirst($status) }} {{ trans('withdrawlist') }}</h3>
            <div class="boxy boxy-white">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                    </div>
                </div>
                @if ($status == 'request')    
                    @include('admin.withdraw.requestlists')
                @elseif ($status == 'pending')    
                    @include('admin.withdraw.pendinglists')
                @elseif ($status == 'rejected')
                    @include('admin.withdraw.rejectedlists')
                @elseif ($status == 'completed')
                    @include('admin.withdraw.completedlists')
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
