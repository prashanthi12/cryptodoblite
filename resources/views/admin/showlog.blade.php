@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>{{ trans('admin.activitylogs') }}</h3>   
            </div>
            <div class="boxy boxy-white">
                @include('adminpartials._loglist')
            </div>    
        </div>
    </div>
</section>
@endsection
