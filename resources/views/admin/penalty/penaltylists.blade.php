<table class="table table-bordered" id="penaltylist">
    <thead>
    <tr>
        <th>{{ trans('admin.username') }}</th>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.comments') }}</th> 
        <th>{{ trans('admin.date') }}</th>           
    </tr>
    </thead>
    <tbody>
        @foreach($penalties as $data)       
        <tr>
            <td><a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a></td>
            <td>{{ $data->amount }} </td>
            <td>{{ $data->comments }}</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>            
        </tr>
        @endforeach
    </tbody>
 </table>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#penaltylist').DataTable();     
    });
</script>
@endpush