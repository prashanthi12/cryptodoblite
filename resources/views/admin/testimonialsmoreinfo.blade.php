@extends('layouts.adminpanel') @section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>{{ $testimonials->title }} Details</h3>   
            </div>
        <div class="panel-body">            
            <div class="row">
                <div class="col-md-12">
                    <div class="boxy boxy-white">
                        <div><a href="/admin/testimonials" style="float:right; padding-right: 20px; padding-bottom: 10px;">Back to List</a></div>
                            <div class="container mt-20 mb-20">
                                <p><b>Author Name :</b> {{ ucfirst($testimonials->testimonialuser->name) }}</p>
                                <p><b>Title :</b> {{ $testimonials->title }}</p>
                                <p><b>Description :</b> {!! $testimonials->description !!}</p>
                                <p><b>Rating :</b> 
                                    @for ($i=1; $i <= 5; $i++)
                                        <span class="glyphicon glyphicon-star{{ ($i <= $testimonials->rating) ? '' : '-empty'}}"></span>
                                    @endfor            
                                </p>
                                <p><b>Date :</b> {{ $testimonials->created_at->format('d/m/Y') }}</p>
                                <p><b>Status :</b> 
                                    @if ( $testimonials->active == 1)
                                        <span class="label label-success">Activate</span>
                                    @elseif ( $testimonials->active == 0)
                                        <span class="label label-danger">Deactivate</span>
                                    @endif
                                </p>
                                <p>
                                    @if ( $testimonials->active == 1)
                                        <form method="post" class="update" action="{{ url('admin/testimonials/'.$testimonials->id.'') }}">
                                            {{ csrf_field()}}
                                            <div class="form-group">
                                                <input type="hidden" name="changedstatus" value="0">
                                                    <button type="submit" value="Remove" class="btn btn-danger btn-xs">Deactivate</button>
                                            </div>
                                        </form>
                                    @elseif ( $testimonials->active == 0)
                                        <form method="post" class="update" action="{{ url('admin/testimonials/'.$testimonials->id.'') }}">
                                            {{ csrf_field()}}
                                            <div class="form-group">
                                                <input type="hidden" name="changedstatus" value="1">
                                                    <button type="submit" value="Remove" class="btn btn-success btn-xs" onclick="this.disabled=true;this.form.submit();">Activate</button>
                                            </div>
                                        </form>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(".update").on("submit", function(){
        return confirm("Do you want to change the status?");
    });
</script>
@endpush