<div class="col-md-12 ">
<form method="post" action="{{ url('admin/deposit/reject/'.$depositid)}}" class="form-horizontal" id="contact">
{{ csrf_field() }}   
   
   
 
    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <textarea  rows="5" class="form-control"  placeholder="Comments" name="comment">{{ old('comment') }}</textarea>
        <small class="text-danger">{{ $errors->first('comment') }}</small>
    </div>  


    
    <div class="form-group">
        {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
       
        <a href="{{ url('admin/deposit/new/') }}" class='btn btn-info'>Back to List</a>
    </div>
</form>
</div>
