<table class="table table-bordered table-striped dataTable" id="releasedepositdatatable">
    <thead>
        <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.interest') }}</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.maturedon') }}</th>
            <th>{{ trans('admin.releasedon') }}</th>
            <th>{{ trans('admin.releasedcomments') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($depositlists as $releaseddeposit)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td><a href="{{ url('admin/users/'.$releaseddeposit->user->name) }}">{{ $releaseddeposit->user->name }}</a></td>
            <td>{{ $releaseddeposit->amount }} </td>
            <td>{{ $releaseddeposit->interest->pluck('amount')->sum() }} {{ config::get('settings.currency') }}</td>
            <td>{{ $releaseddeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $releaseddeposit->matured_on->format('d/m/Y H:i:s') }}</td>
            <td>{{ $releaseddeposit->released_on->format('d/m/Y H:i:s') }}</td>
            <td>{{ $releaseddeposit->comments_on_release }}</td>
            <td>{{ $releaseddeposit->plan->name }}</td>
            <td>{{ $releaseddeposit->paymentgateway->displayname }}</td>
            @if ($releaseddeposit->paymentgateway->id == 9)
                <td><a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $releaseddeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a></td>
            @else
                <td>{{ $releaseddeposit->present()->getTransactionNumber($releaseddeposit->transaction_id) }}</td>
            @endif   
        </tr>
    @endforeach
    </tbody>
</table>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script>
$('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
}); 
$(document).ready(function(){
    // $('#releasedepositdatatable').DataTable();
});
</script>
@endpush