<table class="table table-bordered table-striped dataTable"  id="rejectdepositdatatable">
    <thead>
         <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.rejectedon') }}</th>
            <th>{{ trans('admin.rejectedcomments') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($depositlists as $rejecteddeposit)
        <tr>
            <td>{{ $loop->iteration  }}</td>
            <td><a href="{{ url('admin/users/'.$rejecteddeposit->user->name) }}">{{ $rejecteddeposit->user->name }}</a></td>
            <td>{{ $rejecteddeposit->amount }} </td>
            <td>{{ $rejecteddeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $rejecteddeposit->rejected_on->format('d/m/Y H:i:s') }}</td>
            <td>{{ $rejecteddeposit->comments_on_reject }}</td>
            <td>{{ $rejecteddeposit->plan->name }}</td>
            <td>{{ $rejecteddeposit->paymentgateway->displayname }}</td>
             @if ($rejecteddeposit->paymentgateway->id == 9)
                <td><a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $rejecteddeposit->transaction_id }}'>View Details</a></td>
            @else
                <td>{{ $rejecteddeposit->present()->getTransactionNumber($rejecteddeposit->transaction_id) }}</td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script>
 $('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
});
$(document).ready(function(){
        $('#rejectdepositdatatable').DataTable();
});
</script>
@endpush