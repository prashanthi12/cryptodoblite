@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Deposits Plans Reports</h3>   
                        <div class="boxy boxy-white">
                            <table class="table table-bordered" id="depositplanreportdatatable">
                                <thead>
                                    <tr>
                                    	<th>#</th>
                                        <th>Plan Name</th> 
                                        <th>Option</th> 
                                    </tr>
                                </thead>
                                <tbody> 
                                @foreach($plan as $plans)     
                                    <tr>
                                    	<td>{{ $loop->iteration }}</td>
                                        <td>{{ $plans->name }}</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/plan/export') }}/{{ $plans['id'] }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#depositplanreportdatatable').DataTable();
    });
</script>
@endpush