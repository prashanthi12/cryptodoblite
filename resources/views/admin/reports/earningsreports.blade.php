@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Earnings Reports</h3>   
                        <div class="panel panel-default">
                            <table class="table table-bordered" id="earningsreportdatatable">
                            	<thead>
                            	    <tr>
                                        <th>#</th>
                            	        <th>Name</th> 
                            	        <th>Option</th> 
                            	    </tr>
                                </thead>
                                <tbody>        
                                    <tr>
                                        <td>1</td>
                                        <td>Earnings as Interest</td>
                                        <td><a id="export" href="{{ url('admin/reports/earnings/export/interest') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Referral Commission</td>
                                        <td><a id="export" href="{{ url('admin/reports/earnings/export/referral') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
	                           </tbody>
                            </table>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#earningsreportdatatable').DataTable();
    });
</script>
@endpush