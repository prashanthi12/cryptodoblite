@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Investors Reports</h3>   
                        <div class="boxy boxy-white">
                            <table class="table table-bordered" id="investorsreportdatatable">
                            	<thead>
                            	    <tr>
                                        <th>#</th>
                            	        <th>Name</th> 
                            	        <th>Option</th> 
                            	    </tr>
                                </thead>
                                <tbody>        
                                    <tr>
                                        <td>1</td>
                                        <td>Top 10 Investors</td>
                                        <td><a id="export" href="{{ url('admin/reports/investors/export/1') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Top 100 Investors</td>
                                        <td><a id="export" href="{{ url('admin/reports/investors/export/2') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                            	</tbody>
                            </table>
                        </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#investorsreportdatatable').DataTable();
    });
</script>
@endpush