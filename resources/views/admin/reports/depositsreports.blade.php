@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Deposits Status Reports</h3>   
                        <div class="boxy boxy-white">
                            <table class="table table-bordered" id="depositreportdatatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th> 
                                        <th>Option</th> 
                                    </tr>
                                </thead>
                                <tbody>        
                                    <tr>
                                        <td>1</td>
                                        <td>Active</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/export/active') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>New</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/export/new') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Matured</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/export/matured') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Problem</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/export/problem') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Released</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/export/released') }}" class="btn btn-success btn-xs">Export</a></td>            
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Rejected</td>
                                        <td><a id="export" href="{{ url('admin/reports/deposits/export/rejected') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#depositreportdatatable').DataTable();
    });
</script>
@endpush