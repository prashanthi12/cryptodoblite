@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
                <h3>Funds Transfer Reports</h3>   

                        <div class="boxy boxy-white">
                            <table class="table table-bordered" id="fundsreportdatatable">
                            	<thead>
                            	    <tr>
                                        <th>#</th>
                            	        <th>Name</th> 
                            	        <th>Option</th> 
                            	    </tr>
                                </thead>
                                <tbody>        
                                    <tr>
                                        <td>1</td>
                                        <td>Fund Transfer</td>
                                        <td><a id="export" href="{{ url('admin/reports/fundtransfers/export') }}" class="btn btn-success btn-xs">Export</a></td>
                                    </tr>
                            	</tbody>
                            </table>

            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#fundsreportdatatable').DataTable();
    });
</script>
@endpush