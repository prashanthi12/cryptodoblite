@extends('layouts.adminpanel')

@section('content')
<section class="section">
  	<div class="row">
	    <div class="container">
	        <h3>Users List</h3>       
	        <div class="boxy boxy-white">
	            <div class="row">
	                <div class="col-sm-12">
	                	@foreach($mailUsers as $username)
	                		<div class="col-md-2">
	                			<a href="{{ url('admin/users/') }}/{{ $username->touser->name }}">{{ $username->touser->name }}</a>
	                		</div>
	                	@endforeach
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</section>
@endsection
