<div class="col-md-12 ">
	<form method="post" action="{{ url('admin/ewallet/fund/cancelled/'.$fund_id)}}" class="form-horizontal" >
	{{ csrf_field() }}   
	    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
	        <textarea rows="5" class="form-control" placeholder="Comments" name="comment">{{ old('comment') }}</textarea>
	        <small class="text-danger">{{ $errors->first('comment') }}</small>
	    </div>  
	    <div class="form-group">	       
	        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
	        <a href=" " class='btn btn-default'>{{ trans('forms.reset') }}</a>
	    </div>
	</form>
</div>
