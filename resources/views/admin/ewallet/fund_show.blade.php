@extends('layouts.adminpanel') 
@section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.fund_list') }}</h3>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('layouts.message')
                        </div>
                    </div>
                    <div class="row responsive">
                        <div class="col-md-12">                  
                            @include('admin.ewallet.fund_list')                              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
