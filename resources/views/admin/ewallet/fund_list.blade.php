<table class="table table-bordered table-striped dataTable" id="listdatatable">
    <thead>
        <tr>
            <th>#</th>
            <th>From</th>      
            <th>Amount ( {{ config::get('settings.currency') }} )</th>
            <th>BTC Amount</th>
            <th>Date</th>  
            <th>Bitcoin Address</th> 
            <th>Transaction ID</th> 
            <th>Status</th>
            <th>Approve On</th>
            <th>Cancel On</th>
            <th>Comment</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($fundlist as $fund)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $fund->present()->getUsername($fund['from_user_id']) }}</td>      
            <td>{{ $fund->amount }} </td>
            <td>{{ $fund->total_btc_amount }} </td>
            <td>{{ $fund->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $fund->bitcoin_address }}</td>
            @if($fund->status=='approve')
            <td>{{ $fund->bitcoin_hash_id }}</td>
            @else
            <td>-</td>
            @endif
            <td>{{ ucfirst($fund->status) }}</td>
            @if($fund->status=='approve')
            <td>{{ $fund->approve_at->format('d/m/Y H:i:s') }}</td>
            @else
            <td> - </td>
            @endif
            @if($fund->status=='cancel')
            <td>{{ $fund->cancel_at->format('d/m/Y H:i:s') }}</td>
            @else
            <td>-</td>
            @endif
            <td>{{ $fund->comments_cancel }}</td>
            @if($fund->status=='pending')
            <td>
                <div class="form-group">
                    <div class="flex-button-group">                  
                        <div>
                            <a  href="{{ url('/admin/ewallet/fund/approved/'.$fund->id) }}" class="btn btn-success btn-sm flex-button">Approve</a> 
                        </div>
                        <div>
                            <a id="cancel" href="{{ url('/admin/ewallet/fund/cancelled/'.$fund->id) }}" class="btn btn-danger btn-sm flex-button">Cancel</a>                  
                        </div>      
                    </div>
                </div>
            </td>
            @else
            <td> - </td>
            @endif 
        </tr>
        @endforeach
    </tbody>
</table>

@push('scripts')
<script>
$(document).ready(function(){
    $('#listdatatable').DataTable();           
});
</script>
@endpush