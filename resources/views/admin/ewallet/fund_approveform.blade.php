<div class="col-md-12 ">
	<form method="post" action="{{ url('admin/ewallet/fund/approved/'.$fund->id)}}" class="form-horizontal" >
	{{ csrf_field() }}     
		<div class="row">
	 		<div class="col-md-12">
				<div class="form-group">
	              	<div class="row">
	                  	<div class="col-md-6">
	                       <label>Amount ({{ config::get('settings.currency') }}) </label>
	                  	</div>
	                    <div class="col-md-6">
	                       <label>{{ $fund->amount }} </label>
	                  	</div>
	              	</div>  
	         	</div>
	         	<div class="form-group">
	              	<div class="row">
	                  	<div class="col-md-6">
	                       	<label>BTC Amount ( Equivalent to 1 USD )</label>
	                  	</div>
	                  	<div class="col-md-6">
	                       <label>{{ sprintf("%.8f", $fund->btc_amount) }} </label>
	                  	</div>
	              	</div>  
	         	</div>
	         	<div class="form-group">
	              	<div class="row">
		                <div class="col-md-6">
		                    <label>BTC Amount ( Expected )</label>
		                </div>
	                  	<div class="col-md-6">
	                       <label>{{ $fund->total_btc_amount }} </label>
	                  	</div>
	              	</div>	           	             
	         	</div> 	  
	        	<div class="form-group">
	 				<div class="row">
		                <div class="col-md-6">
		                    <label>Transaction ID</label>
		                </div>
	                    <div class="col-md-6">
	                  	 	<label>
	                  	 		<a href="{{ $fund->present()->getBitcoinUrl($fund->bitcoin_hash_id) }} " target="_blank">{{ $fund->bitcoin_hash_id }}</a>
	                  	 	</label>	                      
	                    </div>
	                </div>
	            </div> 
	 			<div class="form-group{{ $errors->has('received_amount') ? ' has-error' : '' }}">
		 			<div class="row">
		                <div class="col-md-6">
		                    <label>Amount ( {{ config::get('settings.currency') }} )</label>
		                </div>
		                <div class="col-md-6">
		                    <input type="text" class="form-control"  placeholder="Amount" name="received_amount" value="{{ old('received_amount') }}">
		        			<small class="text-danger">{{ $errors->first('received_amount') }}</small>
		                </div>
		            </div>
		        </div>
		 		<div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
					<div class="row">
	                  	<div class="col-md-6">
	                       <label>Comment</label>
	                  	</div>
	 					<div class="col-md-6">
					        <textarea  rows="5" class="form-control"  placeholder="Comments" name="comment">{{ old('comment') }}</textarea>
					        <small class="text-danger">{{ $errors->first('comment') }}</small>
				        </div>
			        </div>
				</div> 
			    <div class="form-group">
					<div class="row">
			      		<div class="col-md-6 col-md-offset-6">	    
	              			<input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
		        			<a href=" " class='btn btn-default'>{{ trans('forms.reset') }}</a>
			        	</div>
			        </div>
			    </div>
			</div>
		</div>
	<input type="hidden" name="fund_id" value="{{$fund->id}}"> 
	</form>
</div>

