<table class="table table-striped">
<thead>
	<th>Url</th>
	<th>Page Views</th>
</thead>
<tbody>
@foreach($mostVisitedPages as  $page)
<tr>
	<td> {{ $page['url'] }}</td>
	<td>{{ $page['pageViews'] }}</td>
</tr>
@endforeach
</tbody>
</table>