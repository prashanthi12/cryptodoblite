<table class="table table-striped">
<thead>
	<th>Url</th>
	<th>Page Views</th>
</thead>
<tbody>
@foreach($topReferrers as  $topReferrer)
<tr>
	<td> {{ $topReferrer['url'] }}</td>
	<td>{{ $topReferrer['pageViews'] }}</td>
</tr>
@endforeach
</tbody>
</table>