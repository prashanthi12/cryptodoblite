<table class="table table-striped">
<thead>
	<th>Browser</th>
	<th>Session</th>
</thead>
<tbody>
@foreach($topBrowsers as  $browser)
<tr>
	<td> {{ $browser['browser'] }}</td>
	<td>{{ $browser['sessions'] }}</td>
</tr>
@endforeach
</tbody>
</table>