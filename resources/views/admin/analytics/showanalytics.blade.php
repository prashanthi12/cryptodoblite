@extends('layouts.adminpanel') @section('content')
<section class="section">

    <div class="row">
        <div class="container">
            <h3>Analytics</h3>
                 <div class="panel panel-default">
   
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                    <h4>Daily Vistors</h4>
                   <div  height="480px">
                            <p>Graph comes here</p>
                   </div>
            </div>
        </div>
        <hr>
                <div class="row">
            <div class="col-md-12">
                <div class="col-md-4">
                    <h4>Top Pages</h4>
                    @include('admin.analytics._table_page')
                </div>
                <div class="col-md-4">
                    <h4>Top Referrers</h4>
                    @include('admin.analytics._table_referrer')
                </div>
                 <div class="col-md-4">
                    <h4>Top Browsers</h4>
                    @include('admin.analytics._table_browser')
                </div>
            </div>
        </div>

    </div>
 </div>
            </div>
        </div>


</section>
@endsection
@push('scritps')

@endpush
