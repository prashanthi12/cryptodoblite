@extends('layouts.adminpanel')
@section('content')
<section class="section">
    <div class="container">
        <div class="content">
            <h3>{{ trans('admin.users') }} <small>( {{ $totalusers }} )</small></h3>        
        </div>
    </div>
    <div class="container mt-20 mb-20">
        @include('layouts.message')
        <div class="boxy boxy-white">
            <form  method="POST" role="search">
            {{ csrf_field() }}
                <div class="input-group form-group">
                    <input type="text" class="form-control" name="q" placeholder="Search users by Username or Email"> 
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
            <table class="table table-bordered table-striped" id="">
                <thead>
                    <tr>
                        <th>{{ trans('admin.name') }}</th>
                        <th>{{ trans('admin.dateofjoin') }}</th>
                        <th>{{ trans('admin.sponsor') }}</th>
                        <th>{{ trans('admin.referrals') }}</th>
                        <th>{{ trans('admin.balance') }} ({{ config::get('settings.currency') }})</th>
                        <th>{{ trans('admin.activedeposit') }} ({{ config::get('settings.currency') }})</th>
                        <th>{{ trans('admin.lifetimedeposit') }} ({{ config::get('settings.currency') }})</th>
                        <th>{{ trans('admin.lifetimeearnings') }} ({{ config::get('settings.currency') }})</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td width="220px">
                            <p class="trim">
                                <a href="{{ url('admin/users') }}/{{ $user->name }} ">
                                <strong>{{ $user->name }}</strong>
                                </a>
                            </p>
                            <p class="trim">
                            @if( $user->isEmailVerified )
                                <a href="mailto:{{ $user->email }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-message" title="{{ $user->email }}"></span>Email</a>
                            @else
                                <a href="mailto:{{ $user->email }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-message" title="{{ $user->email }}"></span>Email</a>
                            @endif
                            @if( $user->isActive )
                                <span class="label label-success"><i class="glyphicon glyphicon-lock" title="verified"></i></span>
                            @else
                                <span class="label label-danger"><i class="glyphicon glyphicon-lock" title="verified"></i></span>
                            @endif
                            @if( $user->isUserProfileCompleted )
                                <span class="label label-success">Profile</span>
                            @else
                                <span class="label label-danger">Profile</span>
                            @endif
                            @if( $user->isKycApproved)
                                <span class="label label-success">KYC</span>
                            @elseif( $user->isKycApproved == 0 || $user->isKycApproved == 2)
                                <span class="label label-danger">KYC</span>
                            @endif
                            </p>
                        </td>
                        <td>
                            <span title="{{ $user->created_at }}">{{ $user->created_at->format('d-m-y') }}</span>
                        </td>
                        <td>{{ $user->sponsorname }}</td>
                        <td class="text-center">{{ $user->referralCount }}</td>
                        <td class="is-money text-right">{{ $user->balance }}</td>
                        <td class="is-money text-right">{{ $user->activeDepositTotal }}</td>
                        <td class="is-money text-right">{{ $user->LifeTimeDeposit }}</td>
                        <td class="is-money text-right">-</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
        </div>
    </div>
</section>
@endsection
