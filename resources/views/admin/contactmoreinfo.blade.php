@extends('layouts.adminpanel') @section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <h3>Contact Us Details</h3>   
            </div>
        <div class="panel-body">            
            <div class="row">
                <div class="col-md-12">
                    <div class="boxy boxy-white">
                        <div><a href="/admin/contactus" style="float:right; padding-right: 20px; padding-bottom: 10px;">Back to List</a></div>
                            <div class="container mt-20 mb-20">
                                <p><b>Sender Name :</b> {{ $contact->fullname }}</p>
                                <p><b>Email :</b> {{ $contact->email }}</p>
                                <p><b>Contact Number :</b> {{ $contact->contactno }}</p>
                                <p><b>Skype / Gtalk :</b> {{ $contact->skype_gtalk }}</p>
                                <p><b>Queries :</b> {{ rawurldecode($contact->queries) }}</p>
                                <p><b>Date :</b> {{ $contact->created_at->format('d/m/Y') }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
