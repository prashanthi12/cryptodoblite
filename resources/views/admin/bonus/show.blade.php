@extends('layouts.adminpanel') @section('content')
<section class="section">
    <div class="row">
        <div class="container">
            <h3>{{ trans('admin.bonuslists') }}</h3>
            <div class="boxy boxy-white">
                <div class="row">
                    <div class="col-md-12">
                        @include('layouts.message')
                    </div>
                </div>
                @include('admin.bonus.bonuslists')               
            </div>
        </div>
    </div>
</section>
@endsection
