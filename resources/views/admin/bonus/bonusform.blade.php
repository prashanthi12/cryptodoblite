<div class="col-md-12 ">
<form method="post" action="{{ url('admin/bonus/send')}}" class="form-horizontal" id="contact">
{{ csrf_field() }}

    @if (\Session::get('bonususerid') == '')

    <div class="form-group{{ $errors->has('users') ? ' has-error' : '' }}">
        <select class="progControlSelect2" multiple="true" name="users[]">
        @foreach ($userlists as $user)
        <option value="{{ $user->user->id }}" >{{ $user->user->name }}</option>
        @endforeach
      </select>
        <small class="text-danger">{{ $errors->first('users') }}</small>
    </div>

    @endif

     <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
        <input type="text" name="amount" value="{{ old('amount') }}" class="form-control"  placeholder="Enter the Bonus Amount">
        <small class="text-danger">{{ $errors->first('amount') }}</small>
    </div> 

    <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
        <textarea  rows="5" class="form-control"  placeholder="Comments" name="comment">{{ old('comment') }}</textarea>
        <small class="text-danger">{{ $errors->first('comment') }}</small>
    </div> 

    <div class="form-group">
        <input value="{{ trans('forms.submit_btn') }}" class="btn btn-primary" type="submit" onclick="this.disabled=true;this.form.submit();"> 
        <a href=" " class='btn btn-default'>{{ trans('forms.reset') }}</a>
    </div>
</form>
</div>

@push('styles')
<style>
.progControlSelect2 {
  width: 350px;
}
form {
  margin-top: 2px;
}
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
@endpush

@push('scripts')
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>


<script>

$(document).ready(function(){
  var $progControl = $(".progControlSelect2").select2({
        placeholder: "Select Users"//placeholder
    });
  $(".iOSSelect2").on("click", function () { $progControl.val(["sw", "oc"]).trigger("change"); });
  $(".clearSelect2").on("click", function () { $progControl.val(null).trigger("change"); });
})

</script>
@endpush
