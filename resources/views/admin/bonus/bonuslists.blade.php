<table class="table table-bordered" id="bonuslist">
    <thead>
    <tr>
        <th>{{ trans('admin.username') }}</th>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.comments') }}</th> 
        <th>{{ trans('admin.date') }}</th>               
    </tr>
    </thead>
    <tbody>
        @foreach($bonuses as $data)       
        <tr>
            <td><a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a></td>
            <td>{{ $data->amount }} </td>
            <td>{{ $data->comments }}</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>            
        </tr>
         @endforeach
    </tbody>
 </table>

 @push('scripts')
<script>
    $(document).ready(function(){
        $('#bonuslist').DataTable();      
    });
</script>
@endpush