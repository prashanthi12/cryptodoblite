@extends('layouts.adminpanel') 
@section('content')
<div class="container"><div class="content"><h3>Message Details</h3></div></div>
<div class="container mt-20 mb-20">
    <div class="panel panel-default">
        <div class="panel-heading">Message Details  
        <a href="{{ url('admin/message/list') }}" class="pull-right">Back to list</a>
        </div>
        <div class="panel-body">  
                @include('layouts.message')
            <div class="col-md-12">
                <div class="conversation-box col-md-8">
                    <input type="hidden" name="baseurl" id="baseurl" value="{{url('/')}}">     
                        @include('admin.message.details')
                </div>
                <div class="participant-box col-md-4">
                    @foreach ($participantDetails as  $data)
                    <div class="flex profile-section">
                     <div class="profile-image-section"> 
                      @if($data->profile_avatar === null)
                        <img src="http://placehold.it/60/60/?text=No+Image">
                      @else 
                        <img src="{{ url($data->profile_avatar) }}" width="60px" height="60px">
                      @endif
                      </div>
                      <div class="profile-data-section">
                        <h3 class="profile-full-name">
                          @if(!empty($data->firstname) && !empty($data->lastname))
                          {{ $data->firstname }} {{ $data->lastname }}
                          @else
                          <span style="color:#999">No Name Given</span>
                          @endif
                        </h3>
                        <p>{{ $data->country_id }}</p>
                          <p> 
                          <div class="badge-group">
                            <div class="email-badge"> 
                              @if($data->email_verified) 
                              <span class="label label-success">Email</span> 
                              @else 
                              <span class="label label-default">Email</span> 
                              @endif
                            </div>
                             <div class="mobile-badge"> 
                                @if($data->mobile_verified) 
                                <span class="label label-success">Mobile</span> 
                                @else 
                                <span class="label label-default">Mobile</span> 
                                @endif
                            </div>
                            <div class="kyc-badge"> 
                                @if($data->kyc_verified) 
                                <span class="label label-success">KYC</span> 
                                @else 
                                <span class="label label-default">KYC</span> 
                                @endif
                            </div>
                          </div>
                        </p>
                      </div>
                    </div>
                    <hr>
                    @endforeach
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
