<div class="modal-dialog">
<!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{ trans('forms.message_details') }}</h4>
        </div>
        <div class="modal-body">
            <form method="post" id="send_mail" action="{{ url('admin/message/users/sendmail/'.$user->id) }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                    <label for="subject" class="control-label">{{ trans('forms.subject') }}:</label>
                    <input type="text" class="form-control" id="subject" name="subject" required="required">
                </div>
                <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                    <label for="message" class="control-label">{{ trans('forms.message') }}:</label>
                    <textarea type="text" class="form-control" id="message" name="message" required="required"></textarea> 
                </div>
                <div class="form-group">
                    <input type="hidden" name="mailmessage" value="">
                    <input value="{{ trans('forms.send') }}" class="btn btn-success" id="sendmail" type="submit" onclick="this.disabled=true;this.form.submit();">
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>


