@foreach($bankwire_result as $data)
    <dl class="bankaccount-info dl-horizontal">
    
        <dt>{{ trans('admin.accountname') }}</dt>       
        <dd>{{ $data['param4'] }}</ds> 
        @if($data['current'] == 1) 
            <span class="label label-success">{{ trans('admin.primary') }}</span>
        @endif
        <dt>{{ trans('admin.accountnumber') }}</dt>
        <dd>{{ $data['param3'] }}</dd>
        <dt>{{ trans('admin.bankname') }}</dt>
        <dd>{{ $data['param1'] }}</dd>
        <dt>{{ trans('admin.ifsc/swiftcode') }}</dt>
        <dd>{{ $data['param2'] }}</dd>
         <dt>{{ trans('admin.address') }}</dt>
        <dd>{{ $data['param5'] }}</dd>
    </dl>
 @endforeach