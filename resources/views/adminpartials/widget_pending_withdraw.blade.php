<div class="widget-grid">
@if($withdrawlistsCount > 0)
  @foreach($withdrawlists as $data)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.username') }} : </small><br/>       
          <a href="{{ url('admin/users/'.$data->user->name) }}">{{ $data->user->name }}</a>
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.amount') }} : </small><br/>       
          {{ $data->amount }} 
      </p>     
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.payment') }} :</small><br/>
          @include('adminpartials._popoveruserpayaccounts')<br/>
      </p>
    </div>
    <div class="grid-item d">
      <p class="form-group flex-button-group flex-vertical">
          <a id="completed" href="#" rel="{{ url('admin/withdraw/complete/'.$data['id'].'') }}" class="btn btn-success btn-xs flex-button flex-button-vertical complete" >{{ trans('admin.approve') }}</a>
                 
          <a id="rejected" href="#" rel="{{ url('admin/withdraw/reject/'.$data['id'].'') }}" class="btn btn-danger btn-xs flex-button flex-button-vertical reject" >{{ trans('admin.reject') }}</a>
      </p>
    </div>
  @endforeach
@else
    <div class="">{{ trans('admin.no_withdraw_found') }}</div>
@endif   
</div>
<div>
    @if($withdrawlistsCount > 5)
       <p class="text-center"> <a href="{{ url('admin/actions/withdraw') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_pendingwithdraw') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() { 
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });

    $('.complete').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this withdraw request ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.reject').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to rejected this withdraw request ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

});
</script>
@endpush