<div class="widget-grid">
@if($newDepositCount > 0)
  @foreach($depositlists as $newdeposit)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.depositby') }} : </small><br/>       
        @if (is_null($newdeposit->user))
          {{ 'system' }}
        @else
          <a href="{{ url('admin/users/'.$newdeposit->user->name) }}">{{ ucfirst($newdeposit->user->name) }}</a>
                @endif
                <br/><small>{{ $newdeposit->created_at->diffForHumans() }}</small></p>
            </div>
            <div class="grid-item b">
                <p class="text-right">{{ config::get('settings.currency') }} <strong>{{ $newdeposit->amount }}</strong> </p>
            </div>
            <div class="grid-item c">
            
            <p><small>{{ trans('admin.depositplan') }} :</small><br/>{{ $newdeposit->plan->name }}<br/>
            <small>{{ trans('admin.paymentvia') }} :</small><br/>{{ $newdeposit->paymentgateway->displayname }}<br/>
            @if ($newdeposit->paymentgateway->id == 9)
            <p>
                <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $newdeposit->transaction_id }}'>
                {{ trans('admin.viewhashdetails') }}
                </a></p>
   
                @if ($newdeposit->present()->getBitcoinActualAmount($newdeposit->transaction_id) != $newdeposit->present()->getBitcoinReceivedAmount($newdeposit->transaction_id))
                    <p><span class="label label-danger" >{{ trans('admin.problem') }}</span></p>
                @endif

            @else
            <small>{{ trans('admin.uniquetransactionid') }} :</small><br/>{{ $newdeposit->present()->getTransactionNumber($newdeposit->transaction_id) }}</p>
            @endif
            </div>
            <div class="grid-item d">
                <p class="form-group flex-button-group flex-vertical">
                    @if ($newdeposit->paymentgateway_id == 1 || $newdeposit->paymentgateway_id == 17)
                        <a href="#" class="btn btn-success btn-xs flex-button flex-button-vertical confirm" rel="{{ url('admin/deposit/confirm/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a>
                    @else
                        <a href="#" class="btn btn-success btn-xs flex-button flex-button-vertical confirm" rel="{{ url('admin/deposit/approve/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a>
                        <!-- <form class="activate" action="{{ url('admin/deposit/approve/'.$newdeposit->id) }}" method="post">
                        {{ csrf_field() }} 
                            {!! Form::submit("CONFIRM", ['class' => 'btn btn-success btn-xs flex-button flex-button-vertical']) !!}
                        </form> -->
                    @endif
                        <a href="#" class="btn btn-danger btn-xs flex-button flex-button-vertical reject" rel="{{ url('admin/deposit/reject/'.$newdeposit->id.'') }}" >{{ trans('admin.reject') }}</a>  
                </p>
            </div>
        @endforeach
  @else
    <div class="">{{ trans('admin.no_depositapproval_found') }}</div>
@endif   
</div>
<div>
    @if($newDepositCount > 5)
       <p class="text-center"> <a href="{{ url('admin/actions/newdeposit') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_pendingdeposits') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {    
    $('#newdepositdatatable').DataTable();

    $(".activate").on("submit", function(){
        return confirm("Do you want to activate this deposit.?");
    });  

    $('.confirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.reject').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to reject this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });     

  $('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
        $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
            if (status == "success") {
                $(response).modal('show');
            }
        });
    });    
});
</script>
@endpush