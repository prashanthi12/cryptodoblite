<div class="tab-container">
<h3>{{ trans('admin.loginhistory') }}</h3>
<table class="table table-bordered table-striped dataTable"  id="logindatatable">
<thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.subject') }}</th> 
        <th>{{ trans('admin.ipaddress') }}</th>               
        <th>{{ trans('admin.dateandtime') }}</th>      
    </tr>
</thead>
<tbody>
@foreach($loginhistory as $data)
<?php
$properties = json_decode($data['properties'], true);
?> 
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td> {{ $data->description }} </td>       
        <td>{{ $properties['ip'] }}</td>        
        <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>        
    </tr>
 @endforeach
</tbody>
</table>
</div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#logindatatable').DataTable();
    });
</script>
@endpush