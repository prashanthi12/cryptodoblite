<div class="container mt-20 mb-20">
    <div class="boxy boxy-white p-20">
        <div class="row">
            <div class="col-md-4">
                <h4>{{ $user->userprofile->firstname }} {{ $user->userprofile->lastname }}</h4>
                <p>{{ trans('admin.username') }} : {{ $user->name }} </p>
                <p>{{ trans('admin.email') }} : {{ $user->email }} </p>
                <p>{{ trans('admin.membersince') }} : {{ $user->created_at->diffForHumans() }}</p>
            </div>
            <div class="col-md-8">
                <span class="mt-20 pull-right">
                    <p> 
                        <a id="message" href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#sendmail">{{ trans('admin.sendmail') }}</a>
                        <a id="message" href="{{ url('admin/message/senduser/'.$user->id) }}" class="btn btn-xs btn-primary">{{ trans('admin.sendmessage') }}</a>
                        <a id="reset" href="#" rel="{{ url('/admin/users/resetpassword/'.$user->id) }}" class="btn btn-xs btn-danger">{{ trans('admin.resetpassword') }}</a>
                        <a href="#" rel="{{ url('/admin/sendbonus/'.$user->id) }}" class="btn btn-xs btn-success" id="bonus">{{ trans('admin.sendbonus') }}</a>
                        <a href="#" rel="{{ url('/admin/sendpenalty/'.$user->id) }}" class="btn btn-xs btn-warning" id="penalty">{{ trans('admin.sendpenalty') }}</a>
                    </p>
                    <p>
                    <!-- <form method="post" class="update" action="{{ url('admin/users/update/'.$user->userprofile->id.'') }}">
                    {{ csrf_field()}} -->
                    @if($user->userprofile->active == 0)
                    <div class="form-group">
                        <input type="hidden" name="userstatus" value="1">         
                        <a href="#" rel="{{ url('admin/users/update/'.$user->userprofile->id.'') }}" class="btn btn-danger btn-xs activate">{{ trans('admin.activateuser') }}</a>
                    </div>
                    <!-- <input type="hidden" name="userstatus" value="1">
                        <button type="submit" class="btn btn-success btn-xs">Activate User</button> -->
                    @else
                   <!--  <input type="hidden" name="userstatus" value="0">
                        <button type="submit" class="btn btn-danger btn-xs">Suspend User</button> -->
                    <div class="form-group">
                        <input type="hidden" name="userstatus" value="0">         
                        <a href="#" rel="{{ url('admin/users/update/'.$user->userprofile->id.'') }}" class="btn btn-danger btn-xs suspend">{{ trans('admin.suspenduser') }}</a>
                    </div>
                    @endif
                    <!-- <form> -->
                    @if($user->userprofile->email_verified == 0)
                      <a id="resend" href="{{ url('/admin/users/resend/'.$user->id) }}" class="btn btn-xs btn-primary">{{ trans('admin.resend_email') }}</a>
                    @endif

                    @if($user->userprofile->mobile_verified == 0)
                      <a id="resend" href="{{ url('/admin/users/resend/mobile/'.$user->id) }}" class="btn btn-xs btn-primary">{{ trans('admin.resend_mobile') }}</a>
                    @endif
                    <a href="{{ url('admin/users/updatereferralgroup/'.$user->userprofile->id.'') }}" id="referralgroup" class="btn btn-xs btn-primary"> {{ trans('admin.changereferralgroup') }}</a>

                    <a href="{{ url('/users/'.$user->id.'/impersonate') }}" class="btn btn-xs btn-primary"> {{ trans('admin.loginasuser') }}</a>
                  </p>
                </span>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="sendmail" role="dialog" style="margin: 120px;">
                @include('admin.message.sendmail')
            </div>
        </div>
        <div class="row boxy-border">
            <div class="col-md-3">
                @if($user->userprofile->profile_avatar != '')
                    <img width="120px" height="120px" src="{{ $user->userprofile->profile_avatar }}">
                @else
                    <img src="http://placehold.it/120x120?text=Your+Avatar">
                @endif
            </div>
            <div class="col-md-3">                          
                <p>{{ trans('admin.mobile') }}: {{ $user->userprofile->mobile }}</p>
                <p>{{ trans('admin.country') }}: {{ $user->userprofile->Country != '' ? $user->userprofile->Country->name : ''}}</p>                          
                <p>{{ trans('admin.referralgroup') }} :
                    <strong>
                        @if($user->referralgroup != null)
                        {{ ucfirst($user->referralgroup->name) }} 
                        @else 
                        {{ "Null" }}
                        @endif
                    </strong>
                </p>
                <p>{{ trans('admin.referralcommission') }} :
                    <strong>
                        @if($user->referralgroup != null)
                        {{ ucfirst($user->referralgroup->referral_commission) }} %
                        @else 
                        {{ "Null" }}
                        @endif
                    </strong>
                </p>                                 
            </div>
            <div class="col-md-3">
                <p>{{ trans('admin.balance') }} : {{ $balance }} {{ config::get('settings.currency') }}</p>
                <p>{{ trans('admin.activedeposit') }} : {{ $activeDeposit }} {{ config::get('settings.currency') }}</p>
                <p>{{ trans('admin.lifetimedeposit') }}: {{ $lifeTimeDeposit }} {{ config::get('settings.currency') }}</p>
                <p>{{ trans('admin.lifetimeearnings') }}: {{ $lifeTimeEarnings }} {{ config::get('settings.currency') }}</p>
            </div>
            <div class="col-md-3">
                <p>{{ trans('admin.userstatus') }} : 
                    @if($user->userprofile->active == 0)
                    <span class="label label-danger">{{ trans('admin.suspended') }}</span>
                    @else
                    <span class="label label-success">{{ trans('admin.active') }}</span>
                    @endif
                </p>
                <p>{{ trans('admin.emailverified') }} : 
                @if($user->userprofile->email_verified == 0)
                    <span class="label label-danger">{{ trans('admin.unverified') }}</span>
                    @else
                    <span class="label label-success">{{ trans('admin.verified') }}</span>
                    @endif
                </p>
                <p>{{ trans('admin.mobileverified') }} : 
                     @if($user->userprofile->mobile_verified == 0)
                    <span class="label label-danger">{{ trans('admin.unverified') }}</span>
                    @else
                    <span class="label label-success">{{ trans('admin.verified') }}</span>
                    @endif
                </p>
                <p>{{ trans('admin.kycapproved') }}: 
                @if ($user->userprofile->kyc_verified == 1)
                <span class="label label-success">{{ trans('admin.verified') }}</span>
                @elseif ($user->userprofile->kyc_verified == 2)
                <span class="label label-danger">{{ trans('admin.rejected') }}</span>
                @else
                <span class="label label-danger">{{ trans('admin.unverified') }}</span>
                @endif
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">                    
                <div class="col-md-3 col-md-offset-3">
                {{ trans('admin.lastloginfrom') }} :
                    @if ($lastlogin) 
                        <?php
                          $properties = json_decode($lastlogin->properties, true);
                        ?>                            
                        {{ $properties['ip'] }}
                    @endif
                </div>
                <div class="col-md-3">
                    {{ trans('admin.lastloginat') }} : 
                @if ($lastlogin) 
                    {{ $lastlogin->created_at->format('d/m/Y H:i:s') }}
                @endif
                </div>
            </div>
        </div>
                <!-- <div class="col-md-3">
                    {{ trans('admin.lastloginat') }} : 
                @if ($lastlogin) 
                    {{ $lastlogin->created_at->format('d/m/Y H:i:s') }}
                @endif
                </div> -->
    </div>
</div>

@push('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.1/sweetalert2.all.min.js"></script>
 -->
<script type="text/javascript">
$("#referralgroup").change(function() {
    var referralid = $(this).val();
    var userid = $('#userid').val();
    swal({
          title: 'Do you want to change user referral group?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Do it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }).then(function () {
            window.location.href = "{{url('admin/users/updatereferralgroup')}}" + "/" + userid + "/" + referralid;
            swal(
            'Updated!',
            'The Referral Group Updated',
            'success'
          )
        }, function (dismiss) {
          // dismiss can be 'cancel', 'overlay',
          // 'close', and 'timer'
          if (dismiss === 'cancel') {
            swal(
              'Cancelled',
              'No Changes made :)',
              'error'
            )
          }
        })
    });
    // var referralid = $(this).val();
    // var userid = $('#userid').val();
    // if(swal("Do you want to change user referral group?", {buttons: true})) {
    //     window.location.href = "{{url('admin/users/updatereferralgroup')}}" + "/" + userid + "/" + referralid;
    //     return true;
    // }
    // return false;
$(document).ready(function(){
    $('.activate').on('click', function(){
        var link = $(this).attr('rel');
        var userstatus = 1;
        // alert(changedstatus);
          swal({
          text: "Do you want to change the status ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            // window.location.href = link;
            $.ajax({
                url: link,
                data: { userstatus: userstatus },
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(data)
                {
                    //alert('success');
                    window.location.reload();
                }
            })
        });
    }); 

    $('.suspend').on('click', function(){
        var link = $(this).attr('rel');
        var userstatus = 0;
        // alert(changedstatus);
          swal({
          text: "Do you want to change the status ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            // window.location.href = link;
            $.ajax({
                url: link,
                data: { userstatus: userstatus },
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(data)
                {
                    //alert('success');
                    window.location.reload();
                }
            })
        });
    });

    $('#reset').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to password reset for this user.?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('#bonus').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to send bonus this user?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('#penalty').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to send penalty this user?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 
});
</script>
@endpush