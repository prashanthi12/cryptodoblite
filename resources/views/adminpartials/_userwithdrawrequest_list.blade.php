<div class="tab-container">
@include('adminpartials._stats_user_withdraw_detail')
<h3>{{ trans('admin.withdrawrequestlist') }}</h3>
<table class="table table-bordered" id="requestdatatable">
    <thead>
    <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
        <th>{{ trans('admin.payment') }}</th>
        <th>{{ trans('admin.requestdate') }}</th>
        <th>{{ trans('admin.actions') }}</th>             
    </tr>
    </thead>
    <tbody>
        @foreach($pendingwithdraws as $data)         
        <tr>
            <td>{{ $data->amount }} </td>
            <td>@include('adminpartials._popoveruserpayaccounts')</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
            <td>
                 <a id="rejected" href="{{ url('admin/withdraw/reject/'.$data['id'].'') }}" class="btn btn-danger btn-xs">Rejected</a>
            </td>            
        </tr>
         @endforeach
    </tbody>
</table>
 </div>

 @push('scripts')
<script>
    $(document).ready(function(){
        $('#requestdatatable').DataTable();

    });
     
     $("#rejected").on("click", function(){
        return confirm("Do you want to reject this withdraw request?");
    });
</script>
@endpush