<div class="mt-20 mb-20">
<div class="p-10">
    <div class="well ">
        <p>{{ trans('admin.totalearnings') }} : {{ $totalearnings }} {{ config::get('settings.currency') }}</p>
    </div>
</div>
<table class="table table-bordered table-striped dataTable"  id="earningdatatable">
    <thead>
        <tr>        
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.earningtype') }}</th>
            <th>{{ trans('admin.creditedat') }}</th>
            <th>{{ trans('admin.from') }}</th>      
        </tr>
    </thead>
    <tbody>
    @foreach($earings as $data)
        @php 
            $fromdetails = json_decode($data['request'], true);  
        @endphp
        <tr>
            <td>{{ $data->amount }}</td>
            <td>{{ str_replace('-', ' ',$data->accountingcode->accounting_code) }}</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
            <td><a href="{{ url('admin/users/'.$data->present()->getUsername($fromdetails['userid'])) }}">{{ ucfirst($data->present()->getUsername($fromdetails['userid'])) }}</a></td>    
        </tr>
    @endforeach
    </tbody>
</table>
{{ $earings->links() }}
</div>

@push('scripts')
<script>
    $(document).ready(function(){
       // $('#earningdatatable').DataTable();
    });
</script>
@endpush