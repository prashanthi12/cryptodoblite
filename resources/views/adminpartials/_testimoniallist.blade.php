<div class="mt-20 mb-20">
<table class="table table-bordered table-striped dataTable"  id="testimonialdatatable">
<thead>
    <tr>
        <th>{{ trans('admin.authorname') }}</th>
        <th>{{ trans('admin.title') }}</th>
        <th>{{ trans('admin.description') }}</th>
        <th>{{ trans('admin.rating') }}</th>
        <th>{{ trans('admin.date') }}</th>
        <th>{{ trans('admin.status') }}</th>
        <th>{{ trans('admin.action') }}</th>      
    </tr>
</thead>
<tbody>
@foreach($testimonials as $data)
    <tr>
        <td>{{ $data->testimonialuser->name }}</td>
        <td>{{ $data->title }}</td>
        <td>
            {!! (substr($data->description, 0, 100)) !!} ...
            <a href="/admin/testimonials/moreinfo/{{ $data->id }}">{{ trans('admin.moreinfo') }}</a>
        </td>
        <td>
            @for ($i=1; $i <= 5 ; $i++)
              <span class="glyphicon glyphicon-star{{ ($i <= $data->rating) ? '' : '-empty'}}"></span>
            @endfor
        </td>
        <td>{{ $data->created_at->format('d/m/Y') }}</td>
        <td>
            @if ( $data->active == 1)
                <span class="label label-success">{{ trans('admin.activate') }}</span>
            @elseif ( $data->active == 0)
                <span class="label label-danger">{{ trans('admin.deactivate') }}</span>
            @endif
        </td>
        <td>
        @if ( $data->active == 1)
            <!-- <form method="post" class="update" action="{{ url('admin/testimonials/'.$data->id.'') }}">
                {{ csrf_field()}} -->
                <div class="form-group">
                    <input type="hidden" name="changedstatus" value="0">
                    <!-- <button value="Remove" class="btn btn-danger btn-xs" rel="{{ url('admin/testimonials/'.$data->id.'') }}">Deactivate</button> -->
                    <a href="#" rel="{{ url('admin/testimonials/'.$data->id.'') }}" class="btn btn-danger btn-xs deactivate">{{ trans('admin.deactivate') }}</a>
                </div>
               <!--  </form> -->
        @elseif ( $data->active == 0)
            <!-- <form method="post" class="update" action="{{ url('admin/testimonials/'.$data->id.'') }}">
                {{ csrf_field()}} -->
                <div class="form-group">
                    <input type="hidden" name="changedstatus" value="1">
                    <!-- <button type="submit" value="Remove" class="btn btn-success btn-xs">Activate</button> -->
                    <a href="#" rel="{{ url('admin/testimonials/'.$data->id.'') }}" class="btn btn-success btn-xs update">{{ trans('admin.activate') }}</a>
                </div>
            <!-- </form> -->
        @endif
        </td>    
    </tr>
 @endforeach
</tbody>
</table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#testimonialdatatable').DataTable();
    });

    $('.update').on('click', function(){
        var link = $(this).attr('rel');
        var changedstatus = 1;
        // alert(changedstatus);
          swal({
          text: "Do you want to change the status ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            // window.location.href = link;
            $.ajax({
                url: link,
                data: { changedstatus: changedstatus },
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(data)
                {
                    //alert('success');
                    window.location.reload();
                }
            })
        });
    });   

    $('.deactivate').on('click', function(){
        var link = $(this).attr('rel');
        var changedstatus = 0;
        // alert(changedstatus);
          swal({
          text: "Do you want to change the status ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            // window.location.href = link;
            $.ajax({
                url: link,
                data: { changedstatus: changedstatus },
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(data)
                {
                    //alert('second');
                    window.location.reload();
                }
            })
        });
    });  

    // $(".update").on("submit", function(){
    //     return confirm("Do you want to change the status?");
    // });
</script>
@endpush