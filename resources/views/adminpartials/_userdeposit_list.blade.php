<div class="container mt-20 mb-20">
<table class="table table-bordered table-striped dataTable"  id="datatable">
<thead>
    <tr>
        <th>#</th>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.depositedon') }}</th>
        <th>{{ trans('admin.plan') }}</th>
        <th>{{ trans('admin.paymentmethod') }}</th>
        <th>{{ trans('admin.actions') }}</th>
    </tr>
</thead>
<tbody>
@foreach($user->deposits as $deposit)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $deposit->amount }}</td>
        <td>{{ $deposit->created_at }}</td>
        <td>{{ $deposit->plan->name }}</td>
        <td>{{ $deposit->paymentgateway->displayname }}</td>
        <td>
            later
        </td>
    </tr>
 @endforeach
</tbody>
</table>
</div>