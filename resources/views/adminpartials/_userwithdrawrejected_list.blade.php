<div class="tab-container">
@include('adminpartials._stats_user_withdraw_detail')
<h3>{{ trans('admin.withdrawrejectedlist') }}</h3>
<table class="table table-bordered" id="rejecteddatatable">
    <thead>
    <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
        <th>{{ trans('admin.payment') }}</th>
        <th>{{ trans('admin.comments') }}</th> 
        <th>{{ trans('admin.completeddate') }}</th>     
    </tr>
    </thead>
    
        @foreach($rejectedwithdraws as $data)             
        <tr>
            <td>{{ $data->amount }}</td>
            <td>@include('adminpartials._popoveruserpayaccounts')</td>
            <td>{{ $data->comments_on_rejected }}</td>  
            <td>{{ $data->rejected_on }}</td>
                      
        </tr>
         @endforeach
  

 </table>
</div>
 @push('scripts')
<script>
    $(document).ready(function(){
        $('#rejecteddatatable').DataTable();

    });
</script>
@endpush