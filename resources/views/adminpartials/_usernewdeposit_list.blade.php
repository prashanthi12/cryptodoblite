<div class="tab-container">
@include('adminpartials._stats_user_deposit_detail')
<h3>{{ trans('admin.newdepositlist') }}</h3>
<table class="table table-bordered table-striped dataTable"  id="newdepositdatatable">
<thead>
     <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
        <th>{{ trans('admin.depositedon') }}</th>
        <th>{{ trans('admin.plan') }}</th>
        <th>{{ trans('admin.paymentmethod') }}</th>
        <th>{{ trans('admin.bitcoindetails') }}</th>
        <th>{{ trans('admin.actions') }}</th>
    </tr>
</thead>
<tbody>
 @foreach($newdeposits as $newdeposit)
    <tr>
        <td>{{ $newdeposit->amount }}</td>
        <td>{{ $newdeposit->created_at->format('d/m/Y H:i:s') }}</td>
        <td>{{ $newdeposit->plan->name }}</td>
        <td>{{ $newdeposit->paymentgateway->displayname }}</td>
         @if ($newdeposit->paymentgateway->id == 9)
             <td><a class='newdeposit-bitcoin' href="#" data-toggle='modal' data-target1='{{ $newdeposit->transaction_id }}'>{{ trans('admin.viewdetails') }}</a></td>
        @else
             <td>-</td>
        @endif
        <td>
            <div class="form-group">
                <div class="flex-button-group">
                   <!--  <div> <a type="button" class="btn btn-default btn-sm flex-button">View</a></div> -->
                    <div>
                        <form class="activate" action="{{ url('admin/deposit/approve/'.$newdeposit->id) }}" method="post">
                             {{ csrf_field() }} 
                             {!! Form::submit("Active", ['class' => 'btn btn-success btn-sm flex-button']) !!}
                        </form>
                    </div>
                    <div>
                        <a id="reject" href="{{ url('admin/deposit/reject/'.$newdeposit->id.'') }}" class="btn btn-danger btn-xs">{{ trans('admin.rejected') }}</a>                        
                    </div>
                </div>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>
</div>
@push('scripts')
<script>
 $('.newdeposit-bitcoin').on('click', function () {
            var $this = $(this).data('target1');

            $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {

                if (status == "success") {
                    $(response).modal('show');
                }
            });
        });
$(document).ready(function(){
        $('#newdepositdatatable').DataTable();

        $(".activate").on("submit", function(){
        return confirm("Do you want to activate this deposit.?");
    });

     $("#reject").on("click", function(){
        return confirm("Do you want to rejected this deposit.?");
    });
});
</script>
@endpush