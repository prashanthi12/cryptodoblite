<div class="widget-grid">
  @foreach($activelogusers as $activeloguser)
    <div class="grid-item a">
      <p>
        <small>#</small><br/>       
          {{ $loop->iteration }}
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.username') }} : </small><br/>       
          {{ $activeloguser['user']['name'] }} 
      </p>     
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.ipaddress') }} :</small><br/>
          {{ $activeloguser['ip_address'] }}
      </p>
    </div>
    <div class="grid-item d">
      <p class="form-group flex-button-group flex-vertical">
          <small>{{ trans('admin.date') }} :</small><br/>
          {{ date("d-m-Y", $activeloguser['last_activity']) }}
      </p>
    </div>
  @endforeach 
</div>
<div>
    @if($activelogusersCount > 5)
       <p class="text-center"> <a href="{{ url('admin/actions/kyc') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_loggedinusers') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

