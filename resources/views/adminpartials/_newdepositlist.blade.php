<table class="table table-bordered table-striped dataTable"  id="newdepositdatatable">
    <thead>
         <tr>
            <th>{{ trans('admin.id') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th>
            <th>{{ trans('admin.depositedon') }}</th>
            <th>{{ trans('admin.plan') }}</th>
            <th>{{ trans('admin.paymentmethod') }}</th>
            <th>{{ trans('admin.paymentdetails') }}</th>
            <th>{{ trans('admin.actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($depositlists as $newdeposit)
        <tr>
            <td>#{{ $loop->iteration  }}</td>
            <td>       
                @if (is_null($newdeposit->user))
                {{ 'system' }}
                @else
                 <a href="{{ url('admin/users/'.$newdeposit->user->name) }}">{{ $newdeposit->user->name }}</a>
                @endif
            </td>
            <td class="text-right">{{ $newdeposit->amount }} </td>
            <td>{{ $newdeposit->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $newdeposit->plan->name }}</td>
            <td>{{ $newdeposit->paymentgateway->displayname }}</td>
            @if ($newdeposit->paymentgateway->id == 9)
            <td>
                <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $newdeposit->transaction_id }}'>
                {{ trans('admin.viewdetails') }}
                @if ($newdeposit->present()->getBitcoinActualAmount($newdeposit->transaction_id) != $newdeposit->present()->getBitcoinReceivedAmount($newdeposit->transaction_id))
                    - {{ trans('admin.problem') }}
                @endif
                </a>
            </td>
            @else
            <td>{{ $newdeposit->present()->getTransactionNumber($newdeposit->transaction_id) }}</td>
            @endif
            <td width="18%">
                <div class="form-group flex-button-group">
                    @if ($newdeposit->paymentgateway_id == 1)
                        <a href="#" class="btn btn-success btn-xs flex-button confirm" rel="{{ url('admin/deposit/confirm/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a>
                    @else
                        <a href="#" class="btn btn-success btn-xs flex-button onlineconfirm" rel="{{ url('admin/deposit/approve/'.$newdeposit->id) }}">{{ trans('admin.confirm') }}</a>
                        <!-- <form class="activate" action="{{ url('admin/deposit/approve/'.$newdeposit->id) }}" method="post">
                        {{ csrf_field() }} 
                            {!! Form::submit("Confirm", ['class' => 'btn btn-success btn-xs flex-button']) !!}
                        </form> -->
                    @endif
                        <a href="#" class="btn btn-danger btn-xs flex-button reject" rel="{{ url('admin/deposit/reject/'.$newdeposit->id.'') }}" >{{ trans('admin.reject') }}</a>  
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ $depositlists->links() }}
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {    
   // $('#newdepositdatatable').DataTable();

    // $(".activate").on("submit", function(){
    //     return confirm("Do you want to activate this deposit.?");
    // });  

    $('.confirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.onlineconfirm').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    }); 

    $('.reject').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to reject this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });     

 $('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');
        $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
            if (status == "success") {
                $(response).modal('show');
            }
        });
    });    
});
</script>
@endpush
