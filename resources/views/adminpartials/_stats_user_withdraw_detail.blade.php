<h4>{{ trans('admin.withdrawdetails') }}</h4>
   <div class="col-md-12 well">
    <div class="col-md-4">
        <p>{{ trans('admin.pending') }}</p>
        <h3 class="is-amount">{{ $pendingsum }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-4">
        <p>{{ trans('admin.completed') }}</p>
        <h3 class="is-amount">{{ $completedsum }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-4">
        <p>{{ trans('admin.lifetime') }}</p>
        <h3 class="is-amount">{{ $lifetimesum }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    
</div>