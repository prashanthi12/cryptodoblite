<div class="container mt-20 mb-20">
    <table class="table table-bordered table-striped dataTable"  id="staffsdatatable">
        <thead>
            <tr>        
                <th>{{ trans('admin.name') }}</th>
                <th>{{ trans('admin.dateofjoin') }}</th>
                <th>{{ trans('admin.tickets') }}</th>  
                <th>{{ trans('admin.actions') }}</th>    
            </tr>
        </thead>
        <tbody>
            @foreach($staffs as $data)
            <tr>
                <td>{{ $data->name }}</td>
                <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
                <td>{{ count($data->agent) }}</td>
                <td>
                    <a href="{{ url('/users/'.$data->id.'/impersonate') }}" class="btn btn-primary btn-xs"> {{ trans('admin.loginasuser') }}</a>&nbsp;
                    <a id="reset" href="{{ url('/admin/users/resetpassword/'.$data->id) }}" class="btn btn-danger btn-xs">{{ trans('admin.resetpassword') }}</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@push('scripts')
<script>
    $(document).ready(function(){
        $('#staffsdatatable').DataTable();

        $("#reset").on("click", function(){
            return confirm("Do you want to password reset for this user.?");
        });
    });
</script>
@endpush