<div class="tab-container">
@include('adminpartials._stats_user_withdraw_detail')
<h3>{{ trans('admin.withdrawcompletedlist') }}</h3>
<table class="table table-bordered" id="completeddatatable">
    <thead>
    <tr>
        <th>{{ trans('admin.amount') }} ({{ config::get('settings.currency') }})</th> 
        <th>{{ trans('admin.payment') }}</th>
        <th>{{ trans('admin.comments') }}</th> 
        <th>{{ trans('admin.completeddate') }}</th>          
       
    </tr>
    </thead>
    
        @foreach($completedwithdraws as $data)         
        <tr>
            <td>{{ $data->amount }}</td>
            <td>@include('adminpartials._popoveruserpayaccounts')</span></td>
            <td>{{ $data->comments_on_complete }}</td>
            <td>{{ $data->completed_on }}</td>            
        </tr>
         @endforeach

 </table>
</div>
 @push('scripts')
<script>
    $(document).ready(function(){
        $('#completeddatatable').DataTable();    
});
</script>
@endpush