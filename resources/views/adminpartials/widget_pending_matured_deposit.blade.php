<div class="widget-grid">
@if($maturedepositCount > 0)
  @foreach($matureddepositlists as $matureddepositlist)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.username') }} : </small><br/>       
          <a href="{{ url('admin/users/'.$matureddepositlist->user->name) }}">{{ $matureddepositlist->user->name }}</a><br/>
        <small>{{ trans('admin.maturedon') }} : </small><br/>       
          {{ $matureddepositlist->matured_on->diffForHumans() }}
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.deposit') }} : </small><br/>       
        <small>{{ trans('admin.amount') }} : </small><br/> 
        <strong>{{ $matureddepositlist->amount }}</strong> {{ config::get('settings.currency') }} <br/>
        <small>{{ trans('admin.interest') }} : </small><br/> 
        {{ $matureddepositlist->interest->pluck('amount')->sum() }} {{ config::get('settings.currency') }}<br/>
        <small>{{ trans('admin.plan') }} : </small><br/> 
        {{ $matureddepositlist->plan->name }}
      </p>     
    </div>
    <div class="grid-item c">           
      <p>
          <small>{{ trans('admin.paymentvia') }} : </small><br/> 
            {{ $matureddepositlist->paymentgateway->displayname }}<br/>
            <small>{{ trans('admin.paymentdetails') }} : </small><br/> 
            @if ($matureddepositlist->paymentgateway->id == 9)
	            <a class='bitcoin' href="#" data-toggle='modal' data-target1='{{ $matureddepositlist->transaction_id }}'>{{ trans('admin.viewdetails') }}</a>
	        @else
	           {{ $matureddepositlist->present()->getTransactionNumber($matureddepositlist->transaction_id) }}
	        @endif
      </p>
    </div>
    <div class="grid-item d">
      	<p class="form-group flex-button-group flex-vertical">
	  		@if ($matureddepositlist ->plan->principle_return == 1)
	            <a href="#" rel="{{ url('admin/releasedeposit/'.$matureddepositlist->id.'') }}" class="btn btn-success btn-xs flex-button flex-button-vertical release">{{ trans('admin.released') }}</a>
	        @else
	            --
	        @endif
      	</p>
    </div>
  @endforeach
@else
    <div class="">{{ trans('admin.no_matureddeposit_found') }}</div>
@endif   
</div>
<div>
    @if($maturedepositCount > 5)
       <p class="text-center"> <a href="{{ url('admin/actions/maturedeposit') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_matureddeposits') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

@push('scripts')
<script type="text/javascript">
$('.bitcoin').on('click', function () {
    var $this = $(this).data('target1');

    $('#bitcoin-modals').load('viewbitcoinwallet/' + $this, function (response, status, xhr) {
        if (status == "success") {
            $(response).modal('show');
        }
    });
}); 

$(document).ready(function() { 
    $('.release').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to release this deposit ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });   

});
</script>
@endpush