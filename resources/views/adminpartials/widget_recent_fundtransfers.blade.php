<div class="widget-grid">
@if($transferfundsCount > 0)
  @foreach($transferfunds as $data)
    <div class="grid-item a">
      <p>
        <small>{{ trans('admin.sender') }} : </small><br/>       
          <a href="{{ url('admin/users/'.$data->present()->getUsername($data->fundtransfer_from_id->user_id)) }}">{{ ucfirst($data->present()->getUsername($data->fundtransfer_from_id->user_id)) }}</a>
      </p>
    </div>
    <div class="grid-item b">
      <p>
        <small>{{ trans('admin.amount') }} : </small><br/>       
          {{ $data->amount }} {{ config('settings.currency') }}
      </p>     
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.receiver') }} : </small><br/>  
          <a href="{{ url('admin/users/'.$data->present()->getUsername($data->fundtransfer_to_id->user_id)) }}"> 
           {{ ucfirst($data->present()->getUsername($data->fundtransfer_to_id->user_id)) }}</a>
      </p>  
    </div>
    <div class="grid-item c">           
      <p>
        <small>{{ trans('admin.date') }} : </small><br/>       
          {{ $data->created_at->diffForHumans() }}
      </p>  
    </div>
  @endforeach
  @else
    <div class="">{{ trans('admin.no_fundtransfer_found') }}</div>
@endif   
</div>
<div>
    @if($transferfundsCount > 5)
       <p class="text-center"> <a href="{{ url('admin/users') }}" class="btn btn-primary btn-xs">{{ trans('admin.viewall_fundtransfers') }}</a></p>
    @endif
</div>
<div class="modal fade" id="bitcoin-modals" role="dialog"></div>

