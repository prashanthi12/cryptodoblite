<h4>{{ trans('admin.depositdetails') }}</h4>
   <div class="col-md-12 well">
    <div class="col-md-3">
        <p>{{ trans('admin.active') }}</p>
        <h3 class="is-amount">{{ $active_deposit_amount }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-2">
        <p>{{ trans('admin.new') }}</p>
        <h3 class="is-amount">{{ $new_deposit_amount }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-2">
        <p>{{ trans('admin.closed') }}</p>
        <h3 class="is-amount">{{ $closeddeposits }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    <div class="col-md-2">
        <p>{{ trans('admin.lifetime') }}</p>
        <h3 class="is-amount">{{ $lifetimedeposits }} <small>{{ config::get('settings.currency') }}</small></h3>
    </div>
    
</div>