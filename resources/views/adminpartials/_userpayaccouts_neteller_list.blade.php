@foreach($neteller_result as $data)
<dl class="bankaccount-info dl-horizontal">    
    <dt>{{ trans('admin.merchantid') }}</dt>
    <dd>
    {{ $data['param1'] }}
     @if($data['current'] == 1) 
    <span class="label label-success">{{ trans('admin.primary') }}</span>
    @endif
    </dd>
    <dt>{{ trans('admin.merchantkey') }}</dt>
    <dd>{{ $data['param2'] }}</dd>
    
</dl>
@endforeach