<div class="tab-container">
	<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-4">
				<h4>{{ trans('admin.userprofile') }}</h4>
			</div>
		</div>
		@include('layouts.message')
	</div>
	<div class="row">
		<div class="col-md-6 p-20">
			<table class="table">
				<tr>
					<p class="text-center">
					@if($user->userprofile->profile_avatar != '')
					<img width="120px" height="120px" src="{{ $user->userprofile->profile_avatar }}">
					@else
					<img src="http://placehold.it/120x120?text=Your+Avatar">
					@endif
					</p>					
				</tr>
				<tr>
					<td>{{ trans('admin.firstname') }} : </td>
					<td>{{ $user->userprofile->firstname }} </td>
				</tr>
				<tr>
					<td>{{ trans('admin.lastname') }} : </td>
					<td>{{ $user->userprofile->lastname }} </td>
				</tr>				
				@if(Config::get('settings.address1'))
				<tr>
					<td>{{ trans('admin.address1') }} : </td>
					<td>{{ $user->userprofile->address1}} </td>
				</tr>
				@endif
				@if(Config::get('settings.address2'))
				<tr>
					<td>{{ trans('admin.address2') }} : </td>
					<td>{{ $user->userprofile->address2}} </td>
				</tr>
				@endif
				@if(Config::get('settings.city'))
				<tr>
					<td>{{ trans('admin.city') }} : </td>
					<td>{{ $user->userprofile->city}} </td>
				</tr>
				@endif
				@if(Config::get('settings.state'))
				<tr>
					<td>{{ trans('admin.state') }} : </td>
					<td>{{ $user->userprofile->state}} </td>
				</tr>
				@endif
				<tr>
					<td>{{ trans('admin.country') }} : </td>
					<td>{{ $user->userprofile->Country != '' ? $user->userprofile->Country->name : ''}} </td>
				</tr>
				<tr>
					<td>{{ trans('admin.mobile') }} : </td>
					<td>{{ $user->userprofile->mobile }} </td>
				</tr>
				<tr>
					<td>{{ trans('admin.ssn/passportno') }} : </td>
					<td>{{ $user->userprofile->ssn }} </td>
				</tr>
				<tr>
					<td>{{ trans('admin.attachproofdocument') }}</td>
					<td>
					@if ($user->userprofile->kyc_doc != '')					
					<!-- <form method="post" action="{{ url('admin/users/attachdocdownload/'.$user->userprofile->id.'') }}">
	                {{ csrf_field()}}	-->
	                <div class="form-group">
                       <!--  <button type="submit" class="btn btn-default btn-xs">{{ $user->userprofile->kyc_doc }}</button> 
                        <a href="{{ url('admin/users/attachdocdownload/'.$user->userprofile->id.'') }}" class="kycdoc"><img src={{ url($user->userprofile->kyc_doc) }}></a> -->
                        <a href="{{ url($user->userprofile->kyc_doc)}}" download>{{ $user->userprofile->kyc_doc }}</a>
	                </div>
	                <!-- </form> -->
	                @if ( $user->userprofile->kyc_verified == 0)            
	                <div>
                        <a href="#" rel="{{ url('admin/users/verifykyc/'.$user->userprofile->id.'') }}" class="btn btn-success btn-sm flex-button verifykyc">{{ trans('admin.verifykyc') }}</a>
                        <!-- <form method="post" class="approvekyc" action="{{ url('admin/users/verifykyc/'.$user->userprofile->id.'') }}">
                            {{ csrf_field() }} 
                            {!! Form::submit("Verify KYC", ['class' => 'btn btn-success btn-sm flex-button']) !!}
                        </form> -->
                    </div>
                    <div>
                    	<a href="#" rel="{{ url('admin/users/rejectkyc/'.$user->userprofile->id.'') }}" class="btn btn-danger btn-sm flex-button rejectkyc">{{ trans('admin.rejectkyc') }}</a>
                        <!-- <form method="post" class="rejectkyc" action="{{ url('admin/users/rejectkyc/'.$user->userprofile->id.'') }}">
                            {{ csrf_field() }} 
                            {!! Form::submit("Reject KYC", ['class' => 'btn btn-danger btn-sm flex-button']) !!}
                        </form> -->
					</div>
                	@elseif( $user->userprofile->kyc_verified == 2)
	                 	<span class="label label-danger">{{ trans('admin.rejected') }}</span>
	                @else
	                 	<span class="label label-success">{{ trans('admin.verified') }}</span>
	                @endif
	                @endif
					</td>
				</tr>
			</table>
		</div>
		<div class="col-md-6 p-20">
			<div class="spacer-120"><p>&nbsp;</p></div>
			<table class="table table-striped">
				@if($paypal)
				<tr>
					<td>{{ trans('admin.paypal') }}</td>
					<td>
						@if(count($paypal_result) > 0)
						@include('adminpartials._userpayaccouts_paypal_list')
						@else
						{{ trans('admin.no_paypal_account_found') }}
						@endif						
					</td>
				</tr>
				@endif
				@if($bank)
				<tr>
					<td>{{ trans('admin.bankaccount') }}</td>
					<td>
						@if(count($bankwire_result) > 0)
						@include('adminpartials._userpayaccouts_bankwire_list')
						@else
						{{ trans('admin.no_bankwire_account_found') }}
						@endif		
					</td>
				</tr>
				@endif
				@if($stp)
				<tr>
					<td>{{ trans('admin.solidtrustpay') }}</td>
					<td>
						@if(count($stpay_result) > 0)
						@include('adminpartials._userpayaccouts_stpay_list')
						@else
						{{ trans('admin.no_solidtrustpay_account_found') }}
						@endif						
					</td>
				</tr>
				@endif
				@if($payeer)
				<tr>
					<td>{{ trans('admin.payeer') }}</td>
					<td>
						@if(count($payeer_result) > 0)
						@include('adminpartials._userpayaccouts_payeer_list')
						@else
						{{ trans('admin.no_payeer_account_found') }}
						@endif			

					</td>
				</tr>
				@endif
				@if($advcash)
				<tr>
					<td>{{ trans('admin.advcash') }}</td>
						<td>
							@if(count($advcash_result) > 0)
							@include('adminpartials._userpayaccouts_advcash_list')
							@else
							{{ trans('admin.no_advcash_account_found') }}
							@endif						
						</td>
				</tr>
				@endif
				@if($bitcoin_direct)
				<tr>
					<td>{{ trans('admin.bitcoinwallet') }}</td>
					<td>
						@if(count($bitcoin_result) > 0)
						@include('adminpartials._userpayaccouts_bitcoin_list')
						@else
						{{ trans('admin.no_bitcoinwallet_found') }}
						@endif			
					</td>
				</tr>
				@endif
				@if($skrill)
				<tr>
					<td>{{ trans('admin.skrill') }}</td>
					<td>
						@if(count($skrill_result) > 0)
						@include('adminpartials._userpayaccouts_skrill_list')
						@else
						{{ trans('admin.no_skrill_account_found') }}
						@endif		
					</td>
				</tr>
				@endif
				@if($okpay)
				<tr>
					<td>{{ trans('admin.okpay') }}</td>
					<td>
						@if(count($okpay_result) > 0)
						@include('adminpartials._userpayaccouts_okpay_list')
						@else
						{{ trans('admin.no_okpay_account_found') }}
						@endif		
					</td>
				</tr>
				@endif
				@if($perfectmoney)
				<tr>
					<td>{{ trans('admin.perfectmoney') }}</td>
					<td>
						@if(count($pm_result) > 0)
						@include('adminpartials._userpayaccouts_perfectmoney_list')
						@else
						{{ trans('admin.no_perfectmoney_account_found') }}
						@endif						
					</td>
				</tr>					
				@endif

				@if($neteller)
				<tr>
					<td>{{ trans('admin.neteller') }}</td>
					<td>
						@if(count($neteller_result) > 0)
						@include('adminpartials._userpayaccouts_neteller_list')
						@else
						{{ trans('admin.no_neteller_account_found') }}
						@endif						
					</td>
				</tr>					
				@endif

				@if($paypal == '0' && $bank == '0' && $stp == '0' && $payeer == '0' && $advcash == '0' && $bitcoin_direct == '0' && $skrill == '0' && $okpay == '0' && $perfectmoney == '0' && $neteller == '0')
                    <tr>
                        <td>{{ trans('admin.no_withdraw_payments_found') }}</td>
                    </tr>
                @endif
			</table>
		</div>
	</div>
	</div>
</div>

@push('scripts')
<script>
$(document).ready(function(){
	$('.verifykyc').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to approve the kyc document ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });     

    $('.rejectkyc').on('click', function(){
        var link = $(this).attr('rel');
          swal({
          text: "Do you want to reject the kyc document ?",
          showCancelButton: true,
          showConfirmButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          allowOutsideClick: true,
        }).then(function(){
            window.location.href = link;
        });
    });  
    // $(".approvekyc").on("submit", function(){
    //     return confirm("Do you want to approve the kyc document?");
    // });

    //  $(".rejectkyc").on("submit", function(){
    //     return confirm("Do you want to reject the kyc document?");
    // });
});
</script>
@endpush