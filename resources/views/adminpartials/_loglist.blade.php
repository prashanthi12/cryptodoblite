<div class="mt-20 mb-20">
  <table class="table table-bordered table-striped dataTable" id="logdatatable">
    <thead>
        <tr>      
            <th>{{ trans('admin.logname') }}</th>
            <th>{{ trans('admin.username') }}</th>
            <th>{{ trans('admin.description') }}</th>        
            <th>{{ trans('admin.ipaddress') }}</th>
            <th>{{ trans('admin.date') }}Date</th>
        </tr>
    </thead>
    <tbody>
      @foreach($loglists as $data)
        @php 
          $properties = json_decode($data['properties'], true); 
          $ip_address = '';
          if ( isset($properties['ip']))
          {
              $ip_address = $properties['ip'];
          }
        @endphp
        <tr>
            <td>{{ ucfirst($data->log_name) }}</td>
            <td>
              @if (is_null($data->loguser))
                {{ 'system' }}
              @else
                <a href="{{ url('admin/users/'.$data->loguser->name) }}">{{ $data->loguser->name }}</a>
              @endif
            </td>
            <td>{{ $data->description }}</td>
            <td>{{ $ip_address }}</td>
            <td>{{ $data->created_at->format('d/m/Y H:i:s') }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
  {{ $loglists->links() }}
</div>

@push('scripts')
<script>
    $(document).ready(function(){
      //  $('#logdatatable').DataTable();
    });
</script>
@endpush