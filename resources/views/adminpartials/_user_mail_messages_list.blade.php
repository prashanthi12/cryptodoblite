<div class="tab-container">
<h3>{{ trans('admin.mail_message_list') }}</h3>
    <table class="table table-bordered table-striped dataTable" id="mailmessagesdatatable">
        <thead>
            <tr>
                <th>{{ trans('forms.to') }}</th>
                <th>{{ trans('mail.subject') }}</th>
                <th>{{ trans('mail.message') }}</th>
                <th>{{ trans('admin.date') }}</th>
            </tr>
        </thead>
        @foreach($mailmessagelists as $data)
        <tbody> 
            <td>{{ $data->touser->name }}</td>
            <td>{{ $data->subject }}</td>          
            <td>{!! $data->message !!}</td>         
            <td>{{ $data->created_at->diffForHumans() }}</td>
        </tbody>
        @endforeach
    </table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#mailmessagesdatatable').DataTable();
    });
</script>
@endpush