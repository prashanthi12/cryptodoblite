@extends('layouts.main') 
@section('banner')
    @include('partials.homebanner')
@endsection
@section('content')
{{-- start of first row --}}
<section id="program">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-3 col-sm-6">
                <p>{{ trans('welcome.text1') }} <span>{{ trans('welcome.textcontent1') }}</span></p>
            </div>
            <div class="col-md-3 col-sm-6">
                <p>{{ trans('welcome.text2') }} <span>{{ trans('welcome.textcontent2') }}</span></p>
            </div>
            <div class="col-md-3 col-sm-6">
                <p>{{ trans('welcome.text3') }} <span>{{ trans('welcome.textcontent3') }}</span></p>
            </div>
            <div class="col-md-3 col-sm-6">
                <p>{{ trans('welcome.text4') }} <span>{{ trans('welcome.textcontent4') }}</span></p>
            </div>          
        </div>
    </div>      
</section>   
{{-- end of first tow--}}
{{-- start of second row --}}
<section id="form">
    <div class="container">
        <h2>{{ trans('welcome.secondtitle') }}</h2>
        <div class="row">
            <div class="col-md-6 text-center">
                <img src="{{ url('/uploads/img/block.png') }}">
                <!-- <div class="row" style="border: 1px solid black;padding: 10px;margin: 0 50px;">
                    <div class="col-sm-6"><p>20 Hours of Video Training</p></div>
                    <div class="col-sm-6"><p>4 Hours Live Webminars</p></div>
                </div> -->
            </div>
            <div class="col-md-6">
                <p>{{ trans('welcome.secondcontent') }}</p>
                <!-- <form>
                    <p>To get started with crypto currency investing and trading, it’s advisebale to learn from the leaders. Eroll for out 7 Days FREE Course on crypto trading. </p>
                    <div class="">
                        <label>Full name</label>
                        <div>
                            <input type="text" name="Full Name" class="form-control">
                        </div>
                    </div>
                    <div class="">
                        <label>Email</label>
                        <div><input type="email" name="email" class="form-control"></div>
                    </div>
                    <div class="">
                        <label>Phone</label>
                        <div><input type="number" name="Phone" class="form-control"></div>
                    </div>
                    <button class="btn btn-custom btn-lg btn-block">Enroll me in Free Course</button>
                </form> -->
            </div>
        </div>
    </div>
</section>
{{-- end of second row --}}
{{-- start of third row --}}
<section id="plans">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-8 col-md-offset-2">
                <h2>{{ trans('welcome.thirdtitle') }}</h2>
                <p>{{ trans('welcome.thirdcontent') }}</p>
            </div>
        </div>
        <div class="row"> 
            <div class="plans">      
            @foreach ($plans as $plan)
                @php
                    if( $plan->plantype_id == 3 ) 
                {
                    $duartionCyle = $plan->duration * 24;
                }
                /*else if ($plan->plantype_id == 2 ) 
                {
                    $duartionCyle = $totalDays;
                }*/
                else 
                {
                    $duartionCyle = $plan->duration;
                }

                $total = ($plan->min_amount * ( $plan->interest_rate / 100 ) * $duartionCyle);
                $totalreturn = $plan->min_amount + $plan->total;
                $roinoreturn = ($total / $plan->min_amount) * 100;
                $roi = $totalreturn + $roinoreturn;
                @endphp

            <div class="boxColor-{{ $loop->iteration }}">
                <div class="box">
                    <h1>{{ $roi }}%</h1>
                    <p>After {{ $plan->duration }} {{ $plan->duration_key }}</p>
                    <p>${{ sprintf("%.2f",$plan->min_amount) }}-${{ sprintf("%.2f",$plan->max_amount) }}</p>
                    <p><small>
                        @if($plan->principle_return)
                            {{ trans('welcome.principalreturn') }}
                        @else 
                            {{ trans('welcome.noprincipalreturn') }}
                        @endif
                    </small></p>
                </div>
            </div>
            @endforeach
            </div>
            <!-- <div class="col-md-2 col-sm-4" style="background: #d698e3; ">
                <div class="box">
                    <h1>200%</h1>
                    <p>After 7 Days</p>
                    <p>$10-$1000</p>
                    <p><small>Principal Return</small></p>
                </div>
            </div>
            <div class="col-md-2 col-sm-4" style="background: #989ae3; ">
                <div class="box">
                    <h1>250%</h1>
                    <p>After 15 Days</p>
                    <p>$10-$1000</p>
                    <p><small>Principal Return</small></p>
                </div>
            </div>
            <div class="col-md-2 col-sm-4" style="background: #b3d0ca;  ">
                <div class="box">
                    <h1>300%</h1>
                    <p>After 30 Days</p>
                    <p>$10-$1000</p>
                    <p><small>Principal Return</small></p>
                </div>
            </div>
            <div class="col-md-2 col-sm-4" style="background: #64b7ca; ">
                <div class="box">
                    <h1>600%</h1>
                    <p>After 60 Days</p>
                    <p>$10-$1000</p>
                    <p><small>Principal Return</small></p>
                </div>
            </div> -->
        </div>
        <div class="text-center">
            <button class="btn btn-custom btn-lg" data-toggle="modal" data-target="#myModal">{{ trans('welcome.interestcalculator') }}</button>
        </div>
    </div>
</section>
{{-- End of third row --}}
{{-- start of fourth row --}}
<section id="support">
    <div class="container">
        <div class="row" style="border: 1px solid black;  padding: 20px; background: #fff">
            <h2>{{ trans('welcome.welcometitle') }}</h2>
            <div class="col-md-6">
                <p><strong>{{ trans('welcome.featurestitle1') }}</strong></p>
                <p>{{ trans('welcome.featurescontent1') }}</p>
            </div>
            <div class="col-md-6">
                <p><strong>{{ trans('welcome.featurestitle2') }}</strong></p>
                <p>{{ trans('welcome.featurescontent2') }}</p>
            </div>
            <div class="col-md-6">
                <p><strong>{{ trans('welcome.featurestitle3') }}</strong></p>
                <p>{{ trans('welcome.featurescontent3') }}</p>
            </div>
            <div class="col-md-6">
                <p><strong>{{ trans('welcome.featurestitle4') }}</strong></p>
                <p>{{ trans('welcome.featurescontent4') }}</p>
            </div>
        </div>
        <div class="row text-center">
            <h3>{{ trans('welcome.we_support') }}</h3>
            <div class="col-md-2 col-sm-4 ">
                <img src="{{ url('/uploads/img/bitcoin.png') }}" alt="Bitcoin">
            </div>
            <div class="col-md-2 col-sm-4 ">
                <img src="{{ url('/uploads/img/litecoin.png') }}" alt="Litecoin">
            </div>
            <div class="col-md-2 col-sm-4 ">
                <img src="{{ url('/uploads/img/namecoin.png') }}" alt="Namecoin">
            </div>
            <div class="col-md-2 col-sm-4 ">
                <img src="{{ url('/uploads/img/ripple.png') }}" alt="Ripple">
            </div>
            <div class="col-md-2 col-sm-4 ">
                <img src="{{ url('/uploads/img/primecoin.png') }}" alt="Primecoin">
            </div>
            <div class="col-md-2 col-sm-4 ">
                <img src="{{ url('/uploads/img/ethereum.png') }}" alt="Ethereum">
            </div>
            <!-- <div class="col-md-2 col-sm-3 ">
            <img src="img/venillacoin.png" alt="Venillacoin">
            </div> -->
        </div>
    </div>
</section>
{{-- End of fourth row --}}

<!-- modal start -->
@include('partials.calculator_modal')
<!-- Popup Modal when first in website -->
<div class="modal fade" id="visitor_modal" role="dialog" style="margin: 50px;">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{ trans('popup.welcomeMessageTitle') }}</h4>
            </div>
            <div class="modal-body">                       
                <div class="panel-body">
                    <p>{!! trans('popup.welcomeMessageContent') !!}</p>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- Popup Modal ends -->

</div><!-- /.modal -->
@endsection

@push('bottomscripts')
<style type="text/css">
    .plans {
       margin: 0 20%;
       text-align: center;
       width: 60%;
       display: flex;
       justify-content: center;
    }
    .boxColor-1, .boxColor-2, .boxColor-3, .boxColor-4
    {
        padding: 15px;
    }
    .boxColor-1
    {
        background: #fba561;
    }
    .boxColor-2 
    {
        background: #d698e3;
    }
    .boxColor-3 
    {
        background: #989ae3;
    }
    .boxColor-4 
    {
        background: #b3d0ca;
    }
</style>
@endpush

@php
if (\Config::get('settings.welcomepopupstatus') == 1)
{
    if (\Session::get('welcomepopupmessage') == '')
    {
    @endphp
    @push('bottomscripts')
    <script>
         $(function() {
           $('#visitor_modal').modal('show');
        });
    </script>
    @endpush
    @php
    }
}
\Session::put('welcomepopupmessage', '1')
@endphp

